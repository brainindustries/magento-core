<?php

namespace Shirtplatform\Core\Plugin\Sales\Helper;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Reorder
{
    /**
     * @var DateTime
     */
    private $_dateTime;

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;
    
    /**
     * @var State
     */
    private $_state;
    
    /**
     * @param DateTime $dateTime
     * @param OrderRepositoryInterface $orderRepository
     * @param State $state
     */
    public function __construct(
        DateTime $dateTime,
        OrderRepositoryInterface $orderRepository,
        State $state
    ) {
        $this->_dateTime = $dateTime;
        $this->_orderRepository = $orderRepository;
        $this->_state = $state;
    }

    /**
     * @param \Magento\Sales\Helper\Reorder $subject
     * @param bool $result
     * @param int $orderId
     */
    public function afterCanReorder($subject, $result, $orderId)
    {
        if ($this->_state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML) {
            return $result;
        }

        if (!$result) {
            return $result;
        }

        //check if the order was ordered from frontend
        $order = $this->_orderRepository->get($orderId);
        if (empty($order->getRemoteIp())) {
            return false;
        }

        //check if order is not older than one year
        $now = $this->_dateTime->gmtTimestamp();
        $oneYear = 24 * 3600 * 365;                
        if ($now - $oneYear > strtotime($order->getCreatedAt())) {
            return false;
        }
        
        // can't order if all products have price 0
        $result = false;
        foreach ($order->getAllVisibleItems() as $item) {
            if ($item->getPrice()) {
                return true;
            }
        }

        return $result;
    }
}