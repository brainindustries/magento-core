<?php

namespace Shirtplatform\Core\Plugin\CatalogUrlRewrite\Model\Products;

use Magento\CatalogUrlRewrite\Model\Products\AppendUrlRewritesToProducts as Subject;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;

class AppendUrlRewritesToProducts
{
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;
 
    /**
     * @var UrlRewriteCollectionFactory
     */
    private $_urlRewriteCollectionFactory;
 
    /**
     * @var ProductUrlRewriteGenerator
     */
    private $_productUrlRewriteGenerator;

    /**
     * @param CategoryRepository $categoryRepository
     * @param UrlRewriteCollectionFactory $urlRewriteCollectionFactory
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator
    ){
        $this->_categoryRepository = $categoryRepository;
        $this->_urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
        $this->_productUrlRewriteGenerator = $productUrlRewriteGenerator;
    }

    /**
     * @param Subject $subject
     * @param ProductInterface[] $products
     * @param array $storesToAdd
     */
    public function beforeExecute($subject, $products, $storesToAdd){
        foreach ($products as $product){
            if ($product->getTypeId() === Configurable::TYPE_CODE){
                if ($this->hasDuplicatePath($product)){
                    $urlKey = $this->getNewUrlKey($product);
                    $product->setUrlKey($urlKey);
                }
            }
        }
        return [$products, $storesToAdd];
    }

    /**
     * @param ProductInterface $product
     * @return string
     */
    private function getNewUrlKey($product){
        $urlKey = $product->getUrlKey();
        $categoryIds = $product->getCategoryIds();

        if (!$categoryIds){
            return $urlKey .'-'. $product->formatUrlKey($product->getSku());
        }
        
        $category = $this->_categoryRepository->get($categoryIds[0]);
        $categoryUrlKey = $category->formatUrlKey($category->getName());

        $product->setUrlKey($urlKey .'-'. $categoryUrlKey);
        if ($this->hasDuplicatePath($product)){
            return $urlKey .'-'. $product->formatUrlKey($product->getSku());
        }

        return $urlKey .'-'. $categoryUrlKey;
    }

    /**
     * @param ProductInterface $product
     * @return bool
     */
    private function hasDuplicatePath($product){
        $urls = $this->_productUrlRewriteGenerator->generate($product);
        $requestPaths = [];
        foreach ($urls as $url){
            $requestPaths[] = $url->getRequestPath();
        }

        if (empty($requestPaths)) {
            return false;
        }

        $urlRewriteCollection = $this->_urlRewriteCollectionFactory->create();
        $rewrites = $urlRewriteCollection->addStoreFilter($product->getStoreId())
            ->addFieldToFilter('entity_type', 'product')
            ->addFieldToFilter('entity_id', ['neq' => $product->getId()])
            ->addFieldToFilter('request_path', $requestPaths);

        return (bool)$rewrites->count();
    }
}