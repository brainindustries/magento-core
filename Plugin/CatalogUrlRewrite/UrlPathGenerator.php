<?php

namespace Shirtplatform\Core\Plugin\CatalogUrlRewrite;

use Magento\Catalog\Model\Category;
use Magento\Store\Model\ScopeInterface;

class UrlPathGenerator {

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Build category URL path, remove parent category from the URL if it's set 
     * in configuration
     * 
     * @access public
     * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $subject
     * @param \Closure $proceed
     * @param \Magento\Catalog\Api\Data\CategoryInterface|\Magento\Framework\Model\AbstractModel $category
     * @return string
     */
    public function aroundGetUrlPath($subject,
                                     $proceed,
                                     $category) {        
        $removeParentCategory = $this->_scopeConfig->getValue(
                'shirtplatform/seo/remove_parent_category_from_url', ScopeInterface::SCOPE_STORE, $category->getStoreId()
        );

        if ($removeParentCategory) {
            if (in_array($category->getParentId(), [Category::ROOT_CATEGORY_ID, Category::TREE_ROOT_ID])) {
                return '';
            }
            $path = $category->getUrlPath();
            if ($path !== null && !$category->dataHasChangedFor('url_key') && !$category->dataHasChangedFor('parent_id')) {
                return $path;
            }
            $path = $category->getUrlKey();
            if ($path === false) {
                return $category->getUrlPath();
            }

            return $path;
        }

        return $proceed($category);
    }

}
