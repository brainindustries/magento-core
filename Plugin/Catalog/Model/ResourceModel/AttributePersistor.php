<?php

namespace Shirtplatform\Core\Plugin\Catalog\Model\ResourceModel;

use Magento\Catalog\Model\ProductRepository;

class AttributePersistor
{
    /**
     * @var ProductRepository
     */
    private $_productRepository;

    /**
     * @param ProductRepository
     */
    public function __construct(ProductRepository $productRepository){
        $this->_productRepository = $productRepository;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\AttributePersistor
     * @param callable $proceed
     * @param string $entityType
     * @param int $link
     * @param string $attributeCode
     * @param mixed $value
     * @return void
     */
    public function aroundRegisterInsert($subject, $proceed, $entityType, $link, $attributeCode, $value)
    {
        if ($entityType == 'Magento\Catalog\Api\Data\ProductInterface'){
            $product = $this->_productRepository->getById($link);
            if ($product->getData($attributeCode) == $value){
                return;
            }
        }
        $proceed($entityType, $link, $attributeCode, $value);
    }
}