<?php

namespace Shirtplatform\Core\Plugin\Catalog\Model\ResourceModel\Product;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class CategoryLink
{
    /**
     * @var CollectionFactory
     */
    private $_collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\CategoryLink $subject
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param array $categoryLinks
     * @return array
     */
    public function beforeSaveCategoryLinks($subject, $product, $categoryLinks = []) : array
    {
        $oldCategoryLinks = $subject->getCategoryLinks($product);
        $linksToDelete = $this->getLinksToDelete($oldCategoryLinks, $categoryLinks);
        
        if (empty($linksToDelete)) {
            return [$product, $categoryLinks];
        }

        $categoryIds = array_column($linksToDelete, 'category_id');
        $categoriesToCheck = $this->getCategoriesByIds($categoryIds);

        foreach ($categoriesToCheck as $_category) {    
            if ($_category->getShirtplatformId() !== null) {
                continue;
            }
            foreach ($linksToDelete as $_link) {
                if ($_category->getId() == $_link['category_id']) {                    
                    $categoryLinks[] = $_link;
                }
            }
        }

        return [$product, $categoryLinks];
    }

    /**
     * @param array $oldCategoryLinks
     * @param array $categoryLinks
     * @return array
     */
    private function getLinksToDelete(array $oldCategoryLinks, array $categoryLinks) : array
    {
        foreach ($oldCategoryLinks as $_key => $_oldLink) {
            foreach ($categoryLinks as $_link) {
                if ($_oldLink['category_id'] == $_link['category_id']) {
                    unset($oldCategoryLinks[$_key]);
                }
            }
        }
        return array_values($oldCategoryLinks);
    }

    /**
     * @param array $categoryIds
     */
    private function getCategoriesByIds(array $categoryIds)
    {
        $categories = $this->_collectionFactory
            ->create()
            ->addFieldToFilter('entity_id', ['in' => $categoryIds]);

        return $categories->getItems();
    }
}