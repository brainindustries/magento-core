<?php

namespace Shirtplatform\Core\Plugin\Catalog\Model\ProductRepository;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Eav\Model\Config;

class MediaGalleryProcessor
{
    /** 
     * @var AdapterInterface 
     */
    private $_connection;

    /**
     * @var Config
     */
    private $_eavConfig;

    /**
     * @var bool
     */
    private $_useOldVersion;

    /**
     * @param Config $eavConfig
     * @param ProductMetadataInterface $productMetadata
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Config $eavConfig,
        ProductMetadataInterface $productMetadata,
        ResourceConnection $resourceConnection
    ) {
        $this->_connection = $resourceConnection->getConnection();
        $this->_eavConfig = $eavConfig;
        $this->_useOldVersion = version_compare('2.4.7', $productMetadata->getVersion()) === 1;
    }

    /**
     * @param \Magento\Catalog\Model\ProductRepository\MediaGalleryProcessor $subject
     * @param Callable $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @param array $mediaGalleryEntries
     * @return void
     */
    public function aroundProcessMediaGallery($subject, $proceed, $product, $mediaGalleryEntries) : void
    {
        // get product images, skip this plugin functionality if it's empty
        if (!$mediaGallery = $product->getMediaGallery('images')) {
            $proceed($product, $mediaGalleryEntries);
            return;
        }
        
        // fetch non platform images from database for future use after proceed
        $nonPlatformImages = $this->getNonPlatformImages($mediaGallery);
        
        // remove image roles if the new entries will set them. If not done, the main image will not be able to be removed and will be duplicite
        $this->handleImageRoles($product, $mediaGalleryEntries);

        // delete platform images on new versions of magento to prevent duplicating
        $platformImagesToDelete = [];
        if ($this->_useOldVersion === false) {
            foreach ($mediaGallery as $_key => $_image){
                if (!in_array($_image['value_id'], $nonPlatformImages) && !isset($mediaGalleryEntries[$_key])) {
                    $_image['removed'] = true;
                    $platformImagesToDelete[] = $_image;
                }
            }
            $product->setData('media_gallery', ["images" => $mediaGallery]);
        }

        // proceed original function
        $proceed($product, $mediaGalleryEntries);

        // get processed media gallery
        $mediaGallery = $product->getMediaGallery('images');

        if ($this->_useOldVersion === true) {
            // prevent non platform images from being removed on old magento versions
            foreach ($mediaGallery as &$_image){
                if (in_array($_image['value_id'], $nonPlatformImages)){
                    $_image['removed'] = false;
                }
            }
        } else {
            // add images flagged with remove = true (from pre-proceed code) to the media gallery, so they can be deleted later in \Magento\Catalog\Model\Product\Gallery\UpdateHandler
            $mediaGallery = array_merge($mediaGallery, $platformImagesToDelete);
        }

        // set result media gallery to the product
        $product->setData('media_gallery', ["images" => $mediaGallery]);
    }

    /**
     * @param array $mediaGallery
     * @return array
     */
    private function getNonPlatformImages(array $mediaGallery) : array
    {
        // create db select
        $select = $this->_connection->select()
            ->from($this->_connection->getTableName('shirtplatform_media_gallery_assigned_view'), 'image_id')
            ->where('image_id IN (?)', array_keys($mediaGallery));

        // get an array of platform image ids
        $images = array_column($this->_connection->fetchAll($select), 'image_id');

        // return non platform image ids
        return array_diff(array_keys($mediaGallery), $images);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $mediaGalleryEntries
     * @return void
     */
    private function handleImageRoles($product, array $mediaGalleryEntries) : void
    {
        // get all image roles from new coming media gallery entries
        $imgRoles = ['image'];
        foreach ($mediaGalleryEntries as $entry) {
            if (!empty($entry['types'])) {
                $imgRoles = array_merge($imgRoles, $entry['types']);
            }
        }
        $imgRoles = array_unique($imgRoles);

        // get attribute ids of the roles
        $imgAttributeIds = [];
        foreach ($imgRoles as $role) {
            $attribute = $this->_eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $role);
            $imgAttributeIds[] = $attribute->getId();
        }

        // delete roles from product
        $this->_connection->delete(
            $this->_connection->getTableName('catalog_product_entity_varchar'), 
            [
                'entity_id = ?' => $product->getId(),                
                'attribute_id IN (?)' => $imgAttributeIds                
            ]
        );
    }
}