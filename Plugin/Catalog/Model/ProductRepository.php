<?php

namespace Shirtplatform\Core\Plugin\Catalog\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

class ProductRepository
{

    /**
     * @var \Magento\Framework\App\State
     */
    private $_appState;    
    
    /** 
     * @var AdapterInterface 
     */
    private $_connection;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $_eavConfig;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @param \Magento\Framework\App\State $appState     
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\App\RequestInterface $request
     * @param ResourceConnection $resourceConnection
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\State $appState,           
        \Magento\Eav\Model\Config $eavConfig,     
        \Magento\Framework\App\RequestInterface $request,
        ResourceConnection $resourceConnection,
        StoreManagerInterface $storeManager        
    ) {
        $this->_appState = $appState;        
        $this->_connection = $resourceConnection->getConnection();
        $this->_eavConfig = $eavConfig;
        $this->_request = $request;
        $this->_storeManager = $storeManager;        
    }

    /**
     * Set websiteIds on existing product in case of async imports. Otherwise
     * there were problems when adding a new color variant when updating products.
     * The variant were assigned only to some store views.
     * 
     * @access public
     * @param \Magento\Catalog\Model\ProductRepository $subject
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param bool $saveOptions     
     * @return array
     */
    public function beforeSave($subject, $product, $saveOptions = false)
    {        
        try {
            $existingProduct = $product->getId() ?
                $subject->getById($product->getId()) :
                $subject->get($product->getSku());
        } catch (NoSuchEntityException $ex) {
            $existingProduct = null;
        }        
        
        if (
            $this->_appState->getAreaCode() == \Magento\Framework\App\Area::AREA_GLOBAL &&
            $this->_getCliCommand() == 'queue:consumers:start' &&
            $existingProduct && empty($product->getWebsiteIds())
        ) {                        
            if ($this->_storeManager->getStore(true)->getCode() == \Magento\Store\Model\Store::ADMIN_CODE) {
                $websiteIds = array_keys($this->_storeManager->getWebsites());                
            } 
            else {                                
                $websiteIds = $existingProduct->getWebsiteIds();
                $currentWebsiteId = $this->_storeManager->getStore()->getWebsiteId();
                
                if (!in_array($currentWebsiteId, $websiteIds)) {
                    array_push($websiteIds, $currentWebsiteId);                    
                }                
            }
            
            $product->setWebsiteIds($websiteIds);            
        }        

        return [$product, $saveOptions];
    }

    /**
     * Update the image roles to store_id = 0, because, for some reason, on API update for 
     * existing products (when updating with store view code), I couldn't make it work.
     * The images were either duplicated for some simple products or the roles set only
     * for that store view
     * 
     * @access public
     * @param \Magento\Catalog\Model\ProductRepository $subject
     * @param \Magento\Catalog\Api\Data\ProductInterface $product     
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function afterSave($subject, $product)
    {        
        if ($this->_appState->getAreaCode() == \Magento\Framework\App\Area::AREA_GLOBAL && $this->_getCliCommand() == 'queue:consumers:start') {
            $imgRoles = ['image', 'small_image', 'thumbnail'];
            $imgAttributeIds = [];

            foreach ($imgRoles as $role) {
                $attribute = $this->_eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $role);
                $imgAttributeIds[] = $attribute->getId();
            }

            $select = $this->_connection->select()->from('catalog_product_entity_varchar')
                ->where('entity_id = ?', $product->getId())
                ->where('attribute_id IN (?)', $imgAttributeIds);
            $currentRoles = $this->_connection->fetchAll($select);

            $existingRolesForDefaultStore = [];
            foreach ($currentRoles as $role) {
                if ($role['store_id'] == 0) {
                    $existingRolesForDefaultStore[] = $role['entity_id'] . '_' . $role['attribute_id'];
                }
            }

            foreach ($currentRoles as $role) {
                $entityIdAttrId = $role['entity_id'] . '_' . $role['attribute_id'];
                if ($role['store_id'] != 0 && !in_array($entityIdAttrId, $existingRolesForDefaultStore)) {                                        
                    $this->_connection->update('catalog_product_entity_varchar', ['store_id' => 0], ['value_id = ?' => $role['value_id']]);
                }
            }            
        }

        return $product;
    }

    /**
     * Get CLI command
     * 
     * @access private
     * @return string
     */
    private function _getCliCommand()
    {        
        $argv = $this->_request->getServer('argv');
        return isset($argv[1]) ? $argv[1]: '';
    }
}
