<?php

namespace Shirtplatform\Core\Plugin\Catalog\Product\Gallery;

use Shirtplatform\Core\Api\AssignedViewRepositoryInterface;
use Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterfaceFactory;

/**
 * Plugin for catalog product gallery create/update handlers.
 */
class CreateHandler {

    /**
     * @var AssignedViewRepositoryInterface
     */
    protected $_assignedViewRepository;    

    /**
     * @var AssignedViewInterface $assignedView
     */
    protected $_assignedViewFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * 
     * @param AssignedViewRepositoryInterface $assignedViewRepository
     * @param AssignedViewInterfaceFactory $assignedViewFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(AssignedViewRepositoryInterface $assignedViewRepository,
                                AssignedViewInterfaceFactory $assignedViewFactory,
                                \Psr\Log\LoggerInterface $logger
    ) {
        $this->_assignedViewFactory = $assignedViewFactory;
        $this->_assignedViewRepository = $assignedViewRepository;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Catalog\Model\Product\Gallery\CreateHandler $mediaGalleryCreateHandler
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Model\Product
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function afterExecute(\Magento\Catalog\Model\Product\Gallery\CreateHandler $mediaGalleryCreateHandler,
                                 \Magento\Catalog\Model\Product $product
    ) {

        $mediaCollection = $this->getMediaEntriesDataCollection(
                $product, $mediaGalleryCreateHandler->getAttribute()
        );


        if (!empty($mediaCollection)) {
            try {
                foreach ($mediaCollection as $item) {
                    if (array_key_exists('value_id', $item) &&
                            array_key_exists('assigned_view_id', $item) &&
                            !array_key_exists('removed', $item)) {
                        $assignedView = $this->_assignedViewFactory->create();
                        $assignedView->setImageId($item['value_id']);
                        $assignedView->setAssignedViewId($item['assigned_view_id']);
                        if (array_key_exists('view_type', $item))
                        {
                            $assignedView->setViewType($item['view_type']);
                        }
                        $this->_assignedViewRepository->save($assignedView);
                    }
                }
            } catch (\Throwable $e) {
                $this->_logger->error($e->getMessage());
            }
        }


        return $product;
    }

    protected function getMediaEntriesDataCollection(\Magento\Catalog\Model\Product $product,
                                                     \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
    ) {
        $attributeCode = $attribute->getAttributeCode();
        $mediaData = $product->getData($attributeCode);
        if (!empty($mediaData['images']) && is_array($mediaData['images'])) {
            return $mediaData['images'];
        }
        return [];
    }

}
