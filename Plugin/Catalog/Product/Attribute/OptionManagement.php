<?php

namespace Shirtplatform\Core\Plugin\Catalog\Product\Attribute;

use Magento\Catalog\Api\ProductAttributeOptionUpdateInterface;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;

class OptionManagement
{
    /**
     * @var ProductAttributeOptionUpdateInterface
     */
    private $_attributeOptionUpdate;

    /**
     * @var \Magento\Eav\Api\AttributeRepositoryInterface
     */
    private $_attributeRepository;
        
    /**     
     *
     * @param ProductAttributeOptionUpdateInterface $attributeOptionUpdate
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository
     * @return void
     */
    public function __construct(
        ProductAttributeOptionUpdateInterface $attributeOptionUpdate,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository        
    ) {
        $this->_attributeOptionUpdate = $attributeOptionUpdate;
        $this->_attributeRepository = $attributeRepository;
    }
    
    /**
     * Checks case sensitivity of the attribute option. If it exists in a different case,
     * update it, otherwise add a new one
     *
     * @access public
     * @param \Magento\Catalog\Api\ProductAttributeOptionManagementInterface $subject
     * @param \Closure $proceed
     * @param string $attributeCode
     * @param \Magento\Eav\Api\Data\AttributeOptionInterface $option
     * @return string|bool
     */
    public function aroundAdd($subject, $proceed, $attributeCode, $option)
    {    
        if (empty($attributeCode)) {
            throw new InputException(__('Empty attribute code'));
        }

        $attribute = $this->_attributeRepository->get('catalog_product', $attributeCode);
        if (!$attribute->usesSource()) {
            throw new StateException(__('Attribute %1 doesn\'t work with options', $attributeCode));
        }

        $existingOptions = $attribute->getSource()->getAllOptions();
        foreach ($existingOptions as $exOption) {
            if (strtolower($exOption['label']) == strtolower($option->getLabel())) {                
                return $this->_attributeOptionUpdate->update($attributeCode, $exOption['value'], $option);
            }
        }            

        return $proceed($attributeCode, $option);
    }
}
