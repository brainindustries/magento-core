<?php

namespace Shirtplatform\Core\Plugin\Catalog\Product\Attribute\Backend\Media;

use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface;
use Magento\Catalog\Model\Product\Attribute\Backend\Media\ImageEntryConverter as CoreImageEntryConverter;

class ImageEntryConverter {

    /**
     * @var ProductAttributeMediaGalleryEntryInterface $entry
     */
    protected $_entry;

    public function beforeConvertFrom(CoreImageEntryConverter $subject,
                                      ProductAttributeMediaGalleryEntryInterface $entry) {
        $this->_entry = $entry;
    }

    public function afterConvertFrom(CoreImageEntryConverter $subject,
                                     $result) {
        if ($this->_entry->getExtensionAttributes()) {
            $result['assigned_view_id'] = $this->_entry->getExtensionAttributes()->getAssignedViewId();
            $result['view_type'] = $this->_entry->getExtensionAttributes()->getViewType();
        }
        return $result;
    }

}
