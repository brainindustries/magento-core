<?php

namespace Shirtplatform\Core\Plugin\Webapi\Controller\Rest;

use shirtplatform\constants\WsConstants;
class SynchronousRequestProcessor
{
    /**
     * Number of attempts the request will try to run in case of exception
     */
    const MAX_REPEAT_COUNTER = 20;

    /**
     * @var \Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger
     */
    private $_eventHandler;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    /**
     * Constructor
     *
     * @access public
     * @param \Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger $databaseLogger
     * @param \Shirtplatform\Core\Helper\Data $helper     
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger $databaseLogger,
        \Shirtplatform\Core\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger        
    ) {
        $this->_eventHandler = $databaseLogger;
        $this->_helper = $helper;
        $this->_logger = $logger;
    }

    /**
     * Process REST API request. Try to process the request self::MAX_REPEAT_COUNTER in case of exception.
     * The exceptions sometimes occured when a new product was imported and indexer_reindex_all_invalid run
     * during the import. Both tried to acquire a database lock, resulting in the exception
     *
     * @param public
     * @param \Magento\Webapi\Controller\Rest\SynchronousRequestProcessor $subject
     * @param \Closure $closure
     * @param \Magento\Framework\Webapi\Rest\Request $request     
     */
    public function aroundProcess($subject, $closure, \Magento\Framework\Webapi\Rest\Request $request)
    {
        //log API calls to platform
        if ($this->_helper->getConfigValue('shirtplatform/webservices/log_api_calls')) {
            WsConstants::$eventHandler = $this->_eventHandler;
        }

        //log data from platform to Magento
        $logPostRequest = $this->_helper->getConfigValue('shirtplatform/import/log_post_request');
        $logOnlyProductPostRequest = $this->_helper->getConfigValue('shirtplatform/import/log_only_product_post_request');

        if ($request->isPost() && $logPostRequest) {
            if (($logOnlyProductPostRequest && preg_match('/\/products/', $request->getUri())) || !$logOnlyProductPostRequest) {
                $this->_logger->info('URI: ' . $request->getUri());
                $this->_logger->info('Content: ' . $request->getContent());    
            }            
        }        

        if (!preg_match('/\/products/', $request->getPathInfo())) {
            return $closure($request);
        }

        $finished = false;
        $counter = 0;

        do {
            try {
                $counter++;
                $closure($request);
                $finished = true;
            } catch (\Magento\Framework\Exception\TemporaryState\CouldNotSaveException $ex) {
                $this->_helper->logError('Temporarystate Could not save exception in REST request for path ' . $request->getPathInfo());
                $this->_helper->logError('Exception message: ' . $ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            } catch (\Magento\Framework\Exception\CouldNotSaveException $ex) {
                $this->_helper->logError('Could not save exception in REST request for path ' . $request->getPathInfo());
                $this->_helper->logError('Exception message: ' . $ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            }
        } while (!$finished && $counter < self::MAX_REPEAT_COUNTER);
    }
}
