<?php

namespace Shirtplatform\Core\Plugin\Email;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Phrase;
use Magento\Store\Model\ScopeInterface;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\Sendmail;

class TransportInterfacePlugin {

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var Sendmail
     */
    private $_laminasTransport;

    /**
     *      
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Omit email sending depending on the system configuration setting.
     * Allow sending to development emails
     *
     * @access public
     * @param TransportInterface $subject
     * @param \Closure $proceed
     * @return void
     * @throws MailException     
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundSendMessage(TransportInterface $subject,
                                      \Closure $proceed) {
        $disableEmails = $this->_scopeConfig->isSetFlag('system/smtp/disable', ScopeInterface::SCOPE_STORE);
        $developmentEmails = $this->_scopeConfig->getValue('system/smtp/development_emails');

        if ($disableEmails and $developmentEmails) {
            $developmentEmails = explode(',', $developmentEmails);            
            $message = Message::fromString($subject->getMessage()->getRawMessage())->setEncoding('utf-8');                        
            $addresses = $this->_getRecipients($message);
            $addresses = explode(',', $addresses);                        
            
            //trim email addresses
            foreach ($addresses as $key => $emailTo) {
                $addresses[$key] = trim($emailTo);
            }
            foreach ($developmentEmails as $key => $devEmail) {
                $developmentEmails[$key] = trim($devEmail);
            }

            //compose new addresses - include only development emails
            $newAddresses = [];
            foreach ($addresses as $emailTo) {
                if (in_array($emailTo, $developmentEmails)) {
                    $newAddresses[] = $emailTo;
                }
            }                        
            

            //send emails if we have any adddresses
            if ($newAddresses) {
                $message->setTo($newAddresses);
                $this->_laminasTransport = new Sendmail();

                try {
                    $this->_laminasTransport->send($message);
                } catch (\Exception $e) {
                    throw new MailException(new Phrase($e->getMessage()), $e);
                }
            }
        }
        else {
            $proceed();
        }
    }

    /**
     * Get recipients list. Needed for magento 2.3
     *
     * @access private
     * @param  \Laminas\Mail\Message $message
     * @throws \Laminas\Mail\Transport\Exception\RuntimeException
     * @return string
     */
    private function _getRecipients($message) {
        $headers = $message->getHeaders();

        if (!$headers->has('to')) {
            throw new Exception\RuntimeException('Invalid email; contains no "To" header');
        }

        $to = $headers->get('to');
        $list = $to->getAddressList();                                        
        if (0 == count($list)) {            
            throw new Exception\RuntimeException('Invalid "To" header; contains no addresses');
        }

        // If not on Windows, return normal string
//        $operatingSystem = strtoupper(substr(PHP_OS, 0, 3));
//        if (!$operatingSystem != 'WIN') {            
//            return $to->getFieldValue(HeaderInterface::FORMAT_ENCODED);
//        }

        // Otherwise, return list of emails
        $addresses = [];
        foreach ($list as $address) {                 
            $addresses[] = $address->getEmail();
        }

        $addresses = implode(', ', $addresses);
        return $addresses;
    }

}
