<?php

namespace Shirtplatform\Core\Plugin\SalesRule\Rule\Condition;

class Product {
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * 
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(\Magento\Catalog\Api\ProductRepositoryInterface $productRepository) {
        $this->_productRepository = $productRepository;
    }

    /**
     * Handle sales rule conditions for shirtplatform products. They are imported as configurable
     * products and when there is a sales rule with SKU condition, it's not applied due to the fact
     * that simple variant has different SKU. This plugin makes it possible to test for this
     * kind of condition
     * 
     * @todo This plugin is inactive (commented out in di.xml). If nobody complains about it, this file
     * can be deleted. In newer versions of Magento, the sales rule have conditions SKU (parent only)
     * and SKU (children only) that fix this issue.
     * @access public
     * @param \Magento\SalesRule\Model\Rule\Condition\Product $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $model
     * @return bool
     */
    public function aroundValidate($subject,
                                   $proceed,
                                   $model) {                
        $result = $proceed($model);        

        // if ($result) {
        //     $attrCode = $subject->getAttribute();

        //     if ($attrCode == 'sku' and $model->getParentItem()) {                
        //         $parentItem = $model->getParentItem();                
        //         //reload the product, because we need the real SKU
        //         $product = $this->_productRepository->getById($parentItem->getProductId(), false, null, true);
                
        //         if ($product->getShirtplatformId()) {
        //             $result = $subject->validateAttribute($product->getSku());                    
        //         }
        //     }
        // }

        return $result;
    }
}