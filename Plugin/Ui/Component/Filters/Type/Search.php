<?php

namespace Shirtplatform\Core\Plugin\Ui\Component\Filters\Type;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\Api\FilterBuilder;

class Search
{
    /**
     * @var CollectionFactory
     */
    private $_collectionFactory;

    /**
     * @var FilterBuilder
     */
    private $_filterBuilder;

    /**
     * @param CollectionFactory $collectionFactory
     * @param FilterBuilder $filterBuilder
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        FilterBuilder $filterBuilder
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_filterBuilder = $filterBuilder;
    }

    public function aroundPrepare($subject, $proceed)
    {
        $value = $subject->getContext()->getRequestParam('search');
        if (empty($value)) {
            return $proceed();
        }
            
        $shirtplatformIds = array_values(array_filter(array_map('trim', explode(',', $value)))) ?? [];
        if (empty($shirtplatformIds)) {
            return $proceed();
        }

        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('shirtplatform_id', ['in' => $shirtplatformIds]);
        if (!$collection->count()) {
            return $proceed();
        }

        $filter = $this->_filterBuilder->setConditionType('in')
            ->setField('shirtplatform_id')
            ->setValue($shirtplatformIds)
            ->create();
        
        $subject->getContext()->getDataProvider()->addFilter($filter);
    }
}