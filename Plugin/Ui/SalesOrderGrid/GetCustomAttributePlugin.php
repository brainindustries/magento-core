<?php

namespace Shirtplatform\Core\Plugin\Ui\SalesOrderGrid;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Shirtplatform\Core\Helper\PrintTechnologyHelper;

/**
 * Class GetCustomAttributePlugin
 * @package Shirtplatform\Core\Plugin\Ui\SalesOrderGrid
 */
class GetCustomAttributePlugin
{

    /** @var ResourceConnection */
    private $resourceConnection;

    /** @var \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory */
    private $printTechnologyFactory;

    /** @var PrintTechnologyHelper */
    private $printTechnologyHelper;

    /**
     * GetCustomAttributePlugin constructor.
     * @param ResourceConnection $resourceConnection
     * @param \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory $printTechnologyFactory
     * @param PrintTechnologyHelper $printTechnologyHelper
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory $printTechnologyFactory,
        PrintTechnologyHelper $printTechnologyHelper
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->printTechnologyFactory = $printTechnologyFactory;
        $this->printTechnologyHelper = $printTechnologyHelper;
    }

    /**
     * @param Document $subject
     * @param \Closure $proceed
     * @param string $attributeCode
     * @return \Magento\Framework\Api\AttributeInterface
     * @throws \Zend_Db_Statement_Exception
     */
    public function aroundGetCustomAttribute(Document $subject, \Closure $proceed, $attributeCode)
    {        
        $value = $proceed($attributeCode);
        if($attributeCode === 'shirtplatform_order_print_technology')
        {            
            $printTechCollection = $this->printTechnologyFactory->create()->getCollection()
                ->addFieldToFilter('order_id', ['eq' => $subject->getData('entity_id')]);                    

            foreach ($printTechCollection as $printTechItem) {
                $printTechnologiesPerOrder[$printTechItem->getOrderId()][$printTechItem->getTechnologyId()] = [
                    'name' => $printTechItem->getName(),
                    'count' => $printTechItem->getCount()
                ];
            }


            // $printTechnologies = $this->printTechnologyFactory->create()->getCollection()
            //     ->addFieldToFilter('order_id', ['eq' => $subject->getData('entity_id')])
            //     ->setOrder('count', 'DESC');

            $list = [];
            if (count($printTechCollection)) {
                foreach($printTechCollection as $printTechnology)
                {
                    $list[] = $printTechnology->getName() . ' - ' . $printTechnology->getCount() . 'x';
                }
                $value->setValue(implode('<br/>', $list));
            }


            // foreach($printTechnologies as $printTechnology)
            // {
            //     $list[] = $printTechnology->getName() . ' - ' . $printTechnology->getCount() . 'x';
            // }
            // $value->setValue(implode('<br/>', $list));
        }
        elseif($attributeCode === 'shirtplatform_order_product_count')
        {
            $connection = $this->resourceConnection->getConnection();
            $value->setValue((int) $connection
                ->query('SELECT SUM(qty_ordered) FROM sales_order_item WHERE parent_item_id IS NOT NULL AND order_id = ' . $subject->getData('entity_id'))
                ->fetchColumn());
        }
        elseif(in_array($attributeCode, [
            'technology_flex_flock',
            'technology_sublimation',
            'technology_digital',
            'technology_laser',
            'technology_digital_direct',
            'technology_direct_to_foil',
            'technology_engraving',
            'technology_screenprint',
            'technology_kornit',
            'technology_other',
            'technology_character_count'
        ]))
        {
            $printTechCollection = $this->printTechnologyFactory->create()->getCollection()
                ->addFieldToFilter('order_id', ['eq' => $subject->getData('entity_id')])
                ->getItems();

            $value->setValue($this->printTechnologyHelper->getCounts(
                $attributeCode,
                $printTechCollection)
            );
        }
        return $value;
    }

}
