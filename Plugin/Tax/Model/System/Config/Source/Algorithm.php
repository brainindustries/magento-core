<?php

namespace Shirtplatform\Core\Plugin\Tax\Model\System\Config\Source;

class Algorithm {
        
    /**
     * Identifier constant for total based calculation where calculations are based
     * on creator prices acting like excl prices. Currently identical to \Magento\Tax\Model\Calculation::CALC_TOTAL_BASE
     */
    const CALC_TOTAL_BASE_CREATOR_PRICES_EXCL_TAX = 'CREATOR_TOTAL_BASE_PRICES_EXCL_TAX';

    /**
     * Identifier constant for total based calculation where calculations are based
     * on creator prices acting like incl prices. Creator product prices are still imported
     * excluding tax, but in this case we'll first calculate the tax to fix the rounding issue
     */
    const CALC_TOTAL_BASE_CREATOR_PRICES_INCL_TAX = 'CREATOR_TOTAL_BASE_PRICES_INCL_TAX';    
    
    /**
     * Add options to configuration "Tax calculation method based on"
     * 
     * @access public
     * @param \Magento\Tax\Model\System\Config\Source\Algorithm $subject
     * @param array $result
     * @return array
     */
    public function afterToOptionArray($subject, $result) {
        $result[] = ['value' => self::CALC_TOTAL_BASE_CREATOR_PRICES_EXCL_TAX, 'label' => __('Total (Creator prices acting like excluding tax)')];
        $result[] = ['value' => self::CALC_TOTAL_BASE_CREATOR_PRICES_INCL_TAX, 'label' => __('Total (Creator prices acting like including tax)')];
        return $result;
    }
}