<?php

namespace Shirtplatform\Core\Plugin\Fooman\EmailAttachments\Model;

use Fooman\EmailAttachments\Model\EmailTypeFactory;
use Fooman\EmailAttachments\Model\NextEmailInfo;

class EmailIdentifier
{
    /**
     * @var EmailTypeFactory
     */
    private $_emailTypeFactory;

    /**
     * @param EmailTypeFactory $emailTypeFactory
     */
    public function __construct(EmailTypeFactory $emailTypeFactory) 
    {
        $this->_emailTypeFactory = $emailTypeFactory;
    }

    /**
     * @access public
     * @param \Fooman\EmailAttachments\Model\EmailIdentifier $subject
     * @param \Fooman\EmailAttachments\Model\EmailType $emailType
     * @param NextEmailInfo $nextEmailInfo
     * @return \Fooman\EmailAttachments\Model\EmailType
     */
    public function afterGetType($subject, $emailType, NextEmailInfo $nextEmailInfo)
    {        
        if ($emailType->getType() != false) {
            return $emailType;
        }

        $templateVars = $nextEmailInfo->getTemplateVars();
        if (isset($templateVars['event_type'])) {
            $varCode = current(array_keys($templateVars));
            return $this->_emailTypeFactory->create(['type' => $templateVars['event_type'], 'varCode' => $varCode]);    
        }
        
        return $emailType;
    }
}