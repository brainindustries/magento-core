<?php

namespace Shirtplatform\Core\Plugin\PageCache\Model;

class Type
{    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

        
    /**     
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @return void
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Don't clean full page cache if it's disabled in admin
     * 
     * @access public
     * @param \Magento\PageCache\Model\Cache\Type $subject
     * @param \Closure $proceed
     * @param string $mode
     * @param string[] $tags
     */
    public function aroundClean($subject, $proceed, $mode = \Zend_Cache::CLEANING_MODE_ALL, array $tags = [])
    {                        
        $cacheCleanDisabled = $this->_scopeConfig->getValue('shirtplatform/general/pagecache_clean_disabled');
        if ($cacheCleanDisabled && $subject->getTag() == \Magento\PageCache\Model\Cache\Type::CACHE_TAG && !empty($tags)) {            
            return false;
        }
        
        return $proceed($mode, $tags);                    
    }
}