<?php

namespace Shirtplatform\Core\Plugin\Config;

class PriceScopePlugin
{
	const PRICE_SCOPE_STORE = 2;

	public function afterToOptionArray(\Magento\Catalog\Model\Config\Source\Price\Scope $subject, $result)
	{
		$result[] = [
			'value' => self::PRICE_SCOPE_STORE,
			'label' => __('Store')
		];
		return $result;
	}
}