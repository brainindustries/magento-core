<?php

namespace Shirtplatform\Core\Block\Adminhtml\Setup;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Integration\Api\IntegrationServiceInterface;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Store\Model\ScopeInterface;

class Form extends \Magento\Backend\Block\Template {

    /**
     * @var CoreHelper
     */
    protected $_coreHelper;

    /**
     * @var IntegrationServiceInterface
     */
    protected $_integrationService;

    /**
     * @var ProductMetadataInterface
     */
    protected $_productMetadata;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Context $context
     * @param CoreHelper $coreHelper
     * @param IntegrationServiceInterface $integrationService
     * @param ProductMetadataInterface $productMetadata
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     * @param JsonHelper $jsonHelper
     * @param DirectoryHelper $directoryHelper
     */
    public function __construct(
        Context $context,
        CoreHelper $coreHelper,
        IntegrationServiceInterface $integrationService,
        ProductMetadataInterface $productMetadata,
        ScopeConfigInterface $scopeConfig,
        array $data = [],
        ?JsonHelper $jsonHelper = null,
        ?DirectoryHelper $directoryHelper = null
    ){
        $this->_coreHelper = $coreHelper;
        $this->_integrationService = $integrationService;
        $this->_productMetadata = $productMetadata;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        $platformUrl = $this->_scopeConfig->getValue('shirtplatform/general/url');
        return str_replace('/rest', '/spring/magento/connect', $platformUrl);
    }

    /**
     * @return string
     */
    public function getDisconnectFormAction()
    {
        return $this->getUrl('shirtplatform/setup/disconnect', [
            'form_key' => $this->getFormKey(),
            'website_id' => $this->getWebsiteId()
        ]);
    }

    /**
     * @return string
     */
    public function getInputs()
    {
        $integrationId = $this->_integrationService->findByName('ShirtPlatform')->getId();
        $integration = $this->_integrationService->get($integrationId);

        $versionParts = explode('.', $this->_productMetadata->getVersion());
        
        $storeViews = $this->getStoreViews();

        $baseUrl = $this->_scopeConfig->getValue('web/secure/base_url', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);

        $data = [
            'endpoint' => $baseUrl .'rest',
            'baseId' => $this->getWebsiteId(),
            'consumerKey' => $integration->getConsumerKey(),
            'consumerSecret' => $integration->getConsumerSecret(),
            'token' => $integration->getToken(),
            'secret' => $integration->getTokenSecret(),
            'version' => $versionParts[0] .'.'. $versionParts[1],
            'callbackUrl' => $baseUrl .'rest/V1/shirtplatform/connector/connect',
        ];

        if (count($storeViews)){
            $data['storeViews'] = [];
            foreach ($storeViews as $storeView){
                $data['storeViews'][] = [
                    'id' => $storeView['id'],
                    'uniqUrl' => $storeView['uniqUrl'],
                    'countryCode' => $storeView['countryCode'],
                    'languageCode' => $storeView['languageCode'],
                    'email' => $storeView['email']
                ];
            }
        }

        $inputsHtml = '';
        foreach ($data as $key => $value){
            if (is_array($value)){
                foreach ($value as $key2 => $value2){
                    foreach ($value2 as $key3 => $value3){
                        $inputsHtml .= '<input type="hidden" name="'. $key .'['. $key2 .'].'. $key3 .'" value="'. $value3 .'" />';
                    }
                }
            } else {
                $inputsHtml .= '<input type="hidden" name="'. $key .'" value="'. $value .'" />';
            }
        }

        return $inputsHtml;
    }

    /**
     * @return array
     */
    public function getStoreViews()
    {
        $stores = $this->_storeManager->getWebsite($this->getWebsiteId())->getStores();

        $storeViews = [];
        foreach ($stores as $store) {
            list($languageCode, $countryCode) = explode('_', $store->getConfig('general/locale/code'));

            $storeViews[$store->getId()] = [
                'id' => $store->getId(),
                'uniqUrl' => $this->_coreHelper->getConnectionUniqUrl($store),
                'countryCode' => strtoupper($countryCode),
                'languageCode' => strtoupper($languageCode),
                'email' => $store->getConfig('trans_email/ident_general/email'),
                'name' => $store->getName()
            ];
        } 

        return $storeViews;
    }
    
    /**
     * @return bool
     */
    public function isConnected()
    {
        $login = $this->_scopeConfig->getValue('shirtplatform/general/login', ScopeInterface::SCOPE_WEBSITES, $this->getWebsiteId());
        $password = $this->_scopeConfig->getValue('shirtplatform/general/password', ScopeInterface::SCOPE_WEBSITES, $this->getWebsiteId());
        $store = $this->_scopeConfig->getValue('shirtplatform/general/store', ScopeInterface::SCOPE_WEBSITES, $this->getWebsiteId());

        if (!$login || !$password || !$store){
            return false;
        }
        
        $stores = $this->_storeManager->getWebsite($this->getWebsiteId())->getStores();
        foreach ($stores as $_store){
            $country = $this->_scopeConfig->getValue('shirtplatform/general/country', ScopeInterface::SCOPE_STORES, $_store->getId());
            $language = $this->_scopeConfig->getValue('shirtplatform/general/language', ScopeInterface::SCOPE_STORES, $_store->getId());

            if (!$country || !$language){
                return false;
            }
        }

        return true;
    }

}