<?php

namespace Shirtplatform\Core\Block\Adminhtml\Setup;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;

	/**
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
	 * @param \Magento\Backend\Model\Auth\Session $authSession
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param array $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Json\EncoderInterface $jsonEncoder,
		\Magento\Backend\Model\Auth\Session $authSession,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		array $data = []
	)
	{
		$this->storeManager = $storeManager;
		parent::__construct($context, $jsonEncoder, $authSession, $data);
	}

	/**
	 * @return void
	 */
	protected function _construct()
	{
		parent::_construct();
		$this->setId('shirtplatform_setup_tabs');
		$this->setDestElementId('setup_form');
		$this->setTitle(__('Websites'));
	}

	/**
	 * @return $this
	 */
	protected function _beforeToHtml()
	{

		$websites = $this->storeManager->getWebsites();
		foreach ($websites as $website) {
			$this->addTab(
				$website->getCode(),
				[
					'label'  => __($website->getName()),
					'title'  => __($website->getName()),
					'url'    => $this->getUrl('*/*/tab', array('_current' => true, 'id' => $website->getId())),
					'class' => 'ajax',
//					'active' => true
				]
			);
		}

		return parent::_beforeToHtml();
	}
}
