<?php

namespace Shirtplatform\Core\Block\Adminhtml\Category\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class RefreshUrlButton implements ButtonProviderInterface {

    /**
     *
     * @var \Magento\Framework\View\Element\UiComponent\Context
     */
    private $_context;
        

    /**
     * 
     * @param \Magento\Framework\View\Element\UiComponent\Context $context
     */
    public function __construct(\Magento\Framework\View\Element\UiComponent\Context $context) {
        $this->_context = $context;
    }

    /**
     * Get refresh categories button data
     * 
     * @access public
     * @return array
     */
    public function getButtonData() {
        $storeId = $this->_context->getRequestParam('store', 0);        
        return [
            'label' => __('Refresh URLs for all categories'),
            'on_click' => sprintf("location.href = '%s';", $this->_context->getUrl('shirtplatform/category/refreshUrl', ['store_id' => $storeId])),
            'class' => 'save primary',
            'sort_order' => 40,
        ];
    }

}
