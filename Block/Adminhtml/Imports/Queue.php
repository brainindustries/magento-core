<?php

namespace Shirtplatform\Core\Block\Adminhtml\Imports;

use Magento\Backend\Block\Template\Context;
use Magento\AsynchronousOperations\Model\ResourceModel\Operation;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Bulk\OperationInterface;
use Shirtplatform\Core\Helper\Product as ProductHelper;

class Queue extends \Magento\Backend\Block\Template
{
    /**
     * @var int
     */
    const LAST_UPDATED_PRODUCTS_COUNT = 5;

    /**
     * @var Operation
     */
    private $_resource;
    
    /**
     * @var ProductHelper
     */
    private $_productHelper;
    
    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;
    
    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;
    
    /**
     * @var SortOrderBuilder
     */
    private $_sortOrderBuilder;

    /**
     * @param Context $context
     * @param Operation $resource
     * @param ProductHelper $productHelper
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        Context $context,
        Operation $resource,
        ProductHelper $productHelper,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        parent::__construct($context);
        $this->_resource = $resource;
        $this->_productHelper = $productHelper;
        $this->_productRepository = $productRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * @access public
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getLastUpdatedProducts()
    {
        $sortOrder = $this->_sortOrderBuilder
            ->setField('updated_at')
            ->setDirection('DESC')
            ->create();

        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('type_id', 'configurable')
            ->setSortOrders([$sortOrder])
            ->setPageSize(self::LAST_UPDATED_PRODUCTS_COUNT)
            ->create();

        return $this->_productRepository->getList($searchCriteria)->getItems();
    }

    /**
     * @access public
     * @return array
     */
    public function getRunningOperations()
    {
        $result = [];

        $connection = $this->_resource->getConnection();
        $select = $connection->select()
            ->from('magento_operation')
            ->where('status = ?', OperationInterface::STATUS_TYPE_OPEN);

        foreach ($connection->fetchAll($select) as $_operation) {
            $productSku = $this->_productHelper->getProductSkuFromOperation($_operation);

            $request = isset($result[$productSku]) 
                ? $result[$productSku]
                : [
                    'count' => 0,
                    'started_at' => $_operation['started_at']
                ];

            $request['count']++;
            
            if (!is_null($_operation['started_at']) && !is_null($request['started_at']) && strtotime($_operation['started_at']) < strtotime($request['started_at'])) {
                $request['started_at'] = $_operation['started_at'];
            }
            elseif (is_null($request['started_at']) && !is_null($_operation['started_at'])) {
                $request['started_at'] = $_operation['started_at'];
            }            
            
            $result[$productSku] = $request;
        }

        return $result;
    }
}