<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2019
 * Time: 11:00
 */

namespace Shirtplatform\Core\Block\Catalog\Product;

use Magento\Catalog\Block\Product\ImageFactory;
use Magento\Catalog\Helper\ImageFactory as HelperFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ResourceConnection;

/**
 * Class ImageBuilder
 * @package Shirtplatform\Core\Block\Catalog\Product
 */
class ImageBuilder extends \Magento\Catalog\Block\Product\ImageBuilder {

    /** @var ResourceConnection */
    private $resourceConnection;

    /**
     * ImageBuilder constructor.
     * @param HelperFactory $helperFactory
     * @param ImageFactory $imageFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
    HelperFactory $helperFactory,
    ImageFactory $imageFactory,
    ResourceConnection $resourceConnection
    ) {
        parent::__construct($helperFactory, $imageFactory);
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Create image block
     * 
     * @access public
     * @param Product|null $product
     * @param string|null $imageId
     * @param array|null $attributes
     * @return \Magento\Catalog\Block\Product\Image
     * @throws \Zend_Db_Statement_Exception
     */
    public function create(Product $product = null,
                           string $imageId = null,
                           array $attributes = null) {
        $image = parent::create($product, $imageId, $attributes);        
        $viewType = $this->getViewType($product, $imageId);
        $image->setViewType(($viewType != null) ? $viewType : 'empty');
        return $image;
    }

    /**
     * @param Product|null $product
     * @param string|null $imageId
     * @return null|string
     * @throws \Zend_Db_Statement_Exception
     */
    public function getViewType($product, $imageId) {
        if ($imageId !== 'category_page_grid') {
            return null;
        }

        if ($product == null || $product->getData('small_image') == null) {
            return null;
        }

        $connection = $this->resourceConnection->getConnection();
        $entryId = $connection->query('SELECT value_id FROM catalog_product_entity_media_gallery WHERE value="' . $product->getData('small_image') . '"')->fetchColumn();
        if ($entryId == null) {
            return null;
        }
        return strtolower($connection->query('SELECT view_type FROM shirtplatform_media_gallery_assigned_view WHERE image_id = ' . $entryId)->fetchColumn());
    }

}
