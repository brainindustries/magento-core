<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2019
 * Time: 11:11
 */

namespace Shirtplatform\Core\Block\Catalog\Product;

/**
 * Class Image
 * @package Shirtplatform\Core\Block\Catalog\Product
 */
class Image extends \Magento\Catalog\Block\Product\Image
{

    /** @var string */
    private $viewType;

    /**
     * @return string
     */
    public function getViewType()
    {
        return $this->viewType;
    }

    /**
     * @param string $viewType
     */
    public function setViewType($viewType)
    {
        $this->viewType = $viewType;
    }

}
