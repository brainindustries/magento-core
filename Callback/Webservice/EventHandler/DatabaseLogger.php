<?php

namespace Shirtplatform\Core\Callback\Webservice\EventHandler;

use Magento\Store\Model\ScopeInterface;

class DatabaseLogger extends \shirtplatform\callback\EmptyCallback {

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    private $_dbConnection;

    /**
     * Did exception occur during last request?
     * 
     * @var Boolean
     */
    private $_exceptionOccured = false;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_helper;

    /**
     * Response of last call
     * 
     * @access private
     * @var array
     */
    private $_lastResponse = array();

    /**
     * @var string
     */
    private $_logTableName;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resource;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $_serializer;

    /**
     * Store request to database?
     * 
     * @access private
     * @var boolean
     */
    private $_storeRequest = true;

    /**
     * Store response to database?
     * 
     * @access private
     * @var boolean
     */
    private $_storeResponse = true;

    /**
     * Store request params to database?
     * 
     * @access private
     * @var boolean
     */
    private $_storeParams = true;

    /**
     * Store exceptions to database?
     * 
     * @access private
     * @var boolean
     */
    private $_storeExceptions = true;

    /**
     * Store wrong response data to database?
     * 
     * @access private
     * @var boolean
     */
    private $_storeWrongResponse = true;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Shirtplatform\Core\Helper\Data $helper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(\Shirtplatform\Core\Helper\Data $helper,
                                \Magento\Framework\App\ResourceConnection $resource,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Serialize\SerializerInterface $serializer) {
        $this->_helper = $helper;
        $this->_resource = $resource;
        $this->_dbConnection = $this->_resource->getConnection();
        $this->_logTableName = $this->_resource->getTableName('shirtplatform_webservice_log');
        $this->_scopeConfig = $scopeConfig;
        $this->_serializer = $serializer;
        $this->_storeRequest = $this->_scopeConfig->getValue('shirtplatform/webservices/store_request', ScopeInterface::SCOPE_STORE);
        $this->_storeResponse = $this->_scopeConfig->getValue('shirtplatform/webservices/store_response', ScopeInterface::SCOPE_STORE);
        $this->_storeParams = $this->_scopeConfig->getValue('shirtplatform/webservices/store_params', ScopeInterface::SCOPE_STORE);
        $this->_storeExceptions = $this->_scopeConfig->getValue('shirtplatform/webservices/store_exception', ScopeInterface::SCOPE_STORE);
        $this->_storeWrongResponse = $this->_scopeConfig->getValue('shirtplatform/webservices/store_wrong_response', ScopeInterface::SCOPE_STORE);
    }

    /**
     * Callback called before each request
     * 
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function preRequest(\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        $this->_exceptionOccured = false;

        if($this->_storeRequest)
        {
            $bind = [
                'method' => $promise->getMethod(),
                'uri' => $promise->getUri()
            ];

            if($this->_storeParams)
            {
                $bind['params'] = $this->_serializer->serialize($promise->getOptions());
            }

            try
            {
                $res = $this->_dbConnection->insert($this->_logTableName, $bind);
                if($res == 1)
                {
                    $promise->data['id'] = $this->_dbConnection->lastInsertId();
                }
            }
            catch (\Exception $ex)
            {
                $this->_helper->logError($ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            }
        }
    }

    /**
     * Callback called after each request
     * 
     * @param type $response
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function postRequest(
        &$response,
        \shirtplatform\rest\promise\PlatformPromise &$promise
    ) {
        $this->_lastResponse = $response;

        if($this->_storeResponse)
        {
            $bind = [
                'method' => $promise->getMethod(),
                'uri' => $promise->getUri()
            ];

            if($response != null)
            {
                $bind['response'] = $this->_serializer->serialize($response);
            }

            if($this->_storeParams)
            {
                $bind['params'] = $this->_serializer->serialize($promise->getOptions());
            }

            try
            {
                if(isset($promise->data['id']) && $promise->data['id'])
                {
                    $this->_dbConnection->update($this->_logTableName, $bind, ['id = ?' => $promise->data['id']]);
                }
                else
                {
                    $res = $this->_dbConnection->insert($this->_logTableName, $bind);
                    if($res == 1)
                    {
                        $promise->data['id'] = $this->_dbConnection->lastInsertId();
                    }
                }
            }
            catch(\Exception $ex)
            {
                $this->_helper->logError($ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            }
        }
    }

    /**
     * Callback called on exception from WS
     * 
     * @param type $exception
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function onException(
        &$exception,
        \shirtplatform\rest\promise\PlatformPromise &$promise
    ) {
        $this->_exceptionOccured = true;

        if($this->_storeExceptions)
        {
            $bind = [
                'response' => $exception->getBody()->getContents(),
                'method' => $promise->getMethod(),
                'uri' => $promise->getUri(),
                'is_exception' => 1,
                'exception' => 'status: ' . $exception->getStatusCode() . ', reason phrase: ' . $exception->getReasonPhrase(),
                'params' => $this->_serializer->serialize($promise->getOptions())
            ];

            try
            {
                if(isset($promise->data['id']) && $promise->data['id'])
                {
                    $this->_dbConnection->update($this->_logTableName, $bind, ['id = ?' => $promise->data['id']]);
                }
                else
                {
                    $res = $this->_dbConnection->insert($this->_logTableName, $bind);
                    if($res == 1)
                    {
                        $promise->data['id'] = $this->_dbConnection->lastInsertId();
                    }
                }
            }
            catch(\Exception $ex)
            {
                $this->_helper->logError($ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            }
        }
    }

    /**
     * Callback called on unexpected response from WS
     * 
     * @param type $msg
     * @param type $response
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function onWrongResponse(
        &$msg,
        &$response,
        \shirtplatform\rest\promise\PlatformPromise &$promise
    ) {

        if($this->_storeWrongResponse)
        {
            $bind = [
                'method' => $promise->getMethod(),
                'uri' => $promise->getUri(),
                'params' => $this->_serializer->serialize($promise->getOptions()),
                'is_wrong_response' => 1,
                'response' => $this->_serializer->serialize($response)
            ];

            try
            {
                if(isset($promise->data['id']) and $promise->data['id'])
                {
                    $this->_dbConnection->update($this->_logTableName, $bind, ['id = ?' => $promise->data['id']]);
                }
                else
                {
                    $res = $this->_dbConnection->insert($this->_logTableName, $bind);
                    if($res == 1)
                    {
                        $promise->data['id'] = $this->_dbConnection->lastInsertId();
                    }
                }
            }
            catch(\Exception $ex)
            {
                $this->_helper->logError($ex->getMessage());
                $this->_helper->logError($ex->getTraceAsString());
            }
        }
    }

    /**
     * @param $msg
     * @param $response
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function onWrongReponse(
        $msg,
        &$response,
        \shirtplatform\rest\promise\PlatformPromise &$promise
    ) {
        $this->onWrongResponse($msg, $response, $promise);
    }
    
    /**
     * Did exception occur during last request?
     *
     * @return void
     */
    public function didExceptionOccur()
    {
        return $this->_exceptionOccured;
    }

    /**
     * Get last response
     * 
     * @access public
     * @return array
     */
    public function getLastResponse()
    {
        return $this->_lastResponse;
    }

}
