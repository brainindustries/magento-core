<?php

namespace Shirtplatform\Core\Cron;

class ApiExceptionReport
{
    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_helper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_dateTime;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @access public
     * @param \Shirtplatform\Core\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Shirtplatform\Core\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_helper = $helper;
        $this->_dateTime = $dateTime;
        $this->_resource = $resource;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }
    
    /**
     * Check platform exceptions from shirtplatform_webservice_log table for yesterday 
     * and send their overview to tech support.      
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        if (!$this->_scopeConfig->getValue('shirtplatform/cron/api_exception_report')) {
            return;
        }

        $now = $this->_dateTime->gmtTimestamp();
        $dateCond = $now - 24 * 3600;
        $connection = $this->_resource->getConnection();
        $logTableName = $this->_resource->getTableName('shirtplatform_webservice_log');
        $select1 = $connection->select()->from($logTableName)            
            ->where('method != ?', 'GET');
        $select2 = $connection->select()->from($logTableName)            
            ->where('method = ?', 'GET')
            ->where('uri LIKE ?', '%commit%');
        $orSelect = $connection->select()->from($logTableName)
            ->where(join(' ', $select1->getPart(\Magento\Framework\DB\Select::SQL_WHERE)))
            ->orWhere(join(' ', $select2->getPart(\Magento\Framework\DB\Select::SQL_WHERE)));        
        $mainSelect = $connection->select()->from($logTableName)
            ->where('is_exception = ?', 1)
            ->where('DATE(created_at) = ?', date('Y-m-d', $dateCond))
            ->where(join(' ' , $orSelect->getPart(\Magento\Framework\DB\Select::SQL_WHERE)));  
        $exceptions = $connection->fetchAll($mainSelect);            

        if (empty($exceptions)) {
            return;
        }

        $subject = 'Platform API exception log summary from ' . $this->_storeManager->getStore()->getBaseUrl();
        $body = 'These are the platform API exceptions for ' . date('Y-m-d', $dateCond) . '. More info in ' . $logTableName . ' table' . "\n\n";
        foreach ($exceptions as $_exception) {
            $body .= 'uri: ' . $_exception['uri'] . "\n";
            $body .= 'exception: ' . $_exception['exception'] . "\n";
            $body .= 'response: ' . $_exception['response'] . "\n";
            $body .= "========================================\n\n";
        }

        $this->_helper->sendEmailToTechSupport($subject, $body);
    }
}
