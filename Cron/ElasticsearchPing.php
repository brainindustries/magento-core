<?php

namespace Shirtplatform\Core\Cron;

class ElasticsearchPing
{
    /**
     * @var \Magento\Elasticsearch\Model\Adapter\Elasticsearch
     */
    private $_adapter;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_helper;

    /**     
     * @access public
     * @param \Magento\Elasticsearch\Model\Adapter\Elasticsearch $adapter
     * @param \Shirtplatform\Core\Helper\Data $helper
     * @return void
     */
    public function __construct(
        \Magento\Elasticsearch\Model\Adapter\Elasticsearch $adapter,
        \Shirtplatform\Core\Helper\Data $helper
    ) {
        $this->_adapter = $adapter;
        $this->_helper = $helper;
    }
    
    /**
     * Ping Elasticsearch and send email to tech support if it's down
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        try {
            $this->_adapter->ping();
        } catch (\Exception $ex) {
            $subject = 'Elasticsearch not available';
            $body = 'Elasticsearch is probably down. The ping() method threw this exception: ' . $ex->getMessage() .
                '. Restart it as soon as possible or there might be duplicate Paypal orders as customers might not be able to finish the order despite being charged, '
                . 'but never reaching the success page.';
            $this->_helper->sendEmailToTechSupport($subject, $body);
        }
    }
}
