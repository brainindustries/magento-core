<?php

namespace Shirtplatform\Core\Cron;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class CleanTables {

    /**
     * @var DateTime
     */
    private $_dateTime;

    /**
     * @var ResourceConnection
     */
    private $_resource;

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var AdapterInterface
     */
    private $_connection;

    /**
     * @param DateTime $dateTime
     * @param ResourceConnection $resource
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        DateTime $dateTime,
        ResourceConnection $resource,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_dateTime = $dateTime;
        $this->_resource = $resource;
        $this->_scopeConfig = $scopeConfig;
        $this->_connection = $resource->getConnection();
    }

    /**
     * Delete records from shirtplatform_webservice_log and magento_operation tables 
     * older than it's set up in admin configuration     
     * 
     * @access public
     */
    public function execute() 
    {
        $now = $this->_dateTime->gmtTimestamp();
        $this->deleteFromWebserviceLogTable($now);
        $this->deleteFromMagentoOperationTalbe($now);
    }

    /**
     * @access private
     * @param int $now
     * @return void
     */
    private function deleteFromWebserviceLogTable(int $now) : void
    {
        $cleanTable = $this->_scopeConfig->getValue('shirtplatform/webservices/clean_log_table');
        $cleanLifetime = $this->_scopeConfig->getValue('shirtplatform/webservices/log_lifetime');

        if (!$cleanTable || !($cleanLifetime > 0)) {
            return;
        }
        
        $timeCond = $now - (24 * 3600 * $cleanLifetime);

        $this->_connection->delete(
            $this->_resource->getTableName('shirtplatform_webservice_log'), 
            ['created_at < ?' => date('Y-m-d H:i:s', $timeCond)]
        );
    }

    /**
     * @access private
     * @param int $now
     * @return void
     */
    private function deleteFromMagentoOperationTalbe(int $now) : void
    {
        $cleanTable = $this->_scopeConfig->getValue('shirtplatform/cron/clean_magento_operation_table');
        $cleanLifetime = $this->_scopeConfig->getValue('shirtplatform/cron/magento_operation_table_lifetime');

        if (!$cleanTable || !($cleanLifetime > 0)) {
            return;
        }

        $timeCond = $now - (24 * 3600 * $cleanLifetime);
        
        $select = $this->_connection->select()
            ->from($this->_resource->getTableName('magento_operation'))
            ->where('started_at < ?', date('Y-m-d H:i:s', $timeCond));
        
        $operationsByTimeCond = $this->_connection->fetchAll($select);
        $couponOperations = $this->selectCouponOperations($timeCond);
        
        $bulkUuids = array_merge(
            array_unique(array_column($operationsByTimeCond, 'bulk_uuid')),
            array_unique(array_column($couponOperations, 'bulk_uuid'))
        );

        $table = $this->_resource->getTableName('magento_operation');

        $this->_connection->delete($table, ['started_at < ?' => date('Y-m-d H:i:s', $timeCond)]);
        $this->_connection->delete($table, ['id IN (?)' => array_column($couponOperations ,'id')]);
        $this->_connection->delete($this->_resource->getTableName('magento_bulk'), ['uuid IN (?)' => $bulkUuids]);
    }

    /**
     * @access private
     * @param int $timeCond
     * @return array
     */
    private function selectCouponOperations(int $timeCond) : array
    {
        $select = $this->_connection->select()
            ->from($this->_resource->getTableName('magento_operation'))
            ->where('topic_name = ?', 'sales.rule.update.coupon.usage')
            ->where('status = ?', 1)
            ->where('(started_at < ? OR started_at IS NULL)', date('Y-m-d H:i:s', $timeCond));
            
        return $this->_connection->fetchAll($select);
    }
}