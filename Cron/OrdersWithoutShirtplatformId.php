<?php

namespace Shirtplatform\Core\Cron;

use Magento\Backend\Model\UrlInterface;
use Shirtplatform\Core\Helper\Data;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use shirtplatform\entity\overview\DesignedOrderedProduct;
use shirtplatform\filter\WsParameters;
use shirtplatform\entity\order\Order;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use shirtplatform\entity\enumerator\OrderFinancialStatus;

class OrdersWithoutShirtplatformId {
    
    /**
     * @var Data
     */
    private $_helper;

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;
    
    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;
    
    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var UrlInterface
     */
    private $_urlInterface;

    /**
     * @param Data $helper
     * @param OrderRepositoryInterface $orderRepositoryInterface
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        Data $helper,
        OrderRepositoryInterface $orderRepository,
        ScopeConfigInterface $scopeConfig,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        UrlInterface $urlInterface
    ){
        $this->_helper = $helper;
        $this->_orderRepository = $orderRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_urlInterface = $urlInterface;
    }

    /**
     * @access public
     * @return void
     */
    public function execute(){
        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('shirtplatform_id', true, 'null')
            ->addFilter('created_at', date('Y-m-d', strtotime('-7 days')), 'gteq')
            ->addFilter('status', ['closed', 'canceled'], 'nin')
            ->create();

        $orders = $this->_orderRepository->getList($searchCriteria)->getItems();

        $emailOrders = [];

        foreach ($orders as $order){
            $itemShirtplatformIds = [];
            foreach ($order->getAllVisibleItems() as $item){
                if ($item->getShirtplatformProductType() !== null){
                    if ($item->getShirtplatformId() === null){
                        $emailOrders[$order->getId()] = $this->_urlInterface->getUrl('sales/order/view', ['order_id' => $order->getId()]);
                        continue;
                    }
                    $itemShirtplatformIds[] = $item->getShirtplatformId();
                }
            }

            if (empty($itemShirtplatformIds)){
                continue;
            }

            $this->_helper->logMessage('Fixing order with missing Shirtplatform ID (order id: '. $order->getId() .').');

            $this->_helper->shirtplatformAuth($order->getStoreId());

            $wsParams = new WsParameters();
            $wsParams->addExpressionContain('id', $itemShirtplatformIds);
            $products = DesignedOrderedProduct::findAll($wsParams);

            if (!$products){
                $msg = "Order ". $order->getId() ." could not be fixed by cron, there was a problem fetching it's products from Shirtplatform.";
                $order->addStatusHistoryComment($msg, 'error_in_order');
                $this->_helper->logMessage($msg);
                continue;
            }

            $mapper = [];
            foreach ($products as $product){
                if (!array_key_exists($product->orderId, $mapper)){
                    $mapper[$product->orderId] = [];
                }
                $mapper[$product->orderId][] = $product->id;
            }

            if (count(array_keys($mapper)) > 1){
                $this->_helper->logMessage('Products are from multiple orders. Creating a new one.');
                
                $countryId = $this->_scopeConfig->getValue('shirtplatform/general/country', ScopeInterface::SCOPE_STORE, $order->getStoreId());
                $newPlatformOrder = Order::createBySession($countryId);

                $wsParams = new WsParameters();
                $wsParams->addExpressionContain('id', array_keys($mapper));
                $wsParams->addExpressionContain('financialStatus', [OrderFinancialStatus::PENDING, OrderFinancialStatus::PAID]);
                $commitedOrders = Order::findAll($wsParams);

                foreach ($mapper as $orderId => $items){
                    if (array_key_exists($orderId, $commitedOrders)){
                        foreach ($commitedOrders[$orderId]->getOrderedProducts() as $orderedProduct){
                            if (!in_array($orderedProduct->id, $items)) {
                                continue;
                            }

                            $origProductId = $orderedProduct->id;
                            $orderedProduct->id = null;
                            $orderedProduct->uuid = null;
                            $orderedProduct->setParents($newPlatformOrder->id);
                            $orderedProduct->__update();

                            $this->_helper->logMessage(sprintf(
                                'Designed product %d was created in session order %d, because original product %d was in commited order %d.',
                                $orderedProduct->id,
                                $newPlatformOrder->id,
                                $origProductId,
                                $orderId
                            ));
                        } 
                    } else {
                        Order::transfer($orderId, $newPlatformOrder->id, null, false, $items);
                    }
                }

                if (count($newPlatformOrder->getOrderedProducts()) === count($itemShirtplatformIds)){
                    $order->setShirtplatformId($newPlatformOrder->id);
                } else {
                    $newPlatformOrder->__delete();
                    $emailOrders[$order->getId()] = $this->_urlInterface->getUrl('sales/order/view', ['order_id' => $order->getId()]);
                    $this->_helper->logMessage('Could not transfer all of the products. Skipping order '. $order->getId() .'.');
                    continue;
                }
            } else {
                $order->setShirtplatformId(array_keys($mapper)[0]);
            }

            if ($item->getShirtplatformId() !== null){
                $this->_helper->logMessage('Saving order '. $order->getId() .' (shirtplatform id: '. $order->getShirtplatformId() .').');
                Order::commit($order->getShirtplatformId(), $order->getIsPaid() ? OrderFinancialStatus::PAID : OrderFinancialStatus::PENDING);
                $this->_orderRepository->save($order);
            }
        }

        if (!empty($emailOrders)){
            $emailBody = "Following orders have missing Shirtplatform ID:\n\n";
            $emailBody .= implode("\n", array_values($emailOrders));
            $this->_helper->sendEmailToTechSupport('Missing Shirtplatform IDs', $emailBody);
        }
    }

}
