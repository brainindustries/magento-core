<?php

namespace Shirtplatform\Core\Cron;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Shirtplatform\Core\Helper\Data;
use Shirtplatform\ProductionTool\Api\NotificationRepositoryInterface;

class ErrorReport {

    /**
     * @var AdapterInterface
     */
    private $_connection;

    /**
     * @var Data
     */
    private $_helper;

    /**
     * @var NotificationRepositoryInterface
     */
    private $_notificationRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @param Data $helper
     * @param NotificationRepositoryInterface $notificationRepository
     * @param ResourceConnection $resource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Data $helper,
        NotificationRepositoryInterface $notificationRepository,
        ResourceConnection $resource,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        $this->_connection = $resource->getConnection();
        $this->_helper = $helper;
        $this->_notificationRepository = $notificationRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @access public
     * @return void
     */
    public function execute(){
        $emailBody = '';

        // cron schedule error items
        $cronScheduleSelect = $this->_connection->select()
            ->from($this->_connection->getTableName('cron_schedule'))
            ->where('status = ?', 'error')
            ->where('scheduled_at >= ?', date('Y-m-d H:i:s', strtotime('-1 hour')))
            ->where('job_code NOT IN(?)', ['klarna_core_clean_logs', 'analytics_collect_data']);

        $cronScheduleItems = $this->_connection->fetchAll($cronScheduleSelect);

        if ($cronScheduleItems){
            $emailBody .= 'There is '. count($cronScheduleItems) ." cron job(s) with error status:\n\n";
            foreach ($cronScheduleItems as $item){
                $emailBody .= 'Schedule ID: '. $item['schedule_id'] .', ';
                $emailBody .= 'Job code: '. $item['job_code'] .', ';
                $emailBody .= 'Executed at: '. $item['executed_at'] ."\n";
                $emailBody .= 'Message: '. $item['messages'] ."\n\n";
            }
        }

        // unprocessed notifications
        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('processed_at', true, 'null')
            ->addFilter('created_at', date('Y-m-d H:i:s', strtotime('-4 hours')), 'lteq')
            ->create();
        
        $notifications = $this->_notificationRepository->getList($searchCriteria)->getItems();

        if ($notifications){
            $emailBody .= !empty($emailBody) ? "\n\n" : '';
            $emailBody .= 'There is '. count($notifications) ." unprocessed notification(s).\n";
            $emailBody .= 'The oldest unprocessed notification is from '. array_shift($notifications)->getCreatedAt() .'.';
        }

        // send email to tech support
        if ($emailBody){
            $this->_helper->sendEmailToTechSupport('Cron errors', $emailBody);        
        }
    }

}
