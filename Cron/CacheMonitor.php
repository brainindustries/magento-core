<?php

namespace Shirtplatform\Core\Cron;

class CacheMonitor {

    /**
     * @var \Magento\Framework\App\State
     */
    private $_appState;

    /**
     * @var \Magento\Framework\App\Cache\Manager
     */
    private $_cacheManager;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_coreHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * 
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\App\Cache\Manager $cacheManager
     * @param \Shirtplatform\Core\Helper\Data $coreHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\Framework\App\State $appState,
                                \Magento\Framework\App\Cache\Manager $cacheManager,
                                \Shirtplatform\Core\Helper\Data $coreHelper,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_appState = $appState;
        $this->_cacheManager = $cacheManager;
        $this->_coreHelper = $coreHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }

    /**
     * Cron that checks if the cache is disabled and if so sends email to tech support
     * 
     * @access public
     */
    public function execute() {
        $cacheMonitorEnabled = $this->_scopeConfig->getValue('shirtplatform/cron/cache_monitor_enabled');        
        
        if ($cacheMonitorEnabled and $this->_appState->getMode() == \Magento\Framework\App\State::MODE_PRODUCTION) {
            $status = $this->_cacheManager->getStatus();
            $disabledCache = false;

            foreach ($status as $type => $enabled) {
                if (!$enabled) {
                    $disabledCache = true;
                    break;
                }
            }

            if ($disabledCache) {
                $store = $this->_storeManager->getStore();
                $baseUrl = $store->getBaseUrl();
                $techEmails = $this->_scopeConfig->getValue('trans_email/ident_tech/email_addresses');
                $this->_coreHelper->logError('Cache is disabled');
                
                if ($techEmails) {
                    $subject = 'Cache Monitor cron';
                    $message = 'Cache is disabled for project ' . $baseUrl . '. Please check it out.';
                    $emails = explode(',', $techEmails ?? '');
                    
                    foreach ($emails as $key => $_email) {
                        if (!filter_var(trim($_email), FILTER_VALIDATE_EMAIL)) {
                            unset($emails[$key]);
                        }
                    }
                    
                    if (!empty($emails)) {
                        $this->_coreHelper->sendEmailToAdmin($subject, $message, $emails);
                    }
                }
            }
        }
    }

}
