<?php

namespace Shirtplatform\Core\Cron;

use Magento\Store\Model\ScopeInterface;

class UnpaidOrderReminder {

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_coreHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_dateTime;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $_emulation;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $_orderCollectionFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $_priceCurrency;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $_timezone;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $_transportBuilder;

    public function __construct(\Shirtplatform\Core\Helper\Data $coreHelper,
                                \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
                                \Magento\Store\Model\App\Emulation $emulation,
                                \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
                                \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
                                \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone) {
        $this->_coreHelper = $coreHelper;
        $this->_dateTime = $dateTime;
        $this->_emulation = $emulation;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_orderRepository = $orderRepository;
        $this->_priceCurrency = $priceCurrency;
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->_timezone = $timezone;
    }

    /**
     * Find pending orders purchased 7 days ago that haven't been paid yet and send
     * reminder email to the customers
     * 
     * @access public
     */
    public function execute() {
        //7 days
        $orderAge = 7 * 24 * 3600;
        $now = $this->_dateTime->gmtTimestamp();
        $dateCond = $now - $orderAge;                

        $collection = $this->_orderCollectionFactory->create()
                ->addAttributeToFilter('status', 'pending')
                ->addAttributeToFilter('payment_table.method', ['neq' => 'cashondelivery']);
        $collection->getSelect()->join(
                        ['payment_table' => 'sales_order_payment'], 'main_table.entity_id = payment_table.parent_id', ['method']
                )
                ->where('DATE(main_table.created_at) = ?', date('Y-m-d', $dateCond));                
        
        $storeOrders = $orderNumbers = [];
        foreach ($collection as $order) {
            $storeOrders[$order->getStoreId()][] = $order;
        }

        foreach ($storeOrders as $storeId => $orders) {
            $enabled = $this->_scopeConfig->getValue('shirtplatform/cron/send_unpaid_order_reminder', ScopeInterface::SCOPE_STORE, $storeId);
            if (!$enabled) {
                continue;
            }

            $templateId = $this->_scopeConfig->getValue('shirtplatform/cron/unpaid_order_email_template', ScopeInterface::SCOPE_STORE, $storeId);
            if (empty($templateId)) {
                $this->_coreHelper->logError('Email template for unpaid orders reminder for store ' . $storeId . ' does not exist.');
                continue;
            }

            $this->_emulation->startEnvironmentEmulation($storeId);

            $from = $this->_scopeConfig->getValue(
                    \Magento\Sales\Model\Order\Email\Container\OrderIdentity::XML_PATH_EMAIL_IDENTITY, ScopeInterface::SCOPE_STORE, $storeId
            );

            foreach ($orders as $_order) {
                $orderNumbers[] = $_order->getIncrementId();
                $customerName = $_order->getCustomerIsGuest() ? $_order->getBillingAddress()->getName() : $_order->getCustomerName();
                $templateVars = [
                    'order_date' => $this->_timezone->formatDate($_order->getCreatedAt(), \IntlDateFormatter::MEDIUM),
                    'order_grand_total' => $this->_priceCurrency->format($_order->getGrandTotal(), false, \Magento\Framework\Pricing\PriceCurrencyInterface::DEFAULT_PRECISION, $storeId),
                    'order' => $_order,
                    'order_id' => $_order->getId(),
                    'store' => $_order->getStore()
                ];
                $transportBuilder = $this->_transportBuilder->setTemplateIdentifier($templateId)
                        ->setTemplateOptions([
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                            'store' => $storeId
                        ])
                        ->setFrom($from)
                        ->addTo($_order->getCustomerEmail(), $customerName)
                        ->setTemplateVars($templateVars);
                $transportBuilder->getTransport()->sendMessage();
                $_order->addCommentToStatusHistory('Unpaid order reminder email sent');
                $this->_orderRepository->save($_order);
            }

            $this->_emulation->stopEnvironmentEmulation();
        }

        $msg = 'Unpaid order reminder email has been sent for these orders: ' . join(', ', $orderNumbers);
        $this->_coreHelper->logMessage($msg);
    }

}
