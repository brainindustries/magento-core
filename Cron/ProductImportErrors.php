<?php

namespace Shirtplatform\Core\Cron;

use Shirtplatform\Core\Helper\Data as CoreHelper;
use Shirtplatform\Core\Helper\Product as ProductHelper;

class ProductImportErrors
{
    /**
     * @var CoreHelper
     */
    private $_coreHelper;

    /**
     * @var ProductHelper
     */
    private $_productHelper;

    /**
     * @param CoreHelper $coreHelper
     * @param ProductHelper $productHelper
    */
    public function __construct(
        CoreHelper $coreHelper,
        ProductHelper $productHelper
    ) {
        $this->_coreHelper = $coreHelper;
        $this->_productHelper = $productHelper;
    }

    public function execute()
    {
        $errors = $this->_productHelper->getAsyncImportErrors('-15 min');

        if (empty($errors)) {
            return;
        }

        $emailBody = "Import failed for following products:\n\n";
        
        foreach ($errors as $_sku => $_error) {
            $emailBody .= $_sku . ":\n\n";
            foreach ($_error as $_message => $_count) {
                $emailBody .= $_message . ' (' . $_count . ")\n";
            }
            $emailBody .= "\n\n";
        }

        $recipients = [];
        
        $techSupport = $this->_coreHelper->getConfigValue('trans_email/ident_tech/email_addresses');
        if (!empty($techSupport)) {
            $recipients = array_merge($recipients, array_map('trim', explode(',', $techSupport)));
        }
        
        $notificationEmail = $this->_coreHelper->getConfigValue('shirtplatform/general/notification_email');
        if (!empty($notificationEmail)) {
            $recipients[] = $notificationEmail;
        }
        
        if (!empty($recipients)){
            $this->_coreHelper->sendEmailToAdmin('Product import failed', $emailBody, array_filter($recipients));
        }
    }
}