<?php

namespace Shirtplatform\Core\Cron;

class ClearCache {

    /**
     * @var \Shirtplatform\Core\Api\CacheCleanInterface
     */
    private $_cacheClean;

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Shirtplatform\Core\Api\CacheCleanInterface $cacheClean
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Shirtplatform\Core\Api\CacheCleanInterface $cacheClean,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_cacheClean = $cacheClean;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Clear cache
     * 
     * @access public
     */
    public function execute() {
        $clearCacheEnabled = $this->_scopeConfig->getValue('shirtplatform/cron/clear_cache_enabled');
        
        if ($clearCacheEnabled) {            
            $this->_cacheClean->fullPage();
        }
    }

}
