<?php

namespace Shirtplatform\Core\Cron;

use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Shirtplatform\Core\Helper\Overview;

class DeleteOldOverviewFiles
{
    /**
     * @var DateTime
     */
    private $_dateTime;

    /**
     * @var File
     */
    private $_ioFile;

    /**
     * @var Overview
     */
    private $_overviewHelper;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_shirtplatformHelper;

    /**
     * @param DateTime $dateTime
     * @param File $ioFile
     * @param Overview $overviewHelper
     * @param \Shirtplatform\Core\Helper\Data $shirtplatformHelper
     */
    public function __construct(        
        DateTime $dateTime,
        File $ioFile,
        Overview $overviewHelper,
        \Shirtplatform\Core\Helper\Data $shirtplatformHelper
    ) {        
        $this->_dateTime = $dateTime;
        $this->_ioFile = $ioFile;
        $this->_overviewHelper = $overviewHelper;
        $this->_shirtplatformHelper = $shirtplatformHelper;
    }

    /**
     * Delete old overview files that are older than value set in admin config
     * 
     * @access public
     * @return void
     */
    public function execute()
    {
        $overviewDir = $this->_overviewHelper->getOverviewDirectory();
        $lifetimeConfig = (int)$this->_shirtplatformHelper->getConfigValue('shirtplatform/cron/order_overviews_lifetime');
        $now = $this->_dateTime->timestamp();
        $maxLifetime = $now - 24 * 3600 * $lifetimeConfig;
        
        try {
            $this->_ioFile->cd($overviewDir);
            $files = $this->_ioFile->ls(File::GREP_FILES);
            $deletedFiles = $notDeletedFiles = [];            

            foreach ($files as $_file) {
                $modifiedTime = strtotime($_file['mod_date']);
                if ($modifiedTime < $maxLifetime) {
                    $filename = $overviewDir . DIRECTORY_SEPARATOR . $_file['text'];

                    if ($this->_ioFile->rm($filename)) {
                        $deletedFiles[] = $filename;    
                    }
                    else {
                        $notDeletedFiles[] = $filename;
                    }                    
                }
            }            

            if (!empty($deletedFiles)) {
                $msg = "These overview files have been deleted by cron: \n" . join("\n", $deletedFiles);
                $this->_shirtplatformHelper->logMessage($msg);
            }
            if (!empty($notDeletedFiles)) {
                $msg = "These overview files couldn't be deleted by cron: \n" . join("\n", $notDeletedFiles);
                $this->_shirtplatformHelper->logError($msg);
            }
        } catch (\Exception $ex) {
            $msg = 'Error in DeleteOldOverviewFiles cron. Excetion: ' . $ex->getMessage();
            $this->_shirtplatformHelper->logError($msg);
        }
    }
}
