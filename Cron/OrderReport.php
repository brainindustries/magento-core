<?php

namespace Shirtplatform\Core\Cron;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\App\Emulation;
use Shirtplatform\Core\Helper\Data;
use Shirtplatform\Core\Model\Email\Sender;

class OrderReport
{
    /**
     * @var WriteInterface
     */
    private $_directory;

    /**
     * @var Emulation
     */
    private $_emulation;

    /**
     * @var Sender
     */
    private $_emailSender;

    /**
     * @var File
     */
    private $_file;

    /**
     * @var Data
     */
    private $_helper;

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @param Sender $emailSender
     * @param Emulation $emulation
     * @param File $file
     * @param Filesystem $filesystem
     * @param Data $helper
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Sender $emailSender,
        Emulation $emulation,
        File $file,
        Filesystem $filesystem,
        Data $helper,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StoreManagerInterface $storeManager
    ) {
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_emailSender = $emailSender;
        $this->_emulation = $emulation;
        $this->_file = $file;
        $this->_helper = $helper;
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_storeManager = $storeManager;
    }

    public function execute()
    {
        $enabled = $this->_helper->getConfigValue('shirtplatform/cron/daily_report_enabled');
        if (!$enabled) {
            return;
        }

        $this->_directory->create('order_report');
        
        $this->sendReports();

        $dayOfMonth = $this->_helper->getConfigValue('shirtplatform/cron/day_of_month_for_monthly_report');
        if (date('d') == $dayOfMonth) {
            $this->sendReports(true);
        }
    }

    /**
     * @access private
     * @param bool $wholeMonth
     * @return \Magento\Sales\Api\Data\OrderInterface[]
     */
    private function fetchOrders(bool $wholeMonth)
    {
        $firstDayOfThisMonth = date('Y-m') . '-01';
        
        $from = $wholeMonth ? $firstDayOfThisMonth . ' -1 month' : 'yesterday';
        $to = $wholeMonth ? $firstDayOfThisMonth : 'today';

        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('created_at', date('Y-m-d H:i:s', strtotime($from)), 'gteq')
            ->addFilter('created_at', date('Y-m-d H:i:s', strtotime($to) - 1), 'lteq')
            ->create();

        return $this->_orderRepository->getList($searchCriteria)->getItems();
    }

    /**
     * @access private
     * @param bool $wholeMonth
     * @return array
     */
    private function getOrdersData(bool $wholeMonth) : array
    {
        $result = [];

        foreach ($this->fetchOrders($wholeMonth) as $_order) {
            $result[$_order->getStoreId()][] = [
                'increment_id' => $_order->getIncrementId(),
                'status' => $_order->getStatus(),
                'grand_total' => $_order->getGrandTotal(),
                'total_qty_ordered' => $_order->getTotalQtyOrdered(),
                'payment_method' => $_order->getPayment()->getMethod(),
                'created_at' => $_order->getCreatedAt(),
                'firstname' => $_order->getBillingAddress()->getFirstname(),
                'lastname' => $_order->getBillingAddress()->getLastname()
            ];
        }

        return $result;
    }

    /**
     * @access private
     * @param array $emailAttachments
     * @param bool $wholeMonth
     * @return void
     */
    private function sendEmail(array $emailAttachments, bool $wholeMonth)
    {
        $recipient = $this->_helper->getConfigValue('trans_email/ident_marketing/email_addresses');
        
        if (empty($recipient)) {
            $this->_helper->logError('Tried to send daily order report email to marketing, but no recipient is configured.');
            return;
        }

        $this->_emailSender->send([
            'template_id' => 'shirtplatform_order_report',
            'var_code' => 'order_report_data',
            'recipient' => $recipient,
            'template_vars' => [
                'wholeMonth' => $wholeMonth,
                'intervalString' => $wholeMonth ? 'Monthly' : 'Daily',
                'attachments' => $emailAttachments
            ]
        ]);
    }

    /**
     * @access private
     * @param bool $wholeMonth
     * @return void
     */
    private function sendReports(bool $wholeMonth = false)
    {
        foreach ($this->getOrdersData($wholeMonth) as $_storeId => $_ordersData) {
            $this->_emulation->startEnvironmentEmulation($_storeId);

            $storeCode = $this->_storeManager->getStore($_storeId)->getCode();
            $date = date($wholeMonth ? 'Ym' : 'Ymd', strtotime('yesterday midnight'));
            
            $fileName = sprintf('orders_%s_%s.csv', $date, $storeCode);
            
            $this->_helper->createCsvFile($fileName, $_ordersData);
            
            $attachmentPath = $this->_directory->getAbsolutePath() . $fileName;

            $this->sendEmail([$attachmentPath], $wholeMonth);
            $this->_file->deleteFile($attachmentPath);

            $this->_emulation->stopEnvironmentEmulation();
        }
    }
}