<?php

namespace Shirtplatform\Core\Cron;

use Magento\Config\Model\Config\Source\Nooptreq;

class CheckTelephoneConfig
{
    /**
     * @var \BrainIndustries\Core\Helper\Data
     */
    private $_brainHelper;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $_cacheTypeList;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $_eavConfig;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $_emulation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * 
     * @access public
     * @param \BrainIndustries\Core\Helper\Data $brainhelper
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Eav\Model\Config $eavConfig     
     * @param \Magento\Store\Model\App\Emulation $emulation
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @return void
     */
    public function __construct(
        \BrainIndustries\Core\Helper\Data $brainhelper,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_brainHelper = $brainhelper;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_eavConfig = $eavConfig;
        $this->_emulation = $emulation;
        $this->_storeManager = $storeManager;
    }

    /**
     * Compare telephone configuration against the cached configuration and clean the EAV cache
     * if there's a difference (is_required)
     * 
     * @access public
     */
    public function execute()
    {
        foreach ($this->_storeManager->getStores() as $store) {
            $this->_emulation->startEnvironmentEmulation($store->getId(), \Magento\Framework\App\Area::AREA_FRONTEND);
            $dataObject = new \Magento\Framework\DataObject([
                'store_id' => $store->getId(),
                'attribute_set_id' => \Magento\Customer\Api\AddressMetadataInterface::ATTRIBUTE_SET_ID_ADDRESS
            ]);

            $this->_eavConfig->getEntityAttributes(\Magento\Customer\Api\AddressMetadataInterface::ENTITY_TYPE_ADDRESS, $dataObject);
            $isTelephoneRequired = $this->_eavConfig->getAttribute('customer_address', 'telephone')->getIsRequired();
            $configTelephoneIsRequired = $this->_brainHelper->getConfigValue('customer/address/telephone_show', $store->getId());            

            if (
                ($configTelephoneIsRequired != Nooptreq::VALUE_REQUIRED && $isTelephoneRequired) ||
                ($configTelephoneIsRequired == Nooptreq::VALUE_REQUIRED && !$isTelephoneRequired)
            ) {
                $this->_cacheTypeList->cleanType(\Magento\Eav\Model\Cache\Type::TYPE_IDENTIFIER);
                $msg = 'EAV cache cleand by cron because the telephone configuration was cached wrong for store ' . $store->getCode(); 
                $this->_brainHelper->logMessage($msg);
            }

            $this->_emulation->stopEnvironmentEmulation();
        }
    }
}
