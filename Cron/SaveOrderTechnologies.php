<?php

namespace Shirtplatform\Core\Cron;

use Shirtplatform\Core\Helper\PrintTechnologyHelper;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class SaveOrderTechnologies
{
    /**
     * @var PrintTechnologyHelper
     */
    private $_helper;

    /**
     * @var CollectionFactory
     */
    private $_collectionFactory;

    /**
     * @param PrintTechnologyHelper $helper
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        PrintTechnologyHelper $helper,
        CollectionFactory $collectionFactory
    ) {
        $this->_helper = $helper;
        $this->_collectionFactory = $collectionFactory;
    }

    public function execute()
    {
        $collection = $this->_collectionFactory->create()
            ->addFieldToFilter('shirtplatform_id', ['notnull' => true])
            ->addFieldToFilter('created_at', ['gteq' => date('Y-m-d H:i:s', strtotime('-6 hours'))])
            ->addFieldToFilter('status', ['nin' => ['closed', 'canceled', 'complete']]);

        $collection->getSelect()
            ->joinLeft(
                ['print_technology_table' => 'shirtplatform_print_technology'], 
                'main_table.entity_id = print_technology_table.order_id',
                []
            )
            ->where('print_technology_table.id IS NULL');

        $this->_helper->saveOrderTechnologies($collection);
    }
}