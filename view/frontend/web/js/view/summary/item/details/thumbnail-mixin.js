define(['jquery', 'ko'], function ($, ko) {
    'use strict';

    return function (thumbnail) {
        return thumbnail.extend({
            defaults: {
                template: 'Shirtplatform_Core/summary/item/details/thumbnail'
            },

            designImages: window.checkoutConfig.design_images || [],
            mouseoverImages: window.checkoutConfig.mouseover_images || [],
            cssRight: ko.observable(),

            initialize: function () {
                var self = this;
                this.cssRight(this.getSummaryWidth());

                $(window).resize(function () {
                    self.cssRight(self.getSummaryWidth());
                });
                return this._super();
            },

            hasDesignImages: function (item) {
                if (typeof(this.designImages['item_' + item['item_id']]) != 'undefined') {
                    if (Object.keys(this.designImages['item_' + item['item_id']]).length) {
                        return true;
                    }
                }

                return false;
            },

            getDesignImages: function (item) {
                if (this.designImages['item_' + item['item_id']]) {
                    return this.designImages['item_' + item['item_id']];
                }

                return {};
            },

            getDesignImagesCount: function (item) {
                var designImages = this.getDesignImages(item);
                return Object.keys(designImages).length;
            },

            getMouseoverImages: function (item) {
                if (this.mouseoverImages['item_' + item['item_id']]) {
                    return this.mouseoverImages['item_' + item['item_id']];
                }

                return {};
            },

            getThumbnailDesignImage: function (item) {
                var designImages = this.getDesignImages(item);
                var objectKeys = Object.keys(designImages);

                if (objectKeys.length) {
                    var key = objectKeys[0];
                    return designImages[key];
                }

                return null;
            },

            getSummaryWidth: function () {
                var width = 383;

                if ($('.opc-sidebar').length) {
                    width = $('.opc-sidebar').width();
                    var padding = parseInt($('.opc-block-summary').css('padding-left'));
                    width -= padding;
                }

                return width;
            }
        });
    }
});