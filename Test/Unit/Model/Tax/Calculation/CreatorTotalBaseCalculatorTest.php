<?php

namespace Shirtplatform\Core\Test\Unit\Model\Tax\Calculation;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\Tax\Api\TaxClassManagementInterface;
use Magento\Tax\Api\Data\AppliedTaxInterface;
use Magento\Tax\Api\Data\AppliedTaxInterfaceFactory;
use Magento\Tax\Api\Data\AppliedTaxRateInterface;
use Magento\Tax\Api\Data\AppliedTaxRateInterfaceFactory;
use Magento\Tax\Api\Data\QuoteDetailsItemExtensionInterface;
use Magento\Tax\Api\Data\QuoteDetailsItemInterface;
use Magento\Tax\Api\Data\TaxDetailsItemInterface;
use Magento\Tax\Api\Data\TaxDetailsItemInterfaceFactory;
use Magento\Tax\Model\Calculation;
use Magento\Tax\Model\Config;
use Magento\Tax\Model\TaxDetails\AppliedTaxRate;
use Magento\Tax\Model\TaxDetails\ItemDetails;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shirtplatform\Core\Model\Tax\Calculation\CreatorTotalBaseCalculator;

class CreatorTotalBaseCalculatorTest extends TestCase
{
    const STORE_ID = 2300;
    const UNIT_PRICE_INCL_TAX = 495.49549549545;
    const UNIT_PRICE_INCL_TAX_ROUNDED = 495.5;
    const CODE = 'CODE';
    const ONCE = 'once';
    const MOCK_METHOD_NAME = 'mock_method_name';
    const MOCK_VALUE = 'mock_value';
    const WITH_ARGUMENT = 'with_argument';

    /** @var array */
    private $configData;

    /** @var MockObject */
    protected $calculator;

    /** @var MockObject */
    protected $mockScopeConfig;

    /** @var MockObject */
    protected $taxItemDetailsDataObjectFactory;

    /** @var MockObject */
    protected $mockCalculationTool;

    /** @var MockObject */
    protected $mockConfig;

    /** @var QuoteDetailsItemInterface|MockObject */
    protected $mockItem;

    /** @var MockObject */
    protected $appliedTaxDataObjectFactory;

    /** @var MockObject */
    protected $appliedTaxRateDataObjectFactory;

    /** @var MockObject */
    protected $mockAppliedTax;

    /** @var DataObject */
    protected $addressRateRequest;

    /** @var Context|MockObject */
    protected $mockContext;

    /** @var Registry|MockObject */
    protected $mockRegistry;

    /** @var MockObject */
    protected $mockExtensionAttributesFactory;

    /** @var MockObject */
    protected $mockCustomAttributeFactory;

    /** @var  AppliedTaxRateInterface */
    protected $appliedTaxRate;

    /**
     * @var TaxDetailsItemInterface
     */
    protected $taxDetailsItem;

    /**
     * @var QuoteDetailsItemExtensionInterface|MockObject
     */
    private $quoteDetailsItemExtension;

    protected function setUp(): void
    {
        $this->mockContext = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockRegistry = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockExtensionAttributesFactory = $this->getMockBuilder(ExtensionAttributesFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockCustomAttributeFactory = $this->getMockBuilder(AttributeValueFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->taxItemDetailsDataObjectFactory = $this->createPartialMock(
            TaxDetailsItemInterfaceFactory::class,
            ['create']
        );
        $this->taxDetailsItem = new ItemDetails(
            $this->mockContext,
            $this->mockRegistry,
            $this->mockExtensionAttributesFactory,
            $this->mockCustomAttributeFactory
        );
        $this->taxItemDetailsDataObjectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->taxDetailsItem);

        $this->mockCalculationTool = $this->getMockBuilder(Calculation::class)
            ->disableOriginalConstructor()
            ->onlyMethods(
                ['__wakeup', 'round', 'getRate', 'getStoreRate', 'getRateRequest', 'getAppliedRates']
            )
            ->getMock();
        $this->mockConfig = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockItem = $this->getMockBuilder(QuoteDetailsItemInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getExtensionAttributes', 'getUnitPrice'])
            ->getMockForAbstractClass();
        $this->quoteDetailsItemExtension = $this->getMockBuilder(QuoteDetailsItemExtensionInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getPriceForTaxCalculation'])
            ->getMockForAbstractClass();
        $this->mockItem->expects($this->any())->method('getExtensionAttributes')
            ->willReturn($this->quoteDetailsItemExtension);

        $this->appliedTaxDataObjectFactory = $this->createPartialMock(
            AppliedTaxInterfaceFactory::class,
            ['create']
        );

        $this->appliedTaxRateDataObjectFactory = $this->createPartialMock(
            AppliedTaxRateInterfaceFactory::class,
            ['create']
        );
        $this->appliedTaxRate = new AppliedTaxRate(
            $this->mockContext,
            $this->mockRegistry,
            $this->mockExtensionAttributesFactory,
            $this->mockCustomAttributeFactory
        );
        $this->appliedTaxRateDataObjectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->appliedTaxRate);
        $this->mockAppliedTax = $this->getMockBuilder(AppliedTaxInterface::class)
            ->getMock();

        $this->mockAppliedTax->expects($this->any())->method('getTaxRateKey')->willReturn('taxKey');
        $this->addressRateRequest = new DataObject();
        $this->mockScopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getValue'])
            ->getMockForAbstractClass();
        $this->mockScopeConfig->expects($this->any())
            ->method('getValue')
            ->with('tax/calculation/round_shipping_incl_tax', ScopeInterface::SCOPE_STORE, self::STORE_ID)
            ->willReturn(true);
    }

    /**
     * Test if row_total_incl_tax is rounded correctly for shipping price
     * in case the shipping prices are entered without tax and rounding is enabled
     * 
     * @access public
     * @param array $config configuration
     * @param array $expected expected values
     * @dataProvider configDataProvider
     */
    public function testShippingRounding($config, $expected)
    {
        $this->initCalculator();
        $this->calculator->expects($this->any())
            ->method('roundAmount')
            ->willReturnCallback(
                function ($amount) {
                    return round((float) $amount, 2);
                }
            );
        $this->configData = $config;
        $this->initMocks(false);

        $this->assertSame(
            $this->taxDetailsItem,
            $this->calculator->calculate($this->mockItem, 1, true)
        );

        $this->assertEqualsWithDelta($expected['row_total_incl_tax'], $this->taxDetailsItem->getRowTotalInclTax(), 0.0001);
    }

    /**
     * Init calculator object
     * 
     * @access private
     */
    private function initCalculator()
    {
        $taxClassService = $this->getMockForAbstractClass(TaxClassManagementInterface::class);
        $this->calculator = $this->getMockBuilder(CreatorTotalBaseCalculator::class)
            ->onlyMethods(['roundAmount'])
            ->setConstructorArgs(
                [
                    'taxClassService' => $taxClassService,
                    'taxDetailsItemDataObjectFactory' => $this->taxItemDetailsDataObjectFactory,
                    'appliedTaxDataObjectFactory' => $this->appliedTaxDataObjectFactory,
                    'appliedTaxRateDataObjectFactory' => $this->appliedTaxRateDataObjectFactory,
                    'calculationTool' => $this->mockCalculationTool,
                    'config' => $this->mockConfig,
                    'scopeConfig' => $this->mockScopeConfig,
                    'storeId' => self::STORE_ID,
                    'addressRateRequest' => $this->addressRateRequest
                ]
            )
            ->getMock();
    }

    /**
     * init mock Items
     *
     * @param bool $isTaxIncluded
     */
    protected function initMockItem($isTaxIncluded)
    {
        $this->mockReturnValues(
            $this->mockItem,
            [
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'getDiscountAmount',
                    self::MOCK_VALUE => 1,
                ],
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'getCode',
                    self::MOCK_VALUE => self::CODE
                ],
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'getType',
                    self::MOCK_VALUE => $this->configData['type']
                ],
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'getUnitPrice',
                    self::MOCK_VALUE => $this->configData['unit_price']
                ],
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'getIsTaxIncluded',
                    self::MOCK_VALUE => $isTaxIncluded
                ]
            ]
        );
    }

    /**
     * init mock config
     *
     */
    protected function initMockConfig()
    {
        $this->mockReturnValues(
            $this->mockConfig,
            [
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'applyTaxAfterDiscount',
                    self::MOCK_VALUE => $this->configData['apply_tax_after_discount'],
                ]
            ]
        );
    }

    /**
     * init mock calculation model
     *
     * @param boolean $isTaxIncluded
     */
    protected function initMockCalculationTool($isTaxIncluded)
    {
        $mockValues = [
            [
                self::ONCE => false,
                self::MOCK_METHOD_NAME => 'getRate',
                self::MOCK_VALUE => $this->configData['rate']
            ],
            [
                self::ONCE => false,
                self::MOCK_METHOD_NAME => 'getAppliedRates',
                self::MOCK_VALUE => [
                    [
                        'id' => 0,
                        'percent' => $this->configData['rate'],
                        'rates' => [
                            [
                                'code' => 'rate1',
                                'title' => 'Rate1',
                                'percent' => $this->configData['rate'],
                            ],
                        ],
                    ],
                ]
            ],
        ];

        if ($isTaxIncluded) {
            $mockValues[] = [
                self::ONCE => false,
                self::MOCK_METHOD_NAME => 'getStoreRate',
                self::MOCK_VALUE => $this->configData['rate']
            ];
        }

        $this->mockReturnValues(
            $this->mockCalculationTool,
            $mockValues
        );
        $this->mockCalculationTool->expects($this->atLeastOnce())
            ->method('round')
            ->willReturnCallback(
                function ($price) {
                    return round((float) $price, 2);
                }
            );
    }

    /**
     * init mock appliedTaxDataObjectFactory
     *
     */
    protected function initMockAppliedTaxDataObjectFactory()
    {
        $this->mockReturnValues(
            $this->appliedTaxDataObjectFactory,
            [
                [
                    self::ONCE => false,
                    self::MOCK_METHOD_NAME => 'create',
                    self::MOCK_VALUE => $this->mockAppliedTax,
                ]
            ]
        );
    }

    /**
     * Configuration data for the test
     * 
     * @access public
     * @return array
     */
    public function configDataProvider()
    {
        return [
            [
                'config' => [
                    'unit_price' => 57.02,
                    'rate' => 21,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 69
                ]
            ],
            [
                'config' => [
                    'unit_price' => 57.025,
                    'rate' => 21,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 69
                ]
            ],
            [
                'config' => [
                    'unit_price' => 57.03,
                    'rate' => 21,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 69
                ]
            ],
            [
                'config' => [
                    'unit_price' => 57.85,
                    'rate' => 21,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 70
                ]
            ],
            [
                'config' => [
                    'unit_price' => 57.44,
                    'rate' => 21,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 69.5
                ]
            ],
            [
                'config' => [
                    'unit_price' => 2.845,
                    'rate' => 23,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 3.5
                ]
            ],
            [
                'config' => [
                    'unit_price' => 2.85,
                    'rate' => 23,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 3.51
                ]
            ],
            [
                'config' => [
                    'unit_price' => 2.855,
                    'rate' => 23,
                    'type' => 'shipping',
                    'apply_tax_after_discount' => false
                ],
                'expected' => [
                    'row_total_incl_tax' => 3.52
                ]
            ]
        ];
    }

    /**
     * @param MockObject $mockObject
     * @param array $mockMap
     */
    private function mockReturnValues($mockObject, $mockMap)
    {
        foreach ($mockMap as $valueMap) {
            if (isset($valueMap[self::WITH_ARGUMENT])) {
                $mockObject->expects(
                    $valueMap[self::ONCE] == true ? $this->once() : $this->atLeastOnce()
                )->method($valueMap[self::MOCK_METHOD_NAME])->with($valueMap[self::WITH_ARGUMENT])
                    ->willReturn(
                        $valueMap[self::MOCK_VALUE]
                    );
            } else {
                $mockObject->expects(
                    $valueMap[self::ONCE] == true ? $this->once() : $this->atLeastOnce()
                )->method($valueMap[self::MOCK_METHOD_NAME])->withAnyParameters()
                    ->willReturn(
                        $valueMap[self::MOCK_VALUE]
                    );
            }
        }
    }

    /**
     * initialize all mocks
     *
     * @param bool $isTaxIncluded
     */
    private function initMocks($isTaxIncluded)
    {
        $this->initMockItem($isTaxIncluded);
        $this->initMockConfig();
        $this->initMockCalculationTool($isTaxIncluded);
        $this->initMockAppliedTaxDataObjectFactory();
    }
}
