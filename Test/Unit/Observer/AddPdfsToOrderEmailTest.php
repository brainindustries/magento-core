<?php

namespace Shirtplatform\Core\Test\Unit\Observer;

use Fooman\EmailAttachments\Model\Api\AttachmentContainerInterface;
use Fooman\EmailAttachments\Model\ContentAttacher;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shirtplatform\Core\Helper\Data;
use Shirtplatform\Core\Helper\Overview;
use Shirtplatform\Core\Observer\AddPdfsToOrderEmail;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Framework\Exception\LocalizedException;

class AddPdfsToOrderEmailTest extends TestCase
{
    /**
     * @var AddPdfsToOrderEmail
     */
    private $addPdfsToOrderEmail;

    /**
     * @var MockObject
     */
    private $contentAttacherMock;

    /**
     * @var MockObject
     */
    private $dataHelperMock;

    /**
     * @var MockObject
     */
    private $directoryListMock;

    /**
     * @var MockObject
     */
    private $observerMock;

    /**
     * @var MockObject
     */
    private $orderMock;

    /**
     * @var MockObject
     */
    private $orderResourceMock;

    /**
     * @var MockObject
     */
    private $overviewHelperMock;

    /**
     * @var MockObject
     */
    private $scopeConfigMock;

    protected function setUp(): void
    {
        // Mock dependencies
        $this->contentAttacherMock = $this->getMockBuilder(ContentAttacher::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->dataHelperMock = $this->getMockBuilder(Data::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->directoryListMock = $this->getMockBuilder(DirectoryList::class)
            ->disableOriginalConstructor()
            ->getMock();


        $this->orderResourceMock = $this->getMockBuilder(OrderResource::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->overviewHelperMock = $this->getMockBuilder(Overview::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scopeConfigMock = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        // Mock order
        $this->orderMock = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->addMethods(['getShirtplatformId', 'getTrySendEmailCount', 'setTrySendEmailCount'])
            ->onlyMethods(['getIncrementId'])
            ->getMock();
        $this->orderMock->method('getShirtplatformId')->willReturn('123');
        $this->orderMock->method('getIncrementId')->willReturn('000000123');

        // Mock observe
        $this->observerMock = $this->getMockBuilder(Observer::class)
            ->disableOriginalConstructor()
            ->addMethods(['getAttachmentContainer', 'getOrder'])
            ->getMock();

        $attachmentContainer = $this->getMockBuilder(AttachmentContainerInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->observerMock->method('getAttachmentContainer')->willReturn($attachmentContainer);
        $this->observerMock->method('getOrder')->willReturn($this->orderMock);

        // Create test subject
        $this->addPdfsToOrderEmail = new AddPdfsToOrderEmail(
            $this->contentAttacherMock,
            $this->dataHelperMock,
            $this->directoryListMock,
            $this->orderResourceMock,
            $this->overviewHelperMock,
            $this->scopeConfigMock
        );
    }

    /**
     * Test if empty overview content throws exception
     */
    public function testEmptyOverviewContentThrowsException()
    {
        $this->overviewHelperMock->method('getOverviewPdf')->willReturn(false);
        $this->orderMock->method('getTrySendEmailCount')->willReturn(0);

        $this->expectException(LocalizedException::class);
        $this->expectExceptionMessage('Overview content is empty for order 000000123');

        $this->addPdfsToOrderEmail->execute($this->observerMock);
    }

    /**
     * Test if try_send_email_count is increased and saved to order
     */
    public function testTrySendEmailCountIsIncreased()
    {
        $this->overviewHelperMock->method('getOverviewPdf')->willReturn(false);
        $this->orderMock->method('getTrySendEmailCount')->willReturn(1);

        $this->orderMock->expects($this->once())
            ->method('setTrySendEmailCount')
            ->with(2);

        $this->orderResourceMock->expects($this->once())
            ->method('saveAttribute')
            ->with($this->orderMock, ['try_send_email_count']);

        $this->expectException(LocalizedException::class);
        $this->addPdfsToOrderEmail->execute($this->observerMock);
    }

    /**
     * Test if email to admin is sent when overview content is empty and 
     * only if try_send_email_count is 0
     */
    public function testEmailToAdminIsSentOnFirstTry()
    {
        $this->overviewHelperMock->method('getOverviewPdf')->willReturn(false);
        $this->orderMock->method('getTrySendEmailCount')->willReturn(0);

        $this->dataHelperMock->expects($this->once())
            ->method('sendEmailToAdmin')
            ->with(
                $this->stringContains("Could't attach overview PDF for order 000000123"),
                $this->stringContains('For more info check brainindustries_error.log file')
            );

        $this->expectException(LocalizedException::class);

        $this->addPdfsToOrderEmail->execute($this->observerMock);
    }    

    /**
     * Test if email to admin is not sent when overview content is empty and
     * AddPdfsToOrderEmail::MAX_RETRIES is reached. Also tests if no exception
     * is raised this time
     */
    public function testNoExceptionOnMaxRetries()
    {        
        $this->overviewHelperMock->method('getOverviewPdf')->willReturn(false);
        $this->orderMock->method('getTrySendEmailCount')->willReturn(AddPdfsToOrderEmail::MAX_RETRIES);

        // Verify admin email is not sent
        $this->dataHelperMock->expects($this->never())
            ->method('sendEmailToAdmin');

        // Execute and assert no exception
        try {
            $this->addPdfsToOrderEmail->execute($this->observerMock);
            $this->assertTrue(true); // Assert we reached this point without exception
        } catch (\Exception $e) {
            $this->fail('Exception should not be thrown when max retries reached');
        }
    }
}
