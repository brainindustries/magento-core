<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 22.2.2018
 * Time: 10:48
 */

namespace Shirtplatform\Core\Api;

interface ProductAttributeOptionManagementInterface
{    
    /**
     * Go over assigned product (configurable and its variants) colors and fix order items options
     * if there are any wrong assignments. This happens if the color changes in platform
     * and after import the same color has different ID
     * 
     * @param string $sku     
     * @return Boolean
     */
    public function fixColors($sku);

    /**
     * Update attribute option
     *
     * @param string $attributeCode
     * @param int $optionId
     * @param \Magento\Eav\Api\Data\AttributeOptionInterface $option
     * @return bool
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function update($attributeCode, $optionId, $option);
}
