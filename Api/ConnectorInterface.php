<?php

namespace Shirtplatform\Core\Api;

/**
 * Interface ConnectorInterface
 */
interface ConnectorInterface
{

    /**
     * @param mixed $data
     * @return void
     */
    public function connect($data);

    /**
     * @param int $websiteId
     * @return void
     */
    public function disconnect($websiteId);

}
