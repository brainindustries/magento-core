<?php

namespace Shirtplatform\Core\Api;

interface ProductAttributeManagementInterface {
    /**
     * Update price for product
     * 
     * @access public
     * @param string $sku
     * @param float $price
     * @param mixed $prices
     * @return void
     */
    public function updatePrice($sku, $price, $prices = []);
}