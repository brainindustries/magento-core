<?php

namespace Shirtplatform\Core\Api;

/**
 * Interface BeforeProductImportInterface
 * @package Shirtplatform\Core\Api
 */
interface BeforeProductImportInterface
{

    /**
     * @param string $sku
     * @return void
     */
    public function beforeImport($sku);

}
