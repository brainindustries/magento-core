<?php

namespace Shirtplatform\Core\Api;

interface AssignedViewRepositoryInterface {
    /**
     * Save Assigned view
     * @param \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView);

    /**
     * Retrieve Assigned view
     * @param string $assignedViewId
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($assignedViewId);

    /**
     * Retrieve assigned views matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Assigned view
     * @param \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView
     * @return bool true on success
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView);

    /**
     * Delete assigned view by ID
     * @param string $assignedViewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($assignedViewId);
}