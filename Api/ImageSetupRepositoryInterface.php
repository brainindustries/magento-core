<?php

namespace Shirtplatform\Core\Api;

/**
 * Interface ImageSetupRepositoryInterface
 * @package Shirtplatform\Core\Api
 */
interface ImageSetupRepositoryInterface
{

    /**
     * @param int $storeViewId
     * @return \Shirtplatform\Core\Model\ImageSetup\ImageSetupResult
     */
    public function getSetup($storeViewId);

}
