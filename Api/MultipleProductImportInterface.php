<?php

namespace Shirtplatform\Core\Api;

/**
 * Interface MultipleProductImportInterface
 * @package Shirtplatform\Core\Api
 */
interface MultipleProductImportInterface
{

    /**
     * Create multiple products
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface[] $products
     * @return \Shirtplatform\Core\Service\V1\MultipleProductImportResponse
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function multipleImport(array $products);

}
