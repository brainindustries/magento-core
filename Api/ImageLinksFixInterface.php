<?php

namespace Shirtplatform\Core\Api;

/**
 * Interface ImageLinksFixInterface
 * @package Shirtplatform\Core\Api
 */
interface ImageLinksFixInterface
{

    /**
     * @param string $sku
     * @return void
     */
    public function processFix($sku);

}
