<?php

namespace Shirtplatform\Core\Api;

interface ProductStoreViewRepositoryInterface
{
    /**
     * Save product attributes in store view
     * 
     * @access public
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function saveInStoreView($product);
}