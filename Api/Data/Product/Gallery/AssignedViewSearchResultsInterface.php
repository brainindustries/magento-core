<?php

namespace Shirtplatform\Core\Api\Data\Product\Gallery;

interface AssignedViewSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface {

    /**
     * Get assigned view list.
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface[]
     */
    public function getItems();

    /**
     * Set assigned view list.
     * @param \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
