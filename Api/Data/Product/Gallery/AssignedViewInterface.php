<?php

namespace Shirtplatform\Core\Api\Data\Product\Gallery;

interface AssignedViewInterface {

    const ID = 'id';
    const IMAGE_ID = 'image_id';
    const ASSIGNED_VIEW_ID = 'assigned_view_id';
    const VIEW_TYPE = 'view_type';
    
    /**
     * Get id
     * @return int
     */
    public function getId();

    /**
     * Set id
     * @param int $id
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setId($id);
    
    /**
     * Get id
     * @return int
     */
    public function getImageId();

    /**
     * Set id
     * @param int $imageId
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setImageId($imageId);
    /**
     * Get id
     * @return int
     */
    public function getAssignedViewId();

    /**
     * Set id
     * @param int $assignedViewId
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setAssignedViewId($assignedViewId);

    /**
     * Get view type
     * @return string
     */
    public function getViewType();

    /**
     * Set view type
     * @param string $viewType
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setViewType($viewType);

}
