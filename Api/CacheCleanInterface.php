<?php

namespace Shirtplatform\Core\Api;

interface CacheCleanInterface
{    
    /**
     * Clean full_page API
     *
     * @access public
     * @return void
     */
    public function fullPage();
}