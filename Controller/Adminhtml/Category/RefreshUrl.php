<?php

namespace Shirtplatform\Core\Controller\Adminhtml\Category;

class RefreshUrl extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Catalog\Api\CategoryListInterface
     */
    private $_categoryList;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator
     */
    private $_categoryUrlPathGenerator;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator
     */
    private $_categoryUrlRewriteGenerator;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\UrlRewriteBunchReplacer
     */
    private $_urlRewriteBunchReplacer;

    /**
     * @var \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler
     */
    private $_urlRewriteHandler;

    /**
     * 
     * @param \Magento\Catalog\Api\CategoryListInterface $categoryList
     * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator
     * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\CatalogUrlRewrite\Model\UrlRewriteBunchReplacer $urlRewriteBunchReplacer
     * @param \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler $urlRewriteHandler
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(\Magento\Catalog\Api\CategoryListInterface $categoryList,
                                \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator,
                                \Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\CatalogUrlRewrite\Model\UrlRewriteBunchReplacer $urlRewriteBunchReplacer,
                                \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler $urlRewriteHandler,
                                \Magento\Backend\App\Action\Context $context) {
        parent::__construct($context);
        $this->_categoryList = $categoryList;
        $this->_categoryUrlPathGenerator = $categoryUrlPathGenerator;
        $this->_categoryUrlRewriteGenerator = $categoryUrlRewriteGenerator;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_storeManager = $storeManager;
        $this->_urlRewriteBunchReplacer = $urlRewriteBunchReplacer;
        $this->_urlRewriteHandler = $urlRewriteHandler;
    }

    /**
     * Refresh URLs for categories
     * 
     * @access public
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute() {        
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        $categories = $this->_categoryList->getList($searchCriteria)->getItems();
        $storeId = $this->_request->getParam('store_id', 0);

        if ($storeId) {
            $this->_refreshUrls($categories, $storeId);
        }
        else {
            foreach ($this->_storeManager->getStores() as $store) {
                $this->_refreshUrls($categories, $store->getId());
            }
        }
        
        $this->messageManager->addSuccessMessage(__('Category URLs have been refreshed'));
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('catalog/category/index', ['store' => $storeId]);
        return $resultRedirect;
    }

    /**
     * Refresh URLs for categories in given store
     * 
     * @access private
     * @param array $categories
     * @param int $storeId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _refreshUrls($categories,
                                  $storeId) {
        foreach ($categories as $_category) {
            $_category->setStoreId($storeId);

            $resultUrlKey = $this->_categoryUrlPathGenerator->getUrlKey($_category);
            if (empty($resultUrlKey)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid URL key'));
            }

            //needed to make the change
            $_category->setOrigData('url_key', null);
            $urlPath = $this->_categoryUrlPathGenerator->getUrlPath($_category);
            $_category->setUrlKey($resultUrlKey)
                    ->setUrlPath($urlPath);

            if ($urlPath) {
                $_category->getResource()->saveAttribute($_category, 'url_path');
                $categoryUrlRewriteResult = $this->_categoryUrlRewriteGenerator->generate($_category);
                $this->_urlRewriteBunchReplacer->doBunchReplace($categoryUrlRewriteResult);
                $productUrlRewriteResult = $this->_urlRewriteHandler->generateProductUrlRewrites($_category);
                $this->_urlRewriteBunchReplacer->doBunchReplace($productUrlRewriteResult);
            }
        }
    }

}
