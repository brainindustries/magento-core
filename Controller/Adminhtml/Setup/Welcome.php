<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 22.11.2017
 * Time: 14:00
 */

namespace Shirtplatform\Core\Controller\Adminhtml\Setup;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Psr\Log\LoggerInterface;

class Welcome extends \Magento\Backend\App\Action
{
	/**
	 * @var LoggerInterface $logger
	 */
	private $logger;
	/**
	 * @var Validator $formKeyValidator
	 */
	private $formKeyValidator;
	/**
	 * @var JsonFactory $resultJsonFactory
	 */
	private $resultJsonFactory;

	/**
	 * Content constructor.
	 *
	 * @param Context $context
	 * @param LoggerInterface $logger
	 * @param Validator $formKeyValidator
	 * @param JsonFactory $resultJsonFactory
	 */
	public function __construct(
		Context $context,
		LoggerInterface $logger,
		JsonFactory $resultJsonFactory
	) {
		$this->logger            = $logger;
		$this->resultJsonFactory = $resultJsonFactory;
		parent::__construct($context);
	}

	/**
	 *
	 */
	public function execute()
	{
		$resultJson = $this->resultJsonFactory->create();

		try {
			$layout = $this->_view->getLayout();
			$block = $layout->createBlock(\Magento\Backend\Block\Template::class)->setTemplate('Shirtplatform_Core::setup/welcome.phtml');

			$response = [
				'title' => __('Welcome to Setup'),
				'content' => $block->toHtml(),
			];
		} catch (\Exception $exception) {
			$resultJson->setStatusHeader(
				\Zend\Http\Response::STATUS_CODE_400,
				\Zend\Http\AbstractMessage::VERSION_11,
				'Bad Request'
			);
			/** @var array $response */
			$response = [
				'message' => __('An error occurred')
			];
			$this->logger->critical($exception);
		}

		return $resultJson->setData($response);
	}

}