<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 15.10.2017
 * Time: 21:10
 */

namespace Shirtplatform\Core\Controller\Adminhtml\Setup;

class Tab extends \Magento\Backend\App\Action
{
	/**
	 * @var \Magento\Framework\App\Request\Http
	 */
	protected $request;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\App\Request\Http $request
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\App\Request\Http $request
	)
	{
		parent::__construct($context);
		$this->request = $request;
	}

	/**
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$websiteId = $this->request->get('id');
		/** @var \Magento\Framework\View\Layout $layout */
		$layout = $this->_view->getLayout();

		/** @var \Shirtplatform\Core\Block\Adminhtml\Setup\Form $block */
		$block = $layout->createBlock(\Shirtplatform\Core\Block\Adminhtml\Setup\Form::class);
		$block->setWebsiteId($websiteId);
		$block->setTemplate('Shirtplatform_Core::setup/form.phtml');

		$this->getResponse()->setBody($block->toHtml());
	}

}