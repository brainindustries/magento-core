<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 15.10.2017
 * Time: 21:10
 */

namespace Shirtplatform\Core\Controller\Adminhtml\Setup;

class Callback extends \Magento\Backend\App\Action
{
	/**
	 * @var \Magento\Framework\Controller\Result\JsonFactory
	 */
	protected $resultJsonFactory;

	/**
	 * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
	 */
	protected $configInterface;

	/** @var \Magento\Framework\App\Cache\Manager */
	private $cacheManager;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
	 * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
     * @param \Magento\Framework\App\Cache\Manager $cacheManager
	 * @internal param \Magento\Framework\View\Result\PageFactory $resultJsonFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
		\Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface,
        \Magento\Framework\App\Cache\Manager $cacheManager
	)
	{
		parent::__construct($context);
		$this->resultJsonFactory = $resultJsonFactory;
		$this->configInterface = $configInterface;
		$this->cacheManager = $cacheManager;
	}

	/**
	 *
	 * @return \Magento\Framework\Controller\Result\Json
	 */
	public function execute()
	{
		$response = ['error' => 'No relevant action found.'];
		$event = $this->getRequest()->getParam('event', false);
		$data = $this->getRequest()->getParam('data', false);
		if ($event && $data && array_key_exists('website_id', $data)) {
			switch ($event) {
				case 'SUCCESS_LOGIN':
					$response = $this->_storePlatformCredentials($data);
					break;
				case 'SHOP_MAPPED':
					$response = $this->_storePlatformShop($data);
					break;
			}
		}

        $this->cacheManager->clean($this->cacheManager->getAvailableTypes());

		return $this->resultJsonFactory->create()->setData($response);
	}

	protected function _storePlatformCredentials(array $data)
	{
		if(array_key_exists('username', $data) && array_key_exists('password', $data)) {
			$this->configInterface->saveConfig(
				'shirtplatform/general/login',
				$data['username'],
				\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
				$data['website_id']
			);

			$this->configInterface->saveConfig(
				'shirtplatform/general/password',
				$data['password'],
				\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
				$data['website_id']
			);
			return ['success' => 'Platform credentials successfuly saved.'];
		}
		return ['error' => 'Platform credentials were not saved.'];
	}

	protected function _storePlatformShop(array $data)
	{
		if(array_key_exists('shopId', $data)) {
			$this->configInterface->saveConfig(
				'shirtplatform/general/store',
				$data['shopId'],
				\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
				$data['website_id']
			);

            foreach($data['viewsMap'] as $storeUnique => $mapSettings)
            {
                $this->configInterface->saveConfig(
                    'shirtplatform/general/country',
                    $mapSettings['countryId'],
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $this->extractStoreIdFromUnique($storeUnique)
                );

                $this->configInterface->saveConfig(
                    'shirtplatform/general/language',
                    $mapSettings['langId'],
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $this->extractStoreIdFromUnique($storeUnique)
                );
            }
			return ['success' => 'Platform shop successfuly saved.'];
		}
		return ['error' => 'Platform shop was not saved.'];
	}

    /**
     * @param string $uniq
     * @return int mixed
     */
	private function extractStoreIdFromUnique($uniq)
    {
        $parts = explode('_', $uniq);
        return (int) $parts[count($parts) - 1];
    }

}
