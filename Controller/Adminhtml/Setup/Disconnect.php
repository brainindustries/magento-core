<?php

namespace Shirtplatform\Core\Controller\Adminhtml\Setup;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Shirtplatform\Core\Api\ConnectorInterface;

class Disconnect extends \Magento\Backend\App\Action
{
    /**
     * @var ConnectorInterface
     */
    protected $_connector;

    /**
     * @var RedirectInterface
     */
    protected $_redirect;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ResultFactory
     */
    protected $_resultFactory;

    /**
     * @param Context $context
     * @param ConnectorInterface $connector
     * @param RedirectInterface $redirect
     * @param ResultFactory $resultFactory
     */
	public function __construct(
        Context $context,
        ConnectorInterface $connector,
        RedirectInterface $redirect,
        ResultFactory $resultFactory
    ){
        $this->_connector = $connector;
        $this->_redirect = $redirect;
        $this->_request = $context->getRequest();
        $this->_resultFactory = $resultFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        $websiteId = $this->_request->getParam('website_id');
        $this->_connector->disconnect($websiteId);

        return $resultRedirect;
	}
}
