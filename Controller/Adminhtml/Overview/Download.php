<?php

namespace Shirtplatform\Core\Controller\Adminhtml\Overview;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

class Download extends Action implements HttpPostActionInterface
{        

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var \Shirtplatform\Core\Helper\Overview
     */
    private $_overviewHelper;

    /**
     * @var JsonFactory
     */
    private $_resultJsonFactory;

    /**     
     * @param Context $context     
     * @param OrderRepositoryInterface $orderRepository
     * @param \Shirtplatform\Core\Helper\Overview $overviewHelper
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,                
        OrderRepositoryInterface $orderRepository,
        \Shirtplatform\Core\Helper\Overview $overviewHelper,
        JsonFactory $jsonFactory
    ) {        
        parent::__construct($context);        
        $this->_orderRepository = $orderRepository;
        $this->_overviewHelper = $overviewHelper;
        $this->_resultJsonFactory = $jsonFactory;
    }

    /**
     * Download overview for order and save it to disk
     * @access public
     */
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
        $orderId = $this->getRequest()->getPost('order_id');        

        if (empty($orderId)) {
            $resultJson->setData([
                'success' => false,
                'message' => 'Missing order ID'
            ]);
            return $resultJson;
        }

        try {
            $order = $this->_orderRepository->get($orderId);

            if ($order->getShirtplatformId()) {
                $result = $this->_overviewHelper->downloadOverview($order);
                
                if ($result === false) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Overview for order ' . $order->getIncrementId() . ' could not be downloaded'));
                }
            }

        } catch (\Exception $ex) {
            $resultJson->setData([
                'success' => false,
                'message' => $ex->getMessage()
            ]);            
            return $resultJson;
        }
    
        $resultJson->setData([
            'success' => true,
            'message' => 'Overview has been downloaded'
        ]);
        return $resultJson;
    }
}
