<?php
declare(strict_types=1);

namespace Shirtplatform\Core\Controller\Adminhtml\Overview;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Shirtplatform\Core\Helper\Overview as OverviewHelper;

class SaveToDisk extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Magento_Sales::actions_view';

    /**
     * @var FileFactory
     */
    private $fileFactory;    

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OverviewHelper
     */
    private $overviewHelper;

    /**
     * @param Action\Context $context
     * @param FileFactory $fileFactory     
     * @param OrderRepositoryInterface $orderRepository
     * @param OverviewHelper $overviewHelper
     */
    public function __construct(
        Action\Context $context,
        FileFactory $fileFactory,        
        OrderRepositoryInterface $orderRepository,
        OverviewHelper $overviewHelper
    ) {
        parent::__construct($context);
        $this->fileFactory = $fileFactory;        
        $this->orderRepository = $orderRepository;
        $this->overviewHelper = $overviewHelper;
    }

    /**
     * Save overview PDF to disk
     *
     * @access public
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $orderId = $this->getRequest()->getParam('order_id');

        if (!$orderId) {
            $resultRedirect->setPath('sales/order/index');
            $this->messageManager->addErrorMessage(__('Missing order ID'));
            return $resultRedirect;
        }                

        try {
            $order = $this->orderRepository->get($orderId);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('Order not found'));
            return $resultRedirect;
        }
        
        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);        
        if (empty($order->getShirtplatformId())) {
            $this->messageManager->addErrorMessage(__('Order is not a Shirtplatform order'));
            return $resultRedirect;
        }

        $fullPath = $this->overviewHelper->getFullOverviewPath($order);        

        try {            
            $fileName = $this->overviewHelper->getOverviewFilename($order);
            if (file_exists($fullPath)) {
                return $this->fileFactory->create(
                    $fileName,
                    ['type' => 'filename', 'value' => $fullPath],
                    DirectoryList::VAR_DIR
                );
            }

            $msg = '%1 does not exist. Please download it from Shirtplatform and then try again';
            $this->messageManager->addErrorMessage(__($msg, $fileName));
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        return $resultRedirect;
    }
}