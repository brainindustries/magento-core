<?php

namespace Shirtplatform\Core\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory;
use Shirtplatform\Core\Helper\PrintTechnologyHelper;

/**
 * Class PrintTechnologyCount
 * @package Shirtplatform\Core\Ui\Component\Listing\Column
 */
class PrintTechnologyCount extends Column
{

    /** @var ShirtplatformPrintTechnologyFactory */
    private $printTechnologyFactory;

    /** @var PrintTechnologyHelper */
    private $printTechnologyHelper;

    /**
     * PrintTechnologyCount constructor.
     * @param ShirtplatformPrintTechnologyFactory $printTechnologFactory
     * @param PrintTechnologyHelper $printTechnologyHelper
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ShirtplatformPrintTechnologyFactory $printTechnologyFactory,
        PrintTechnologyHelper $printTechnologyHelper,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->printTechnologyFactory = $printTechnologyFactory;
        $this->printTechnologyHelper = $printTechnologyHelper;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {        
        $orderIds = $printTechnologiesPerOrder = [];
        foreach ($dataSource['data']['items'] as $item) {
            $orderIds[] = $item['entity_id'];
        }

        $printTechCollection = $this->printTechnologyFactory->create()->getCollection()
            ->addFieldToFilter('order_id', ['in' => $orderIds])
            ->getItems();

        foreach ($printTechCollection as $printTechItem) {
            $printTechnologiesPerOrder[$printTechItem->getOrderId()][$printTechItem->getTechnologyId()] = $printTechItem;            
        }        

        if(isset($dataSource['data']['items']))
        {
            $title = $this->getData('name');
            foreach($dataSource['data']['items'] as &$item)
            {
                if (isset($printTechnologiesPerOrder[$item['entity_id']])) {
                    $item[$title] = $this->printTechnologyHelper->getCounts($title, $printTechnologiesPerOrder[$item['entity_id']]);
                }                
            }
        }        

        return $dataSource;
    }

}
