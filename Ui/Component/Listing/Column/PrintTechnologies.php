<?php

namespace Shirtplatform\Core\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class PrintTechnologies
 * @package Shirtplatform\Core\Ui\Component\Listing\Column
 */
class PrintTechnologies extends Column
{

    /** @var \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory */
    private $printTechnologyFactory;

    /**
     * PrintTechnologies constructor.
     * @param \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory $printTechnologyFactory
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory $printTechnologyFactory,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->printTechnologyFactory = $printTechnologyFactory;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items']))
        {
            $orderIds = $printTechnologiesPerOrder = [];
            foreach ($dataSource['data']['items'] as $item) {
                $orderIds[] = $item['entity_id'];
            }
            
            $printTechCollection = $this->printTechnologyFactory->create()->getCollection()
                    ->addFieldToFilter('order_id', ['in' => $orderIds]);                    

            foreach ($printTechCollection as $printTechItem) {
                $printTechnologiesPerOrder[$printTechItem->getOrderId()][$printTechItem->getTechnologyId()] = [
                    'name' => $printTechItem->getName(),
                    'count' => $printTechItem->getCount()
                ];
            }

            foreach($dataSource['data']['items'] as &$item)
            {                
                $list = [];

                if (isset($printTechnologiesPerOrder[$item['entity_id']])) {
                    foreach($printTechnologiesPerOrder[$item['entity_id']] as $printTechnology)
                    {
                        $list[] = $printTechnology['name'] . ' - ' . $printTechnology['count'] . 'x';
                    }
                    $item[$this->getData('name')] = implode('<br/>', $list);
                }
            }
        }
        return $dataSource;
    }

}
