<?php

namespace Shirtplatform\Core\Exception;

use Magento\Framework\Exception\LocalizedException;

class HttpNotSuccessfulException extends LocalizedException
{
}
