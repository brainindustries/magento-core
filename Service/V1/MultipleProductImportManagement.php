<?php

namespace Shirtplatform\Core\Service\V1;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Shirtplatform\Core\Api\MultipleProductImportInterface;
use shirtplatform\rest\REST;

/**
 * Class MultipleProductImportManagement
 * @package Shirtplatform\Core\Service\V1
 */
class MultipleProductImportManagement implements MultipleProductImportInterface
{

    /** @var ProductRepositoryInterface */
    private $productRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface[] $products
     * @return MultipleProductImportResponse
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function multipleImport(array $products)
    {
        $this->processImageLinks($products);

        $createdIds = [];
        foreach($products as $product)
        {
            $create = $this->productRepository->save($product);
            $createdIds[] = $create->getId();
        }

        $response = new MultipleProductImportResponse();
        $response->setProductIds($createdIds);
        return $response;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface[] $products
     */
    private function processImageLinks(array $products)
    {
        $links = [];
        foreach($products as $product)
        {
            if($product->getMediaGalleryEntries() == null)
            {
                continue;
            }

            foreach($product->getMediaGalleryEntries() as $galleryEntry)
            {
                $links[$galleryEntry->getContent()->getBase64EncodedData()] = $galleryEntry->getContent()->getBase64EncodedData();
            }
        }

        $links = $this->downloadImages($links);

        foreach($products as $product)
        {
            $entries = [];
            foreach($product->getMediaGalleryEntries() as $galleryEntry)
            {
                $galleryEntry->getContent()->setBase64EncodedData(
                    base64_encode($links[$galleryEntry->getContent()->getBase64EncodedData()])
                );
                $entries[] = $galleryEntry;
            }
            $product->setMediaGalleryEntries($entries);
        }
    }

    /**
     * @param string[]|array $links
     * @return string[]|array
     */
    private function downloadImages(array $links)
    {
        REST::getInstance();
        $client = new \GuzzleHttp\Client([
            'timeout'  => 10
        ]);

        $requestArr = [];
        foreach($links as $index => $link)
        {
            $requestArr[$index] = $client->getAsync($link);
        }
        $result = \GuzzleHttp\Promise\unwrap($requestArr);

        $resultArray = [];
        foreach($result as $index => $response)
        {
            $resultArray[$index] = $response->getBody()->getContents();
        }
        return $resultArray;
    }

}
