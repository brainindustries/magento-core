<?php

namespace Shirtplatform\Core\Service\V1;

/**
 * Class MultipleProductImportResponse
 * @package Shirtplatform\Core\Service\V1
 */
class MultipleProductImportResponse
{

    /** @var array|int[] */
    private $productIds;

    /**
     * @return array|int[]
     */
    public function getProductIds()
    {
        return $this->productIds;
    }

    /**
     * @param array|int[] $productIds
     * @return void
     */
    public function setProductIds(array $productIds)
    {
        $this->productIds = $productIds;
    }

}
