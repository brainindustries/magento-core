<?php

namespace Shirtplatform\Core\Observer\Mollie;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Framework\ObjectManagerInterface;

class DisableOrderConfirmation implements ObserverInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $_globalConfig;

    /**
     * @var ModuleManager
     */
    private $_moduleManager;

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;    

    /**
     * @param ScopeConfigInterface $globalConfig
     * @param ModuleManager $moduleManager
     * @param ObjectManagerInterface $objectManager     
     */
    public function __construct(
        ScopeConfigInterface $globalConfig,
        ModuleManager $moduleManager,
        ObjectManagerInterface $objectManager              
    ) {
        $this->_globalConfig = $globalConfig;
        $this->_moduleManager = $moduleManager;
        $this->_objectManager = $objectManager;        
    }

    /**
     * Disable sending of order confirmation email if Mollie plugin is enabled.
     * The confirmation email will be handled asyncronously. The reason is that
     * sometimes downloading of the overview files takes a long time and 
     * customers are redirected to the cart page and confused whether 
     * the checkout process was successful -> which it was.
     * 
     * @access public
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {        
        if (!$this->_moduleManager->isEnabled('Mollie_Payment')) {
            return;
        }

        $order = $observer->getOrder();

        if (
            !$order->getEmailSent() &&
            $this->_globalConfig->getValue('sales_email/general/async_sending') &&            
            class_exists('Mollie\Payment\Service\Order\SendOrderEmails')
        ) {            
            $sendOrderEmails = $this->_objectManager->get(\Mollie\Payment\Service\Order\SendOrderEmails::class);
            $sendOrderEmails->disableOrderConfirmationSending();            
        }
    }
}
