<?php

namespace Shirtplatform\Core\Observer\Mollie;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Spi\OrderResourceInterface;

class SendOrderConfirmation implements ObserverInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $_globalConfig;    

    /**
     * @var OrderResourceInterface
     */
    private $_orderResource;    

    /**
     * @param ScopeConfigInterface $globalConfig     
     * @param OrderResourceInterface $orderResource     
     */
    public function __construct(
        ScopeConfigInterface $globalConfig,        
        OrderResourceInterface $orderResource        
    ) {
        $this->_globalConfig = $globalConfig;        
        $this->_orderResource = $orderResource;        
    }

    /**
     * Set send_email order flag for mollie orders. In mollie_process_transaction_start event
     * the order confirmation email was disabled, so it needs to be handled here, so that
     * customers get the email.
     * 
     * @access public
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {                
        $order = $observer->getOrder();        
        $paymentStatus = $order->getPayment()->getAdditionalInformation('payment_status');        

        if (!$order->getEmailSent() && $this->_globalConfig->getValue('sales_email/general/async_sending') & in_array($paymentStatus, ['paid', 'authorized'])) {            
            $order->setSendEmail(true);            
            $this->_orderResource->saveAttribute($order, 'send_email');
        }        
    }

}