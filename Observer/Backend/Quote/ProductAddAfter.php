<?php

namespace Shirtplatform\Core\Observer\Backend\Quote;

use Magento\Framework\Event\ObserverInterface;

class ProductAddAfter implements ObserverInterface {
    /**
     * Set shirtplatform_product_type for quote item
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $items = $observer->getItems();
        
        foreach ($items as $_item) {
            $product = $_item->getProduct();
            
            if (!$_item->getParentItem()) {
                $_item->setShirtplatformProductType($product->getShirtplatformProductType());                
            }
        }
    }
}