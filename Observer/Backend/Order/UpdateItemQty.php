<?php

namespace Shirtplatform\Core\Observer\Backend\Order;

use Magento\Framework\Event\ObserverInterface;
use shirtplatform\entity\order\DesignedOrderedProduct;

class UpdateItemQty implements ObserverInterface {
    
    private $_coreHelper;
    
    /**     
     * @param \Shirtplatform\Core\Helper\Data $coreHelper
     */
    public function __construct(\Shirtplatform\Core\Helper\Data $coreHelper) {
        $this->_coreHelper = $coreHelper;
    }

    /**
     * Update product quantity in platform if it's changed during order edit
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $request = $observer->getRequestModel();
        $adminOrder = $observer->getOrderCreateModel();        
        
        if ($request->getPost('update_items')) {
            $items = $request->getPost('item', []);
            $quote = $adminOrder->getQuote();
                 
            //get platform item IDs
            foreach ($items as $itemId => $info) {
                $item = $quote->getItemById($itemId);
                if (!$item) {
                    continue;
                }
                
                $origData = $item->getOrigData();                
                if ($item->getShirtplatformId() and $item->getShirtplatformOrigOrderId() and $origData['qty'] != $item->getQty()) {
                     $this->_coreHelper->shirtplatformAuth($item->getStoreId());
                     $platformProduct = DesignedOrderedProduct::find($item->getShirtplatformId(), $item->getShirtplatformOrigOrderId());                                          
                     
                     if ($platformProduct->id) {
                         $platformProduct->amount = $item->getQty();
                         $platformProduct->__update();
                     }                     
                }
            }
                        
        }
    }

}
