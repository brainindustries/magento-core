<?php

namespace Shirtplatform\Core\Observer\Backend\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;

class ResetTrySendEmailCount implements ObserverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var OrderResource
     */
    private $_orderResource;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderResource $orderResource
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderResource $orderResource
    ) {
        $this->_orderRepository = $orderRepository;  
        $this->_orderResource = $orderResource;
    }

    /**
     * Reset try_send_email_count
     * 
     * @access public
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getRequest();
        $id = $request->getParam('order_id');

        try {
            $order = $this->_orderRepository->get($id);
            $order->setTrySendEmailCount(0);
            $this->_orderResource->saveAttribute($order, 'try_send_email_count');            
        } catch (NoSuchEntityException $e) {
            return;
        }
    }
}