<?php

namespace Shirtplatform\Core\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;

class ConvertOrderToQuote implements ObserverInterface {

    /**
     * @var \Shirtplatform\Core\Helper\Order
     */
    private $_helper;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * 
     * @param \Shirtplatform\Core\Helper\Order $helper
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(\Shirtplatform\Core\Helper\Order $helper,
                                \Magento\Framework\App\RequestInterface $request) {
        $this->_helper = $helper;
        $this->_request = $request;
    }

    /**
     * Create platform order duplicate and set quote_items attributes
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $actionName = $this->_request->getFullActionName();        
        $order = $observer->getOrder();
        $quote = $observer->getQuote();            

        if ($order->getShirtplatformId() and in_array($actionName, ['sales_order_create_reorder', 'sales_order_edit_start', 'shirtplatform_rma_order_exchange_start'])) {            
            $this->_helper->initAdminShirtplatformOrder($order, $quote);
        }
    }

}
