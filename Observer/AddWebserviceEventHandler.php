<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;
use shirtplatform\constants\WsConstants;

class AddWebserviceEventHandler implements ObserverInterface {

    /**
     * @var \Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger
     */
    private $_eventHandler;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger $databaseLogger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger $databaseLogger,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_eventHandler = $databaseLogger;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Add event handler to webservice calls that will log into shirtplatform_webservice_log table
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $logApiCalls = $this->_scopeConfig->getValue('shirtplatform/webservices/log_api_calls', ScopeInterface::SCOPE_STORE);
        
        if ($logApiCalls) {
            WsConstants::$eventHandler = $this->_eventHandler;
        }
    }

}
