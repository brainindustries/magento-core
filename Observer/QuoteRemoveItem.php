<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use shirtplatform\entity\order\DesignedOrderedProduct;

class QuoteRemoveItem implements ObserverInterface {    

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $_eventManager;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_coreHelper;

    /**
     * @var \Shirtplatform\Creator\Helper\Data
     */
    private $_helper;

    /**
     * 
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Shirtplatform\Creator\Helper\Data $helper
     */
    public function __construct(\Magento\Framework\Event\ManagerInterface $eventManager,
                                \Shirtplatform\Core\Helper\Data $coreHelper,
                                \Shirtplatform\Creator\Helper\Data $helper) {        
        $this->_eventManager = $eventManager;
        $this->_coreHelper = $coreHelper;
        $this->_helper = $helper;
    }

    /**
     * Remove products from platform order and dispatch event
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quoteItem = $observer->getQuoteItem();

        if ($quoteItem->getShirtplatformId()) {
            $this->_coreHelper->shirtplatformAuth($quoteItem->getStoreId());
            $shirtplatformProduct = DesignedOrderedProduct::find($quoteItem->getShirtplatformId(), $quoteItem->getShirtplatformOrigOrderId());

            if ($shirtplatformProduct) {
                $shirtplatformProduct->__delete();

                $this->_helper->logMessage('deleted platform product ' . $shirtplatformProduct->id . ' in QuoteRemoveItem observer');                    
                $msg = "\n";
                foreach (debug_backtrace() as $l) {
                    if (isset($l['file'])) {
                        $msg .= $l['file'];
                    }
                    if (isset($l['line'])) {
                        $msg .= ' ('. $l['line'] .')';
                    }
                    if (isset($l['class'])) {
                        $msg .= '   '. $l['class'];
                    }
                    if (isset($l['function'])) {
                        $msg .= '   '. $l['function'] .'()';
                    }
                    $msg .= "\n";
                }
                
                $this->_helper->logMessage($msg);
            }
            
            $this->_eventManager->dispatch('shirtplatform_quote_item_remove_after', ['quote_item' => $quoteItem]);
        }
    }

}
