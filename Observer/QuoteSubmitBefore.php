<?php
namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\ObserverInterface;

class QuoteSubmitBefore implements ObserverInterface {
    /**
     * Set shirtplatform_id and shirtplatform_product_type for order items
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {                
        $order = $observer->getOrder();
        $quote = $observer->getQuote();
        $quoteItems = $quote->getItemsCollection();                
        
        foreach ($order->getAllItems() as $orderItem) {
            $_quoteItem = $quoteItems->getItemById($orderItem->getQuoteItemId());            
            $orderItem->setShirtplatformId($_quoteItem->getShirtplatformId());
            $orderItem->setShirtplatformUuid($_quoteItem->getShirtplatformUuid());
            $orderItem->setShirtplatformProductType($_quoteItem->getShirtplatformProductType());                                    
        }
    }
}