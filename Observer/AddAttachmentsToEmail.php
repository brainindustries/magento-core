<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Fooman\EmailAttachments\Model\ContentAttacher;

class AddAttachmentsToEmail implements ObserverInterface
{
    const MIME_TYPES = [
        'pdf' => 'application/pdf',
        'csv' => 'text/csv',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'svg' => 'image/svg+xml',
        'cdr' => 'image/vnd.coreldraw',
    ];

    /**
     * @var ContentAttacher
     */
    private $_contentAttacher;

    /**
     * @var string
     */
    private $_varCode;

    /**
     * @param ContentAttacher $contentAttacher
     */
    public function __construct(
        ContentAttacher $contentAttacher,
        string $varCode = 'shirtplatform_default_email'
    ) {
        $this->_contentAttacher = $contentAttacher;
        $this->_varCode = $varCode;
    }

    /**
     * @access public
     * @param Observer $observer          
     * @return void
     */
    public function execute(Observer $observer) : void
    {
        $emailData = $observer->getData($this->_varCode);

        if (!isset($emailData['attachments']) || empty($emailData['attachments'])) {
            return;
        }

        foreach ($emailData['attachments'] as $_attachmentPath) {
            // get the file content
            $content = file_get_contents($_attachmentPath);
 
            // get the file name
            $pathSegments = explode('/', $_attachmentPath);
            $filename = array_pop($pathSegments);
 
            // get the file extension
            $extension = strtolower(substr($filename, strrpos($filename, '.') + 1));
 
            // get the mime type
            $mimeType = self::MIME_TYPES[$extension];
            
            // attach the content
            $this->_contentAttacher->addGeneric($content, $filename, $mimeType, $observer->getAttachmentContainer());
        }
    }
}