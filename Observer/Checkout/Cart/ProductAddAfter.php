<?php

namespace Shirtplatform\Core\Observer\Checkout\Cart;

use Magento\Framework\Event\ObserverInterface;

class ProductAddAfter implements ObserverInterface {
    /**
     * Set shirtplatform_product_type quote item attribute
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getQuoteItem();
        $shirtplatformProductType = $item->getProduct()->getShirtplatformProductType();
        $item->setShirtplatformProductType($shirtplatformProductType);        
    }
}