<?php

namespace Shirtplatform\Core\Observer\Checkout;

use Magento\Framework\Event\ObserverInterface;

class RestoreQuote implements ObserverInterface
{

    /**
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * @var \Shirtplatform\Core\Helper\Order
     */
    private $_orderHelper;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $_quoteRepository;    

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_customerSession;

    /**
     * 
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Shirtplatform\Core\Helper\Order $orderHelper
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Shirtplatform\Core\Helper\Order $orderHelper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,        
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_orderHelper = $orderHelper;
        $this->_quoteRepository = $quoteRepository;        
        $this->_customerSession = $customerSession;
    }

    /**
     * Create platform order duplicate when restoring quote
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $itemMapper = $this->_orderHelper->createPlatformOrderDuplicate($order);

        if (!empty($itemMapper)) {
            $oldPlatformProductIds = array_keys($itemMapper);
            $platformOrder = $this->_coreRegistry->registry('session_platform_order');

            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                if (in_array($quoteItem->getShirtplatformId(), $oldPlatformProductIds)) {
                    $newPlatformProduct = $itemMapper[$quoteItem->getShirtplatformId()];
                    $quoteItem->setShirtplatformId($newPlatformProduct->id);
                    $quoteItem->setShirtplatformUuid($newPlatformProduct->uuid);
                    $quoteItem->setShirtplatformOrigOrderId($platformOrder->id);
                }
            }

            /** 
             * needed when a customer chooses to register during checkout and redirects back from a payment gateway
             * (e.g. after unsuccessful payment). Otherwise he's unable to continue in checkout
             */
            if ($this->_customerSession->isLoggedIn() && $this->_customerSession->getCustomerId() != $quote->getCustomerId()) {
                $quote->setCustomer($this->_customerSession->getCustomerData());
                $quote->setCustomerIsGuest(false);
                $quote->getBillingAddress()->setCustomerId($quote->getCustomerId());

                if (!$quote->getIsVirtual()) {
                    $quote->getShippingAddress()->setCustomerId($quote->getCustomerId());
                }
            }

            $this->_quoteRepository->save($quote);
        } else {
            $msg = 'Error in restoreQuote for order with increment_id ' . $order->getIncrementId() .
                '. Platform order duplicate not created';
            $this->_orderHelper->logError($msg);
        }
    }
}
