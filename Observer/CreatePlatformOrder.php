<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CreatePlatformOrder
 * @package Shirtplatform\Core\Observer 
 */
class CreatePlatformOrder implements ObserverInterface {    

    /**
     * @var \Shirtplatform\Core\Helper\Order
     */
    private $_helper;

    /**
     * CreatePlatformOrder constructor.
     *      
     * @param \Shirtplatform\Core\Helper\Order $helper     
     */
    public function __construct(\Shirtplatform\Core\Helper\Order $helper) {
        $this->_helper = $helper;
    }

    /**
     *      
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        /**
         * @var \Magento\Sales\Model\Order $order
         */
        $order = $observer->getEvent()->getOrder();
        $this->_helper->createPlatformOrder($order);
    }

}
