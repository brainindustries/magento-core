<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Fooman\EmailAttachments\Model\ContentAttacher;
use Shirtplatform\Core\Helper\Data;
use Shirtplatform\Core\Helper\Overview;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;

class AddPdfsToOrderEmail implements ObserverInterface
{
    /**
     * Max number of retries when trying to resend order confirmation email with overview
     */
    const MAX_RETRIES = 20;

    /**
     * @var ContentAttacher
     */
    private $_contentAttacher;

    /**
     * @var Data
     */
    private $_dataHelper;

    /**
     * @var DirectoryList 
     */
    private $_directoryList;

    /**
     * @var OrderResource
     */
    private $_orderResource;

    /**
     * @var Overview
     */
    private $_overviewHelper;

    /**     
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;    

    /**
     * @param ContentAttacher $contentAttacher     
     * @param Data $dataHelper
     * @param DirectoryList $directoryList
     * @param OrderResource $orderResource
     * @param Overview $overviewHelper
     * @param ScopeConfigInterface $scopeConfig     
     */
    public function __construct(
        ContentAttacher $contentAttacher,
        Data $dataHelper,
        DirectoryList $directoryList,
        OrderResource $orderResource,
        Overview $overviewHelper,
        ScopeConfigInterface $scopeConfig        
    ) {
        $this->_contentAttacher = $contentAttacher;
        $this->_dataHelper = $dataHelper;
        $this->_directoryList = $directoryList;
        $this->_orderResource = $orderResource;
        $this->_overviewHelper = $overviewHelper;        
        $this->_scopeConfig = $scopeConfig;        
    }

    /**
     * Get the PDF filename from path
     * 
     * @access private
     * @param string $path file path
     * @return string
     */
    private function _getPdfFilename($path) {
        $parts = explode('/', $path);
        return $parts[count($parts) - 1];
    }

    /**
     * Attach custom PDF attachment to order email
     * 
     * @access private
     * @param string $relativePath relative path from the upload directory
     * @param Observer $observer
     */
    private function _attachCustomPdfAttachment($relativePath, $observer) {
        try {
            $uploadDir = $this->_directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR;                        
            $filename = $this->_getPdfFilename($relativePath);
            $fileContent = file_get_contents($uploadDir . $relativePath);
            $this->_contentAttacher->addPdf(
                $fileContent,
                $filename,
                $observer->getAttachmentContainer()
            );            
        } catch (\Exception $ex) {
            $this->_dataHelper->logError($ex->getMessage());
        }
    }

    
    /**
     * Add custom PDFs and overview to order email
     * 
     * @access public
     * @param Observer $observer          
     * @return [void]
     */
    public function execute(Observer $observer) {
        $order = $observer->getOrder();

        $pdfAttachment1 = $this->_scopeConfig->getValue('sales_email/order/pdf_attachment_1', ScopeInterface::SCOPE_STORE, $order->getStoreId());
        $pdfAttachment2 = $this->_scopeConfig->getValue('sales_email/order/pdf_attachment_2', ScopeInterface::SCOPE_STORE, $order->getStoreId());        

        if ($pdfAttachment1) {
            $this->_attachCustomPdfAttachment($pdfAttachment1, $observer);
        }
        if ($pdfAttachment2) {
            $this->_attachCustomPdfAttachment($pdfAttachment2, $observer);
        }

        if ($order->getShirtplatformId()) {
            try {                
                $overviewContent = $this->_overviewHelper->getOverviewPdf($order);                

                if (empty($overviewContent)) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Overview content is empty for order ' . $order->getIncrementId()));
                }

                $this->_contentAttacher->addPdf(
                    $overviewContent,
                    'OrderOverview_' . $order->getIncrementId() . '.pdf',
                    $observer->getAttachmentContainer()
                );                
            } catch (\Exception $e) {                
                $trySendEmailCount = $order->getTrySendEmailCount();
                $this->_dataHelper->logError($e->getMessage());

                if ($trySendEmailCount == 0) {
                    $body = $e->getMessage(). '. For more info check brainindustries_error.log file';
                    $this->_dataHelper->sendEmailToAdmin("Could't attach overview PDF for order " . $order->getIncrementId(), $body);                
                }

                $order->setTrySendEmailCount($trySendEmailCount + 1);                
                $this->_orderResource->saveAttribute($order, ['try_send_email_count']);                                

                //the last attempt to send email actually sends the email and sets the email_sent column in the database
                if ($order->getTrySendEmailCount() < self::MAX_RETRIES) {
                    throw $e;   
                }                
            }
        }
    }
}