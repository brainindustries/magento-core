<?php

namespace Shirtplatform\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class LayoutLogger implements ObserverInterface
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    /**
     *      
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    private $_remoteAddress;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_logger = $logger;
        $this->_remoteAddress = $remoteAddress;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Write layout XML on server
     *      
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $fullActionName = $observer->getFullActionName();
        $logEnabled = $this->_scopeConfig->getValue('shirtplatform/log/layout_xml', ScopeInterface::SCOPE_STORE);

        if ($logEnabled) {
            $allowedIp = array_filter(explode(',', $this->_scopeConfig->getValue('shirtplatform/log/allowed_ip', ScopeInterface::SCOPE_STORE) ?? ''));
            $allowedActions = array_filter(explode(',', $this->_scopeConfig->getValue('shirtplatform/log/action_names', ScopeInterface::SCOPE_STORE) ?? ''));

            foreach ($allowedIp as $key => $val) {
                $allowedIp[$key] = trim($val);
            }
            foreach ($allowedActions as $key => $val) {
                $allowedActions[$key] = trim($val);
            }

            $ip = $this->_remoteAddress->getRemoteAddress();
            if (empty($allowedIp) || in_array($ip, $allowedIp)) {
                if (empty($allowedActions) || in_array($fullActionName, $allowedActions)) {
                    $xml = $observer->getLayout()->getNode()->asNiceXml();
                    $this->_logger->info('full action name: ' . $fullActionName);
                    $this->_logger->info($xml);
                    $this->_logger->info('======================================================');                    
                }
            }
        }
    }
}
