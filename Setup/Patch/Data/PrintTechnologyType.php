<?php

namespace Shirtplatform\Core\Setup\Patch\Data;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use shirtplatform\filter\WsParameters;
use shirtplatform\entity\overview\Order as OverviewOrder;
use Shirtplatform\Core\Helper\Data as ShirtplatformHelper;

class PrintTechnologyType implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var ShirtplatformHelper
     */
    private $_shirtplatformHelper;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @access public
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ShirtplatformHelper $shirtplatformHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ShirtplatformHelper $shirtplatformHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_shirtplatformHelper = $shirtplatformHelper;
        $this->_storeManager = $storeManager;
    }

    /**
     * Set type column in shirtplatform_print_technology table
     * 
     * @access public
     * @return void
     */
    public function apply()
    {
        $connection = $this->_moduleDataSetup->getConnection();
        $connection->startSetup();

        //get magento orders for each technology
        $table = $this->_moduleDataSetup->getTable('shirtplatform_print_technology');
        $select = $connection->select()->from($table, 'name')->distinct();
        $techNames = $connection->fetchCol($select);

        foreach ($techNames as $_techName) {
            $select->reset();
            $select->from($table)
                ->where('name = ?', $_techName)
                ->order('id ' . \Zend_Db_Select::SQL_DESC)
                ->limit(1);
            $results = $connection->fetchRow($select);
            $magentoOrderIds[] = $results['order_id'];
        }

        //get magento orders
        if (isset($magentoOrderIds)) {
            $searchCriteria = $this->_searchCriteriaBuilder->addFilter('entity_id', $magentoOrderIds, 'in')->create();
            $orders = $this->_orderRepository->getList($searchCriteria)->getItems();

            foreach ($orders as $_order) {
                $platformOrderIds[] = $_order->getShirtplatformId();                
            }
        }

        //get print technology info
        if (isset($platformOrderIds)) {
            $isLoggedIn = false;

            foreach ($this->_storeManager->getStores() as $store) {
                try {
                    $this->_shirtplatformHelper->shirtplatformAuth($store->getId());
                    $isLoggedIn = true;
                    break;
                }
                catch (\Exception $e) {}
            }
            
            if ($isLoggedIn) {
                $wsParams = new WsParameters();
                $wsParams->addExpressionContain('id', $platformOrderIds);
                $platformOrders = OverviewOrder::findAll($wsParams);

                $techInfo = [];
                $designElements = ['designElementMotive', 'designElementText', 'designElementPimpText', 'designElementQrcode', 'designElementSvgText'];

                foreach ($platformOrders as $pOrder) {
                    foreach ($pOrder->designedProducts as $product) {
                        foreach ($product->design->compositions as $composition) {
                            foreach ($designElements as $designElementName) {
                                foreach ($composition->$designElementName as $designElement) {
                                    if ($designElement->layers) {
                                        foreach ($designElement->layers as $layer) {                                        
                                            $techInfo[$layer->printTechnology->id] = [
                                                'id' => $layer->printTechnology->id,
                                                'name' => $layer->printTechnology->name,
                                                'type' => $layer->printTechnology->type
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //update type in shirtplatform_print_technology table
        if (isset($techInfo)) {
            foreach ($techInfo as $info) {
                $connection->update($table, ['type' => $info['type']], ['technology_id = ?' => $info['id']]);
            }
        }

        $connection->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
