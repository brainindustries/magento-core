<?php

namespace Shirtplatform\Core\Setup\Patch\Data;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Api\BookmarkRepositoryInterface;

class FixSalesOrderGridBookmarks implements DataPatchInterface
{
    /**
     * @var AppState
     */
    private $_appState;

    /**
     * @var BookmarkRepositoryInterface
     */
    private $_bookmarkRepository;    

    /**
     * @var ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var SerializerInterface
     */
    private $_serializer;    

    /**
     * @access public
     * @param AppState $appState
     * @param BookmarkRepositoryInterface $bookmarkRepository     
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ObjectManagerInterface $objectManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SerializerInterface $serializer     
     */
    public function __construct(
        AppState $appState,
        BookmarkRepositoryInterface $bookmarkRepository,        
        ModuleDataSetupInterface $moduleDataSetup,
        ObjectManagerInterface $objectManager,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SerializerInterface $serializer        
    ) {
        $this->_appState = $appState;
        $this->_bookmarkRepository = $bookmarkRepository;        
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_objectManager = $objectManager;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_serializer = $serializer;        
    }

    /**
     * Go through existing bookmarks of sales_order_grid ui component and set
     * correct positions of columns
     * 
     * @access public
     * @return void
     */
    public function apply()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();

        $searchCriteria = $this->_searchCriteriaBuilder->addFilter('namespace', 'sales_order_grid')->create();
        $bookmarks = $this->_bookmarkRepository->getList($searchCriteria)->getItems();

        if ($bookmarks) {
            //UiComponentFactory cannot be injected in constructor and needs to be called in 
            //adminhtml area otherwise 'area is not set' exception occurs            
            $grid = $this->_appState->emulateAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML, function() {
                return $this->_objectManager->get(UiComponentFactory::class)->create('sales_order_grid');
            });
            $columnsInfo = $this->_getColumnsInfo($grid);
        }

        if (isset($columnsInfo)) {
            foreach ($bookmarks as $book) {                
                $config = $this->_processConfig($book, $columnsInfo);
                $book->setConfig($this->_serializer->serialize($config));
                $this->_bookmarkRepository->save($book);
            }
        }
        
        $this->_moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get sales_order_grid columns info
     * 
     * @access private
     * @return array
     */
    private function _getColumnsInfo($grid)
    {
        $columnsInfo = [];        

        //get sales_order_grid ui component column definition
        if ($grid) {
            $columns = $grid->getComponent('sales_order_columns');
            $components = $columns->getChildComponents();            

            foreach ($components as $comp) {
                $config = $comp->getConfig();
                $columnsInfo[$comp->getName()] = [
                    'name' => $comp->getName(),
                    'sortOrder' => $config['sortOrder'] ?? 0
                ];
            }

            //sort columns by sortOrder
            uasort($columnsInfo, function ($a, $b) {
                return $a['sortOrder'] <=> $b['sortOrder'];
            });
        }

        return $columnsInfo;
    }

    /**
     * Process config - sort columns and positions
     * 
     * @access private
     * @param \Magento\Ui\Api\Data\BookmarkInterface $bookmark
     * @param array $columnsInfo
     */
    private function _processConfig($bookmark, $columnsInfo)
    {
        $config = $bookmark->getConfig();
        $identifier = $bookmark->getIdentifier();
        
        if (isset($config['current']['columns']) && isset($config['current']['positions'])) {
            $columns = $config['current']['columns'];
            $positions = $config['current']['positions'];
        }
        elseif (isset($config['views'][$identifier]['data']['columns']) && isset($config['views'][$identifier]['data']['positions'])) {
            $columns = $config['views'][$identifier]['data']['columns'];
            $positions = $config['views'][$identifier]['data']['positions'];
        }
        else {
            return $config;
        }        

        //check if all the technology columns are present
        foreach ($columnsInfo as $name => $info) {
            if (preg_match('/^technology_/', $name) && !isset($columns[$name])) {
                $columns[$name] = [
                    'visble' => true,
                    'sorting' => false
                ];
                $positions[$name] = $info['sortOrder'];
            }
        }

        //add sortOrder to columns and positions
        foreach ($columns as $name => $info) {
            if (isset($columnsInfo[$name])) {
                $columns[$name]['sortOrder'] = $columnsInfo[$name]['sortOrder'];
            } else {
                unset($columns[$name]);
            }
        }
        foreach ($positions as $columnName => $pos) {
            if (isset($columnsInfo[$columnName])) {
                $positions[$columnName] = $columnsInfo[$columnName]['sortOrder'];
            } else {
                unset($positions[$columnName]);
            }
        }

        //sort columns and positions by their sortOrder
        uasort($columns, function ($a, $b) {
            return $a['sortOrder'] <=> $b['sortOrder'];
        });
        uasort($positions, function ($a, $b) {
            return $a <=> $b;
        });

        //remove sortOrder from columns
        foreach ($columns as $name => $info) {
            unset($columns[$name]['sortOrder']);
        }
        //re-number positions
        $newPos = 0;
        foreach ($positions as $columnName => $pos) {
            $positions[$columnName] = $newPos;
            $newPos++;
        }

        //set columns and positions back to bookmark
        if (isset($config['current'])) {
            $config['current']['columns'] = $columns;
            $config['current']['positions'] = $positions;
        }
        elseif (isset($config['views'][$identifier])) {
            $config['views'][$identifier]['data']['columns'] = $columns;
            $config['views'][$identifier]['data']['positions'] = $positions;
        }        

        return $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }
}
