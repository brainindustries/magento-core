<?php

namespace Shirtplatform\Core\Setup\Patch\Data;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Eav\Api\AttributeSetManagementInterface;
use Magento\Eav\Api\Data\AttributeSetInterfaceFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class CreateEssentialAttributes implements DataPatchInterface
{        

    /**
     * Default attribute properties
     */
    private $_attributeDefaults = [
        'type' => 'int',
        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'used_in_product_listing' => true,
        'unique' => false,
        'group' => null
    ];

    /**
     * Category attribute specific properties
     */
    private $_categoryAttributes = [
        'shirtplatform_id' => [
            'type' => 'varchar',
            'label' => 'ShirtPlatform Product ID',
            'group' => 'General'
        ],
        'shirtplatform_version' => [
            'type'  => 'varchar',
            'label' => 'ShirtPlatform Version',
            'group' => 'General'
        ]
    ];

    /**
     * Product attribute specific properties
     */
    private $_productAttributes = [
        'size' => [
            'label' => 'Size',
            'input' => 'select',            
            'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL
        ],
        'shirtplatform_id' => [
            'type' => 'varchar',
            'label' => 'ShirtPlatform Product ID'            
        ],
        'shirtplatform_version' => [
            'type'  => 'varchar',
            'label' => 'ShirtPlatform Version'            
        ],
        'shirtplatform_size_id' => [
            'label'    => 'ShirtPlatform Product Size ID',
            'default'  => '',
            'apply_to' => ''     
        ],
        'shirtplatform_color_id' => [
            'label'    => 'ShirtPlatform Product Color ID',
            'default'  => '',
            'apply_to' => ''            
        ],
        'shirtplatform_product_type' => [
            'label' => 'Shirtplatform Product Type',
            'input' => 'select',
            'source' => \Shirtplatform\Core\Model\Config\Source\ProductType::class,
            'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL
        ]
    ];

    /**
     * @var AttributeSetInterfaceFactory
     */
    private $_attributeSetFactory;

    /**
     * @var AttributeSetManagementInterface
     */
    private $_attributeSetManagement;    

    /**
     * @var EavSetupFactory
     */
    private $_eavSetup;

    /**
     * @var ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $_productAttributeRepository;    

    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $_setupInstance;

    /**
     * @param AttributeSetInterfaceFactory $attributeSetFactory
     * @param AttributeSetManagementInterface $attributeSetManagement     
     * @param EavSetupFactory $eavSetup
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ProductAttributeRepositoryInterface $productAttributeRepository     
     */
    public function __construct(
        AttributeSetInterfaceFactory $attributeSetFactory,
        AttributeSetManagementInterface $attributeSetManagement,        
        EavSetupFactory $eavSetup,
        ModuleDataSetupInterface $moduleDataSetup,
        ProductAttributeRepositoryInterface $productAttributeRepository        
    ) {
        $this->_attributeSetFactory = $attributeSetFactory;
        $this->_attributeSetManagement = $attributeSetManagement;        
        $this->_eavSetup = $eavSetup;
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_productAttributeRepository = $productAttributeRepository;        
    }

    /**
     * Create category and product attributes, add product attributes to a newly created
     * "Shirtplatform Product" attribute set and change color attribute to a visual swatch
     * 
     * @access public
     * @return void
     */
    public function apply()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();

        $this->_setupInstance = $this->_eavSetup->create(['setup' => $this->_moduleDataSetup]);
        $this->_createAttributeSetIfNotExists();
        $this->_createCategoryAttributes();
        $this->_createProductAttributes();
        $this->_addAttributesToSet();

        //change "color" attribute to be a visual swatch
        $colorAttributeRow = $this->_setupInstance->getAttribute(\Magento\Catalog\Model\Product::ENTITY, 'color');            
        if ($colorAttributeRow) {                
            $colorAttribute = $this->_productAttributeRepository->get('color');                        
            $colorAttribute->setSwatchInputType('visual');
            $this->_productAttributeRepository->save($colorAttribute);            
        }

        $this->_moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Create "Shirtplatform Product" attribute Set if it doesn't exist
     * 
     * @access private
     * @return void
     */
    private function _createAttributeSetIfNotExists()
    {
        $entityTypeId = $this->_setupInstance->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $defaultAttributeSetId = $this->_setupInstance->getDefaultAttributeSetId($entityTypeId);
        $attributeSet = $this->_setupInstance->getAttributeSet($entityTypeId, \Shirtplatform\Core\Helper\Product::SHIRTPLATFORM_ATTRIBUTESET_NAME);

        if (!$attributeSet) {
            $attributeSet = $this->_attributeSetFactory->create();
            $attributeSet->setData([
                'attribute_set_name' => \Shirtplatform\Core\Helper\Product::SHIRTPLATFORM_ATTRIBUTESET_NAME,
                'entity_type_id' => $entityTypeId,
            ]);
            $this->_attributeSetManagement->create(\Magento\Catalog\Model\Product::ENTITY, $attributeSet, $defaultAttributeSetId);
        }        
    }    

    /**
     * Create category attributes
     * 
     * @access private     
     * @return void
     */
    private function _createCategoryAttributes()
    {        
        foreach ($this->_categoryAttributes as $code => $data) {
            $attributeData = array_merge($this->_attributeDefaults, $data);
            $this->_setupInstance->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $code, $attributeData);    
        }
    }

    /**
     * Create product attributes
     * 
     * @access private     
     * @return void
     */
    private function _createProductAttributes()
    {                                 
        foreach ($this->_productAttributes as $code => $data) {
            $attributeData = array_merge($this->_attributeDefaults, $data);
            $this->_setupInstance->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $code, $attributeData);            
        }        
    }

    /**
     * Add product attributes to "Shirtplatform Product" attribute set
     * 
     * @access private     
     * @return void
     */
    private function _addAttributesToSet()
    {
        $attributeCodes = array_keys($this->_productAttributes);
		$attributeCodes = array_merge($attributeCodes, ['color', 'manufacturer']);

        foreach ($attributeCodes as $code) {
			try {
				$this->_setupInstance->addAttributeToSet(\Magento\Catalog\Model\Product::ENTITY, \Shirtplatform\Core\Helper\Product::SHIRTPLATFORM_ATTRIBUTESET_NAME, 'General', $code);
			} catch (\Exception $e) {
				echo 'Error : ' . $e->getMessage() . "\n";
			}
		}
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
