<?php

namespace Shirtplatform\Core\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Integration\Api\AuthorizationServiceInterface;
use Magento\Integration\Api\IntegrationServiceInterface;
use Magento\Integration\Api\OauthServiceInterface;

class CreateIntegration implements DataPatchInterface
{
    const SHIRTPLATFORM_INTEGRATION_NAME = 'ShirtPlatform';

    /**
	 * @var AuthorizationServiceInterface
	 */
	private $_authorizationService;

    /**
	 * @var IntegrationServiceInterface
	 */
	private $_integrationService;

    /**
     * @var ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
	 * @var OauthServiceInterface
	 */
	private $_oauthService;

    /**
     * @access public
     * @param AuthorizationServiceInterface $authorizationService
     * @param IntegrationServiceInterface $integrationService
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param OauthServiceInterface $oauthService
     */
    public function __construct(
        AuthorizationServiceInterface $authorizationService,
        IntegrationServiceInterface $integrationService,
        ModuleDataSetupInterface $moduleDataSetup,
        OauthServiceInterface $oauthService
    ) {
        $this->_authorizationService = $authorizationService;
        $this->_integrationService = $integrationService;
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_oauthService = $oauthService;
    }

    /**
     * Create "ShirtPlatform" Integration
     * 
     * @access public
     * @return void
     */
    public function apply()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();

        $integration = $this->_integrationService->findByName(self::SHIRTPLATFORM_INTEGRATION_NAME);

		if (!$integration->getId()) {
			$integrationData = [
				'name' => self::SHIRTPLATFORM_INTEGRATION_NAME,
				'status' => '1',
				'setup_type' => '0'
			];

			try {
				// Create Integration
				$integration = $this->_integrationService->create($integrationData);
				$integrationId = $integration->getId();
				$consumerId = $integration->getConsumerId();

				// Create access token
				$this->_oauthService->createAccessToken($consumerId);

				// Grant permissions
				$this->_authorizationService->grantAllPermissions($integrationId);

			} catch (\Exception $e) {
				echo 'Error : ' . $e->getMessage() . "\n";
			}
		}

        $this->_moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
