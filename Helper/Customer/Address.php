<?php

namespace Shirtplatform\Core\Helper\Customer;

use Magento\Framework\Exception\NoSuchEntityException;

class Address extends \Magento\Customer\Helper\Address {

    /**
     * Retrieve attribute visibility
     *
     * @access public
     * @param string $code
     * @return bool     
     */
    public function isAttributeVisible($code) {
        try {
            return parent::isAttributeVisible($code);
        } catch (NoSuchEntityException $ex) {
            return false;
        }
    }

}
