<?php

namespace Shirtplatform\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnologyFactory;
use shirtplatform\resource\ProductionResource;
use shirtplatform\filter\WsParameters;

/**
 * Class PrintTechnologyHelper
 * @todo Unit tests
 * @package Shirtplatform\Core\Helper
 */
class PrintTechnologyHelper extends AbstractHelper
{
    const DESIGN_ELEMENTS = [
        'designElementMotive', 
        'designElementText', 
        'designElementPimpText', 
        'designElementQrcode', 
        'designElementSvgText'
    ];

    const TECH_NAME_TYPE_MAPPER = [
        'technology_flex_flock' => 'FLEX_FLOCK',
        'technology_sublimation' => 'SUBLIMATION',
        'technology_digital' => 'DIGITAL',
        'technology_laser' => 'LASER',
        'technology_digital_direct' => 'DIGITAL_DIRECT',
        'technology_direct_to_foil' => 'DIRECT_TO_FOIL',
        'technology_engraving' => 'ENGRAVING',
        'technology_screenprint' => 'SCREENPRINT',
        'technology_kornit' => 'KORNIT',
        'technology_other' => 'OTHER',
        'technology_character_count' => 'technology_character_count'  
    ];

    /** @var \Shirtplatform\Core\Helper\Data */
    private $shirtplatformData;

    /** @var ShirtplatformPrintTechnologyFactory */
    private $printTechnologyFactory;

    /**
     * 
     * @access public
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Shirtplatform\Core\Helper\Data $shirtplatformData
     * @param ShirtplatformPrintTechnologyFactory $printTechnologyFactory     
     * @return void
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Shirtplatform\Core\Helper\Data $shirtplatformData,
        ShirtplatformPrintTechnologyFactory $printTechnologyFactory
    ) {
        parent::__construct($context);
        $this->shirtplatformData = $shirtplatformData;
        $this->printTechnologyFactory = $printTechnologyFactory;
    }

    /**
     * Save order technologies
     *
     * @access public
     * @param \Magento\Sales\Model\Order|array $orders
     * @return bool
     */
    public function saveOrderTechnologies($orders)
    {
        // get order_id, shirtplatform_id and store_id of all valid orders
        $validOrders = $this->getValidOrders($orders);
        if (empty($validOrders)) {
            return false;
        }

        foreach ($validOrders as $storeId => $_orders) {
            // get existing print technology data of selected orders
            $items = $this->printTechnologyFactory->create()->getCollection()
                ->addFieldToFilter('order_id', ['in' => array_values($_orders)])
                ->addFieldToSelect('id');

            // delete existing print technology data
            foreach ($items as $shirtplatformPrintTechnology) {
                $shirtplatformPrintTechnology->delete();
            }

            // log into shirtplatform
            try {
                $this->shirtplatformData->shirtplatformAuth($storeId);
            } catch (\Exception $e) {
                return false;
            }

            $platformOrderIds = array_keys($_orders);
            $printTechData = $this->getPrintTechnologyDataForOrders($platformOrderIds);

            foreach ($printTechData as $_platformOrderId => $data) {
                $_orderId = $_orders[$_platformOrderId];

                // create and save print technology item
                foreach ($data as $printTechnologyId => $technologyData) {
                    $characterCount = ProductionResource::countCharaters($_platformOrderId, $printTechnologyId);
                    $printTechnologyModel = $this->printTechnologyFactory->create();
                    $printTechnologyModel->setOrderId($_orderId);
                    $printTechnologyModel->setName($technologyData['name']);
                    $printTechnologyModel->setType($technologyData['type']);
                    $printTechnologyModel->setTechnologyId($printTechnologyId);
                    $printTechnologyModel->setCount((int) $technologyData['count']);
                    $printTechnologyModel->setCharacterCount((int) $characterCount);
                    $printTechnologyModel->save();
                }
            }
        }
        return true;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface|array $orders
     * @return array
     */
    private function getValidOrders($orders)
    {
        $result = [];
        if (!is_countable($orders)) {
            $orders = [$orders];
        }
        foreach ($orders as $_order) {
            if ($_order->getShirtplatformId()) {
                $result[$_order->getStoreId()][$_order->getShirtplatformId()] = $_order->getId();
            }
        }
        return $result;
    }

    /**
     * Get print technology data for multiple orders
     * 
     * @access public
     * @param int[] $platformOrderIds
     * @return array
     */
    public function getPrintTechnologyDataForOrders($platformOrderIds)
    {
        $wsParams = new WsParameters();
        $wsParams->addExpressionContain('id', $platformOrderIds);
        $designedProductFilter = new \shirtplatform\filter\Filter();
        $designedProductFilter->setFilterName('designedProductsFilter');
        $wsParams->addRelatedFilter($designedProductFilter);

        try {
            $platformOrders = \shirtplatform\entity\overview\Order::findAll($wsParams);
        } catch (\Exception $e) {
            return [];
        }

        $result = [];
        foreach ($platformOrders as $pOrder) {
            $result[$pOrder->id] = $this->getPrintTechnologyDataForSingleOrder($pOrder);
        }

        return $result;
    }

    /**
     * Get print technology data for single order
     * 
     * @access private
     * @param \shirtplatform\entity\overview\Order $platformOrder
     * @return array
     */
    private function getPrintTechnologyDataForSingleOrder($platformOrder)
    {
        $results = [];
        foreach ($platformOrder->designedProducts as $product) {
            foreach ($product->design->compositions as $composition) {
                $compositionPrints = $this->getPrintsForComposition($composition);
                foreach ($compositionPrints as $printTechnologyId => $techInfo) {
                    if (isset($results[$printTechnologyId])) {
                        $results[$printTechnologyId]['count'] += $product->amount;
                    } else {
                        $results[$printTechnologyId]['id'] = $printTechnologyId;
                        $results[$printTechnologyId]['name'] = $techInfo['name'];
                        $results[$printTechnologyId]['type'] = $techInfo['type'];
                        $results[$printTechnologyId]['count'] = $product->amount;
                    }
                }
            }
        }
        return $results;
    }

    /**
     * @param $composition
     * @return array
     */
    private function getPrintsForComposition($composition)
    {
        $results = [];
        foreach (self::DESIGN_ELEMENTS as $designElementName) {
            foreach ($composition->$designElementName as $designElement) {
                if ($designElement->layers) {
                    foreach ($designElement->layers as $layer) {
                        $results[$layer->printTechnology->id] = [
                            'id' => $layer->printTechnology->id,
                            'name' => $layer->printTechnology->name,
                            'type' => $layer->printTechnology->type
                        ];
                    }
                }
            }
        }
        return $results;
    }

    /**
     * @param string $title
     * @param array $prints
     * @return int
     */
    public function getCounts($title, $prints)
    {
        if ($title === 'technology_character_count') {
            return $this->getCharacterCount($prints);
        }
        return $this->getTechnologyCount($title, $prints);
    }

    /**
     * @param string $title
     * @param array $prints
     * @return int
     */
    private function getTechnologyCount($title, array $prints)
    {
        if ($title === 'technology_flex_flock') {
            $count = 0;
            foreach ($prints as $print) {
                if ($print->getType() == 'FLEX_FLOCK') {                    
                    $count += (int) $print->getCount();
                }
            }
            return $count;
        }

        foreach ($prints as $print) {
            if (isset(self::TECH_NAME_TYPE_MAPPER[$title])) {
                $type = self::TECH_NAME_TYPE_MAPPER[$title];

                if ($print->getType() == $type) {
                    return $print->getCount();
                }
            }                                
        }
        
        return 0;
    }

    /**
     * @param array $prints
     * @return int
     */
    private function getCharacterCount(array $prints)
    {
        $count = 0;
        foreach ($prints as $print) {
            if ($print->getType() == 'FLEX_FLOCK') {
                $count += $print->getCharacterCount();
            }
        }
        return $count;
    }
}
