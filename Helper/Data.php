<?php

namespace Shirtplatform\Core\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Shirtplatform\Core\Model\User\CookieUserFactory;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use shirtplatform\constants\WsConstants;
use shirtplatform\resource\AuthResource;
use shirtplatform\utils\user\User;
use shirtplatform\entity\product\Product;
use Magento\Framework\Filesystem;

class Data extends \BrainIndustries\Core\Helper\Data
{

    /**
     * ID of store that is currently logged in platform
     * 
     * @access private
     * @static
     * @var int
     */
    private static $_loggedInPlatformUnderStoreId;

    /**
     * Webservice URL to be used. Some platform resources are public and don't need authorization.
     * Therefore we can simply set WsConstants$webservicesURL and save the url also in this
     * property to indicate, it has been explicitely set
     * 
     * @access private
     * @var string
     */
    private $_webservicesUrl;

    /**
     * Shirtplatform cookie user
     * 
     * @access private
     * @var CookieUserFactory
     */
    private $_cookieUserFactory;

    /**
     * @var EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @var DirectoryList
     */
    protected $_directoryList;

    /**
     * @var File
     * TODO - maybe we should use Magento\Framework\Filesystem\Driver\File
     */
    protected $_ioFile;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var array
     */
    private $_productsAndSizes = [];

    /**
     * 
     * @param Context $context
     * @param \Psr\Log\LoggerInterface $errorLogger
     * @param Filesystem $filesystem
     * @param \Psr\Log\LoggerInterface $logger
     * @param TransportInterface $mailTransport
     * @param CookieUserFactory $cookieUserFactory
     * @param EncryptorInterface $encryptor
     * @param DirectoryList $directoryList
     * @param File $ioFile
     * @param DateTime $dateTime
     */
    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $errorLogger,
        Filesystem $filesystem,
        \Psr\Log\LoggerInterface $logger,
        TransportInterface $mailTransport,
        CookieUserFactory $cookieUserFactory,
        EncryptorInterface $encryptor,
        DirectoryList $directoryList,
        File $ioFile,
        DateTime $dateTime
    ) {
        parent::__construct($context, $errorLogger, $filesystem, $logger, $mailTransport);
        $this->_cookieUserFactory = $cookieUserFactory;
        $this->_encryptor = $encryptor;
        $this->_directoryList = $directoryList;
        $this->_ioFile = $ioFile;
        $this->_dateTime = $dateTime;
    }

    /**
     * Set webservices URL based on configuration
     * 
     * @access public
     * @param bool $forceRewrite rewrite webservices url if it has been already set
     */
    public function setWebservicesUrl($forceRewrite = false)
    {
        if (empty($this->_webservicesUrl) or $forceRewrite) {
            $url = $this->getConfigValue('shirtplatform/general/url');

            if ($url) {
                WsConstants::$webservicesURL = $url;
                $this->_webservicesUrl = $url;
            }
        }
    }

    /**
     * Authorization to shirtplatform          
     * 
     * @access public
     * @param int $storeId
     * @return bool
     * @throws \Exception
     */
    public function shirtplatformAuth($storeId)
    {
        if (self::$_loggedInPlatformUnderStoreId != $storeId) {
            //if self::$_loggedInPlatformUnderStoreId != null => should be probably logged out
            //so that all session data is cleared. This needs to be checked

            $url = $this->getConfigValue('shirtplatform/general/url', $storeId);
            $user = $this->getConfigValue('shirtplatform/general/login', $storeId);
            $pass = $this->getConfigValue('shirtplatform/general/password', $storeId);
            $shop = $this->getConfigValue('shirtplatform/general/store', $storeId);

            if (!isset($url) || !isset($user) || !isset($pass) || !isset($shop)) {
                return false;
            }

            $pass = $this->_encryptor->decrypt($pass);

            if (self::$_loggedInPlatformUnderStoreId) {
                $this->shirtplatformLogout();
            }

            $userClass = $this->_cookieUserFactory->create(['storeId' => $storeId]);
            User::setUserClass($userClass);
            $this->setWebservicesUrl(true);

            //make sure that the sessionID in a cookie is not associated with some other storeId (important in admin)
            $cookieStoreId = $userClass->getCookieStoreId();
            if ($cookieStoreId and $cookieStoreId != $storeId) {
                $this->shirtplatformLogout();
            }

            if (!AuthResource::isLogged()) {
                if (!AuthResource::wsLogin($user, $pass)) {
                    throw new \Exception('Could not log in into shirtplatform');
                }
            }

            User::setShopId($shop);
            self::$_loggedInPlatformUnderStoreId = $storeId;
            return true;
        } else {
            return true;
        }
    }

    /**
     * Logout from shirtplatform
     * 
     * @access public
     */
    public function shirtplatformLogout()
    {
        AuthResource::logout();
        self::$_loggedInPlatformUnderStoreId = null;
    }

    /**
     * Get directory with locks for the orders.
     *
     * @access public
     * @return string
     */
    public function getLockDirectory()
    {
        return $this->_directoryList->getPath(DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR . 'locks';
    }

    /**
     * Try to create lock. Lock is implemented as file.      
     *
     * If successful, method returns true otherwise
     * (file is already created, creating file fails) returns false.
     * On fail there may be generated some log messages. If the file exists
     * and is older than $maxLifetime seconds, it deletes it first.
     *
     * @param string $filename     
     * @param int $maxLifetime number of seconds after which the lock is released
     * @return bool
     */
    public function lock(
        $filename,
        $maxLifetime = 10
    ) {
        $lockDirectory = $this->getLockDirectory();
        $lockFilename = $lockDirectory . DIRECTORY_SEPARATOR . $filename;

        try {
            $this->_ioFile->checkAndCreateFolder($lockDirectory, 0755);
        } catch (\Exception $ex) {
            // Lock directory does not exist, we cannot get lock
            $msg = __FILE__ . ":" . __METHOD__ . ': Cannot create directory "' . $lockDirectory .
                '". Error message: ' . $ex->getMessage();
            $this->logError($msg);
            return false;
        }

        if ($this->_ioFile->fileExists($lockFilename)) {
            $modified = filemtime($lockFilename);
            $now = $this->_dateTime->gmtTimestamp();

            //if the file is older than $timeCondition seconds - delete it. It might have been left by some exception
            if ($now - $modified > $maxLifetime) {
                $this->_ioFile->rm($lockFilename);
            } else {
                return false;
            }
        }

        // Lock that        
        return (bool) $this->_ioFile->write($lockFilename, 'lock');
    }

    /**
     * Does lock exist with the $filename exist?
     * 
     * @access public
     * @param string $filename
     * @return bool
     */
    public function lockExists($filename)
    {
        $lockDirectory = $this->getLockDirectory();
        $lockFilename = $lockDirectory . DIRECTORY_SEPARATOR . $filename;
        return $this->_ioFile->fileExists($lockFilename);        
    }

    /**
     * Free lock.
     *
     * Method tries to delete a lock file. If lock file does
     * not exist method returns true.
     *
     * @access public
     * @param string $filename     
     * @return bool
     */
    public function freeLock($filename)
    { 
        $lockDirectory = $this->getLockDirectory();
        $lockFilename = $lockDirectory . DIRECTORY_SEPARATOR . $filename;

        if (!$this->_ioFile->fileExists($lockFilename)) {
            // No lock - success
            return true;
        }        

        return $this->_ioFile->rm($lockFilename);
    }

    /**
     * Get uniqUrl string that acts as a connection between store view and shirtplatform shop
     * 
     * @access public
     * @param \Magento\Store\Model\Store $store
     * @return string
     */
    public function getConnectionUniqUrl($store)
    {
        $storeUrl = parse_url($store->getConfig('web/unsecure/base_url'));
        return $storeUrl['host'] . '_' . $store->getCode() . '_' . $store->getId();
    }

    /**
     * Send email to marketing
     *
     * @access public
     * @param string $subject
     * @param string $body
     * @return void
     */
    public function getProductColorsAndSizes($storeId, $platformProductId)
    {
        if (isset($this->_productsAndSizes[$storeId][$platformProductId])) {
            return $this->_productsAndSizes[$storeId][$platformProductId];
        }

        $result = [];

        $this->shirtplatformAuth($storeId);
        $platformProduct = Product::find($platformProductId);

        foreach ($platformProduct->getSku() as $sku) {
            if ($sku['available'] == 'TRUE') {
                $colorId = $sku->assignedColor->productColor->parents[1];
                $colorName = $sku->assignedColor->productColor->name;
                $sizeId = $sku->assignedSize->productSize->parents[1];
                $sizeName = $sku->assignedSize->productSize->name;

                $result[$colorId]['name'] = $colorName;
                $result[$colorId]['sizes'][$sizeId] = $sizeName;
            }
        }

        $this->_productsAndSizes[$storeId][$platformProductId] = $result;
        return $result;
    }

    public function sendEmailToMarketing($subject, $body)
    {
        $emails = $this->getConfigValue('trans_email/ident_marketing/email_addresses');
        if (empty($emails)) {
            return;
        }

        $emails = array_map('trim', explode(',', $emails));
        $this->sendEmailToAdmin($subject, $body, $emails);
    }

    /**
     * Send email to tech support
     *
     * @access public
     * @param string $subject
     * @param string $body
     * @return void
     */
    public function sendEmailToTechSupport($subject, $body)
    {
        $emails = $this->getConfigValue('trans_email/ident_tech/email_addresses');
        if (empty($emails)) {
            return;
        }

        $emails = array_map('trim', explode(',', $emails));
        $this->sendEmailToAdmin($subject, $body, $emails);
    }
}
