<?php

namespace Shirtplatform\Core\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\HTTP\Client\Curl;

/**
 * Class Overview
 * @package Shirtplatform\Core\Helper
 */
class Overview extends \Magento\Framework\App\Helper\AbstractHelper
{    

    /**
     * @var Curl
     */
    private $_curl;

    /**
     * @var DirectoryList
     */
    private $_directoryList;

    /**
     * @var File     
     */
    private $_ioFile;

    /** @var Data */
    private $_coreHelper;

    /** @var \Magento\Framework\Locale\Resolver */
    private $_locale;

    /**
     * Overview constructor.
     * @param Context $context
     * @param Curl $curl
     * @param DirectoryList $directoryList
     * @param File $ioFile
     * @param Data $_coreHelper
     * @param \Magento\Framework\Locale\Resolver $_locale
     */
    public function __construct(
        Context $context,
        Curl $curl,
        DirectoryList $directoryList,
        File $ioFile,
        Data $coreHelper,
        \Magento\Framework\Locale\Resolver $locale
    ) {
        parent::__construct($context);
        $this->_curl = $curl;
        $this->_directoryList = $directoryList;
        $this->_ioFile = $ioFile;
        $this->_coreHelper = $coreHelper;
        $this->_locale = $locale;
    }

    /**
     * Get path to the overview directory
     * 
     * @access public
     * @return string
     */
    public function getOverviewDirectory()
    {
        return $this->_directoryList->getPath(DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR . 'overview';
    }

    /**
     * Get path to overview PDF file
     * 
     * @access public
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return string
     */
    public function getFullOverviewPath($order)
    {
        return $this->getOverviewDirectory() . DIRECTORY_SEPARATOR . $this->getOverviewFilename($order);
    }
    
    /**
     * Get overview filename
     * 
     * @access public
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return string
     */
    public function getOverviewFilename($order)
    {
        return 'overview_' . $order->getShirtplatformId() . '.pdf';
    }

    /**
     * Download overview and save it to var/overview directory
     * 
     * @access public
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return int|false (number of bytes written to disk or bool)
     */
    public function downloadOverview($order)
    {
        try {
            $pdfContent = $this->_getOverviewPdfContent($order->getStoreId(), $order->getShirtplatformId(), $order->getIncrementId());
        } catch (\Exception $ex) {
            $msg = "Couldn't download overview for order " . $order->getIncrementId() . ". Error: " . $ex->getMessage();
            $this->_coreHelper->logError($msg);
        }

        if (!empty($pdfContent)) {
            try {
                $this->_ioFile->checkAndCreateFolder($this->getOverviewDirectory(), 0755);
            } catch (\Exception $ex) {
                $msg = 'Cannot create overview directory. Exception: ' . $ex->getMessage();
                $this->_coreHelper->logError($msg);
                return false;
            }
            
            return $this->_ioFile->write($this->getFullOverviewPath($order), $pdfContent);
        }

        return false;
    }

    /**
     * Get overview PDF as string
     * 
     * @param \Magento\Sales\Model\Order $order
     * @return bool|string     
     */
    public function getOverviewPdf(\Magento\Sales\Model\Order $order)
    {        
        $filename = $this->getFullOverviewPath($order);        

        if (file_exists($filename)) {                        
            return file_get_contents($filename);
        }

        $bytesWritten = $this->downloadOverview($order);
        if ($bytesWritten !== false) {            
            return file_get_contents($filename);
        }

        return false;        
    }

    /**
     * Get overview PDF content
     * 
     * @access private
     * @param int $storeId
     * @param int $orderId
     * @param int $newOrderId
     * @return string
     * @throws \Exception|\Magento\Framework\Exception\LocalizedException
     */
    private function _getOverviewPdfContent($storeId, $orderId, $newOrderId)
    {
        $this->_coreHelper->shirtplatformAuth($storeId);
        $shopId = $this->scopeConfig->getValue('shirtplatform/general/store', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        $this->_locale->emulate($storeId);
        $langCode = substr($this->_locale->getLocale(), 0, 2);
        $this->_locale->revert();
        $editorUrl = $this->scopeConfig->getValue('shirtplatform/general/editor_url');
        $overviewLink = $this->generateOverviewLink($editorUrl, $langCode, $shopId, $orderId, $newOrderId);                
        
        $this->_curl->get($overviewLink);
        $response = $this->_curl->getBody();

        if ($this->_curl->getStatus() != 200) {
            $msg = __('Status code: %1', $this->_curl->getStatus());
            throw new \Magento\Framework\Exception\LocalizedException($msg);
        }            

        return $response;        
    }

    /**
     * Check if overview file exists on disk
     * 
     * @access public
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function overviewFileExists($order)
    {
        $filename = $this->getFullOverviewPath($order);
        return file_exists($filename);
    }

    /**
     * Remove overview file from disk
     * 
     * @access public
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    public function removeOverview(\Magento\Sales\Model\Order $order)
    {        
        if ($this->overviewFileExists($order)) {
            $filename = $this->getFullOverviewPath($order);
            $this->_ioFile->rm($filename);
        }
    }    

    /**
     * @param string $editorUrl
     * @param string $langCode
     * @param int $shopId
     * @param int $orderId
     * @param int $newOrderId
     * @return string
     */
    private function generateOverviewLink($editorUrl, $langCode, $shopId, $orderId, $newOrderId)
    {
        $params = [
            '{lang}' => $langCode,
            '{shopId}' => $shopId,
            '{orderId}' => $orderId,
            '{newOrderId}' => $newOrderId,
            '{session}' => \shirtplatform\utils\user\User::getSessionId()
        ];

        return $editorUrl . str_replace(
            array_keys($params),
            array_values($params),
            'index.php?r=overview/index&lang={lang}&shopId={shopId}&orderIds={orderId}&newOrderId={newOrderId}&template=t2&result=pdf&session={session}'
        );
    }
}
