<?php

namespace Shirtplatform\Core\Helper;

use Magento\Framework\App\ResourceConnection;
use Shirtplatform\Core\Api\AssignedViewRepositoryInterface;
use shirtplatform\resource\PublicResource;

class Image extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var AssignedViewRepository
     */
    private $_assignedViewRepository;

    /**
     * @var \Magento\Catalog\Helper\ImageFactory
     */
    private $_catalogImageHelperFactory;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_dataHelper;

    /**
     * @var \Magento\Catalog\Model\Product\Gallery\ReadHandler
     */
    private $_galleryReader;

    /**
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    private $_imageBuilder;

    /**
     * Catalog product configuration
     *
     * @var \Magento\Catalog\Helper\Product\Configuration
     */
    private $_productConfig;

    /** @var ResourceConnection */
    private $resourceConnection;

    /**
     * Image constructor.
     * @param AssignedViewRepositoryInterface $assignedViewRepository
     * @param \Magento\Catalog\Helper\ImageFactory $catalogImageHelperFactory
     * @param Data $dataHelper
     * @param \Magento\Catalog\Model\Product\Gallery\ReadHandler $galleryReader
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Catalog\Helper\Product\Configuration $productConfig
     * @param \Magento\Framework\App\Helper\Context $context
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
    AssignedViewRepositoryInterface $assignedViewRepository,
    \Magento\Catalog\Helper\ImageFactory $catalogImageHelperFactory,
    \Shirtplatform\Core\Helper\Data $dataHelper,
    \Magento\Catalog\Model\Product\Gallery\ReadHandler $galleryReader,
    \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
    \Magento\Catalog\Helper\Product\Configuration $productConfig,
    \Magento\Framework\App\Helper\Context $context,
    ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->_assignedViewRepository = $assignedViewRepository;
        $this->_catalogImageHelperFactory = $catalogImageHelperFactory;
        $this->_dataHelper = $dataHelper;
        $this->_galleryReader = $galleryReader;
        $this->_imageBuilder = $imageBuilder;
        $this->_productConfig = $productConfig;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Get images with designs
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item|\Magento\Sales\Model\Order\Item $item
     * @param array $dimensions (width, height)
     * @param boolean $excludeHumanView
     * @return array
     */
    public function getDesignImages($item,
                                    $dimensions = [],
                                    $excludeHumanView = false) {
        $images = [];
        $this->_dataHelper->setWebservicesUrl();

        if (!isset($dimensions['width'])) {
            $dimensions['width'] = 70;
        }
        if (!isset($dimensions['height'])) {
            $dimensions['height'] = 70;
        }

        $product = $item->getProduct();
        if ($product) {
            $this->_galleryReader->execute($product);
            /** @var \Magento\Framework\Data\Collection $productImages */
            $productImages = $product->getMediaGalleryImages();

            if (count($productImages) === 0) {
                return $this->getCachedImages($item, $dimensions);
            }

            $imageIds = [];
            foreach ($productImages as $_image) {
                $imageIds[] = $_image->getValueId();
            }

            $assignedViews = $this->_assignedViewRepository->getByGallery($imageIds);
            foreach ($assignedViews as $view) {               
                if ($excludeHumanView and $view->getViewType() == 'HUMAN') {                    
                    continue;
                }
                
                $assignedViewId = $view->getAssignedViewId();
                $imageUrl = PublicResource::getOrderedProductImageUrl($item->getShirtplatformId(), $item->getShirtplatformUuid(), $assignedViewId, $dimensions['width'], $dimensions['height']);
                $images[$view->getImageId()] = $imageUrl;
            }
        }

        return $images;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item|\Magento\Sales\Model\Order\Item $item
     * @param array $dimensions
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getCachedImages($item,
                                     array $dimensions) {
        $product = $item->getProduct();
        $connection = $this->resourceConnection->getConnection();
        $cached = $connection->query('SELECT product_id, image_id, assigned_view_id FROM shirtplatform_media_gallery_cache WHERE product_id = ' . $product->getId())->fetchAll();
        $images = [];
        foreach ($cached as $row) {
            $imageUrl = PublicResource::getOrderedProductImageUrl($item->getShirtplatformId(), $item->getShirtplatformUuid(), $row['assigned_view_id'], $dimensions['width'], $dimensions['height']);
            $images[$row['image_id']] = $imageUrl;
        }
        return $images;
    }

    /**
     * Get collection product images
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item|\Magento\Sales\Model\Order\Item $item
     * @param array $dimensions (width, height)
     * @param string $imageId image type
     * @param bool|false $excludeHumanView
     * @return array
     */
    public function getCollectionImages($item,
                                        $dimensions = [],
                                        $imageId = 'small_image',
                                        $excludeHumanView = false)
    {
        $images = [];
        $this->_dataHelper->setWebservicesUrl();

        if (!isset($dimensions['width'])) {
            $dimensions['width'] = 70;
        }
        if (!isset($dimensions['height'])) {
            $dimensions['height'] = 70;
        }

        $mainProduct = $item->getProduct();
        if ($mainProduct) {
            $assignedColorId = $mainProduct->getShirtplatformAssignedColorId();

            if ($item->getHasChildren()) {
                if ($item instanceof \Magento\Sales\Model\Order\Item) {
                    $childenItems = $item->getChildrenItems();
                }
                else {
                    $childenItems = $item->getChildren();
                }

                $childProduct = reset($childenItems)->getProduct();

                if (is_object($childProduct)) {
                    $assignedColorId = $childProduct->getShirtplatformColorId();
                }
            }

            $this->_galleryReader->execute($mainProduct);
            $productImages = $mainProduct->getMediaGalleryImages();
            $imageIds = $params = [];

            if ($item instanceof \Magento\Sales\Model\Order\Item) {
                $options = [];
                $productOptions = $item->getProductOptions();

                if (isset($productOptions['options'])) {
                    $options = $productOptions['options'];
                }
            }
            else {
                $options = $this->_productConfig->getOptions($item);
            }

            //get custom texts
            foreach ($options as $_option) {
                $optionModel = $mainProduct->getOptionById($_option['option_id']);
                if ($optionModel) {
                    $params[] = $optionModel->getSku() . ';text=' . $_option['value'];
                }
            }

            //editable collections
            if (!empty($params))
            {
                foreach ($productImages as $_image) {
                    $imageIds[] = $_image->getValueId();
                }

                $assignedViews = $this->_assignedViewRepository->getByGallery($imageIds);
                $templateProductId = $mainProduct->getShirtplatformId();
                foreach ($assignedViews as $view) {
                    if ($excludeHumanView and $view->getViewType() == 'HUMAN') {
                        continue;
                    }

                    $assignedViewId = $view['assigned_view_id'];
                    $imageUrl = PublicResource::getCollectionProductImageUrl(
                                    $templateProductId, $assignedViewId, $assignedColorId, null, $dimensions['width'], $dimensions['height']
                    );

                    $imageUrlParts = explode('?', $imageUrl);
                    if (count($imageUrlParts) > 1) {
                        $imageUrl = $imageUrlParts[0] . '/elements/' . join('/', $params);

                        for ($i = 1; $i < count($imageUrlParts); $i++) {
                            $imageUrl .= '?' . $imageUrlParts[$i];
                        }
                    }
                    else {
                        $imageUrl .= '/elements/' . join('/', $params);
                    }

                    $images[$view->getImageId()] = $imageUrl;
                }

                if (count($images) === 0) {
                    return $this->getCachedEditableImages($mainProduct, $assignedColorId, $dimensions, $params);
                }
            }
            //designed collections
            else {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $childProduct = $objectManager->create('Magento\Catalog\Model\Product')->load(((isset($childProduct)) ? $childProduct->getId() : $mainProduct->getId()));
                $productImages = $childProduct->getMediaGalleryImages();

                foreach($productImages as $_image)
                {
                    $imageIds[] = $_image->getValueId();
                }

                $imageToView = [];
                foreach($this->_assignedViewRepository->getByGallery($imageIds) as $view)
                {
                    $imageToView[$view->getImageId()] = $view;
                }

                foreach ($productImages as $_image)
                {
                    if(isset($imageToView[$_image->getId()]))
                    {
                        if($excludeHumanView && $imageToView[$_image->getId()]->getViewType() == 'HUMAN')
                        {
                            continue;
                        }
                    }

                    $helper = $this->_catalogImageHelperFactory->create()
                            ->init($mainProduct, $imageId)
                            ->setImageFile($_image->getFile())
                            ->resize($dimensions['width'], $dimensions['height']);
                    $images[$_image->getId()] = $helper->getUrl();
                }

                if (count($images) === 0)
                {
                    return $this->getCachedDesignedImages($mainProduct, $assignedColorId, $dimensions);
                }
            }
        }

        return $images;
    }

    /**
     * @param \Magento\Catalog\Model\Product $mainProduct
     * @param int $assignedColorId
     * @param array $dimensions
     * @param array $params
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getCachedEditableImages($mainProduct,
                                             $assignedColorId,
                                             array $dimensions,
                                             array $params) {
        $connection = $this->resourceConnection->getConnection();
        $cached = $connection->query('SELECT product_id, image_id, assigned_view_id FROM shirtplatform_media_gallery_cache WHERE product_id = ' . $mainProduct->getId())->fetchAll();
        $images = [];
        foreach ($cached as $row) {
            $imageUrl = PublicResource::getCollectionProductImageUrl(
                            $mainProduct->getShirtplatformId(), $row['assigned_view_id'], $assignedColorId, null, $dimensions['width'], $dimensions['height']
            );

            $imageUrlParts = explode('?', $imageUrl);
            if (count($imageUrlParts) > 1) {
                $imageUrl = $imageUrlParts[0] . '/elements/' . join('/', $params);

                for ($i = 1; $i < count($imageUrlParts); $i++) {
                    $imageUrl .= '?' . $imageUrlParts[$i];
                }
            }
            else {
                $imageUrl .= '/elements/' . join('/', $params);
            }

            $images[$row['image_id']] = $imageUrl;
        }
        return $images;
    }

    /**
     * @param \Magento\Catalog\Model\Product $mainProduct
     * @param int $assignedColorId
     * @param array $dimensions
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getCachedDesignedImages($mainProduct,
                                             $assignedColorId,
                                             array $dimensions) {
        $connection = $this->resourceConnection->getConnection();
        $cached = $connection->query('SELECT product_id, image_id, assigned_view_id FROM shirtplatform_media_gallery_cache WHERE product_id = ' . $mainProduct->getId())->fetchAll();
        $images = [];
        foreach ($cached as $row) {
            $imageUrl = PublicResource::getCollectionProductImageUrl(
                            $mainProduct->getShirtplatformId(), $row['assigned_view_id'], $assignedColorId, null, $dimensions['width'], $dimensions['height']
            );
            $images[$row['image_id']] = $imageUrl;
        }
        return $images;
    }

    /**
     * Retrieve default magento product image
     *
     * @access public
     * @param \Magento\Sales\Model\Order\Item $item
     * @param string $imageId
     * @param array $attributes
     * @return string image HTML
     */
    public function getDefaultMagentoImage($item,
                                           $imageId,
                                           $attributes = []) {
        $product = $item->getProduct();
        if ($product) {
            return $this->_imageBuilder->setProduct($product)
                            ->setImageId($imageId)
                            ->setAttributes($attributes)
                            ->create()
                            ->toHtml();
        }

        return '';
    }

}
