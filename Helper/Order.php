<?php

namespace Shirtplatform\Core\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Registry;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Shirtplatform\Core\Callback\Webservice\EventHandler\DatabaseLogger;
use Magento\Sales\Api\Data\OrderAddressInterface;
use shirtplatform\entity\collection\DesignedCollectionProduct;
use shirtplatform\entity\enumerator\OrderFinancialStatus;
use shirtplatform\entity\order\DesignedOrderedProduct;
use shirtplatform\entity\order\Order as ShirtplatformOrder;
use shirtplatform\constants\WsConstants;
use shirtplatform\filter\WsParameters;
use shirtplatform\rest\REST;
use Shirtplatform\Core\Model\Config\Source\ProductType;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Framework\Filesystem;

class Order extends \BrainIndustries\Core\Helper\Data
{
    /**
     * @var Cart
     */
    private $_cartHelper;

    /**
     * @var Registry
     */
    private $_coreRegistry;

    /**
     * @var ExtensibleDataObjectConverter
     */
    private $_dataObjectConverter;  

    /**
     * @var Data
     */
    private $_coreHelper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $_shipmentRepository;

    /**
     * @param Context $context
     * @param \Psr\Log\LoggerInterface $errorLogger
     * @param Filesystem $filesystem
     * @param \Psr\Log\LoggerInterface $logger
     * @param TransportInterface $mailTransport
     * @param Registry $registry
     * @param Cart $cartHelper
     * @param ExtensibleDataObjectConverter $dataObjectConverter
     * @param DatabaseLogger $platformCallback
     * @param Data $coreHelper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param OrderRepositoryInterface $orderRepository
     * @param ShipmentRepositoryInterface $shipmentRepository     
     */
    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $errorLogger,
        Filesystem $filesystem,
        \Psr\Log\LoggerInterface $logger,
        TransportInterface $mailTransport,
        Registry $registry,
        Cart $cartHelper,
        ExtensibleDataObjectConverter $dataObjectConverter,
        DatabaseLogger $platformCallback,
        Data $coreHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        OrderRepositoryInterface $orderRepository,
        ShipmentRepositoryInterface $shipmentRepository        
    ) {
        parent::__construct($context, $errorLogger, $filesystem, $logger, $mailTransport);
        $this->_coreRegistry = $registry;
        $this->_cartHelper = $cartHelper;
        $this->_dataObjectConverter = $dataObjectConverter;
        $this->_coreHelper = $coreHelper;
        $this->_objectManager = $objectManager;
        $this->_orderRepository = $orderRepository;
        $this->_shipmentRepository = $shipmentRepository;        

        if (WsConstants::$eventHandler == null) {
            WsConstants::$eventHandler = $platformCallback;
        }
    }

    /**
     * Get shirtplatform collection products from their IDs
     * 
     * @acess private
     * @param array $productIds
     * @return array
     */
    private function _getShirtplatformCollectionProducts(array $productIds)
    {
        $wsParams = new WsParameters();
        $wsParams->addExpressionContain('id', $productIds);
        return DesignedCollectionProduct::findAllByShop($wsParams);
    }

    /**
     * Create shirtplatform designed collection product with attributes from magento order item
     * 
     * @access public
     * @param \shirtplatform\entity\collection\DesignedCollectionProduct $designedCollectionProduct
     * @param \Magento\Sales\Model\Order\Item $magentoOrderItem
     * @param \shirtplatform\entity\order\Order $platformOrder
     * @return DesignedOrderedProduct
     */
    public function createShirtplatformOrderedCollectionProduct(
        $designedCollectionProduct,
        $magentoOrderItem,
        $platformOrder
    ) {
        $assignedColorId = $designedCollectionProduct->color->id;
        $assignedSizeId = $designedCollectionProduct->size->id;

        //maybe this is not even needed as the above $designedCollectionProduct should already return assigned color and size IDs
        if ($magentoOrderItem->getHasChildren()) {
            $childenItems = $magentoOrderItem->getChildrenItems();
            $child = reset($childenItems)->getProduct();

            if (is_object($child)) {
                $assignedColorId = $child->getShirtplatformColorId();
                $assignedSizeId = $child->getShirtplatformSizeId();
            }
        }

        $customOptions = [];
        $productOptions = $magentoOrderItem->getProductOptions();

        if (isset($productOptions['options'])) {
            foreach ($productOptions['options'] as $option) {
                $customOptions[] = [
                    'name' => $option['label'],
                    'text' => $option['value']
                ];
            }
        }

        return DesignedOrderedProduct::createCustomizedOrderedProduct(
            $platformOrder->getShopId(),
            $platformOrder->id,
            $designedCollectionProduct->id,
            $assignedColorId,
            $assignedSizeId,
            (int) $magentoOrderItem->getQtyOrdered(),
            $customOptions
        );
    }

    /**
     * Move designed ordered products from their original orders (created when 
     * added to cart by creator) to the current platform order.
     * 
     * @access public
     * @param array $designedOrderItems order items
     * @param \shirtplatform\entity\order\Order $platformOrder
     * @param array $mapper
     */
    public function moveDesignedProductsToOrder(
        $designedOrderItems,
        $platformOrder,
        $mapper
    ) {
        $designedProductIds = [];

        foreach ($designedOrderItems as $orderItem) {
            if (isset($mapper[$orderItem->getShirtplatformId()])) {
                $origOrderId = $mapper[$orderItem->getShirtplatformId()];

                if ($origOrderId != $platformOrder->id) {
                    $designedProductIds[$origOrderId][] = $orderItem->getShirtplatformId();
                }
            }
        }

        if (count($designedProductIds)) {
            foreach ($designedProductIds as $origOrderId => $itemIds) {
                $this->logMessage('transfering products ' . join(', ', $itemIds) . ' from ' . $origOrderId . ' to ' . $platformOrder->id);
                ShirtplatformOrder::transfer($origOrderId, $platformOrder->id, null, false, $itemIds);
                $this->_checkLastResponse('transferring products');

                if ($this->_didExceptionOccur()) {
                    $this->logMessage('could not transfer products ' . join(', ', $itemIds) . ' from ' . $origOrderId . ' to ' . $platformOrder->id);
                }
            }
        } else {
            $this->logMessage('no products were moved');
        }        
    }

    private function _checkLastResponse($after = '')
    {
        if (WsConstants::$eventHandler and method_exists(WsConstants::$eventHandler, 'getLastResponse')) {
            $response = WsConstants::$eventHandler->getLastResponse();
            $this->logMessage('last response after ' . $after);
            $this->logMessage($response);
        }
    }

    private function _didExceptionOccur()
    {
        if (WsConstants::$eventHandler and method_exists(WsConstants::$eventHandler, 'didExceptionOccur')) {
            return WsConstants::$eventHandler->didExceptionOccur();
        }

        return false;
    }

        
    /**
     * Create product duplicates in session order
     *
     * @access private
     * @param int[] $deletedPlatformProductIds IDs of deleted designed order items
     * @param \shirtplatform\entity\order\Order $platformOrder session order
     * @param \Magento\Sales\Model\Order\Item[] order items
     * @return void
     */
    private function _createProductDuplicatesInSessionOrder($deletedPlatformProductIds, $platformOrder, $orderItems)
    {
        if (empty($deletedPlatformProductIds)) {
            return;
        }

        //group deleted platform products by their order ID
        $mapper = $deletedProducts = [];
        $param = new WsParameters();
        $param->addExpressionContain('id', $deletedPlatformProductIds);
        $param->appendDefaultFilter = false;
        $deletedPlatformProducts = \shirtplatform\entity\overview\DesignedOrderedProduct::findAll($param);

        foreach ($deletedPlatformProducts as $product) {
            $mapper[$product->orderId][] = $product->id;
        }        

        foreach ($mapper as $orderId => $productIds) {
            $wsParams = new WsParameters();
            $wsParams->addExpressionContain('id', $productIds);
            $wsParams->addSimpleExpression('deleted', '=', true);
            $deletedProducts[$orderId] = &DesignedOrderedProduct::findAll([$orderId], $wsParams, null, true);
        }    

        REST::getInstance()->wait();

        //create duplicates of deleted products in session order
        foreach ($deletedProducts as $orderId => $designedProducts) {
            foreach ($designedProducts as $product) {
                $origProductId = $product->id;
                $product->id = null;
                $product->uuid = null;
                $product->setParents($platformOrder->id);
                $product->__update();                
                $this->_setShirtplatformDataToOrderItem($orderItems, $origProductId, $product);
                $msg = 'Created duplicate product ' . $product->id . ' of original ' . $origProductId . ' in session order ' . $platformOrder->id;
                $this->logMessage($msg);
            }
        }
    }
    
    /**
     * Set shirtplatform_id and shirtplatform_uuid to order item (after it has been duplicated)
     *
     * @access private
     * @param \Magento\Sales\Model\Order\Item[] $orderItems
     * @param int $origShirtplatformId
     * @param shirtplatform\entity\order\DesignedOrderedProduct $product
     * @return void
     */
    private function _setShirtplatformDataToOrderItem($orderItems, $origShirtplatformId, $product)
    {
        foreach ($orderItems as $item) {
            if ($item->getShirtplatformId() == $origShirtplatformId) {
                $item->setShirtplatformId($product->id);
                $item->setShirtplatformUuid($product->uuid);
                break;
            }
        }
    }

    /**
     * Create shirtplatform order with products that are in magento order
     * 
     * @access public
     * @param \Magento\Sales\Model\Order $order     
     * @param boolean $commitOrder 
     * @return \shirtplatform\entity\order\Order|null
     */
    public function createPlatformOrder($order, $commitOrder = true)
    {
        $orderItems = $order->getAllItems();
        $storeViewId = $order->getStoreId();
        $isPlatformOrder = false;
        $platformOrder = null;

        if (count($orderItems)) {
            //These arrays contain order items divided into collection products (to create or move) and base products             
            $collectionItemsToAdd = $collectionItemsToMove = $shirtplatformDesignedItems = $shirtplatformItemsToCreate = [];
            //IDs of all designed ordered products (shirtplatform_id of all order items)
            $itemShirtplatformIds = [];

            $customDesignItems = [];

            foreach ($orderItems as $_orderItem) {
                $product = $_orderItem->getProduct();

                if ($_orderItem->getShirtplatformId() and $product->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                    $isPlatformOrder = true;
                    $shirtplatformDesignedItems[$_orderItem->getShirtplatformId()] = $_orderItem;
                } elseif ($product->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT) {

                    if ($_orderItem->getShirtplatformId()) {
                        $isPlatformOrder = true;
                        $collectionItemsToMove[$_orderItem->getShirtplatformId()] = $_orderItem;
                    } else {
                        $productShirtplatformId = $product->getShirtplatformId();

                        if (isset($productShirtplatformId)) {
                            $isPlatformOrder = true;
                            $collectionItemsToAdd[$productShirtplatformId][] = $_orderItem;
                        }
                    }
                } elseif ($_orderItem->getShirtplatformProductType() == ProductType::CUSTOMDESIGN_PRODUCT) {
                    $isPlatformOrder = true;
                    $customDesignItems[] = $_orderItem;
                } elseif ($product->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                    $isPlatformOrder = true;
                    $shirtplatformItemsToCreate[] = $_orderItem;
                }

                if ($_orderItem->getShirtplatformId()) {
                    $itemShirtplatformIds[] = $_orderItem->getShirtplatformId();
                }
            }

            if ($isPlatformOrder) {
                try {
                    $this->_coreHelper->shirtplatformAuth($storeViewId);
                    $platformOrder = $this->getShirtplatformSessionOrder($storeViewId);
                    $orderedProducts = $platformOrder->getOrderedProducts();
                    $currentSessionOrderProductIds = []; //just for debugging purposes

                    //delete platform products that don't belong to this magento order
                    if ($orderedProducts) {
                        foreach ($orderedProducts as $product) {
                            $currentSessionOrderProductIds[] = $product->id;

                            if (!in_array($product->id, $itemShirtplatformIds)) {
                                $product->__delete();
                                $this->logMessage('deleted platform product ' . $product->id . ' in createPlatformOrder');                                
                            }
                        }
                    }

                    $this->logMessage('session order id: ' . $platformOrder->id);
                    $this->logMessage('session order products before deleting');
                    $this->logMessage($currentSessionOrderProductIds);

                    //get mapper of platformDesignedProductId => platformOrderId
                    $mapper = [];
                    if (!empty($itemShirtplatformIds)) {
                        $param = new WsParameters();
                        $param->addExpressionContain('id', $itemShirtplatformIds);

                        $platformProducts = \shirtplatform\entity\overview\DesignedOrderedProduct::findAll($param);
                        if ($platformProducts) {
                            foreach ($platformProducts as $product) {
                                $mapper[$product->id] = $product->orderId;
                            }
                        }
                    }

                    $this->logMessage('mapper data');
                    $this->logMessage($mapper);

                    //create designed collection items in shirtplatform
                    if (count($collectionItemsToAdd)) {
                        $shirtplatformProducts = $this->_getShirtplatformCollectionProducts(array_keys($collectionItemsToAdd));

                        foreach ($collectionItemsToAdd as $id => $orderItemList) {
                            foreach ($orderItemList as $_orderItem) {
                                if (array_key_exists($id, $shirtplatformProducts)) {
                                    $shirtplatformProduct = $shirtplatformProducts[$id];
                                    $shirtplatformOrderedProduct = $this->createShirtplatformOrderedCollectionProduct(
                                        $shirtplatformProduct,
                                        $_orderItem,
                                        $platformOrder
                                    );
                                    $_orderItem->setShirtplatformId($shirtplatformOrderedProduct->id);
                                }
                            }
                        }
                    }
                                        
                    $allPlatformOrderIds = $deletedPlatformProductIds = [];
                    foreach ($mapper as $orderId) {
                        if (!in_array($orderId, $allPlatformOrderIds)) {
                            $allPlatformOrderIds[] = $orderId;
                        }
                    }

                    foreach ($itemShirtplatformIds as $id) {
                        if (!in_array($id, array_keys($mapper))) {
                            $deletedPlatformProductIds[] = $id;
                        }
                    }
                                        
                    //find if there are some commited orders with any of the products that are gonna be moved => products cannot be moved from already commited orders
                    //we'll create duplicates in the session order instead                
                    if (!empty($allPlatformOrderIds)) {
                        $wsParams = new WsParameters();
                        $wsParams->addExpressionContain('id', $allPlatformOrderIds);
                        $wsParams->addExpressionContain('financialStatus', [OrderFinancialStatus::PENDING, OrderFinancialStatus::PAID]);
                        $commitedOrders = ShirtplatformOrder::findAll($wsParams);

                        if (!empty($commitedOrders)) {
                            foreach ($commitedOrders as $comOrder) {
                                $orderedProducts = $comOrder->getOrderedProducts();
        
                                foreach ($orderedProducts as $designedProduct) {
                                    if (!isset($mapper[$designedProduct->id])) {                    
                                        continue;
                                    }
                                                                                    
                                    //create duplicate in the session order
                                    $origProductId = $designedProduct->id;
                                    $designedProduct->id = null;
                                    $designedProduct->uuid = null;
                                    $designedProduct->setParents($platformOrder->id);
                                    $designedProduct->__update();
                                    unset($mapper[$origProductId]);
                                    unset($shirtplatformDesignedItems[$origProductId]);
                                    unset($collectionItemsToMove[$origProductId]);
                                    $this->_setShirtplatformDataToOrderItem($orderItems, $origProductId, $designedProduct);
                                    $msg = 'Designed product ' . $designedProduct->id . ' was created in session order ' . $platformOrder->id . ', because original product ' . 
                                        $origProductId . ' was in commited order ' . $comOrder->id;
                                    $this->logMessage($msg);
                                }
                            }
                        }                    
                    }

                    //move collection items to the current platform order
                    if (count($collectionItemsToMove)) {
                        $this->moveDesignedProductsToOrder($collectionItemsToMove, $platformOrder, $mapper);
                    }

                    //move creator products to the current platform order
                    if (count($shirtplatformDesignedItems)) {
                        $this->moveDesignedProductsToOrder($shirtplatformDesignedItems, $platformOrder, $mapper);
                    }
                    
                    if (count($customDesignItems)) {
                        if ($this->_moduleManager->isEnabled('Shirtplatform_CustomDesigns')) {                        
                            $customDesignHelper = $this->_objectManager->get(\Shirtplatform\CustomDesigns\Helper\Data::class);
                            $customDesignRepository = $this->_objectManager->get(\Shirtplatform\CustomDesigns\Model\CustomDesignRepository::class);
                            $shopId = $this->scopeConfig->getValue('shirtplatform/general/store', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeViewId);
                            foreach ($customDesignItems as $item){
                                $designId = $item->getBuyRequest()->getData('customdesign_design_id');
                                $designColor = $item->getBuyRequest()->getData('customdesign_color_id');
                                $designSize = $item->getBuyRequest()->getData('customdesign_size_id');                                
                                $design = $customDesignRepository->getById($designId);
                                $design->setAssignedColorId($designColor);
                                $design->setAssignedSizeId($designSize);
                                $seDesign = json_decode($customDesignHelper->getDesignJson($design, $item->getQtyOrdered()), true);
                                $platformCustomDesignProduct = DesignedOrderedProduct::createUsingCreatorSE($shopId, $platformOrder->id, $seDesign);
                                $item->setShirtplatformId($platformCustomDesignProduct->id);
                            }
                        }
                        else {
                            $subject = 'Check order ' . $order->getIncrementId();
                            $message = 'There are custom design products in Magento. However the module is not enabled in the system ' 
                                . 'and therefore the products were not created in platform';
                            $this->logError($message);
                            $this->_coreHelper->sendEmailToTechSupport($subject, $message);   
                        }
                    }                    

                    //$this->sendEmailToAdmin('Setting shirtplatform id', '#' . $order->getId() . ' => #' . $platformOrder->id);
                    $this->_createProductDuplicatesInSessionOrder($deletedPlatformProductIds, $platformOrder, $orderItems);                    
                    $order->setShirtplatformId($platformOrder->id);

                    if (count($shirtplatformItemsToCreate)){
                        foreach ($shirtplatformItemsToCreate as $_key => $_item){
                            $platformItem = $this->createPlatformOrderItem($_item, $platformOrder, $order->getStoreId());
                            if ($platformItem !== false){
                                $shirtplatformItemsToCreate[$_key]->setShirtplatformId($platformItem->id);
                                $shirtplatformItemsToCreate[$_key]->setShirtplatformUuid($platformItem->uuid);
                            }
                        }
                    }

                    //set increment_id as uniqueId
                    $platformOrder->uniqueId = $order->getIncrementId();
                    $platformOrder->__update();

                    if ($commitOrder) {
                        \shirtplatform\entity\order\Order::commit($platformOrder->id, OrderFinancialStatus::PENDING);
                        $this->logMessage('commiting order ' . $platformOrder->id . ' as pending');                        
                        $this->_checkLastResponse('commiting order');                        
                    }
                } catch (\Exception $ex) {
                    $subject = 'Error when creating platform order';
                    $message = 'Some error occured when trying to create platform order. '
                        . 'Please check out the order details. Magento order number: ' . $order->getIncrementId();

                    if (isset($platformOrder)) {
                        $message .= ', current platform order id: ' . $platformOrder->id;
                    }

                    $message .= ".\n\n Error message: " . $ex->getMessage();
                    $this->logError($message);
                    $this->_coreHelper->sendEmailToTechSupport($subject, $message);
                }
            }
        }

        return $platformOrder;
    }    
    
    public function createPlatformOrderItem($orderItem, $platformOrder, $storeId){
        $shopId = $this->scopeConfig->getValue('shirtplatform/general/store', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if ($orderItem->getHasChildren()) {
            $childenItems = $orderItem->getChildrenItems();
            $child = reset($childenItems)->getProduct();

            if (is_object($child)) {
                $assignedColorId = $child->getShirtplatformColorId();
                $assignedSizeId = $child->getShirtplatformSizeId();
                $productId = preg_replace('/(^\D+|-\d+)/', '', $child->getSku());
                return DesignedOrderedProduct::createFromBase($shopId, $platformOrder->id, $productId , $assignedColorId, $assignedSizeId, (int)$orderItem->getQtyOrdered());
            }
        }
        return false;
    }

    /**
     * Create duplicate of platform order
     * 
     * @access public
     * @param \Magento\Sales\Model\Order $magentoOrder
     * @param array shirtplatform array mapping id => qty of items that should be in the duplicite order.
     * Sometimes we want to have only some items, e.g. exchange order
     * @return array that maps old designed product ID to new product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createPlatformOrderDuplicate(
        $magentoOrder,
        $initOnlyItems = []
    ) {
        $storeId = $magentoOrder->getStoreId();
        $this->_coreHelper->shirtplatformAuth($storeId);
        $oldOrder = ShirtplatformOrder::find($magentoOrder->getShirtplatformId());

        if (!$oldOrder or !$oldOrder->id) {
            $this->logError('Could not find platform order ' . $magentoOrder->getShirtplatformId() . ' when creating order duplicate');
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Platform order not found'));
        }

        //find shirtplatform items already in cart
        $shirtplatformIdsInCart = [];
        $quote = $this->_cartHelper->getQuote();
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            if ($quoteItem->getShirtplatformId()) {
                $shirtplatformIdsInCart[] = $quoteItem->getShirtplatformId();
            }
        }

        //get session order/create new order and delete products that don't belong to this order if it has some
        $newOrder = $this->getShirtplatformSessionOrder($storeId);
        if ($newOrder->getOrderedProducts()) {
            foreach ($newOrder->getOrderedProducts() as $product) {
                if (!in_array($product->id, $shirtplatformIdsInCart)) {
                    $product->__delete();                    
                }
            }
        }

        $this->_coreRegistry->register('session_platform_order', $newOrder);
        $orderedProducts = $oldOrder->getOrderedProducts();
        $itemMapper = [];

        foreach ($orderedProducts as $id => $designedProduct) {
            if (empty($initOnlyItems) or isset($initOnlyItems[$id])) {
                $designedProduct->id = null;
                $designedProduct->uuid = null;

                if (isset($initOnlyItems[$id])) {
                    $designedProduct->amount = $initOnlyItems[$id];
                }

                $designedProduct->setParents($newOrder->id);
                $designedProduct->__update();
                $itemMapper[$id] = $designedProduct;
            }
        }

        $this->_coreRegistry->register('platform_order_item_mapper', $itemMapper);
        return $itemMapper;
    }

    /**
     * Get current session order or create a new session order
     * 
     * @access public
     * @param int $storeId
     * @return Order
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getShirtplatformSessionOrder($storeId)
    {
        $countryId = $this->scopeConfig->getValue('shirtplatform/general/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        $this->_coreHelper->shirtplatformAuth($storeId);

        //find platform order by session
        $platformOrder = ShirtplatformOrder::findBySession();
        if ($platformOrder and $platformOrder->id) {
            //$this->sendEmailToAdmin('FindBySession order', '#' . $platformOrder->id . ', financialStatus: ' . $platformOrder->financialStatus);

            if ($platformOrder->deleted) {
                $platformOrder->__undelete();
                //$this->sendEmailToAdmin('Undeleting order', '#' . $platformOrder->id);
            }

            //$this->sendEmailToAdmin('Using order from findBySession', '#' . $platformOrder->id);
            return $platformOrder;
        }

        $platformOrder = ShirtplatformOrder::createBySession($countryId);
        $this->logMessage('created session order in Order Helper. Platform order id: ' . $platformOrder->id);        

        if (!isset($platformOrder->id)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Could not create shirtplatform order'));
        }

        //$this->sendEmailToAdmin('Using order from createBySession', '#' . $platformOrder->id);
        return $platformOrder;
    }

    /**
     * Create platform order duplicate and set quote_items attributes
     * 
     * @access public
     * @param \Magento\Sales\Model\Order\Item $order
     * @param \Magento\Quote\Model\Quote\Item $quote
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function initAdminShirtplatformOrder($order, $quote)
    {
        if ($order->getShirtplatformId()) {
            $initOnlyItems = [];

            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                if ($quoteItem->getShirtplatformId()) {
                    $initOnlyItems[$quoteItem->getShirtplatformId()] = $quoteItem->getQty();
                }
            }

            $itemMapper = $this->createPlatformOrderDuplicate($order, $initOnlyItems);
            $platformOrder = $this->_coreRegistry->registry('session_platform_order');

            if ($platformOrder == null) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Platform order has not been initialized yet'));
            }

            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $itemShirtplatformId = $quoteItem->getShirtplatformId();

                if ($itemShirtplatformId and isset($itemMapper[$itemShirtplatformId])) {
                    $quoteItem->setShirtplatformId($itemMapper[$itemShirtplatformId]->id);
                    $quoteItem->setShirtplatformOrigOrderId($platformOrder->id);
                    $quoteItem->setShirtplatformUuid($itemMapper[$itemShirtplatformId]->uuid);
                }
            }
        }
    }

    /**
     * Update designed product amount in platform when quantities in magento 
     * cart are changed
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $productInfo
     */
    public function updateDesignedProductQty($quote, $productInfo)
    {
        $platformProducts = $platformQuoteItems = [];

        foreach ($productInfo as $itemId => $itemInfo) {
            $quoteItem = $quote->getItemById($itemId);

            if ($quoteItem and $quoteItem->getShirtplatformId()) {
                $platformQuoteItems[$itemId] = $quoteItem;
            }
        }

        //get platform products
        if (!empty($platformQuoteItems)) {
            $this->_coreHelper->shirtplatformAuth($quote->getStoreId());

            foreach ($platformQuoteItems as $quoteItem) {
                $shirtplatformId = $quoteItem->getShirtplatformId();
                $parentId = $quoteItem->getShirtplatformOrigOrderId();
                $platformProducts[$quoteItem->getId()] = &DesignedOrderedProduct::find($shirtplatformId, $parentId, null, true);
            }

            \shirtplatform\rest\REST::getInstance()->wait();
        }

        //update amounts
        if (!empty($platformProducts)) {
            foreach ($platformProducts as $quoteItemId => $shirtProduct) {
                $quoteItem = $platformQuoteItems[$quoteItemId];

                if ((int) $quoteItem->getQty() != $shirtProduct->amount) {
                    $shirtProduct->amount = (int) $quoteItem->getQty();
                    $shirtProduct->__update();
                }
            }
        }
    }

    /**
     * Checks if order shipping and billing addresses are equal.
     *
     * @access public
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function areAddressesEqual($order)
    {
        $shippingAddress = $order->getShippingAddress();
        $billingAddress = $order->getBillingAddress();
        $shippingData = $this->_dataObjectConverter->toFlatArray($shippingAddress, [], OrderAddressInterface::class);
        $billingData = $this->_dataObjectConverter->toFlatArray($billingAddress, [], OrderAddressInterface::class);
        unset(
            $shippingData['address_type'],
            $shippingData['entity_id'],
            $shippingData['vat_is_valid'],
            $shippingData['vat_request_id'],
            $shippingData['vat_request_date'],
            $shippingData['vat_request_success']
        );
        unset(
            $billingData['address_type'],
            $billingData['entity_id'],
            $billingData['vat_is_valid'],
            $billingData['vat_request_id'],
            $billingData['vat_request_date'],
            $billingData['vat_request_success']
        );

        foreach ($shippingData as $key => $val) {
            if (empty($val)) {
                unset($shippingData[$key]);
            }
        }

        foreach ($billingData as $key => $val) {
            if (empty($val)) {
                unset($billingData[$key]);
            }
        }

        return $shippingData == $billingData;
    }

    /**
     * @param int $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrderById($orderId){
        return $this->_orderRepository->get($orderId);
    }

    /**
     * @param int $shipmentId
     * @return \Magento\Sales\Api\Data\ShipmentInterface
     */
    public function getShipmentById($shipmentId){
        return $this->_shipmentRepository->get($shipmentId);
    }
}
