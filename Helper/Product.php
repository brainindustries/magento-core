<?php

namespace Shirtplatform\Core\Helper;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProduct;
use Magento\AsynchronousOperations\Model\ResourceModel\Operation as OperationResource;
use Magento\Framework\Serialize\SerializerInterface;

class Product extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SHIRTPLATFORM_ATTRIBUTESET_NAME = 'ShirtPlatform Product';

    /**
     * @var \Magento\Eav\Api\AttributeRepositoryInterface
     */
    private $_attributeRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_shirtplatformHelper;

    /**     
     * @var \Magento\Swatches\Helper\Data
     */
    private $_swatchHelper;

    /**
     * @var \Magento\Swatches\Helper\Media
     */
    private $_swatchMediaHelper;

    /**
     * @var OperationResource
     */
    private $_operationResource;

    /**
     * @var SerializerInterface
     */
    private $_serializer;
    
    /**
     * @access public
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Shirtplatform\Core\Helper\Data $shirtplatformHelper,
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param \Magento\Swatches\Helper\Media $swatchMediaHelper
     * @param \Magento\Framework\App\Helper\Context $context
     * @param OperationResource $operationResource
     * @param SerializerInterface $serializer
     * @return void
     */
    public function __construct(
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Shirtplatform\Core\Helper\Data $shirtplatformHelper,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magento\Swatches\Helper\Media $swatchMediaHelper,
        \Magento\Framework\App\Helper\Context $context,
        OperationResource $operationResource,
        SerializerInterface $serializer
    ) {
        parent::__construct($context);
        $this->_attributeRepository = $attributeRepository;
        $this->_productRepository = $productRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_shirtplatformHelper = $shirtplatformHelper;
        $this->_swatchHelper = $swatchHelper;
        $this->_swatchMediaHelper = $swatchMediaHelper;
        $this->_operationResource = $operationResource;
        $this->_serializer = $serializer;
    }
    
    /**
     * Get list of products by their shirtplatform_id values
     *
     * @access public
     * @param int[] $platformProductIds
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    public function getProductsByShirtplatformId($platformProductIds)
    {
        $this->_searchCriteriaBuilder->addFilter('shirtplatform_id', $platformProductIds, 'in');
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        return $this->_productRepository->getList($searchCriteria)->getItems();
    }

    /**
     * Get swatches data used in product listing for configurable products. If
     * $attributeCode is set, only swatches for this product attribute are returned
     * 
     * @access public
     * @param \Magento\Catalog\Model\Product $product
     * @param string $attributeCodeFilter product attribute code used as optional filter
     * @return array
     */
    public function getSwatchesForListing($product, $attributeCodeFilter = null)
    {
        $swatchesData = [];
        $swatchAttributes = $this->_swatchHelper->getSwatchAttributesAsArray($product);

        //filter only swatches which are used in product listing
        foreach ($swatchAttributes as $attributeId => $attribute) {
            if (empty($attribute['used_in_product_listing'])) {
                unset($swatchAttributes[$attributeId]);
            }
        }

        if ($product->getTypeId() == ConfigurableProduct::TYPE_CODE) {
            $optionIdsForAttributes = [];
            $typeInstance = $product->getTypeInstance();

            foreach ($typeInstance->getUsedProducts($product) as $variant) {
                if ($variant->isSaleable()) {
                    foreach ($typeInstance->getConfigurableAttributes($product) as $attribute) {
                        $productAttribute = $attribute->getProductAttribute();
                        $productAttributeId = $productAttribute->getId();

                        if (isset($swatchAttributes[$productAttributeId])) {
                            if (is_null($attributeCodeFilter) or $productAttribute->getAttributeCode() == $attributeCodeFilter) {
                                $attributeCode = $productAttribute->getAttributeCode();
                                $optionIdsForAttributes[$attributeCode][$variant->getData($attributeCode)] = 1;
                            }
                        }
                    }
                }
            }

            if (!empty($optionIdsForAttributes)) {
                foreach ($optionIdsForAttributes as $attributeCode => $optionIds) {
                    $swatchesData[$attributeCode] = $this->_swatchHelper->getSwatchesByOptionsId(array_keys($optionIds));
                }
            }
        }

        return $swatchesData;
    }

    /**
     * Get available product sizes
     * 
     * @access public
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getProductSizes($product)
    {
        $sizes = [];

        if ($product->getTypeId() == ConfigurableProduct::TYPE_CODE) {
            $typeInstance = $product->getTypeInstance();

            foreach ($typeInstance->getConfigurableOptions($product) as $options) {
                foreach ($options as $configuration) {
                    if ($configuration['attribute_code'] == 'size') {
                        $sizes[] = $configuration['option_title'];
                    }
                }
            }
        }

        return array_unique($sizes);
    }
    
    /**
     * Get option_id from color name
     *
     * @access private
     * @param array $colorOptions
     * @param string $colorName
     * @return int|false if not found
     */
    private function _getOptionIdFromColorName($colorOptions, $colorName) {
        foreach ($colorOptions as $option) {
            if ($option['label'] == $colorName) {
                return $option['value'];
            }
        }

        //sometimes the color attributes have duplicite values with different letter case, e.g. Blue and blue
        //it should be fixed during imports, but for now we iterate over the values one more time and check
        //only lower case values
        foreach ($colorOptions as $option) {
            if (strtolower($option['label']) == strtolower($colorName)) {
                return $option['value'];
            }
        }

        return false;
    }

    /**
     * Get available product colors and sizes
     * 
     * @access public
     * @param int $storeId
     * @param int $platformProductId
     * @return array
     */
    public function getAvailableProductColorsAndSizes($storeId, $platformProductId){
        $result = $colorAttrOptions = [];        
        $this->_shirtplatformHelper->shirtplatformAuth($storeId);
        $platformProduct = \shirtplatform\entity\product\Product::find($platformProductId);        
        $colorAttribute = $this->_attributeRepository->get('catalog_product', 'color');
        $isSwatchAttribute = $this->_swatchHelper->isVisualSwatch($colorAttribute);

        if ($isSwatchAttribute) {
            $magentoProducts = $this->getProductsByShirtplatformId([$platformProductId]);
            
            if (count($magentoProducts)) {
                $mProduct = reset($magentoProducts);
                $colorAttrOptions = $colorAttribute->getSource()->getAllOptions(false, true);
                $swatches = $this->getSwatchesForListing($mProduct, 'color');

                if (isset($swatches['color'])) {
                    $colorSwatches = $swatches['color'];
                }
            }
        }

        foreach ($platformProduct->getSku() as $sku){
            if ($sku['available'] == 'TRUE'){
                $colorId = $sku->assignedColor->productColor->parents[1];
                $colorName = $sku->assignedColor->productColor->name;
                $colorOptionId = $this->_getOptionIdFromColorName($colorAttrOptions, $colorName);
                $sizeId = $sku->assignedSize->productSize->parents[1];
                $sizeName = $sku->assignedSize->productSize->name;

                $result[$colorId]['name'] = $colorName; 
                $result[$colorId]['sizes'][$sizeId] = $sizeName; 

                if ($isSwatchAttribute && $colorOptionId && isset($colorSwatches[$colorOptionId]) && strpos($colorSwatches[$colorOptionId]['value'], '/') == 0) {
                    $result[$colorId]['swatchUrl'] = $this->_swatchMediaHelper->getSwatchMediaUrl() . $colorSwatches[$colorOptionId]['value'];
                }
            }
        }

        return $result;
    }

    /**
     * @access public
     * @param string $timeString
     * @return array
     */
    public function getAsyncImportErrors($timeString = '-1 hour')
    {
        $result = [];

        $connection = $this->_operationResource->getConnection();
        $select = $connection->select()
            ->from('magento_operation')
            ->where('started_at >= ?', date('Y-m-d H:i:s', strtotime($timeString)))
            ->where('status = ?', \Magento\Framework\Bulk\OperationInterface::STATUS_TYPE_NOT_RETRIABLY_FAILED)
            ->where('result_message NOT IN (?)', 'The product is already attached.');

        foreach ($connection->fetchAll($select) as $_operation) {
            $productSku = $this->getProductSkuFromOperation($_operation);

            if (!isset($result[$productSku])) {
                $result[$productSku] = [];
            }
    
            $message = $_operation['result_message'];
            if (isset($result[$productSku][$message])) {
                $result[$productSku][$message]++;
            } else {
                $result[$productSku][$message] = 1;
            }
        }

        return $result;
    }

    /**
     * @param array $operation
     * @return string
     */
    public function getProductSkuFromOperation($operation)
    {
        $data = $this->_serializer->unserialize($operation['serialized_data']);
        if (!$data || !isset($data['meta_information'])) {
            return '';
        }
        
        $metaInformation = $this->_serializer->unserialize($data['meta_information']);
        if (isset($metaInformation['product'])) {
            $productSku = $metaInformation['product']['sku'];
        } elseif (isset($metaInformation['sku'])) {
            $productSku = $metaInformation['sku'];
        } else {
            return '';
        }

        if (strpos($productSku, '-')) {
            $productSku = substr($productSku, 0, strpos($productSku, '-'));
        }
        
        return $productSku;
    }
}
