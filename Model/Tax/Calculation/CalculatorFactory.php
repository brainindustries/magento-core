<?php

namespace Shirtplatform\Core\Model\Tax\Calculation;

use Magento\Customer\Api\Data\AddressInterface as CustomerAddress;

class CalculatorFactory extends \Magento\Tax\Model\Calculation\CalculatorFactory {    

    /**
     * Create new calculator
     *
     * @param string $type Type of calculator
     * @param int $storeId
     * @param CustomerAddress $billingAddress
     * @param CustomerAddress $shippingAddress
     * @param null|int $customerTaxClassId
     * @param null|int $customerId
     * @return \Magento\Tax\Model\Calculation\AbstractCalculator
     * @throws \InvalidArgumentException
     */
    public function create(
        $type,
        $storeId,
        CustomerAddress $billingAddress = null,
        CustomerAddress $shippingAddress = null,
        $customerTaxClassId = null,
        $customerId = null
    ) {                        
        switch ($type) {
            case self::CALC_UNIT_BASE:
                $className = \Magento\Tax\Model\Calculation\UnitBaseCalculator::class;
                break;
            case self::CALC_ROW_BASE:
                $className = \Magento\Tax\Model\Calculation\RowBaseCalculator::class;
                break;
            case self::CALC_TOTAL_BASE:
                $className = \Magento\Tax\Model\Calculation\TotalBaseCalculator::class;
                break;
            case \Shirtplatform\Core\Plugin\Tax\Model\System\Config\Source\Algorithm::CALC_TOTAL_BASE_CREATOR_PRICES_EXCL_TAX:
            case \Shirtplatform\Core\Plugin\Tax\Model\System\Config\Source\Algorithm::CALC_TOTAL_BASE_CREATOR_PRICES_INCL_TAX:
                $className = \Shirtplatform\Core\Model\Tax\Calculation\CreatorTotalBaseCalculator::class;
                break;
            default:
                throw new \InvalidArgumentException('Unknown calculation type: ' . $type);
        }                
        
        /** @var \Magento\Tax\Model\Calculation\AbstractCalculator $calculator */
        $calculator = $this->objectManager->create($className, ['storeId' => $storeId]);
        if (null != $shippingAddress) {
            $calculator->setShippingAddress($shippingAddress);
        }
        if (null != $billingAddress) {
            $calculator->setBillingAddress($billingAddress);
        }
        if (null != $customerTaxClassId) {
            $calculator->setCustomerTaxClassId($customerTaxClassId);
        }
        if (null != $customerId) {
            $calculator->setCustomerId($customerId);
        }
        return $calculator;
    }
}