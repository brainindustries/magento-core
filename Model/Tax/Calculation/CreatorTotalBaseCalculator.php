<?php

namespace Shirtplatform\Core\Model\Tax\Calculation;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Tax\Api\Data\AppliedTaxInterfaceFactory;
use Magento\Tax\Api\Data\AppliedTaxRateInterfaceFactory;
use Magento\Tax\Api\Data\QuoteDetailsItemInterface;
use Magento\Tax\Api\Data\TaxDetailsItemInterfaceFactory;
use Magento\Tax\Api\TaxClassManagementInterface;
use Magento\Tax\Model\Calculation;

/**
 * This class was written to handle tax calculations for custom requirements for projects based on 
 * Shirtplatform products. 
 * 
 * The products are imported without tax, but creator shows them with tax included.
 * Therefore during the calculations, we always make sure the calculation of tax is done with 
 * product price including tax - calculateWithTaxInPrice
 * 
 * For shipping (if using calculateWithTaxNotInPrice) - there are changes to make the the final
 * price (including) tax looking nice (e.g. 70 instead of 69.99 or 70.01). Also it's possible to use
 * the base price for calculations to have more than 2 digits (this can be set in Shirtplatform editor).
 * 
 * Calculations for other item types (weee, discount, etc.) are done as in the original Magento code -
 * TotalBaseCalculator
 * 
 * It's a bit complicated as I added the itemType check in the beginning, but I wanted to keep the original
 * Magento calculations for as many items as possible. Those guys probably know what they are doing :)
 */
class CreatorTotalBaseCalculator extends \Magento\Tax\Model\Calculation\TotalBaseCalculator
{    
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Constructor
     *
     * @param TaxClassManagementInterface $taxClassService
     * @param TaxDetailsItemInterfaceFactory $taxDetailsItemDataObjectFactory
     * @param AppliedTaxInterfaceFactory $appliedTaxDataObjectFactory
     * @param AppliedTaxRateInterfaceFactory $appliedTaxRateDataObjectFactory
     * @param Calculation $calculationTool
     * @param \Magento\Tax\Model\Config $config
     * @param ScopeConfigInterface $scopeConfig
     * @param int $storeId
     * @param \Magento\Framework\DataObject $addressRateRequest
     */
    public function __construct(
        TaxClassManagementInterface $taxClassService,
        TaxDetailsItemInterfaceFactory $taxDetailsItemDataObjectFactory,
        AppliedTaxInterfaceFactory $appliedTaxDataObjectFactory,
        AppliedTaxRateInterfaceFactory $appliedTaxRateDataObjectFactory,
        Calculation $calculationTool,
        \Magento\Tax\Model\Config $config,
        ScopeConfigInterface $scopeConfig,
        $storeId,
        \Magento\Framework\DataObject $addressRateRequest = null
    ) {
        parent::__construct(
            $taxClassService,
            $taxDetailsItemDataObjectFactory,
            $appliedTaxDataObjectFactory,
            $appliedTaxRateDataObjectFactory,
            $calculationTool,
            $config,
            $storeId,
            $addressRateRequest
        );
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    protected function calculateWithTaxInPrice(
        QuoteDetailsItemInterface $item,
        $quantity,
        $round = true
    ) {
        if ($item->getType() != 'product') {
            return parent::calculateWithTaxInPrice($item, $quantity, $round);
        }

        $taxRateRequest = $this->getAddressRateRequest()->setProductClassId(
            $this->taxClassManagement->getTaxClassId($item->getTaxClassKey())
        );
        $rate = $this->calculationTool->getRate($taxRateRequest);
        $storeRate = $this->calculationTool->getStoreRate($taxRateRequest, $this->storeId);

        $discountTaxCompensationAmount = 0;
        $applyTaxAfterDiscount = $this->config->applyTaxAfterDiscount($this->storeId);
        $discountAmount = $item->getDiscountAmount();

        // Calculate $rowTotalInclTax
        $priceInclTax = $this->calculationTool->round($item->getUnitPrice());

        $algorithm = $this->config->getAlgorithm($this->storeId);

        if ($algorithm == \Shirtplatform\Core\Plugin\Tax\Model\System\Config\Source\Algorithm::CALC_TOTAL_BASE_CREATOR_PRICES_INCL_TAX) {
            if (!$item->getIsTaxIncluded()) {
                $priceInclTax = $this->calculationTool->round($item->getUnitPrice() + $item->getUnitPrice() * $rate / 100);
            }
        }

        $rowTotalInclTax = $priceInclTax * $quantity;
        //this caused lowering the price twice for 0% tax
        // if (!$this->isSameRateAsStore($rate, $storeRate)) {
        //     $priceInclTax = $this->calculatePriceInclTax($priceInclTax, $storeRate, $rate, $round);
        //     $rowTotalInclTax = $priceInclTax * $quantity;
        // }
        $rowTaxExact = $this->calculationTool->calcTaxAmount($rowTotalInclTax, $rate, true, false);
        $deltaRoundingType = self::KEY_REGULAR_DELTA_ROUNDING;
        if ($applyTaxAfterDiscount) {
            $deltaRoundingType = self::KEY_TAX_BEFORE_DISCOUNT_DELTA_ROUNDING;
        }
        // $rowTax = $this->roundAmount($rowTaxExact, $rate, true, $deltaRoundingType, $round, $item);
        $rowTax = $rowTotalInclTax - $this->calculationTool->round($rowTotalInclTax / (1 + $rate / 100));
        $rowTotal = $rowTotalInclTax - $rowTax;
        $price = $rowTotal / $quantity;
        if ($round) {
            $price = $this->calculationTool->round($price);
        }

        //Handle discount
        if ($applyTaxAfterDiscount) {
            //TODO: handle originalDiscountAmount
            $taxableAmount = max($rowTotalInclTax - $discountAmount, 0);
            $rowTaxAfterDiscount = $this->calculationTool->calcTaxAmount(
                $taxableAmount,
                $rate,
                true,
                false
            );
            $rowTaxAfterDiscount = $this->roundAmount(
                $rowTaxAfterDiscount,
                $rate,
                true,
                self::KEY_REGULAR_DELTA_ROUNDING,
                $round,
                $item
            );
            // Set discount tax compensation
            $discountTaxCompensationAmount = $rowTax - $rowTaxAfterDiscount;
            $rowTax = $rowTaxAfterDiscount;
        }

        // Calculate applied taxes
        /** @var  \Magento\Tax\Api\Data\AppliedTaxInterface[] $appliedTaxes */
        $appliedRates = $this->calculationTool->getAppliedRates($taxRateRequest);
        $appliedTaxes = $this->getAppliedTaxes($rowTax, $rate, $appliedRates);

        return $this->taxDetailsItemDataObjectFactory->create()
            ->setCode($item->getCode())
            ->setType($item->getType())
            ->setRowTax($rowTax)
            ->setPrice($price)
            ->setPriceInclTax($priceInclTax)
            ->setRowTotal($rowTotal)
            ->setRowTotalInclTax($rowTotalInclTax)
            ->setDiscountTaxCompensationAmount($discountTaxCompensationAmount)
            ->setAssociatedItemCode($item->getAssociatedItemCode())
            ->setTaxPercent($rate)
            ->setAppliedTaxes($appliedTaxes);
    }

    /**
     * {@inheritdoc}
     */
    protected function calculateWithTaxNotInPrice(QuoteDetailsItemInterface $item, $quantity, $round = true)
    {
        if ($item->getType() == 'shipping') {
            $taxRateRequest = $this->getAddressRateRequest()->setProductClassId(
                $this->taxClassManagement->getTaxClassId($item->getTaxClassKey())
            );
            $rate = $this->calculationTool->getRate($taxRateRequest);
            $appliedRates = $this->calculationTool->getAppliedRates($taxRateRequest);

            $applyTaxAfterDiscount = $this->config->applyTaxAfterDiscount($this->storeId);
            $discountAmount = $item->getDiscountAmount();
            $discountTaxCompensationAmount = 0;

            // Calculate $rowTotal
            $rawPrice = $item->getUnitPrice(); // can use 3 decimal places, used for calculations
            $price = $this->calculationTool->round($item->getUnitPrice()); // rounded price, used for saving
            $rowTotal = $this->calculationTool->round($rawPrice * $quantity);
            $rowTaxes = [];
            $rowTaxesBeforeDiscount = [];
            $appliedTaxes = [];
            $rowTotalForTaxCalculation = $this->getPriceForTaxCalculation($item, $rawPrice) * $quantity;
            //Apply each tax rate separately
            foreach ($appliedRates as $appliedRate) {
                $taxId = $appliedRate['id'];
                $taxRate = $appliedRate['percent'];
                $rowTaxPerRate = $this->calculationTool->calcTaxAmount($rowTotalForTaxCalculation, $taxRate, false, false);
                $deltaRoundingType = self::KEY_REGULAR_DELTA_ROUNDING;
                if ($applyTaxAfterDiscount) {
                    $deltaRoundingType = self::KEY_TAX_BEFORE_DISCOUNT_DELTA_ROUNDING;
                }                
                
                $rowTaxPerRate = $this->roundAmount($rowTaxPerRate, $taxId, false, $deltaRoundingType, $round, $item);
                
                //Check if shipping including tax rounding is needed (e.g. prices like 69.99, 70.01)
                $roundPriceInclTax = $this->scopeConfig->getValue('tax/calculation/round_shipping_incl_tax', ScopeInterface::SCOPE_STORE, $this->storeId);

                if ($roundPriceInclTax) {
                    $tempRowTotalInclTax = $rowTotal + $rowTaxPerRate;
                    $lowerRowTotalInclTax = floor($tempRowTotalInclTax);
                    $higherRowTotalInclTax = ceil($tempRowTotalInclTax);
                    $lowerNumberDiff = $lowerRowTotalInclTax - $tempRowTotalInclTax;
                    $higherNumberDiff = $higherRowTotalInclTax - $tempRowTotalInclTax;

                    if (abs($lowerNumberDiff) > 0.001 && abs($lowerNumberDiff) < 0.02) {
                        $rowTaxPerRate += $lowerNumberDiff;
                    }
                    elseif (abs($higherNumberDiff) > 0.001 && abs($higherNumberDiff) < 0.02) {
                        $rowTaxPerRate += $higherNumberDiff;
                    }                    
                }
                //=========================

                $rowTaxAfterDiscount = $rowTaxPerRate;

                //Handle discount
                if ($applyTaxAfterDiscount) {
                    //TODO: handle originalDiscountAmount
                    $taxableAmount = max($rowTotalForTaxCalculation - $discountAmount, 0);
                    if ($taxableAmount && !$applyTaxAfterDiscount) {
                        $taxableAmount = $rowTotalForTaxCalculation;
                    }
                    $rowTaxAfterDiscount = $this->calculationTool->calcTaxAmount(
                        $taxableAmount,
                        $taxRate,
                        false,
                        false
                    );
                    $rowTaxAfterDiscount = $this->roundAmount(
                        $rowTaxAfterDiscount,
                        $taxId,
                        false,
                        self::KEY_REGULAR_DELTA_ROUNDING,
                        $round,
                        $item
                    );
                }
                $appliedTaxes[$taxId] = $this->getAppliedTax(
                    $rowTaxAfterDiscount,
                    $appliedRate
                );

                $rowTaxes[] = $rowTaxAfterDiscount;
                $rowTaxesBeforeDiscount[] = $rowTaxPerRate;
            }
            $rowTax = array_sum($rowTaxes);
            $rowTaxBeforeDiscount = array_sum($rowTaxesBeforeDiscount);
            $rowTotalInclTax = $rowTotal + $rowTaxBeforeDiscount;            
            $priceInclTax = $rowTotalInclTax / $quantity;

            if ($round) {
                $priceInclTax = $this->calculationTool->round($priceInclTax);
            }

            return $this->taxDetailsItemDataObjectFactory->create()
                ->setCode($item->getCode())
                ->setType($item->getType())
                ->setRowTax($rowTax)
                ->setPrice($price)
                ->setPriceInclTax($priceInclTax)
                ->setRowTotal($rowTotal)
                ->setRowTotalInclTax($rowTotalInclTax)
                ->setDiscountTaxCompensationAmount($discountTaxCompensationAmount)
                ->setAssociatedItemCode($item->getAssociatedItemCode())
                ->setTaxPercent($rate)
                ->setAppliedTaxes($appliedTaxes);
        } elseif ($item->getType() != 'product') {
            return parent::calculateWithTaxNotInPrice($item, $quantity, $round);
        }

        return $this->calculateWithTaxInPrice($item, $quantity, $round);
    }

    /**
     * Get price for tax calculation.
     *
     * @param QuoteDetailsItemInterface $item
     * @param float $price
     * @return float
     */
    private function getPriceForTaxCalculation(QuoteDetailsItemInterface $item, float $price)
    {
        if ($item->getExtensionAttributes() && $item->getExtensionAttributes()->getPriceForTaxCalculation()) {
            $priceForTaxCalculation = $this->calculationTool->round(
                $item->getExtensionAttributes()->getPriceForTaxCalculation()
            );
        } else {
            $priceForTaxCalculation = $price;
        }

        return $priceForTaxCalculation;
    }
}
