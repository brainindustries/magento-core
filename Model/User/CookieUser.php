<?php

namespace Shirtplatform\Core\Model\User;

class CookieUser extends \shirtplatform\utils\user\SimpleUser {

    /**
     * Session ID
     * 
     * @access private
     * @var string
     */
    private static $sessionId;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $_cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $_cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_dateTime;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    private $_sessionManager;
    
    /**
     * Current store ID associated with this login (will be stored
     * as part of the value in cookie)
     * 
     * @var int
     */
    private $_storeId;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * 
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param int $storeId
     */
    public function __construct(\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
                                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
                                \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
                                \Magento\Framework\Session\SessionManagerInterface $sessionManager,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                $storeId = null) {
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_dateTime = $dateTime;
        $this->_sessionManager = $sessionManager;
        $this->_storeManager = $storeManager;
        $this->_storeId = $storeId;                
        
        if ($storeId === null) {
            $this->_storeId = $this->_storeManager->getStore()->getId();
        }
    }

    /**
     * Unset all properties
     * 
     * @access public
     */
    public function unsetAll() {        
        self::setAccountId(null);
        self::setSessionId(null);
        self::setShopId(null);
        self::setUserId(null);
        $cookieName = $this->_getCookieName();
        $this->_cookieManager->deleteCookie($cookieName);        
    }

    /**
     * Get cookie name
     * 
     * @access private
     * @return string
     */
    private function _getCookieName() {
        $store = $this->_storeManager->getStore();
        return 'platform_session_' . $store->getCode();
    }

    /**
     * Get session ID
     * 
     * @access public
     * @return string
     */
    public function getSessionId() {
        if (isset(self::$sessionId)) {
            return self::$sessionId;
        }

        $cookieName = $this->_getCookieName();        
        $value = self::$sessionId = $this->_cookieManager->getCookie($cookieName);        
        $exp = $value ? explode(';', $value) : [];        
        
        //format of the value should be storeId;sessionId
        if (isset($exp[1])) {
            self::$sessionId = $exp[1];
        }
        
        return self::$sessionId;
    }

    /**
     * Set session ID
     * 
     * @access public
     * @param string $id
     */
    public function setSessionId($id) {
        self::$sessionId = $id;
        $cookieName = $this->_getCookieName();
        $duration = $this->_dateTime->timestamp() + (3600 * 24 * 30);

        $metadata = $this->_cookieMetadataFactory
                ->createPublicCookieMetadata()
                ->setDuration($duration)
                ->setPath($this->_sessionManager->getCookiePath())
                ->setDomain($this->_sessionManager->getCookieDomain())
                ->setHttpOnly(true);
        
        $value = $this->_storeId . ';' . $id;
        $this->_cookieManager->setPublicCookie($cookieName, $value, $metadata);
    }

    /**
     * Get store ID in the cookie
     * 
     * @access public
     * @return int
     */
    public function getCookieStoreId() {
        $cookieName = $this->_getCookieName();        
        $value = $this->_cookieManager->getCookie($cookieName);        
        $exp = $value ? explode(';', $value) : [];        
        $storeId = null;
        
        //format of the value should be storeId;sessionId
        if (count($exp) == 2) {
            $storeId = $exp[0];
        }
        
        return $storeId;
    }
}
