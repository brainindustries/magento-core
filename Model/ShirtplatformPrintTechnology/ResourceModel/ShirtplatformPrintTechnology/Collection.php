<?php

namespace Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel\ShirtplatformPrintTechnology;

/**
 * Class Collection
 * @package Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel\ShirtplatformPrintTechnology
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'shirtplatform_print_technology_collection';
    protected $_eventObject = 'shirtplatform_post_print_technology';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(\Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model\ShirtplatformPrintTechnology::class, \Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel\ShirtplatformPrintTechnology::class);
    }

}
