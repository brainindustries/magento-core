<?php

namespace Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel;

/**
 * Class ShirtplatformPrintTechnology
 * @package Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel
 */
class ShirtplatformPrintTechnology extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * ShirtplatformPrintTechnology constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('shirtplatform_print_technology', 'id');
    }

}
