<?php

namespace Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model;

/**
 * Class ShirtplatformPrintTechnology
 * @package Shirtplatform\Core\Model\ShirtplatformPrintTechnology\Model
 */
class ShirtplatformPrintTechnology
    extends \Magento\Framework\Model\AbstractModel
    // implements \Magento\Framework\DataObject\IdentityInterface
{

    /** @var string */
    const CACHE_TAG = 'shirtplatform_printTechnology';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(\Shirtplatform\Core\Model\ShirtplatformPrintTechnology\ResourceModel\ShirtplatformPrintTechnology::class);
    }

    /**
     * @return array|string[]
     */
    // public function getIdentities()
    // {        
    //     return [self::CACHE_TAG . '_' . $this->getId()];
    // }

}
