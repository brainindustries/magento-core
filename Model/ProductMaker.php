<?php

namespace Shirtplatform\Core\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\OptionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Eav\Model\AttributeSetRepository;
use Magento\Tax\Model\TaxClass\Source\Product as ProductTaxClassSource;
use Magento\Store\Api\WebsiteRepositoryInterface;

class ProductMaker
{
    /**
     * @var ProductFactory
     */
    private $_productFactory;

    /**
     * @var OptionFactory
     */
    private $_optionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var AttributeSetRepository
     */
    private $_attributeSetRepository;

    /**
     * @var ProductTaxClassSource
     */
    private $_productTaxClassSource;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $_websiteRepository;

    /**
     * @param ProductFactory $productFactory
     * @param OptionFactory $optionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AttributeSetRepository $attributeSetRepository
     * @param ProductTaxClassSource $productTaxClassSource
     * @param WebsiteRepositoryInterface $websiteRepository
     */
    public function __construct(
        ProductFactory $productFactory,
        OptionFactory $optionFactory,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AttributeSetRepository $attributeSetRepository,
        ProductTaxClassSource $productTaxClassSource,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->_productFactory = $productFactory;
        $this->_optionFactory = $optionFactory;
        $this->_productRepository = $productRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_attributeSetRepository = $attributeSetRepository;
        $this->_productTaxClassSource = $productTaxClassSource;
        $this->_websiteRepository = $websiteRepository;
    }

    /**
     * @param array $data Can contain product_data, custom_options and media_gallery_entries arrays
     * @return \Magento\Catalog\Model\Product
     */
    public function createProduct($data)
    {
        // create new product, set default and custom product data
        $product = $this->_productFactory->create(['data' => $this->addDefaultData($data['product_data'])]);

        // create custom options and add it to the product
        if (isset($data['custom_options']) && is_countable($data['custom_options'])) {
            $product->setHasOptions(true);
            foreach ($data['custom_options'] as $_options) {
                $_options['product_id'] = $product->getId();
                $_options['store_id'] = $product->getStoreId();
                $option = $this->_optionFactory->create(['data' => $_options]);
                $product->addOption($option);
            }
        }

        // set media gallery entries to the product
        if (isset($data['media_gallery_entries'])) {
            $product->setMediaGalleryEntries($data['media_gallery_entries']);
        }

        // save and return the product
        return $this->_productRepository->save($product);        
    }

    /**
     * @param string $name
     * @return int
     */
    public function getAttributeSetId($attributeSetName)
    {
        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('attribute_set_name', $attributeSetName)
            ->setPageSize(1)
            ->create();

        $list = $this->_attributeSetRepository->getList($searchCriteria)->getItems();
        $attributeSet = current($list);

        return $attributeSet->getAttributeSetId();
    }

    /**
     * @param string $label
     * @return int
     */
    private function getTaxClassId($label = 'Taxable Goods')
    {
        $result = null;
        foreach ($this->_productTaxClassSource->getAllOptions() as $taxClass) {
            if ($taxClass['label'] === $label) {
                $result = $taxClass['value'];
                break;
            }
        }
        
        return $result;
    }

    /**
     * @param bool $excludeAdmin
     * @return array
     */
    private function getWebsiteIds($excludeAdmin = true)
    {
        $result = [];
        foreach ($this->_websiteRepository->getList() as $_website) {
            if ($excludeAdmin && $_website->getCode() === 'admin') {
                continue;
            }
            $result[] = $_website->getId();
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function addDefaultData($data)
    {
        return array_merge([
            'website_ids' => $this->getWebsiteIds(),
            'tax_class_id' => $this->getTaxClassId(),
            'status' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED,
            'price' => 0,
            'can_save_custom_options' => true,
            'stock_data' => [
                'use_config_manage_stock' => 0, 
                'manage_stock' => 0, 
                'is_in_stock' => 1
            ]
        ], $data);
    }
}