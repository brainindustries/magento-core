<?php

namespace Shirtplatform\Core\Model\Migration;

use Magento\Framework\App\Filesystem\DirectoryList;

class Entity
{

    protected $_sourceStoreId;
    protected $_destinationStoreId;
    protected $_destinationWebsiteId;
    protected $_destinationStoreName;

    /**
     * Array mapping source coustomer_group_id => destination coustomer_group_id
     * 
     * @var array
     */
    protected $_customerGroupMapper = [];

    /**
     * @var \Magento\Store\Model\Store
     */
    protected $_destinationStore;

    /**
     * @var \DOMXPath
     */
    private $_config;

    /**
     * \Shirtplatform\Core\Helper\Data
     */
    private $_coreHelper;

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    protected $_sourceDb;

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    protected $_destinationDb;

    /**
     *
     * @var DirectoryList
     */
    private $_directoryList;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    private $_moduleReader;

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\MysqlFactory
     */
    private $_mysqlFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var \Magento\Framework\App\Arguments\ValidationState
     */
    private $_validationState;

    /**
     * @param \Shirtplatform\Core\Helper\Data $coreHelper
     * @param DirectoryList $directoryList     
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     * @param \Magento\Framework\DB\Adapter\Pdo\MysqlFactory $mysqlFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Arguments\ValidationState $validationState     
     */
    public function __construct(
        \Shirtplatform\Core\Helper\Data $coreHelper,
        DirectoryList $directoryList,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\DB\Adapter\Pdo\MysqlFactory $mysqlFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Arguments\ValidationState $validationState
    ) {
        $this->_coreHelper = $coreHelper;
        $this->_destinationDb = $resource->getConnection();
        $this->_directoryList = $directoryList;
        $this->_moduleReader = $moduleReader;
        $this->_mysqlFactory = $mysqlFactory;
        $this->_storeManager = $storeManager;
        $this->_validationState = $validationState;
    }

    /**
     * Init configuration and source database
     * 
     * @access private
     * @throws \Exception
     */
    private function _init()
    {
        $etcDir = $this->_directoryList->getPath(DirectoryList::CONFIG);
        $configFile = $etcDir . '/migration/config.xml';

        if (!file_exists($configFile)) {
            $etcDir = $this->_moduleReader->getModuleDir(\Magento\Framework\Module\Dir::MODULE_ETC_DIR, 'Shirtplatform_Core');
            $configFile = $etcDir . '/migration/config.xml';

            if (!file_exists($configFile)) {
                $msg = 'Configuration file is missing. Please copy Shirtplatform/Core/etc/migration/config.xml.dist to '
                    . 'magento app/etc/migration directory, rename it to config.xml and set source database '
                    . 'credentials together with store ID';
                throw new \Exception($msg);
            }
        }

        $xml = file_get_contents($configFile);
        $document = new \Magento\Framework\Config\Dom($xml, $this->_validationState);
        $this->_config = new \DOMXPath($document->getDom());

        $sourceDb = $this->_config->query('//source/database')->item(0);
        $dbHost = $sourceDb->getAttribute('host');
        $dbName = $sourceDb->getAttribute('name');
        $dbUser = $sourceDb->getAttribute('user');
        $dbPassword = $sourceDb->getAttribute('password');
        $config = [
            'host' => $dbHost,
            'dbname' => $dbName,
            'username' => $dbUser,
            'password' => $dbPassword,
            'charset' => 'utf8'
        ];
        $this->_sourceDb = $this->_mysqlFactory->create('Magento\Framework\DB\Adapter\Pdo\Mysql', $config);

        $this->_sourceStoreId = $this->_config->query('//source//store_id')->item(0)->nodeValue;
        $this->_destinationStoreId = $this->_config->query('//destination/store_id')->item(0)->nodeValue;
        $this->_destinationStore = $this->_storeManager->getStore($this->_destinationStoreId);
        $this->_destinationWebsiteId = $this->_destinationStore->getWebsiteId();
        $this->_destinationStoreName = $this->_destinationStore->getWebsite()->getName() . "\n" .
            $this->_destinationStore->getGroup()->getName() . "\n" .
            $this->_destinationStore->getName();
    }

    /**
     * Create customer group mapper
     * 
     * @access protected
     */
    protected function _createCustomerGroupMapper()
    {
        $select = $this->_sourceDb->select()->from('customer_group');
        $sourceGroups = $this->_sourceDb->fetchAll($select);

        $select = $this->_destinationDb->select()->from('customer_group');
        $destGroups = $this->_destinationDb->fetchAll($select);

        if (!empty($sourceGroups) and !empty($destGroups)) {
            foreach ($sourceGroups as $sGroup) {
                foreach ($destGroups as $dGroup) {
                    if ($sGroup['customer_group_code'] == $dGroup['customer_group_code']) {
                        $this->_customerGroupMapper[$sGroup['customer_group_id']] = $dGroup['customer_group_id'];
                        break;
                    }
                }
            }
        }
    }

    /**
     * Get configuration value for path
     * 
     * @access protected
     * @param string $path
     * @return string|null
     */
    protected function _getConfigValue($path)
    {
        $item = $this->_config->query($path)->item(0);

        if ($item) {
            return $item->nodeValue;
        }

        return null;
    }

    /**
     * Get field mapper between source and destination table from config
     * 
     * @access public
     * @param string $table
     * @return array
     */
    public function getFieldMapper($table)
    {
        $result = [];
        $fieldMapper = $this->_config->query('//field_mapper/' . $table . '/*');

        if ($fieldMapper->length) {
            foreach ($fieldMapper as $field) {
                $result[$field->nodeName] = $field->nodeValue;
            }
        }

        return $result;
    }
    
    /**
     * Log error to error file
     *
     * @access protected
     * @param string $message
     * @return void
     */
    protected function _logError($message)
    {
        $this->_coreHelper->logError($message);
    }

    /**
     * Main migration function. Should be overriden in a child class
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output     
     */
    public function migrate($output)
    {
        $this->_init();
    }

    /**
     * Validate if the migration was successful. Compares source and destination
     * data. Should be overriden in a child class
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function testMigratedData($output)
    {
    }
}
