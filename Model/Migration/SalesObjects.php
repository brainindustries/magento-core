<?php

namespace Shirtplatform\Core\Model\Migration;

class SalesObjects extends Entity {
    
    /**
     * Number of orders with import errors
     * 
     * @var int
     */
    private $_errorCount = 0;
    
    /**
     * Extra fee title
     * 
     * @var string
     */
    private $_extraFeeTitle;

    /**
     * Array mapping main tables (source => destination)
     * 
     * @var array
     */
    private $_mainTableMapper = [
        'sales_flat_order' => 'sales_order',
        'sales_flat_invoice' => 'sales_invoice',
        'sales_flat_creditmemo' => 'sales_creditmemo',
        'sales_flat_shipment' => 'sales_shipment'
    ];

    /**
     * Array mapping related tables
     * 
     * @var array
     */
    private $_relatedTables = [
        'sales_flat_order' => [
            'sales_flat_order_address' => 'sales_order_address',
            'sales_flat_order_item' => 'sales_order_item',
            'sales_flat_order_payment' => 'sales_order_payment',
            'sales_flat_order_status_history' => 'sales_order_status_history'
        ],
        'sales_flat_invoice' => [
            'sales_flat_invoice_item' => 'sales_invoice_item',
            'sales_flat_invoice_comment' => 'sales_invoice_comment'
        ],
        'sales_flat_creditmemo' => [
            'sales_flat_creditmemo_item' => 'sales_creditmemo_item',
            'sales_flat_creditmemo_comment' => 'sales_creditmemo_comment'
        ],
        'sales_flat_shipment' => [
            'sales_flat_shipment_item' => 'sales_shipment_item',
            'sales_flat_shipment_comment' => 'sales_shipment_comment',
            'sales_flat_shipment_track' => 'sales_shipment_track'
        ]
    ];

    /**
     * Array mapping tables to their foreign keys
     * 
     * @var array
     */
    private $_foreignKeys = [
        'sales_flat_order_address' => 'parent_id',
        'sales_flat_order_item' => 'order_id',
        'sales_flat_order_payment' => 'parent_id',
        'sales_flat_order_status_history' => 'parent_id',
        'sales_flat_invoice_item' => 'parent_id',
        'sales_flat_invoice_comment' => 'parent_id',
        'sales_flat_creditmemo_item' => 'parent_id',
        'sales_flat_creditmemo_comment' => 'parent_id',
        'sales_flat_shipment_item' => 'parent_id',
        'sales_flat_shipment_comment' => 'parent_id',
        'sales_flat_shipment_track' => 'parent_id'
    ];

    /**
     * Array mapping destination entity_id => relation_child_real_id
     * 
     * @var array
     */
    private $_orderChildRelations = [];

    /**
     * Array mapping destination entity_id => relation_parent_real_id
     * 
     * @var array
     */
    private $_orderParentRelations = [];

    /**
     * Get sales data (order|invoice|creditmemo|shipment) from source database
     * 
     * @access private     
     * @param int $limit
     * @param int $offset
     * @return array
     */
    private function _getSourceData($limit,
                                    $offset) {
        $salesData = [];
        $select = $this->_sourceDb->select()->from('sales_flat_order')
                ->where('store_id = ?', $this->_sourceStoreId)
//                ->where('entity_id IN (?)', [190203, 154360, 149557, 83526, 40888, 16948, 12053]) //for testing general
//                ->where('entity_id = ?', 12053) //for testing creditmemo invoice_id
//                ->where('entity_id = ?', 34248) //for testing shipment
//                ->where('entity_id = ?', 194486);
                ->limit($limit, $offset);
        $orders = $this->_sourceDb->fetchAll($select);

        foreach ($orders as $_order) {
            $orderId = $_order['entity_id'];
            $salesData[$orderId]['sales_flat_order'] = $_order;

            foreach ($this->_relatedTables['sales_flat_order'] as $sourceTable => $destTable) {
                $fk = $this->_foreignKeys[$sourceTable];
                $select = $this->_sourceDb->select()->from($sourceTable)
                        ->where($fk . ' = ?', $orderId);
                $salesData[$orderId]['sales_flat_order']['related_tables'][$sourceTable] = $this->_sourceDb->fetchAll($select);
            }

            foreach ($this->_mainTableMapper as $sourceMainTable => $destMainTable) {
                if ($sourceMainTable == 'sales_flat_order') {
                    continue;
                }

                $select = $this->_sourceDb->select()->from($sourceMainTable)
                        ->where('order_id = ?', $orderId);
                $salesDocuments = $this->_sourceDb->fetchAll($select);

                if (!empty($salesDocuments)) {
                    foreach ($salesDocuments as $document) {
                        $documentId = $document['entity_id'];
                        $salesData[$orderId][$sourceMainTable][$documentId] = $document;

                        foreach ($this->_relatedTables[$sourceMainTable] as $sourceTable => $destTable) {
                            $fk = $this->_foreignKeys[$sourceTable];
                            $select = $this->_sourceDb->select()->from($sourceTable)
                                    ->where($fk . ' = ?', $documentId);
                            $salesData[$orderId][$sourceMainTable][$documentId]['related_tables'][$sourceTable] = $this->_sourceDb->fetchAll($select);
                        }
                    }
                }
            }
        }

        return $salesData;
    }

    /**
     * Get destination data that correspond to their source data (after migration)
     * 
     * @access private
     * @param array $sourceData
     * @return array
     */
    private function _getDestinationData($sourceData) {
        $destData = [];

        foreach ($sourceData as $sourceKey => $sourceRow) {
            $select = $this->_destinationDb->select()->from('sales_order')
                    ->where('increment_id = ?', $sourceRow['sales_flat_order']['increment_id']);
            $orders = $this->_destinationDb->fetchAll($select);

            foreach ($orders as $_order) {
                $orderId = $_order['entity_id'];
                $destData[$sourceKey]['sales_order'] = $_order;

                foreach ($this->_relatedTables['sales_flat_order'] as $sourceTable => $destTable) {
                    $fk = $this->_foreignKeys[$sourceTable];
                    $select = $this->_destinationDb->select()->from($destTable)
                            ->where($fk . ' = ?', $orderId);
                    $destData[$sourceKey]['sales_order']['related_tables'][$destTable] = $this->_destinationDb->fetchAll($select);
                }

                foreach ($this->_mainTableMapper as $sourceMainTable => $destMainTable) {
                    if ($sourceMainTable == 'sales_flat_order') {
                        continue;
                    }

                    $select = $this->_destinationDb->select()->from($destMainTable)
                            ->where('order_id = ?', $orderId);
                    $salesDocuments = $this->_destinationDb->fetchAll($select);

                    if (!empty($salesDocuments)) {
                        foreach ($salesDocuments as $document) {
                            $documentId = $document['entity_id'];
                            $destData[$sourceKey][$destMainTable][$documentId] = $document;

                            foreach ($this->_relatedTables[$sourceMainTable] as $sourceTable => $destTable) {
                                $fk = $this->_foreignKeys[$sourceTable];
                                $select = $this->_destinationDb->select()->from($destTable)
                                        ->where($fk . ' = ?', $documentId);
                                $destData[$sourceKey][$destMainTable][$documentId]['related_tables'][$destTable] = $this->_destinationDb->fetchAll($select);
                            }
                        }
                    }
                }
            }
        }

        return $destData;
    }

    /**
     * Process sales data - synchronize orders, invoices, creditmemos, shipments
     * and their related tables
     * 
     * @access private
     * @param array $sourceData
     * @throws \Exception
     */
    private function _processData($sourceData, $output) {
        foreach ($sourceData as $sourceOrderId => $sourceRow) {
            try {
                $this->_destinationDb->beginTransaction();
                $orderData = $this->_processOrderData($sourceRow['sales_flat_order']);

                foreach ($sourceRow as $sourceMainTable => $mainTableDocuments) {
                    if ($sourceMainTable == 'sales_flat_order') {
                        continue;
                    }

                    foreach ($mainTableDocuments as $sourceKey => $sourceEntityData) {
                        $extra = [];

                        if ($sourceMainTable == 'sales_flat_creditmemo' and $sourceEntityData['invoice_id']) {
                            $destInvoiceId = $this->_findDestInvoiceId($sourceRow['sales_flat_invoice'], $sourceEntityData['invoice_id']);
                            $extra['invoice_id'] = $destInvoiceId;
                        }

                        $this->_processEntityData($sourceEntityData, $sourceMainTable, $orderData, $extra);
                    }
                }

                $this->_destinationDb->commit();
            } catch (\Exception $ex) {
                $this->_destinationDb->rollback();                
                $msg = 'Error importing order data for source order number ';
                if (isset($sourceRow['sales_flat_order']['increment_id'])) {
                    $msg .= $sourceRow['sales_flat_order']['increment_id'];
                }
                $this->_logError($msg);
                $this->_errorCount++;
                                
//                throw new \Exception($ex->getMessage());
            }
        }
    }

    /**
     * Process data from the source row and insert/updates destination database
     * order tables
     * 
     * @access private
     * @param array $sourceOrderData
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array $destOrder
     */
    private function _processOrderData($sourceOrderData) {
        //get order
        $select = $this->_destinationDb->select()->from('sales_order')
                ->where('increment_id = ?', $sourceOrderData['increment_id']);
        $destOrder = $this->_destinationDb->fetchAll($select);

        if (!empty($destOrder)) {
            $destOrder = $destOrder[0];
        }

        //get field mapper for order
        $fieldMapper = $this->getFieldMapper('sales_order');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_order'));
        }

        //collect order data            
        foreach ($fieldMapper as $sourceField => $destField) {
            $destOrder[$destField] = $sourceOrderData[$sourceField];
        }

        $notNull = ['extra_fee', 'base_extra_fee', 'extra_fee_excl_tax', 'base_extra_fee_excl_tax'];
        foreach ($notNull as $field) {
            if (is_null($destOrder[$field])) {
                $destOrder[$field] = '';
            }
        }

        $destOrder['extra_fee_title'] = '';
        if ($destOrder['extra_fee'] and $destOrder['extra_fee_excl_tax']) {
            $destOrder['extra_fee_tax_amount'] = $destOrder['extra_fee'] - $destOrder['extra_fee_excl_tax'];
            $destOrder['base_extra_fee_tax_amount'] = $destOrder['base_extra_fee'] - $destOrder['base_extra_fee_excl_tax'];
            
            if ($destOrder['extra_fee'] > 0) {
                $destOrder['extra_fee_title'] = $this->_extraFeeTitle;
            }
        }

        //status
        if (preg_match('/production_wh/', $destOrder['status']) or $destOrder['status'] == 'production_selfmanufacture') {
            $destOrder['status'] = 'in_production';
        }
        
        $destOrder['store_id'] = $this->_destinationStoreId;
        $destOrder['store_name'] = $this->_destinationStoreName;

        if (isset($this->_customerGroupMapper[$sourceOrderData['customer_group_id']])) {
            $destOrder['customer_group_id'] = $this->_customerGroupMapper[$sourceOrderData['customer_group_id']];
        }
        else {
            $destOrder['customer_group_id'] = $sourceOrderData['customer_group_id'];
        }

        //get order related data
        $relatedTables = [];
        foreach ($sourceOrderData['related_tables'] as $sourceTable => $tableData) {
            $destTable = $this->_relatedTables['sales_flat_order'][$sourceTable];

            if (!empty($destOrder['entity_id'])) {
                $fk = $this->_foreignKeys[$sourceTable];
                $select = $this->_destinationDb->select()->from($destTable)
                        ->where($fk . ' = ?', $destOrder['entity_id']);
                $destRelatedData = $this->_destinationDb->fetchAll($select);
                $relatedTables[$destTable] = $destRelatedData;
            }
        }

        //update sales_order
        if (isset($destOrder['entity_id'])) {
            $this->_destinationDb->update('sales_order', $destOrder, ['entity_id = ?' => $destOrder['entity_id']]);
        }
        //insert to sales_order
        else {
            $this->_destinationDb->insert('sales_order', $destOrder);
            $destOrder['entity_id'] = $this->_destinationDb->lastInsertId();
        }

        //store relation between parent and child orders
        if ($destOrder['relation_child_real_id']) {
            $this->_orderChildRelations[$destOrder['entity_id']] = $destOrder['relation_child_real_id'];
        }
        if ($destOrder['relation_parent_real_id']) {
            $this->_orderParentRelations[$destOrder['entity_id']] = $destOrder['relation_parent_real_id'];
        }

        $destOrder['related_tables'] = $relatedTables;
        $destOrder = $this->_processOrderAddress($sourceOrderData, $destOrder);

        //update billing_address_id and shipping_address_id
        foreach ($destOrder['related_tables']['sales_order_address'] as $addressType => $address) {
            if ($addressType == 'billing') {
                $destOrder['billing_address_id'] = $address['entity_id'];
            }
            elseif ($addressType == 'shipping') {
                $destOrder['shipping_address_id'] = $address['entity_id'];
            }
        }

        $updateData = $destOrder;
        unset($updateData['related_tables']);
        $this->_destinationDb->update('sales_order', $updateData, ['entity_id = ?' => $destOrder['entity_id']]);
        $destOrder = $this->_processOrderPayment($sourceOrderData, $destOrder);
        $idOrderItemMapper = $this->_processOrderItems($sourceOrderData, $destOrder);
        $this->_processOrderComment($sourceOrderData, $destOrder);

        $destOrder['id_order_item_mapper'] = $idOrderItemMapper;
        return $destOrder;
    }

    /**
     * Process entity (invoice, creditmemo, shipment) data - insert/update
     * it to destination database
     * 
     * @access private
     * @param array $sourceEntityData
     * @param string $sourceMainTable
     * @param array $orderData
     * @param array $extra
     * @return array destination entity data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processEntityData($sourceEntityData,
                                        $sourceMainTable,
                                        $orderData,
                                        $extra) {
        //get entity
        $destMainTable = $this->_mainTableMapper[$sourceMainTable];
        $select = $this->_destinationDb->select()->from($destMainTable)
                ->where('increment_id = ?', $sourceEntityData['increment_id']);
        $destEntity = $this->_destinationDb->fetchAll($select);

        if (!empty($destEntity)) {
            $destEntity = $destEntity[0];
        }

        //get field mapper for the main entity
        $fieldMapper = $this->getFieldMapper($destMainTable);
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', $destMainTable));
        }

        //collect main entity data            
        foreach ($fieldMapper as $sourceField => $destField) {
            $destEntity[$destField] = $sourceEntityData[$sourceField];
        }

        if ($sourceMainTable != 'sales_flat_shipment') {
            $notNull = ['extra_fee', 'base_extra_fee', 'extra_fee_excl_tax', 'base_extra_fee_excl_tax'];
            foreach ($notNull as $field) {
                if (is_null($destEntity[$field])) {
                    $destEntity[$field] = '';
                }
            }

            $destEntity['extra_fee_title'] = '';
            if ($destEntity['extra_fee'] and $destEntity['extra_fee_excl_tax']) {
                $destEntity['extra_fee_tax_amount'] = $destEntity['extra_fee'] - $destEntity['extra_fee_excl_tax'];
                $destEntity['base_extra_fee_tax_amount'] = $destEntity['base_extra_fee'] - $destEntity['base_extra_fee_excl_tax'];
                
                if ($destEntity['extra_fee'] > 0) {
                    $destEntity['extra_fee_title'] = $this->_extraFeeTitle;
                }
            }
        }

        if ($sourceMainTable == 'sales_flat_shipment' and ! empty($orderData['customer_id'])) {
            $destEntity['customer_id'] = $orderData['customer_id'];
        }

        //this should set invoice_id for creditmemo
        foreach ($extra as $field => $value) {
            $destEntity[$field] = $value;
        }        
        
        $destEntity['store_id'] = $this->_destinationStoreId;
        $destEntity['order_id'] = $orderData['entity_id'];
        $destEntity['billing_address_id'] = $orderData['billing_address_id'];
        
        if (isset($orderData['shipping_address_id'])) {
            $destEntity['shipping_address_id'] = $orderData['shipping_address_id'];
        }

        //update main entity table
        if (isset($destEntity['entity_id'])) {
            $this->_destinationDb->update($destMainTable, $destEntity, ['entity_id = ?' => $destEntity['entity_id']]);
        }
        //insert to main entity table
        else {
            $this->_destinationDb->insert($destMainTable, $destEntity);
            $destEntity['entity_id'] = $this->_destinationDb->lastInsertId();
        }

        //get entity related data
        $relatedTables = [];
        foreach ($sourceEntityData['related_tables'] as $sourceTable => $tableData) {
            $destTable = $this->_relatedTables[$sourceMainTable][$sourceTable];

            if (!empty($destEntity['entity_id'])) {
                $fk = $this->_foreignKeys[$sourceTable];
                $select = $this->_destinationDb->select()->from($destTable)
                        ->where($fk . ' = ?', $destEntity['entity_id']);
                $destRelatedData = $this->_destinationDb->fetchAll($select);
                $relatedTables[$destTable] = $destRelatedData;
            }
        }

        $destEntity['related_tables'] = $relatedTables;
        $this->_processEntityItems($sourceEntityData, $sourceMainTable, $destEntity, $orderData);
        $this->_processEntityComment($sourceEntityData, $sourceMainTable, $destEntity);

        if ($sourceMainTable == 'sales_flat_shipment') {
            $this->_processTracks($sourceEntityData, $destEntity);
        }

        return $destEntity;
    }

    /**
     * Insert/updates records in sales_order_address table
     * 
     * @acces private
     * @param array $sourceOrderData
     * @param array $destOrder     
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processOrderAddress($sourceOrderData,
                                          $destOrder) {
        //find billing and shipping address
        if (isset($destOrder['related_tables']['sales_order_address'])) {
            $addresses = [];

            foreach ($destOrder['related_tables']['sales_order_address'] as $address) {
                if ($address['address_type'] == 'billing') {
                    $addresses['billing'] = $address;
                }
                elseif ($address['address_type'] == 'shipping') {
                    $addresses['shipping'] = $address;
                }
            }

            if (!empty($addresses)) {
                $destOrder['related_tables']['sales_order_address'] = $addresses;
            }
        }

        //collect data for order addresses
        $fieldMapper = $this->getFieldMapper('sales_order_address');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_order_address'));
        }

        foreach ($sourceOrderData['related_tables']['sales_flat_order_address'] as $sourceAddress) {
            $addressType = $sourceAddress['address_type'];

            foreach ($fieldMapper as $sourceField => $destField) {
                $destOrder['related_tables']['sales_order_address'][$addressType][$destField] = $sourceAddress[$sourceField];
            }
        }

        //add customer_id and customer_address_id fields
        $destCustomerData = [];
        if ($sourceOrderData['customer_id']) {
            $destCustomerData = $this->_getCustomerData($sourceOrderData['customer_id']);
        }

        if (!empty($destCustomerData)) {
            $destOrder['customer_id'] = $destCustomerData['customer_entity']['entity_id'];

            foreach ($sourceOrderData['related_tables']['sales_flat_order_address'] as $sourceAddress) {
                if ($sourceAddress['customer_id']) {
                    $addressType = $sourceAddress['address_type'];
                    $destOrder['related_tables']['sales_order_address'][$addressType]['customer_id'] = $destCustomerData['customer_entity']['entity_id'];
                    $destAddress = $this->_findDestinationAddress($sourceAddress, $destCustomerData['customer_address_entity']);

                    if (!empty($destAddress)) {
                        $destOrder['related_tables']['sales_order_address'][$addressType]['customer_address_id'] = $destAddress['entity_id'];
                    }
                }
            }
        }

        foreach ($destOrder['related_tables']['sales_order_address'] as $addressType => $address) {
            $destOrder['related_tables']['sales_order_address'][$addressType]['parent_id'] = $destOrder['entity_id'];
            $address['parent_id'] = $destOrder['entity_id'];

            //update sales_order_address
            if (isset($address['entity_id'])) {
                $addressId = $address['entity_id'];
                $this->_destinationDb->update('sales_order_address', $address, ['entity_id = ?' => $addressId]);
            }
            //insert to sales_order_address
            else {
                $this->_destinationDb->insert('sales_order_address', $address);
                $destOrder['related_tables']['sales_order_address'][$addressType]['entity_id'] = $this->_destinationDb->lastInsertId();
            }
        }

        return $destOrder;
    }

    /**
     * Insert/updates records in sales_order_payment table
     * 
     * @access private
     * @param array $sourceOrderData
     * @param array $destOrder
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processOrderPayment($sourceOrderData,
                                          $destOrder) {
        $fieldMapper = $this->getFieldMapper('sales_order_payment');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_order_payment'));
        }

        foreach ($sourceOrderData['related_tables']['sales_flat_order_payment'] as $key => $sourcePayment) {
            //collect data
            foreach ($fieldMapper as $sourceField => $destField) {
                if ($destField == 'additional_information' and ! empty($sourcePayment[$sourceField])) {
                    //fix serialized values                    
                    $arr = unserialize($sourcePayment[$sourceField]);
                    $destOrder['related_tables']['sales_order_payment'][$key][$destField] = json_encode($arr);
                }
                else {
                    $destOrder['related_tables']['sales_order_payment'][$key][$destField] = $sourcePayment[$sourceField];
                }
            }

            //fix for gopay_standard
            if ($destOrder['related_tables']['sales_order_payment'][$key]['method'] == 'gopay_standard') {
                $destOrder['related_tables']['sales_order_payment'][$key]['method'] = 'gopay_gateway';
                if (empty($destOrder['related_tables']['sales_order_payment'][$key]['additional_information'])) {
                    $destOrder['related_tables']['sales_order_payment'][$key]['additional_information'] = '{"method_title":"Platobn\u00e1 Karta \/ Internet Banking"}';
                }                
            }
            
            $destOrder['related_tables']['sales_order_payment'][$key]['parent_id'] = $destOrder['entity_id'];

            //update sales_order_payment
            if (isset($destOrder['related_tables']['sales_order_payment'][$key]['entity_id'])) {
                $paymentId = $destOrder['related_tables']['sales_order_payment'][$key]['entity_id'];
                $this->_destinationDb->update('sales_order_payment', $destOrder['related_tables']['sales_order_payment'][$key], ['entity_id = ?' => $paymentId]);
            }
            //insert into sales_order_payment
            else {
                $this->_destinationDb->insert('sales_order_payment', $destOrder['related_tables']['sales_order_payment'][$key]);
            }
        }

        return $destOrder;
    }

    /**
     * Find entity_id of invoice in destination database that matches source invoice_id
     * 
     * @access private
     * @param array $sourceInvoices
     * @param int $sourceInvoiceId
     * @return int|null
     */
    private function _findDestInvoiceId($sourceInvoices,
                                        $sourceInvoiceId) {
        $sourceIncrementId = null;
        $destInvoiceId = null;

        foreach ($sourceInvoices as $sInvoice) {
            if ($sInvoice['entity_id'] == $sourceInvoiceId) {
                $sourceIncrementId = $sInvoice['increment_id'];
                break;
            }
        }

        if ($sourceIncrementId) {
            $select = $this->_destinationDb->select()->from('sales_invoice')
                    ->where('increment_id = ?', $sourceIncrementId);
            $destInvoice = $this->_destinationDb->fetchAll($select);

            if (!empty($destInvoice)) {
                $destInvoiceId = $destInvoice[0]['entity_id'];
            }
        }

        return $destInvoiceId;
    }

    /**
     * Insert/update/delete records in sales_order table
     * 
     * @access private
     * @param array $sourceOrderData
     * @param array $destOrder
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array $idMapper array mapping source_order_item_id => dest_order_item_id
     */
    private function _processOrderItems($sourceOrderData,
                                        $destOrder) {
        $fieldMapper = $this->getFieldMapper('sales_order_item');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_order_item'));
        }

        //get order items divided into shirtplatform items and other items
        if (isset($sourceOrderData['related_tables']['sales_flat_order_item'])) {
            $sourceOrderItems = [];

            foreach ($sourceOrderData['related_tables']['sales_flat_order_item'] as $orderItem) {
                if ($orderItem['shirt_id']) {
                    $sourceOrderItems['shirtplatform'][$orderItem['shirt_id']] = $orderItem;
                }
                elseif ($orderItem['sku'] == 'RB01-RBTSKID') {
                    $sourceOrderItems['RB'][$orderItem['item_id']] = $orderItem;
                }
                else {
                    $sourceOrderItems['other'][$orderItem['item_id']] = $orderItem;
                }
            }
        }
        if (isset($destOrder['related_tables']['sales_order_item'])) {
            $destOrderItems = [];

            foreach ($destOrder['related_tables']['sales_order_item'] as $orderItem) {
                if ($orderItem['shirtplatform_id']) {
                    $destOrderItems['shirtplatform'][$orderItem['shirtplatform_id']] = $orderItem;
                }
                elseif ($orderItem['sku'] == 'RB01-RBTSKID') {
                    $destOrderItems['RB'][$orderItem['item_id']] = $orderItem;
                }
                else {
                    $destOrderItems['other'][$orderItem['item_id']] = $orderItem;
                }
            }
        }

        $idMapper = [];

//        if ($sourceOrderData['increment_id'] == '32718219') {
//            \Zend_Debug::dump('source shirtplatform items');
//            \Zend_Debug::dump($sourceOrderItems['shirtplatform']);
//            \Zend_Debug::dump($sourceOrderItems['other']);
//        }
        //collect data for insert/update - shirtplatform product    
        if (isset($sourceOrderItems['shirtplatform'])) {
            foreach ($sourceOrderItems['shirtplatform'] as $shirtId => $orderItem) {
                //get operation
                $operation = 'to_insert';
                if (isset($destOrderItems['shirtplatform'][$shirtId])) {
                    $operation = 'to_update';
                    $destOrderItems[$operation][$shirtId]['item_id'] = $destOrderItems['shirtplatform'][$shirtId]['item_id'];
                    $idMapper['existing'][$orderItem['item_id']] = $destOrderItems[$operation][$shirtId]['item_id'];

                    //unset this item, because in the end the items that are left will be deleted
                    unset($destOrderItems['shirtplatform'][$shirtId]);
                }

                foreach ($fieldMapper as $sourceField => $destField) {
                    $destOrderItems[$operation][$shirtId][$destField] = $orderItem[$sourceField];
                }
                if ($operation == 'to_insert') {
                    $destOrderItems[$operation][$shirtId]['item_id'] = $orderItem['item_id'];
                }
            }
        }

        //collect data for insert/update - Remove Background product    
        if (isset($sourceOrderItems['RB'])) {
            foreach ($sourceOrderItems['RB'] as $sourceItemId => $sourceItem) {
                $orderItem = null;
                $foundDestItem = null;
                $foundDestItemKey = null;

                if (isset($destOrderItems['RB'])) {
                    foreach ($destOrderItems['RB'] as $destItemId => $destItem) {
                        if ($sourceItem['product_options'] == $destItem['product_options']) {
                            $orderItem = $sourceItem;
                            $foundDestItem = $destItem;
                            $foundDestItemKey = $destItemId;
                            break;
                        }
                    }
                }

                //get operation
                $operation = 'to_insert';
                if ($orderItem) {
                    $operation = 'to_update';
                    $destOrderItems[$operation][$sourceItemId]['item_id'] = $foundDestItem['item_id'];
                    $idMapper['existing'][$sourceItemId] = $foundDestItem['item_id'];

                    //unset this item, because in the end the items that are left will be deleted
                    unset($destOrderItems['RB'][$foundDestItemKey]);
                }
                else {
                    $orderItem = $sourceItem;
                }

                foreach ($fieldMapper as $sourceField => $destField) {
                    $destOrderItems[$operation][$sourceItemId][$destField] = $orderItem[$sourceField];
                }

                if ($operation == 'to_insert') {
                    $destOrderItems[$operation][$sourceItemId]['item_id'] = $orderItem['item_id'];
                }
            }
        }

        //collect data - other products. We only insert them and later the old will be removed as we have no way how to match them
        if (isset($sourceOrderItems['other'])) {
            foreach ($sourceOrderItems['other'] as $sourceItemId => $orderItem) {
                foreach ($fieldMapper as $sourceField => $destField) {
                    $destOrderItems['to_insert'][$sourceItemId . '-o'][$destField] = $orderItem[$sourceField];
                }
                $destOrderItems['to_insert'][$sourceItemId . '-o']['item_id'] = $orderItem['item_id'];
            }
        }

        //insert new items
        if (isset($destOrderItems['to_insert'])) {
            foreach ($destOrderItems['to_insert'] as $orderItem) {
                $sourceItemId = $orderItem['item_id'];
                unset($orderItem['item_id']);
                $orderItem['order_id'] = $destOrder['entity_id'];
                $orderItem['store_id'] = $this->_destinationStoreId;

                //fix serialized values
                if (!empty($orderItem['product_options'])) {
                    $arr = unserialize($orderItem['product_options']);
                    $orderItem['product_options'] = json_encode($arr);
                }
                if (!empty($orderItem['weee_tax_applied'])) {
                    $arr = unserialize($orderItem['weee_tax_applied']);
                    $orderItem['weee_tax_applied'] = json_encode($arr);
                }

                $this->_destinationDb->insert('sales_order_item', $orderItem);
                $idMapper['existing'][$sourceItemId] = $this->_destinationDb->lastInsertId();
            }
        }

        //update existing items
        if (isset($destOrderItems['to_update'])) {
            foreach ($destOrderItems['to_update'] as $orderItem) {
                $orderItem['order_id'] = $destOrder['entity_id'];

                //fix serialized values
                $str = unserialize($orderItem['product_options']);
                $orderItem['product_options'] = json_encode($str);
                $str = unserialize($orderItem['weee_tax_applied']);
                $orderItem['weee_tax_applied'] = json_encode($str);

                $this->_destinationDb->update('sales_order_item', $orderItem, ['item_id = ?' => $orderItem['item_id']]);
            }
        }

        //delete what's left (order was probably edited)
        if (isset($destOrderItems['shirtplatform'])) {
            foreach ($destOrderItems['shirtplatform'] as $orderItem) {
                $idMapper['removed'][$orderItem['item_id']] = $orderItem['item_id'];
                $this->_destinationDb->delete('sales_order_item', ['item_id = ?' => $orderItem['item_id']]);
            }
        }
        if (isset($destOrderItems['other'])) {
            foreach ($destOrderItems['other'] as $orderItem) {
                $idMapper['removed'][$orderItem['item_id']] = $orderItem['item_id'];
                $this->_destinationDb->delete('sales_order_item', ['item_id = ?' => $orderItem['item_id']]);
            }
        }
        if (isset($destOrderItems['RB'])) {
            foreach ($destOrderItems['RB'] as $orderItem) {
                $idMapper['removed'][$orderItem['item_id']] = $orderItem['item_id'];
                $this->_destinationDb->delete('sales_order_item', ['item_id = ?' => $orderItem['item_id']]);
            }
        }

        return $idMapper;
    }

    /**
     * Update/insert to entity item tables
     * 
     * @access private
     * @param array $sourceEntityData
     * @param string $sourceMainTable
     * @param array $destEntity destination entity data
     * @param array $destOrderData full destination documents processed so far
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processEntityItems($sourceEntityData,
                                         $sourceMainTable,
                                         $destEntity,
                                         $destOrderData) {
        $sourceTable = $sourceMainTable . '_item';
        $destTable = $this->_relatedTables[$sourceMainTable][$sourceTable];
        $fieldMapper = $this->getFieldMapper($destTable);
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', $destTable));
        }

        $idOrderItemMapper = $destOrderData['id_order_item_mapper'];
        $destItems = [];

        //collect data for entity items
        foreach ($sourceEntityData['related_tables'][$sourceTable] as $key => $sourceItem) {
            $destKey = $key;
            $destOrderItemId = null;

            if (isset($idOrderItemMapper['existing'][$sourceItem['order_item_id']])) {
                $destOrderItemId = $idOrderItemMapper['existing'][$sourceItem['order_item_id']];
                $destKey = $destOrderItemId;
            }

            foreach ($fieldMapper as $sourceField => $destField) {
                if ($destField == 'weee_tax_applied' and ! empty($sourceItem[$sourceField])) {
                    //fix serialized values                
                    $arr = unserialize($sourceItem[$sourceField]);
                    $destItems[$destKey][$destField] = json_encode($arr);
                }
                else {
                    $destItems[$destKey][$destField] = $sourceItem[$sourceField];
                }
            }

            $destItems[$destKey]['parent_id'] = $destEntity['entity_id'];
            if ($destOrderItemId) {
                $destItems[$destKey]['order_item_id'] = $destOrderItemId;
//                $destItems[$destKey]['parent_id'] = $destEntity['entity_id'];

                //find corresponding item 
                foreach ($destEntity['related_tables'][$destTable] as $destEntityItem) {
                    if ($destEntityItem['order_item_id'] == $destOrderItemId) {
                        $destItems[$destKey]['entity_id'] = $destEntityItem['entity_id'];
                    }
                }
            }
        }

        foreach ($destItems as $item) {
            if (!empty($item['entity_id'])) {
                $this->_destinationDb->update($destTable, $item, ['entity_id = ?' => $item['entity_id']]);
            }
            else {
                $this->_destinationDb->insert($destTable, $item);
            }
        }

        //delete items which corresponding order items have been deleted
        if (isset($idOrderItemMapper['removed'])) {
            foreach ($idOrderItemMapper['removed'] as $orderItemId) {
                $this->_destinationDb->delete($destTable, ['order_item_id = ?' => $orderItemId]);
            }
        }
    }

    /**
     * Process order comments and insert new ones. Comments are only added, there is
     * no need to update existing
     * 
     * @access private
     * @param string $sourceOrderData
     * @param string $destOrder     
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processOrderComment($sourceOrderData,
                                          $destOrder) {
        $fieldMapper = $this->getFieldMapper('sales_order_status_history');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_order_status_history'));
        }

        //remove comments that exist in both databases        
        if (isset($destOrder['related_tables']['sales_order_status_history'])) {
            foreach ($sourceOrderData['related_tables']['sales_flat_order_status_history'] as $sourceKey => $sourceComment) {
                foreach ($destOrder['related_tables']['sales_order_status_history'] as $destKey => $destComment) {
                    if ($sourceComment['created_at'] == $destComment['created_at']) {
                        unset($sourceOrderData['related_tables']['sales_flat_order_status_history'][$sourceKey]);
                        unset($destOrder['related_tables']['sales_order_status_history'][$destKey]);
                        break;
                    }
                }
            }
        }

        //collect new comments
        $newComments = [];
        if (isset($sourceOrderData['related_tables']['sales_flat_order_status_history'])) {
            foreach ($sourceOrderData['related_tables']['sales_flat_order_status_history'] as $key => $sourceComment) {
                foreach ($fieldMapper as $sourceField => $destField) {
                    $newComments[$key][$destField] = $sourceComment[$sourceField];
                }
                $newComments[$key]['parent_id'] = $destOrder['entity_id'];
            }
        }

        if (!empty($newComments)) {
            foreach ($newComments as $comment) {
                $this->_destinationDb->insert('sales_order_status_history', $comment);
            }
        }
    }

    /**
     * Process the comments and insert new ones. Comments are only added, there is
     * no need to update existing
     * 
     * @access private
     * @param array $sourceEntityData
     * @param string $sourceMainTable
     * @param array $destEntity     
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processEntityComment($sourceEntityData,
                                           $sourceMainTable,
                                           $destEntity) {
        $sourceTable = $sourceMainTable . '_comment';
        $destTable = $this->_relatedTables[$sourceMainTable][$sourceTable];
        $fieldMapper = $this->getFieldMapper($destTable);
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', $destTable));
        }

        //remove comments that exist in both databases        
        foreach ($sourceEntityData['related_tables'][$sourceTable] as $sourceKey => $sourceComment) {
            foreach ($destEntity['related_tables'][$destTable] as $destKey => $destComment) {
                if ($sourceComment['created_at'] == $destComment['created_at']) {
                    unset($sourceEntityData['related_tables'][$sourceTable][$sourceKey]);
                    unset($destEntity['related_tables'][$destTable][$destKey]);
                    break;
                }
            }
        }

        //collect new comments
        $newComments = [];
        if (isset($sourceEntityData['related_tables'][$sourceTable])) {
            foreach ($sourceEntityData['related_tables'][$sourceTable] as $key => $sourceComment) {
                foreach ($fieldMapper as $sourceField => $destField) {
                    $newComments[$key][$destField] = $sourceComment[$sourceField];
                }
                $newComments[$key]['parent_id'] = $destEntity['entity_id'];
            }
        }

        if (!empty($newComments)) {
            foreach ($newComments as $comment) {
                $this->_destinationDb->insert($destTable, $comment);
            }
        }
    }

    /**
     * Process the tracks and insert new ones. Tracks are only added, there is
     * no need to update existing
     * 
     * @access private
     * @param array $sourceEntityData     
     * @param array $destEntity     
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _processTracks($sourceEntityData,
                                    $destEntity) {
        $fieldMapper = $this->getFieldMapper('sales_shipment_track');
        if (empty($fieldMapper)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing field mapper for table %1', 'sales_shipment_track'));
        }

        //remove tracks that exist in both databases
        foreach ($sourceEntityData['related_tables']['sales_flat_shipment_track'] as $sourceKey => $sourceTrack) {
            foreach ($destEntity['related_tables']['sales_shipment_track'] as $destKey => $destTrack) {
                if ($sourceTrack['track_number'] == $destTrack['track_number'] and $sourceTrack['created_at'] == $destTrack['created_at']) {
                    unset($sourceEntityData['related_tables']['sales_flat_shipment_track'][$sourceKey]);
                    unset($destEntity['related_tables']['sales_shipment_track'][$destKey]);
                    break;
                }
            }
        }

        //collect new tracks
        $newTracks = [];
        if (isset($sourceEntityData['related_tables']['sales_flat_shipment_track'])) {
            foreach ($sourceEntityData['related_tables']['sales_flat_shipment_track'] as $key => $sourceTrack) {
                foreach ($fieldMapper as $sourceField => $destField) {
                    $newTracks[$key][$destField] = $sourceTrack[$sourceField];
                }
                $newTracks[$key]['parent_id'] = $destEntity['entity_id'];
                $newTracks[$key]['order_id'] = $destEntity['order_id'];
            }
        }

        if (!empty($newTracks)) {
            foreach ($newTracks as $track) {
                $this->_destinationDb->insert('sales_shipment_track', $track);
            }
        }
    }

    /**
     * Process parent - child relations between orders
     * 
     * @access private
     */
    private function _processOrderRelations() {
        if (!empty($this->_orderChildRelations)) {
            foreach ($this->_orderChildRelations as $entityId => $childRealId) {
                $orderData = [];
                $select = $this->_destinationDb->select()->from('sales_order')
                        ->where('increment_id = ?', $childRealId);
                $relationRecord = $this->_destinationDb->fetchAll($select);

                if (!empty($relationRecord)) {
                    $orderData['relation_child_id'] = $relationRecord[0]['entity_id'];
                    $this->_destinationDb->update('sales_order', $orderData, ['entity_id = ?' => $entityId]);
                }
            }
        }
        if (!empty($this->_orderParentRelations)) {
            foreach ($this->_orderParentRelations as $entityId => $parentRealId) {
                $destData = [];
                $select = $this->_destinationDb->select()->from('sales_order')
                        ->where('increment_id = ?', $parentRealId);
                $relationRecord = $this->_destinationDb->fetchAll($select);

                if (!empty($relationRecord)) {
                    $destOrder['relation_parent_id'] = $relationRecord[0]['entity_id'];
                    $this->_destinationDb->update('sales_order', $orderData, ['entity_id = ?' => $entityId]);
                }
            }
        }
    }

    /**
     * Get destination customer data from source customerId
     * 
     * @access private
     * @param int $sourceCustomerId
     * @return array
     */
    private function _getCustomerData($sourceCustomerId) {
        $destCustomerData = [];

        $select = $this->_sourceDb->select()->from('customer_entity')
                ->where('entity_id = ?', $sourceCustomerId);
        $sourceCustomer = $this->_sourceDb->fetchAll($select);

        if (!empty($sourceCustomer)) {
            $sourceCustomer = $sourceCustomer[0];
            $select = $this->_destinationDb->select()->from('customer_entity')
                    ->where('email = ?', $sourceCustomer['email'])
                    ->where('store_id = ?', $this->_destinationStoreId);
            $destCustomer = $this->_destinationDb->fetchAll($select);

            if (!empty($destCustomer)) {
                $destCustomer = $destCustomer[0];
                $select = $this->_destinationDb->select()->from('customer_address_entity')
                        ->where('parent_id = ?', $destCustomer['entity_id']);
                $destCustomerData['customer_address_entity'] = $this->_destinationDb->fetchAll($select);
                $destCustomerData['customer_entity'] = $destCustomer;
            }
        }

        return $destCustomerData;
    }

    /**
     * Find address from destination database which equals the $sourceAddress.
     * Compared attributes are firstname, lastname, street and city
     * 
     * @access private
     * @param array $sourceAddress
     * @param array $destAddresses
     * @return array
     */
    private function _findDestinationAddress($sourceAddress,
                                             $destAddresses) {
        $result = [];

        $firstname = $sourceAddress['firstname'];
        $lastname = $sourceAddress['lastname'];
        $street = $sourceAddress['street'];
        $city = $sourceAddress['city'];

        foreach ($destAddresses as $dest) {
            if ($firstname == $dest['firstname'] and $lastname == $dest['lastname'] and $street == $dest['street'] and $city == $dest['city']) {
                $result = $dest;
                break;
            }
        }

        return $result;
    }

    /**
     * Main migration function
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output     
     */
    public function migrate($output) {
        parent::migrate($output);
        $this->_createCustomerGroupMapper();
        $select = $this->_sourceDb->select()->from('sales_flat_order', ['COUNT(*) AS count'])
                ->where('store_id = ?', $this->_sourceStoreId);

        $result = $this->_sourceDb->fetchAll($select);
        $entityCount = $result[0]['count'];
        $output->writeln('Total orders count: ' . $entityCount);
        $limit = 1000;        
        $offset = 0;
        $processed = 0;
        $end = false;
        $this->_extraFeeTitle = $this->_getConfigValue('//destination/extra_fee_title');

        while (!$end) {
            $salesData = $this->_getSourceData($limit, $offset);
            $this->_processData($salesData, $output);
            $processed += count($salesData);

            if ($offset + $limit >= $entityCount) {
                $end = true;
            }

            $offset += $limit;
            $output->writeln('Processed orders: ' . $processed);
        }

        $this->_processOrderRelations();
        
        if ($this->_errorCount) {
            $msg = 'There were '. $this->_errorCount .' errors during the import. Please check the log file var/log/shirtplatform_migration.log';
            $output->writeln($msg);
        }
    }

    /**
     * Validate if the migration was successful. Compares source and destination
     * data
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function testMigratedData($output) {
        $output->writeln('=====================Start testing data after migration=====================');
        $select = $this->_sourceDb->select()->from('sales_flat_order', ['COUNT(*) AS count'])
                ->where('store_id = ?', $this->_sourceStoreId);

        $result = $this->_sourceDb->fetchAll($select);
        $entityCount = $result[0]['count'];        
        $limit = 1000;
        $offset = 0;
        $processed = 0;
        $end = false;

        while (!$end) {
            $sourceData = $this->_getSourceData($limit, $offset);
            $destData = $this->_getDestinationData($sourceData);

            foreach ($sourceData as $sourceKey => $sourceRow) {
                $orderItemMapper = $this->_testOrderData($sourceRow['sales_flat_order'], $destData[$sourceKey]['sales_order'], $output);

                foreach ($sourceRow as $sourceMainTable => $sourceDocuments) {
                    if ($sourceMainTable == 'sales_flat_order') {
                        continue;
                    }

                    //collect destination entities by their increment id
                    $destMainTable = $this->_mainTableMapper[$sourceMainTable];
                    $destEntities = [];

                    foreach ($destData[$sourceKey][$destMainTable] as $destMainEntity) {
                        $destEntities[$destMainEntity['increment_id']] = $destMainEntity;
                    }

                    foreach ($sourceDocuments as $sourceEntity) {
                        if (isset($destEntities[$sourceEntity['increment_id']])) {
                            $this->_testEntityData($sourceEntity, $sourceMainTable, $destEntities[$sourceEntity['increment_id']], $orderItemMapper, $output);
                        }
                        else {
                            $msg = 'Missing record in ' . $destMainTable . ' for increment_id ' . $sourceEntity['increment_id'];
                            $output->writeln('<error>' . $msg . '</error>');
                        }
                    }
                }
            }

            $processed += count($sourceData);

            if ($offset + $limit >= $entityCount) {
                $end = true;
            }

            $offset += $limit;
            $output->writeln('Tested orders: ' . $processed);
        }
    }

    /**
     * Test order data consistency
     * 
     * @access private
     * @param array $sourceRow
     * @param array $destRow
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return array order item mapper mapping source item_id => destination item_id
     */
    private function _testOrderData($sourceRow,
                                    $destRow,
                                    $output) {
        $orderItemMapper = [];

        //order
        $fieldMapper = $this->getFieldMapper('sales_order');
        foreach ($fieldMapper as $sourceField => $destField) {
            if (in_array($sourceField, ['created_at', 'updated_at'])) {
                continue;
            }

            if ($destField == 'status') {
                if ((preg_match('/production_wh/', $sourceRow[$sourceField]) or $sourceRow[$sourceField] == 'production_selfmanufacture') and $destRow[$destField] != 'in_production') {
                    $msg = 'Order ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
            elseif ($sourceRow[$sourceField] != $destRow[$destField]) {
                if (in_array($destField, ['extra_fee', 'base_extra_fee', 'extra_fee_excl_tax', 'base_extra_fee_excl_tax'])) {
                    if ($sourceRow[$sourceField] == null and $destRow[$destField] == 0) {
                        continue;
                    }
                }
                $msg = 'Order ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                $output->writeln('<error>' . $msg . '</error>');
            }
        }

        //addresses
        $fieldMapper = $this->getFieldMapper('sales_order_address');
        $sourceAddresses = $destAddresses = [];

        foreach ($sourceRow['related_tables']['sales_flat_order_address'] as $address) {
            $addressType = $address['address_type'];
            $sourceAddresses[$addressType] = $address;
        }

        foreach ($destRow['related_tables']['sales_order_address'] as $address) {
            $addressType = $address['address_type'];
            $destAddresses[$addressType] = $address;
        }

        foreach ($sourceAddresses as $addressType => $_sourceAddress) {
            $_destAddress = $destAddresses[$addressType];

            foreach ($fieldMapper as $sourceField => $destField) {
                if ($_sourceAddress[$sourceField] != $_destAddress[$destField]) {
                    $msg = $addressType . ' address for ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }

        //payment
        $fieldMapper = $this->getFieldMapper('sales_order_payment');
        foreach ($sourceRow['related_tables']['sales_flat_order_payment'] as $key => $sourcePayment) {
            foreach ($fieldMapper as $sourceField => $destField) {
                if ($destField == 'additional_information') {
                    if (! empty($sourcePayment[$sourceField])) {
                        $sourceVal = unserialize($sourcePayment[$sourceField]);
                        $sourceVal = json_encode($sourceVal);
                        if ($sourceVal != $destRow['related_tables']['sales_order_payment'][$key][$destField]) {
                            $msg = 'Payment record for ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                            $output->writeln('<error>' . $msg . '</error>');
                        }
                    }
                }
                elseif ($destField == 'method' and $sourcePayment[$sourceField] == 'gopay_standard') {
                    if ($destRow['related_tables']['sales_order_payment'][$key][$destField] != 'gopay_gateway') {
                        $msg = 'Payment record for ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' should be gopay_gateway';
                        $output->writeln('<error>' . $msg . '</error>');
                    }
                }
                elseif ($sourcePayment[$sourceField] != $destRow['related_tables']['sales_order_payment'][$key][$destField]) {
                    $msg = 'Payment record for ' . $destRow['increment_id'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }

        //order items
        $fieldMapper = $this->getFieldMapper('sales_order_item');
        $sourceItems = $destItems = [];

        foreach ($sourceRow['related_tables']['sales_flat_order_item'] as $item) {
            if ($item['shirt_id']) {
                $sourceItems[$item['shirt_id']] = $item;
            }
        }
        foreach ($destRow['related_tables']['sales_order_item'] as $item) {
            if ($item['shirtplatform_id']) {
                $destItems[$item['shirtplatform_id']] = $item;
            }
        }

        foreach ($sourceItems as $shirtplatformId => $_sourceItem) {
            foreach ($fieldMapper as $sourceField => $destField) {
                if (in_array($sourceField, ['created_at', 'updated_at'])) {
                    continue;
                }

                if (isset($destItems[$shirtplatformId])) {
                    if (in_array($destField, ['product_options', 'weee_tax_applied']) and ! empty($_sourceItem[$sourceField])) {
                        $sourceVal = unserialize($_sourceItem[$sourceField]);
                        $sourceVal = json_encode($sourceVal);
                        if ($sourceVal != $destItems[$shirtplatformId][$destField]) {
                            $msg = 'Order item with shirtplatform_id ' . $shirtplatformId . ' for order ' . $destRow['increment_id'] .
                                    ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                            $output->writeln('<error>' . $msg . '</error>');
                        }
                    }
                    elseif ($_sourceItem[$sourceField] != $destItems[$shirtplatformId][$destField]) {
                        $msg = 'Order item with shirtplatform_id ' . $shirtplatformId . ' for order ' . $destRow['increment_id'] .
                                ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                        $output->writeln('<error>' . $msg . '</error>');
                    }
                    $orderItemMapper[$_sourceItem['item_id']] = $destItems[$shirtplatformId]['item_id'];
                }
                else {
                    $msg = 'Missing order item with shirtplatform_id ' . $shirtplatformId . ' for order ' . $destRow['increment_id'];
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }

        //order comments
        if (count($sourceRow['related_tables']['sales_flat_order_status_history']) > count($destRow['related_tables']['sales_order_status_history'])) {
            $msg = 'Number of comments for order ' . $destRow['increment_id'] . ' is different';
            $output->writeln('<error>' . $msg . '</error>');
        }

        return $orderItemMapper;
    }

    private function _testEntityData($sourceEntity,
                                     $sourceMainTable,
                                     $destEntity,
                                     $orderItemMapper,
                                     $output) {
        //main entity
        $destMainTable = $this->_mainTableMapper[$sourceMainTable];
        $fieldMapper = $this->getFieldMapper($destMainTable);

        foreach ($fieldMapper as $sourceField => $destField) {
            if (in_array($sourceField, ['created_at', 'updated_at'])) {
                continue;
            }

            if (!empty($sourceEntity[$sourceField]) and !empty($destEntity[$destField]) and $sourceEntity[$sourceField] != $destEntity[$destField]) {                
                $msg = 'Error in document with increment_id ' . $sourceEntity['increment_id'] . ', fields ' . $sourceField .
                        ' and ' . $destField . " don't match";
                $output->writeln('<error>' . $msg . '</error>');
            }
        }

        //entity items
        $sourceTable = $sourceMainTable . '_item';
        $destTable = $this->_relatedTables[$sourceMainTable][$sourceTable];
        $fieldMapper = $this->getFieldMapper($destTable);
        $destItems = [];

        foreach ($destEntity['related_tables'][$destTable] as $item) {
            $destItems[$item['order_item_id']] = $item;
        }

        foreach ($sourceEntity['related_tables'][$sourceTable] as $sourceItem) {
            if (!isset($orderItemMapper[$sourceItem['order_item_id']])) {
                //remove background or item missing shirtplatform_id
//                $msg = 'Order item with ' . $sourceItem['order_item_id'] . ' missing in order item mapper';
//                $output->writeln('<error>' . $msg . '</error>');
                continue;
            }

            $orderItemId = $orderItemMapper[$sourceItem['order_item_id']];
            $_destItem = $destItems[$orderItemId];

            foreach ($fieldMapper as $sourceField => $destField) {
                if ($destField == 'weee_tax_applied' and ! empty($sourceItem[$sourceField])) {
                    $sourceVal = unserialize($sourceItem[$sourceField]);
                    $sourceVal = json_encode($sourceVal);
                    if ($sourceVal != $_destItem[$destField]) {
                        $msg = 'Error validating ' . $destTable . ', destination item with etity_id ' . $_destItem['entity_id'] .
                                ' has different value in column ' . $destField;
                        $output->writeln('<error>' . $msg . '</error>');
                    }
                }
                elseif ($sourceItem[$sourceField] != $_destItem[$destField]) {
                    $msg = 'Error validating ' . $destTable . ', destination item with etity_id ' . $_destItem['entity_id'] .
                            ' has different value in column ' . $destField;
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }

        //comments
        $sourceTable = $sourceMainTable . '_comment';
        $destTable = $this->_relatedTables[$sourceMainTable][$sourceTable];
        if (count($sourceEntity['related_tables'][$sourceTable]) != count($destEntity['related_tables'][$destTable])) {
            $msg = 'Number of comments for entity ' . $destEntity['increment_id'] . ' is different';
            $output->writeln('<error>' . $msg . '</error>');
        }

        //shipment tracks
        if ($sourceMainTable == 'sales_flat_shipment') {
            $fieldMapper = $this->getFieldMapper('sales_shipment_track');
            $destTracks = [];

            foreach ($destEntity['related_tables']['sales_shipment_track'] as $track) {
                $key = $track['track_number'] . '-' . $track['created_at'];
                $destTracks[$key] = $track;
            }

            foreach ($sourceEntity['related_tables']['sales_flat_shipment_track'] as $sourceTrack) {
                $key = $sourceTrack['track_number'] . '-' . $sourceTrack['created_at'];
                if (!isset($destTracks[$key])) {
                    $msg = 'Destination shipment track with track_number ' . $sourceTrack['track_number'] . ' and created_at ' .
                            $sourceTrack['created_at'] . " doesn't exist";
                    $output->writeln('<error>' . $msg . '</error>');
                    continue;
                }

                $_destTrack = $destTracks[$key];
                foreach ($fieldMapper as $sourceField => $destField) {
                    if (in_array($sourceField, ['created_at', 'updated_at'])) {
                        continue;
                    }

                    if ($sourceTrack[$sourceField] != $_destTrack[$destField]) {
                        $msg = 'Shipment track with track_number ' . $_destTrack['track_number'] . ' and created_at ' .
                                $_destTrack['created_at'] . ' fields ' . $sourceField . ' and ' . $destField . " don't match";
                        $output->writeln('<error>' . $msg . '</error>');
                    }
                }
            }
        }
    }

}
