<?php

namespace Shirtplatform\Core\Model\Migration;

class Customers extends Entity
{

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory
     */
    private $_entityTypeFactory;

    /**
     * @param \Shirtplatform\Core\Helper\Data $coreHelper
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Eav\Model\Entity\TypeFactory $entityTypeFactory
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     * @param \Magento\Framework\DB\Adapter\Pdo\MysqlFactory $mysqlFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Arguments\ValidationState $validationState     
     */
    public function __construct(
        \Shirtplatform\Core\Helper\Data $coreHelper,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Eav\Model\Entity\TypeFactory $entityTypeFactory,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\DB\Adapter\Pdo\MysqlFactory $mysqlFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Arguments\ValidationState $validationState
    ) {
        parent::__construct($coreHelper, $directoryList, $moduleReader, $mysqlFactory, $resource, $storeManager, $validationState);
        $this->_entityTypeFactory = $entityTypeFactory;
    }

    /**
     * Read source attributes and save them to class property
     * 
     * @access private
     */
    private function _readSourceAttributes()
    {
        //get customer attributes
        $select = $this->_sourceDb->select()->from(['ea' => 'eav_attribute'])
            ->join(['eat' => 'eav_entity_type'], 'ea.entity_type_id = eat.entity_type_id', ['entity_type_id', 'entity_type_code'])
            ->where('eat.entity_type_code = ?', 'customer');
        $result = $this->_sourceDb->fetchAssoc($select);

        foreach ($result as $attr) {
            $this->_sourceAttributes['customer'][$attr['attribute_id']] = $attr;
        }

        //get customer address attributes
        $select = $this->_sourceDb->select()->from(['ea' => 'eav_attribute'])
            ->join(['eat' => 'eav_entity_type'], 'ea.entity_type_id = eat.entity_type_id', ['entity_type_id', 'entity_type_code'])
            ->where('eat.entity_type_code = ?', 'customer_address');
        $result = $this->_sourceDb->fetchAssoc($select);

        foreach ($result as $attr) {
            $this->_sourceAttributes['customer_address'][$attr['attribute_id']] = $attr;
        }
    }

    /**
     * Read destination attributes and save them to class property
     * 
     * @access private
     */
    private function _readDestinationAttributes()
    {
        $customerEntity = $this->_entityTypeFactory->create()->loadByCode('customer');
        $customerAddressEntity = $this->_entityTypeFactory->create()->loadByCode('customer_address');
        $customerAttributes = $customerEntity->getAttributeCollection();
        $customerAddressAttributes = $customerAddressEntity->getAttributeCollection();

        foreach ($customerAttributes as $attr) {
            $this->_destinationAttributes['customer'][$attr->getAttributeId()] = $attr->getData();
        }

        foreach ($customerAddressAttributes as $attr) {
            $this->_destinationAttributes['customer_address'][$attr->getAttributeId()] = $attr->getData();
        }
    }

    /**
     * Create mapper between source and destination attributes
     * 
     * @access private
     */
    private function _mapAttributes()
    {
        //map customer attributes
        foreach ($this->_sourceAttributes['customer'] as $sourceAttr) {
            foreach ($this->_destinationAttributes['customer'] as $destAttr) {
                if ($destAttr['attribute_code'] == $sourceAttr['attribute_code']) {
                    $this->_attributeIdMapper[$sourceAttr['attribute_id']] = $destAttr['attribute_id'];
                    break;
                }
            }
        }

        //map customer address attributes
        foreach ($this->_sourceAttributes['customer_address'] as $sourceAttr) {
            foreach ($this->_destinationAttributes['customer_address'] as $destAttr) {
                if ($destAttr['attribute_code'] == $sourceAttr['attribute_code']) {
                    $this->_attributeIdMapper[$sourceAttr['attribute_id']] = $destAttr['attribute_id'];
                    break;
                }
            }
        }
    }

    /**
     * Go over all entity attribute tables and collect data
     * 
     * @access private
     * @param string $source (source or destination)
     * @param string $entity (customer, customer_address)
     * @param string $condField column that will be used in where condition
     * @param string|int $condValue value of the condition field
     * @return array
     */
    private function _getEntityAttributeData(
        $source,
        $entity,
        $condField,
        $condValue
    ) {
        $out = [];
        if ($source == 'source') {
            $database = $this->_sourceDb;
            $attributes = $this->_sourceAttributes;
        } else {
            $database = $this->_destinationDb;
            $attributes = $this->_destinationAttributes;
        }

        $tables = ['datetime', 'decimal', 'int', 'text', 'varchar'];
        foreach ($tables as $_table) {
            $tableName = $entity . '_entity_' . $_table;
            $select = $database->select()->from($tableName)
                ->where($condField . ' = ?', $condValue);
            $result = $database->fetchAssoc($select);

            foreach ($result as $row) {
                if (isset($attributes[$entity][$row['attribute_id']])) {
                    $attr = $attributes[$entity][$row['attribute_id']];
                    $out[$attr['attribute_code']] = $row;
                }
            }
        }

        return $out;
    }

    /**
     * Get customer data from the source database
     * 
     * @access private
     * @param int $limit
     * @param int $offset
     * @return array
     */
    private function _getSourceCustomerData(
        $limit,
        $offset
    ) {
        $customerData = [];

        //get customer record
        $select = $this->_sourceDb->select()->from('customer_entity')
            ->where('store_id = ?', $this->_sourceStoreId)
            //                ->where('entity_id = ?', 33228)
            ->limit($limit, $offset);
        $result = $this->_sourceDb->fetchAssoc($select);

        foreach ($result as $row) {
            //get customer attributes                
            $customerData[$row['entity_id']]['customer'] = $this->_getEntityAttributeData('source', 'customer', 'entity_id', $row['entity_id']);
            $customerData[$row['entity_id']]['customer']['entity'] = $row;

            //get customer_address record
            $select = $this->_sourceDb->select()->from('customer_address_entity')
                ->where('parent_id = ?', $row['entity_id']);
            $addressResult = $this->_sourceDb->fetchAll($select);

            foreach ($addressResult as $address) {
                $addressId = $address['entity_id'];
                $customerData[$row['entity_id']]['customer_address'][$addressId] = $this->_getEntityAttributeData('source', 'customer_address', 'entity_id', $address['entity_id']);
                $customerData[$row['entity_id']]['customer_address'][$addressId]['entity'] = $address;
            }
        }

        return $customerData;
    }

    /**
     * Get customer data from the destination database
     * 
     * @access private
     * @param array $sourceData
     * @return array
     */
    private function _getDestinationCustomerData($sourceData)
    {
        $customerData = [];

        foreach ($sourceData as $sourceCustomerId => $sourceCustomer) {
            //get customer record
            $select = $this->_destinationDb->select()->from('customer_entity')
                ->where('email = ?', $sourceCustomer['customer']['entity']['email'])
                ->where('store_id = ?', $this->_destinationStoreId);
            $result = $this->_destinationDb->fetchAssoc($select);

            foreach ($result as $row) {
                //get customer attributes                
                $customerData[$sourceCustomerId]['customer'] = $this->_getEntityAttributeData('destination', 'customer', 'entity_id', $row['entity_id']);
                $customerData[$sourceCustomerId]['customer']['entity'] = $row;

                //get customer_address record
                $select = $this->_destinationDb->select()->from('customer_address_entity')
                    ->where('parent_id = ?', $row['entity_id']);
                $addressResult = $this->_destinationDb->fetchAll($select);

                foreach ($addressResult as $address) {
                    $addressId = $address['entity_id'];
                    $customerData[$sourceCustomerId]['customer_address'][$addressId] = $this->_getEntityAttributeData('destination', 'customer_address', 'entity_id', $addressId);
                    $customerData[$sourceCustomerId]['customer_address'][$addressId]['entity'] = $address;
                }
            }
        }

        return $customerData;
    }

    /**
     * Process customer data - insert/update records in customer_entity, customer_address_entity
     * and their EAV attribute tables
     * 
     * @access private
     * @param array $customerData
     */
    private function _processCustomerData($customerData)
    {
        //each iteration should be done in a transaction and try catch block
        foreach ($customerData as $sourceCustomer) {
            try {
                $this->_destinationDb->beginTransaction();
                $staticAttributes = ['prefix', 'firstname', 'middlename', 'lastname', 'suffix', 'dob', 'password_hash', 'taxvat', 'gender'];
                $staticDbData = $eavDbData = [];

                $select = $this->_destinationDb->select()->from('customer_entity')
                    ->where('email = ?', $sourceCustomer['customer']['entity']['email'])
                    ->where('store_id = ?', $this->_destinationStoreId);
                $destCustomer = $this->_destinationDb->fetchAll($select);

                //collect static attributes
                foreach ($staticAttributes as $attrCode) {
                    if (isset($sourceCustomer['customer'][$attrCode])) {
                        $staticDbData[$attrCode] = $sourceCustomer['customer'][$attrCode]['value'];
                    }
                }
                $staticDbData['password_hash'] .= ':0';

                //update
                if ($destCustomer) {
                    $customerId = $destCustomer[0]['entity_id'];

                    if ($destCustomer[0]['website_id'] != $this->_destinationWebsiteId) {
                        $staticDbData['website_id'] = $this->_destinationWebsiteId;
                    }

                    //update customer_entity
                    $this->_destinationDb->update('customer_entity', $staticDbData, ['entity_id = ?' => $customerId]);

                    $this->_processEavData($eavDbData, $customerId);
                }
                //insert
                else {
                    $groupId = $sourceCustomer['customer']['entity']['group_id'];
                    if (isset($this->_customerGroupMapper[$groupId])) {
                        $groupId = $this->_customerGroupMapper[$groupId];
                    }

                    $staticDbData['website_id'] = $this->_destinationWebsiteId;
                    $staticDbData['email'] = $sourceCustomer['customer']['entity']['email'];
                    $staticDbData['group_id'] = $groupId;
                    $staticDbData['increment_id'] = $sourceCustomer['customer']['entity']['increment_id'];
                    $staticDbData['store_id'] = $this->_destinationStoreId;
                    $staticDbData['created_at'] = $sourceCustomer['customer']['entity']['created_at'];
                    $staticDbData['updated_at'] = $sourceCustomer['customer']['entity']['updated_at'];
                    $staticDbData['is_active'] = $sourceCustomer['customer']['entity']['is_active'];
                    $staticDbData['disable_auto_group_change'] = $sourceCustomer['customer']['entity']['disable_auto_group_change'];
                    $staticDbData['created_in'] = $this->_destinationStore->getName();

                    //insert to customer_entity table
                    $this->_destinationDb->insert('customer_entity', $staticDbData);
                    $customerId = $this->_destinationDb->lastInsertId();

                    $this->_processEavData($eavDbData);
                }

                //collect EAV attributes
                foreach ($sourceCustomer['customer'] as $attrCode => $attrData) {
                    if ($attrCode == 'entity') {
                        continue;
                    }

                    if (!in_array($attrCode, $staticAttributes) and isset($this->_attributeIdMapper[$attrData['attribute_id']])) {
                        $destAttrId = $this->_attributeIdMapper[$attrData['attribute_id']];
                        $destAttrData = $this->_destinationAttributes['customer'][$destAttrId];

                        if ($destAttrData['backend_type'] != 'static') {
                            $tableName = 'customer_entity_' . $destAttrData['backend_type'];
                            $eavDbData[$tableName][$destAttrId]['attribute_id'] = $destAttrId;
                            $eavDbData[$tableName][$destAttrId]['entity_id'] = $customerId;
                            $eavDbData[$tableName][$destAttrId]['value'] = $attrData['value'];
                        }
                    }
                }

                //process customer eav data
                if ($destCustomer) {
                    $customerId = $destCustomer[0]['entity_id'];
                    $this->_processEavData($eavDbData, $customerId);
                } else {
                    $this->_processEavData($eavDbData);
                }

                $addressMapper = $this->_processCustomerAddressData($sourceCustomer, $customerId);

                //set default billing and shipping address
                $defaultBillingId = $defaultShippingId = null;
                if (isset($sourceCustomer['customer']['default_billing'])) {
                    $sourceDefaultBillingId = $sourceCustomer['customer']['default_billing']['value'];

                    if (isset($addressMapper[$sourceDefaultBillingId])) {
                        $defaultBillingId = $addressMapper[$sourceDefaultBillingId];
                    }
                }
                if (isset($sourceCustomer['customer']['default_shipping'])) {
                    $sourceDefaultShippingId = $sourceCustomer['customer']['default_shipping']['value'];

                    if (isset($addressMapper[$sourceDefaultShippingId])) {
                        $defaultShippingId = $addressMapper[$sourceDefaultShippingId];
                    }
                }

                if ($defaultBillingId or $defaultShippingId) {
                    $data = ['default_billing' => $defaultBillingId, 'default_shipping' => $defaultShippingId];
                    $this->_destinationDb->update('customer_entity', $data, ['entity_id = ?' => $customerId]);
                }

                $this->_destinationDb->commit();
            } catch (\Exception $ex) {
                $this->_destinationDb->rollback();
                throw new \Exception($ex->getMessage());
            }
        }
    }

    /**
     * Insert/update customer/customer_address eav attributes to their respective tables.
     * If $customerId is set it checks existing data, otherwise it only creates
     * new records
     * 
     * @access private
     * @param array $eavData
     * @param null|int $entityId
     */
    private function _processEavData(
        $eavData,
        $entityId = null
    ) {
        $existingEavData = [];

        //get existing attributes
        if ($entityId) {
            foreach ($eavData as $table => $attrData) {
                $select = $this->_destinationDb->select()->from($table)
                    ->where('entity_id = ?', $entityId);
                $result = $this->_destinationDb->fetchAll($select);

                foreach ($result as $row) {
                    $existingEavData[$table][$row['attribute_id']] = $row;
                }
            }
        }

        foreach ($eavData as $tableName => $tableData) {
            foreach ($tableData as $attrData) {
                $attrId = $attrData['attribute_id'];
                if (isset($existingEavData[$tableName]) and isset($existingEavData[$tableName][$attrId]) and in_array($attrId, $existingEavData[$tableName][$attrId])) {
                    $valueId = $existingEavData[$tableName][$attrId]['value_id'];
                    $this->_destinationDb->update(
                        $tableName,
                        $attrData,
                        ['value_id = ?' => $valueId]
                    );
                } else {
                    $this->_destinationDb->insert($tableName, $attrData);
                }
            }
        }
    }

    /**
     * Update/insert customer address data
     * 
     * @access private
     * @param array $customerData
     * @param int $customerId
     * @return array $addressMapper mapping sourceAddressId => destinationAddressId
     */
    private function _processCustomerAddressData(
        $customerData,
        $customerId
    ) {
        $existingAddresses = $addressMapper = [];
        $select = $this->_destinationDb->select()->from('customer_address_entity')
            ->where('parent_id = ?', $customerId);
        $addressEntities = $this->_destinationDb->fetchAssoc($select);

        $staticAttributes = [
            'city',
            'company',
            'country_id',
            'fax',
            'firstname',
            'lastname',
            'middlename',
            'postcode',
            'prefix',
            'region',
            'region_id',
            'street',
            'suffix',
            'telephone',
            'vat_id',
            'vat_is_valid',
            'vat_request_date',
            'vat_request_id',
            'vat_request_success'
        ];

        //get existing address attributes
        foreach ($addressEntities as $address) {
            $destAddressId = $address['entity_id'];
            $existingAddresses[$destAddressId] = $this->_getEntityAttributeData('destination', 'customer_address', 'entity_id', $destAddressId);
            $existingAddresses[$destAddressId]['entity'] = $address;
        }


        if (isset($customerData['customer_address'])) {
            foreach ($customerData['customer_address'] as $address) {
                $staticDbData = $eavDbData = [];
                $destAddress = $this->_findDestinationAddress($address, $existingAddresses);

                //collect static attributes
                foreach ($staticAttributes as $attrCode) {
                    if (isset($address[$attrCode])) {
                        if ($address[$attrCode]['value'] == null) {
                            $staticDbData[$attrCode] = '';
                        } else {
                            $staticDbData[$attrCode] = $address[$attrCode]['value'];
                        }
                    }
                }

                //update
                if (!empty($destAddress)) {
                    $destAddressId = $destAddress['entity']['entity_id'];
                    $this->_destinationDb->update('customer_address_entity', $staticDbData, ['entity_id = ?' => $destAddressId]);
                }
                //insert
                else {
                    $staticDbData['increment_id'] = $address['entity']['increment_id'];
                    $staticDbData['parent_id'] = $customerId;
                    $staticDbData['created_at'] = $address['entity']['created_at'];
                    $staticDbData['updated_at'] = $address['entity']['updated_at'];
                    $staticDbData['is_active'] = $address['entity']['is_active'];

                    $this->_destinationDb->insert('customer_address_entity', $staticDbData);
                    $destAddressId = $this->_destinationDb->lastInsertId();
                }

                $addressMapper[$address['entity']['entity_id']] = $destAddressId;

                //collect EAV attributes
                foreach ($address as $attrCode => $attrData) {
                    if ($attrCode == 'entity') {
                        continue;
                    }

                    if (!in_array($attrCode, $staticAttributes) and isset($this->_attributeIdMapper[$attrData['attribute_id']])) {
                        $destAttrId = $this->_attributeIdMapper[$attrData['attribute_id']];
                        $destAttrData = $this->_destinationAttributes['customer_address'][$destAttrId];

                        if ($destAttrData['backend_type'] != 'static') {
                            $tableName = 'customer_address_entity_' . $destAttrData['backend_type'];
                            $eavDbData[$tableName][$destAttrId]['attribute_id'] = $destAttrId;
                            $eavDbData[$tableName][$destAttrId]['entity_id'] = $destAddressId;
                            $eavDbData[$tableName][$destAttrId]['value'] = $attrData['value'];
                        }
                    }
                }

                //process customer address eav data
                if (!empty($destAddress)) {
                    $destAddressId = $destAddress['entity']['entity_id'];
                    $this->_processEavData($eavDbData, $destAddressId);
                } else {
                    $this->_processEavData($eavDbData);
                }
            }
        }

        return $addressMapper;
    }

    /**
     * Find address from destination database which equals the $sourceAddress.
     * Compared attributes are firstname, lastname, street, street_number and city
     * 
     * @access private
     * @param array $sourceAddress
     * @param array $destAddresses
     * @return array
     */
    private function _findDestinationAddress(
        $sourceAddress,
        $destAddresses
    ) {
        $result = [];

        foreach ($destAddresses as $dest) {
            if (!isset($sourceAddress['firstname']) and !isset($sourceAddress['lastname']) and !isset($sourceAddress['street']) and !isset($sourceAddress['street_number']) and !isset($sourceAddress['city']) and !isset($sourceAddress['company']) and !isset($sourceAddress['prefix']) and !isset($sourceAddress['postcode']) and !isset($sourceAddress['telephone'])) {
                continue;
            }
            if (!isset($dest['street_number'])) {
                continue;
            }

            $firstname = isset($sourceAddress['firstname']['value']) ? $sourceAddress['firstname']['value'] : '';
            $lastname = isset($sourceAddress['lastname']['value']) ? $sourceAddress['lastname']['value'] : '';
            $street = isset($sourceAddress['street']['value']) ? $sourceAddress['street']['value'] : '';
            $streetNumber = isset($sourceAddress['street_number']['value']) ? $sourceAddress['street_number']['value'] : '';
            $city = isset($sourceAddress['city']['value']) ? $sourceAddress['city']['value'] : '';
            $company = isset($sourceAddress['company']['value']) ? $sourceAddress['company']['value'] : '';
            $prefix = isset($sourceAddress['prefix']['value']) ? $sourceAddress['prefix']['value'] : '';
            $postcode = isset($sourceAddress['postcode']['value']) ? $sourceAddress['postcode']['value'] : '';
            $telephone = isset($sourceAddress['telephone']['value']) ? $sourceAddress['telephone']['value'] : '';

            if ($firstname == $dest['entity']['firstname'] and $lastname == $dest['entity']['lastname'] and $street == $dest['entity']['street'] and $streetNumber == $dest['street_number']['value'] and $city == $dest['entity']['city'] and $company == $dest['entity']['company'] and $prefix == $dest['entity']['prefix'] and $postcode == $dest['entity']['postcode'] and $telephone == $dest['entity']['telephone']) {
                $result = $dest;
                break;
            }
        }

        return $result;
    }

    /**
     * Main migration function
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output     
     */
    public function migrate($output)
    {
        parent::migrate($output);
        $this->_readDestinationAttributes();
        $this->_readSourceAttributes();
        $this->_mapAttributes();
        $this->_createCustomerGroupMapper();

        //get customer count
        $select = $this->_sourceDb->select()->from('customer_entity', ['COUNT(*) AS count'])
            ->where('store_id = ?', $this->_sourceStoreId);
        $result = $this->_sourceDb->fetchAll($select);
        $customersCount = $result[0]['count'];
        $output->writeln('Total number of customers to be processed: ' . $customersCount);
        //        $customersCount = 10;
        $limit = 1000;
        $offset = 0;
        $processed = 0;
        $end = false;

        while (!$end) {
            $customerData = $this->_getSourceCustomerData($limit, $offset);
            $this->_processCustomerData($customerData);
            $processed += count($customerData);

            if ($offset + $limit >= $customersCount) {
                $end = true;
            }

            $offset += $limit;
            $output->writeln('Processed customers: ' . $processed);
        }
    }

    /**
     * Validate if the migration was successful. Compares source and destination
     * data
     * 
     * @access public
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function testMigratedData($output)
    {
        $output->writeln('=====================Start testing data after migration=====================');
        $select = $this->_sourceDb->select()->from('customer_entity', ['COUNT(*) AS count'])
            ->where('store_id = ?', $this->_sourceStoreId);
        $result = $this->_sourceDb->fetchAll($select);
        $customersCount = $result[0]['count'];
        //        $customersCount = 10;
        $limit = 1000;
        $offset = 0;
        $processed = 0;
        $end = false;

        while (!$end) {
            $sourceData = $this->_getSourceCustomerData($limit, $offset);
            $destinationData = $this->_getDestinationCustomerData($sourceData);

            foreach ($sourceData as $customerId => $sourceCustomer) {
                $this->_testCustomerData($sourceCustomer['customer'], $destinationData[$customerId]['customer'], $output);

                if (isset($sourceCustomer['customer_address'])) {
                    $this->_testCustomerAddressData($sourceCustomer['customer_address'], $destinationData[$customerId]['customer_address'], $output);
                }
            }

            $processed += count($sourceData);

            if ($offset + $limit >= $customersCount) {
                $end = true;
            }

            $offset += $limit;
            $output->writeln('Tested customers: ' . $processed);
        }
    }

    /**
     * Test customer data consistency
     * 
     * @access private
     * @param array $sourceCustomer
     * @param array $destinationCustomer
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    private function _testCustomerData(
        $sourceCustomer,
        $destinationCustomer,
        $output
    ) {
        //test static attributes
        $staticAttributes = ['prefix', 'firstname', 'middlename', 'lastname', 'suffix', 'dob', 'password_hash', 'taxvat', 'gender'];
        foreach ($staticAttributes as $attr) {
            if (isset($sourceCustomer[$attr])) {
                $sourceValue = $sourceCustomer[$attr]['value'];
                $destValue = $destinationCustomer['entity'][$attr];

                if ($attr == 'dob') {
                    $sourceValue = date('Y-m-d', strtotime($sourceValue));
                    $destValue = date('Y-m-d', strtotime($destValue));
                } elseif ($attr == 'password_hash') {
                    $sourceValue .= ':0';
                }

                if ($sourceValue != $destValue) {
                    $msg = 'Customer (' . $destinationCustomer['entity']['email'] . ') attributes ' . $attr . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }

        //test custom attributes
        foreach ($destinationCustomer as $attr => $attrData) {
            if ($attr == 'entity') {
                continue;
            }

            if (isset($sourceCustomer[$attr]) and $sourceCustomer[$attr]['value'] != $attrData['value']) {
                $msg = 'Customer (' . $destinationCustomer['entity']['email'] . ') attributes ' . $attr . " don't match";
                $output->writeln('<error>' . $msg . '</error>');
            }
        }
    }

    /**
     * Test customer address data consistency
     * 
     * @access private
     * @param array $sourceAddresses
     * @param array $destinationAddresses
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    private function _testCustomerAddressData(
        $sourceAddresses,
        $destinationAddresses,
        $output
    ) {
        $staticAttributes = [
            'city',
            'company',
            'country_id',
            'fax',
            'firstname',
            'lastname',
            'middlename',
            'postcode',
            'prefix',
            'region',
            'region_id',
            'street',
            'suffix',
            'telephone',
            'vat_id',
            'vat_is_valid',
            'vat_request_date',
            'vat_request_id',
            'vat_request_success'
        ];

        $comparedAddresses = $destinationAddresses;
        $addressIdMapper = [];

        //create addressId mapper
        foreach ($sourceAddresses as $sourceAddressId => $sourceAddressData) {
            $foundAddress = $this->_findDestinationAddress($sourceAddressData, $comparedAddresses);

            if (!empty($foundAddress)) {
                $addressIdMapper[$sourceAddressId] = $foundAddress['entity']['entity_id'];
                unset($comparedAddresses[$foundAddress['entity']['entity_id']]);
            }
        }

        foreach ($sourceAddresses as $sourceAddressId => $sourceAddressData) {
            if (!isset($addressIdMapper[$sourceAddressId])) {
                $msg = 'Source address ID ' . $sourceAddressId . " doesn't have a corresponding destination address";
                $output->writeln('<error>' . $msg . '</error>');
                continue;
            }

            $destAddressId = $addressIdMapper[$sourceAddressId];
            $destAddress = $destinationAddresses[$destAddressId];

            //compare static attributes
            foreach ($staticAttributes as $attr) {
                if (isset($sourceAddressData[$attr]) and $sourceAddressData[$attr]['value'] != $destAddress['entity'][$attr]) {
                    $msg = 'Source address ID (' . $sourceAddressId . ') and destination address ID (' . $destAddressId . ') ' .
                        'attributes ' . $attr . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }

            //compare custom attributes
            foreach ($destAddress as $attr => $destAttrData) {
                if ($attr == 'entity') {
                    continue;
                }

                if (isset($sourceAddressData[$attr]) and $sourceAddressData[$attr]['value'] != $destAttrData['value']) {
                    $msg = 'Source address ID (' . $sourceAddressId . ') and destination address ID (' . $destAddressId . ') ' .
                        'attributes ' . $attr . " don't match";
                    $output->writeln('<error>' . $msg . '</error>');
                }
            }
        }
    }
}
