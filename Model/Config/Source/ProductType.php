<?php

/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * Used in creating options for Yes|No config value selection
 *
 */

namespace Shirtplatform\Core\Model\Config\Source;

class ProductType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    const COLLECTION_PRODUCT = 1;
    const BASE_PRODUCT = 2;
    const CUSTOMDESIGN_PRODUCT = 3;

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        $array = [];
        $options = $this->toOptionArray();
        foreach ($options as $option) {
            $array[$option['value']] = $option['label'];
        }
        return $array;
    }

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions() {
        return [
            ['value' => '', 'label' => ' '],
            ['value' => self::COLLECTION_PRODUCT, 'label' => __('Shirtplatform Collection Product')],
            ['value' => self::BASE_PRODUCT, 'label' => __('Shirtplatform Base Product')],
            ['value' => self::CUSTOMDESIGN_PRODUCT, 'label' => __('Shirtplatform Custom Design Product')]
        ];
    }

    /**
     * Retrieve flat column definition
     *
     * @codeCoverageIgnore
     * @return array
     */
    public function getFlatColumns() {
        $columns = [];
        $attributeCode = $this->getAttribute()->getAttributeCode();

        $columns[$attributeCode] = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'length' => NULL,
            'unsigned' => true,
            'nullable' => true,
            'default' => null,
            'extra' => null,
            'comment' => $attributeCode . ' column',
        ];


        return $columns;
    }    
}
