<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Yes|No config value selection
 *
 */
namespace Shirtplatform\Core\Model\Config\Source;

class Shops implements \Magento\Framework\Option\ArrayInterface
{
	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return [['value' => 1, 'label' => __('Shop 1')], ['value' => 0, 'label' => __('Shop 2')]];
	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return [0 => __('Shop 1'), 1 => __('Shop 2')];
	}
}
