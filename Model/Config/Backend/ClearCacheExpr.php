<?php

namespace Shirtplatform\Core\Model\Config\Backend;

use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;

class ClearCacheExpr extends \Magento\Framework\App\Config\Value
{
    const CLEAR_CACHE_TIME = 'clear_cache_time';

    const CLEAR_CACHE_CRON_EXPR = 'shirtplatform/cron/clear_cache_cron_expr';

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $_configWriter;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,        
        array $data = []
    ) {        
        $this->_configWriter = $configWriter;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Convert the saved configuration values into a valid cron schedule.
     *
     * @access public
     * @return \Magento\Framework\App\Config\Value
     * @throws CouldNotSaveException
     */
    public function afterSave()
    {
        $timeComponents = explode(',', $this->getValue());
        $cleanTime = $this->getFieldsetDataValue(self::CLEAR_CACHE_TIME);
        $cronExprString = $this->_createCronExpression($timeComponents, $cleanTime);

        try {
            $this->_configWriter->save(self::CLEAR_CACHE_CRON_EXPR, $cronExprString);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Failed to write crontab expression.'), $e);
        }

        return parent::afterSave();
    }

    /**
     * Generate a crontab expression from the given data.
     *
     * @access private
     * @param array $timeComponents     
     * @return string
     */
    private function _createCronExpression(array $timeComponents = [])
    {
        $timeComponents = array_pad($timeComponents, 3, 0);

        $expression = [
            (int)$timeComponents[1],
            (int)$timeComponents[0],
            '*',
            '*',
            '*',
        ];

        return \implode(' ', $expression);
    }
}
