<?php

namespace Shirtplatform\Core\Model\Config\Backend;

class PdfFile extends \Magento\Config\Model\Config\Backend\File {

    /**
     * Upload max file size in kilobytes
     *
     * @var int
     */
    protected $_maxFileSize = 5000;

    /**
     * Getter for allowed extensions of uploaded files.
     *
     * @return string[]
     */
    protected function _getAllowedExtensions() {
        return ['pdf'];
    }

}
