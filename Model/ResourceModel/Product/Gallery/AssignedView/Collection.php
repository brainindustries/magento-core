<?php

namespace Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct() {
        $this->_init('Shirtplatform\Core\Model\Product\Gallery\AssignedView', 'Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView');
    }

}
