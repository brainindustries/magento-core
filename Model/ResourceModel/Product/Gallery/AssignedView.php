<?php

namespace Shirtplatform\Core\Model\ResourceModel\Product\Gallery;

class AssignedView extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('shirtplatform_media_gallery_assigned_view', 'id');
    }

}
