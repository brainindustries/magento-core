<?php

namespace Shirtplatform\Core\Model\ImageSetup;

/**
 * Class ImageSetupResult
 * @package Shirtplatform\Core\Model\ImageSetup
 */
class ImageSetupResult
{

    /** @var int */
    private $height;

    /** @var int */
    private $width;

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return void
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }



}
