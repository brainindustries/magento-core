<?php

namespace Shirtplatform\Core\Model\ImageSetup;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Shirtplatform\Core\Api\ImageSetupRepositoryInterface;

/**
 * Class ImageSetupManagement
 * @package Shirtplatform\Core\Model\ImageSetup
 */
class ImageSetupManagement implements ImageSetupRepositoryInterface
{

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * ImageSetupManagement constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param int $storeViewId
     * @return \Shirtplatform\Core\Model\ImageSetup\ImageSetupResult
     */
    public function getSetup($storeViewId)
    {
        $width = trim($this->scopeConfig->getValue(
            'shirtplatform/images/width',
            ScopeInterface::SCOPE_STORE,
            $storeViewId
        ) ?? '');
        $width = ($width == null) ? null : (int) $width;
        $height = trim($this->scopeConfig->getValue(
            'shirtplatform/images/height',
            ScopeInterface::SCOPE_STORE,
            $storeViewId
        ) ?? '');
        $height = ($height == null) ? null : (int) $height;

        $result = new ImageSetupResult();
        $result->setWidth($width);
        $result->setHeight($height);
        return $result;
    }

}
