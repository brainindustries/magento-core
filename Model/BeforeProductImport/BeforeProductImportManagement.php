<?php

namespace Shirtplatform\Core\Model\BeforeProductImport;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NotFoundException;
use Shirtplatform\Core\Api\BeforeProductImportInterface;

/**
 * Class BeforeProductImportManagement
 * @package Shirtplatform\Core\Model\BeforeProductImport
 */
class BeforeProductImportManagement implements BeforeProductImportInterface {    

    /** @var ResourceConnection */
    private $resourceConnection;

    /**
     * BeforeProductImportManagement constructor.     
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection) {        
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param string $sku
     * @throws \Zend_Db_Statement_Exception
     */
    public function beforeImport($sku) {
        return;
        // $connection = $this->resourceConnection->getConnection();
        // $configurableProductId = $connection->query('SELECT entity_id FROM catalog_product_entity WHERE sku = "' . $sku . '"')->fetchColumn();
        // if ($configurableProductId == null) {
        //     return;
        // }

        // $productIds = [];
        // $productIds[] = $configurableProductId;
        // foreach ($connection->query('SELECT product_id FROM catalog_product_super_link WHERE parent_id = ' . $configurableProductId)->fetchAll() as $link) {
        //     $productIds[] = $link['product_id'];
        // }

        // if (empty($productIds)) {
        //     return;
        // }        

        // $connection->delete('catalog_product_entity_media_gallery_value_to_entity', ['entity_id IN (?)' => $productIds]);
    }

}
