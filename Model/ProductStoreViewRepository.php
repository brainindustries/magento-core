<?php

namespace Shirtplatform\Core\Model;

class ProductStoreViewRepository implements \Shirtplatform\Core\Api\ProductStoreViewRepositoryInterface
{
    const ALLOWED_ATTRIBUTES_TO_UPDATE = ['name', 'price', 'description', 'short_description'];    

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Action
     */
    private $_productAction;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;    
    
    /**     
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;
    
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Action $productAction
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @return void
     */
    public function __construct(        
        \Magento\Catalog\Model\ResourceModel\Product\Action $productAction,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,        
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {        
        $this->_productAction = $productAction;
        $this->_productRepository = $productRepository;        
        $this->_storeManager = $storeManager;
    }
    
    /**
     * Save product attributes in store view
     *
     * @inheritdoc
     */
    public function saveInStoreView($product)
    {        
        if (empty($product->getSku())) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Missing SKU for products in %1', __CLASS__));
        }

        $attrData = [];
        $storeId = $this->_storeManager->getStore()->getId();
        $existingProduct = $this->_productRepository->get($product->getSku(), true, $storeId);    

        foreach ($product->getData() as $attrCode => $value) {
            if (in_array($attrCode, self::ALLOWED_ATTRIBUTES_TO_UPDATE)) {
                $attrData[$attrCode] = $value;
            }
        }

        if (!empty($attrData)) {
            $this->_productAction->updateAttributes([$existingProduct->getId()], $attrData, $storeId);
        }        
        
        return $existingProduct;        
    }
}
