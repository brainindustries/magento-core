<?php

namespace Shirtplatform\Core\Model;

use Shirtplatform\Core\Api\ConnectorInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use shirtplatform\resource\AuthResource;
use shirtplatform\utils\user\User;
use Shirtplatform\Core\Model\User\CookieUserFactory;

class Connector implements ConnectorInterface {

    /**
     * @var WriterInterface
     */
    protected $_configWriter;

    /**
     * @var CookieUserFactory
     */
    protected $_cookieUserFactory;

    /**
     * @var EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var CoreHelper
     */
    protected $_coreHelper;

    /**
     * @param WriterInterface $configWriter
     * @param CookieUserFactory $cookieUserFactory
     * @param EncryptorInterface $encryptor
     * @param StoreManagerInterface $storeManager
     * @param RequestInterface $request
     * @param TypeListInterface $cacheTypeList
     * @param CoreHelper $coreHelper
     */
    public function __construct(
        WriterInterface $configWriter,
        CookieUserFactory $cookieUserFactory,
        EncryptorInterface $encryptor,
        StoreManagerInterface $storeManager,
        RequestInterface $request,
        TypeListInterface $cacheTypeList,
        CoreHelper $coreHelper
    ){
        $this->_configWriter = $configWriter;
        $this->_cookieUserFactory = $cookieUserFactory;
        $this->_encryptor = $encryptor;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_coreHelper = $coreHelper;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function connect($data)
    {
        // check if we received the right data
        if (!$data['stores'] || !is_array($data['stores']) || !isset($data['shop-url'])){
            return false;
        }

        // check if shop url matches
        try {
            $storeToCheck = $this->getStoreFromName($data['shop-url']);
            $uniqUrl = 'https://' . $this->_coreHelper->getConnectionUniqUrl($storeToCheck);
            if ($uniqUrl !== $data['shop-url']) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

        // authenticate on shirtplatform
        $userClass = $this->_cookieUserFactory->create(['storeId' => $storeToCheck->getId()]);
        User::setUserClass($userClass);        
        $this->_coreHelper->setWebservicesUrl(true);

        if (AuthResource::wsLogin($data['login'], $data['password']) === false) {
            return false;
        }
        AuthResource::logout();
        
        // save data for stores (and get website id)
        $websiteId = null;
        foreach ($data['stores'] as $_store){
            $store = $this->getStoreFromName($_store['name']);

            if (!$websiteId){
                $websiteId = $store->getWebsiteId();
            }

            $this->_configWriter->save('shirtplatform/general/country', $_store['country'], ScopeInterface::SCOPE_STORES, $store->getId());
            $this->_configWriter->save('shirtplatform/general/language', $_store['language'], ScopeInterface::SCOPE_STORES, $store->getId());
        }
        
        // encrypt password
        $data['password'] = $this->_encryptor->encrypt($data['password']);

        // save data for website
        $this->_configWriter->save('shirtplatform/general/login', $data['login'], ScopeInterface::SCOPE_WEBSITES, $websiteId);
        $this->_configWriter->save('shirtplatform/general/password', $data['password'], ScopeInterface::SCOPE_WEBSITES, $websiteId);
        $this->_configWriter->save('shirtplatform/general/store', $data['store'], ScopeInterface::SCOPE_WEBSITES, $websiteId);

        // clean cache
        $this->_cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);

        return true;
    }

    /**
     * @param int $websiteId
     */
    public function disconnect($websiteId)
    {
        $website = $this->_storeManager->getWebsite($websiteId);
        foreach ($website->getStores() as $_store){
            $this->_configWriter->delete('shirtplatform/general/country', ScopeInterface::SCOPE_STORES, $_store->getId());
            $this->_configWriter->delete('shirtplatform/general/language', ScopeInterface::SCOPE_STORES, $_store->getId());
        }
        
        $this->_configWriter->delete('shirtplatform/general/login', ScopeInterface::SCOPE_WEBSITES, $websiteId);
        $this->_configWriter->delete('shirtplatform/general/password', ScopeInterface::SCOPE_WEBSITES, $websiteId);
        $this->_configWriter->delete('shirtplatform/general/store', ScopeInterface::SCOPE_WEBSITES, $websiteId);

        $this->_cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
    }

    /**
     * @param string $name
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    private function getStoreFromName($name)
    {
        $segments = explode('_', $name);
        $storeId = array_pop($segments);
        return $this->_storeManager->getStore($storeId);
    }
}