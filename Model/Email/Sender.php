<?php

namespace Shirtplatform\Core\Model\Email;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\Order\Email\SenderBuilderFactory;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use Psr\Log\LoggerInterface;
use Shirtplatform\Core\Helper\Data;

/**
 * CUSTOM SHIRTPLATFORM E-MAIL SENDER (with attachments)
 * 
 * Follow steps below to use it:
 * 
 * 1. Create a custom HTML e-mail template in view/frontend/email directory
 * 
 * 2. Register the new template in the 'email_templates.xml' file
 *      <template id="{custom_template_id}" label="{custom_template_label}" file="{custom_template_file_name}" type="html" module="{custom_module_name}" area="frontend"/>
 * 
 * 3. Create a new virtual type for AddAttachmentsToEmail observer:
 *      <virtualType name="{custom_observer_class}" type="Shirtplatform\Core\Observer\AddAttachmentsToEmail">
 *		    <arguments>
 *			    <argument name="varCode" xsi:type="string">{custom_var_code}</argument>
 *		    </arguments>
 *	    </virtualType>
 *
 * 4. Add the new observer to 'events.xml' file:
 *      <event name="fooman_emailattachments_before_send_{custom_event_type}">
 *		    <observer name="{custom_observer_name}" instance="{custom_observer_class}"/>
 *	    </event>
 * 
 * 5. Call 'send' method from this class:
 *      $sender->send([
 *          'event_type'    => string {custom_event_type},
 *          'template_id'   => string {custom_template_id},
 *          'recipient'     => string|array,
 *          'var_code'      => string {custom_var_code},
 *          'template_vars' => array
 *      ]);
 * 
 *      template_id (optional)  - name of the e-mail template that will be used
 *      event_type (optional)   - suffix of event name that will be dispatched from fooman extension, template_id will be used if this is not set
 *      recipient (optional)	- contains an e-mail address or multiple e-mail addresses where e-mail will be sent, support email address from config will be used if not filled
 *      var_code (optional)	    - array key where template data will be stored in both html template and add attachments observer, event_type value will be used if this is not set
 *      template_vars		    - actual e-mail template data, include 'attachments' array containing file paths that will be to attached to the e-mail
 *                                you can include 'message' if you use default template_id
 */
class Sender extends \Magento\Sales\Model\Order\Email\Sender
{
    /**
     * @var Data
     */
    private $_helper;

    /**
     * @var OrderFactory
     */
    private $_orderFactory;

    /**
     * @param Data $helper
     * @param IdentityInterface $identityContainer
     * @param LoggerInterface $logger
     * @param OrderFactory $orderFactory
     * @param Renderer $addressRenderer
     * @param SenderBuilderFactory $senderBuilderFactory
     * @param Template $templateContainer
     */
    public function __construct(
        Data $helper,
        IdentityInterface $identityContainer,
        LoggerInterface $logger,
        OrderFactory $orderFactory,
        Renderer $addressRenderer,
        SenderBuilderFactory $senderBuilderFactory,
        Template $templateContainer
    ) {
        parent::__construct($templateContainer, $identityContainer, $senderBuilderFactory, $logger, $addressRenderer);
        $this->_helper = $helper;
        $this->_orderFactory = $orderFactory;
    }

    /**
     * @access public
     * @param array $data
     * @return void
     */
    public function send($data = []) : void
    {
        $order = $this->_orderFactory->create();

        if (!empty($data)) {
            $order->setData($data);
        }
        
        foreach ($this->getRecipients($data) as $_recipient) {
            $order->setData('recipient', $_recipient);
            $this->checkAndSend($order);
        }
    }

    /**
     * @access protected
     * @param Order $order
     * @return void
     */
    protected function prepareTemplate(Order $order) : void
    {
        $this->templateContainer->setTemplateOptions($this->getTemplateOptions());
        $this->identityContainer->setCustomerEmail($order->getRecipient());
 
        $templateId = $order->hasTemplateId() ? $order->getTemplateId() : $this->identityContainer->getTemplateId();
        $eventType = $order->hasEventType() ? $order->getEventType() : $templateId;
        $varCode = $order->hasVarCode() ? $order->getVarCode() : $eventType;

        $this->templateContainer->setTemplateId($templateId);
        $this->templateContainer->setTemplateVars([
            $varCode => $order->getTemplateVars(),
            'event_type' => $eventType
        ]);
    }

    /**
     * @param array $data
     * @return array
     */
    private function getRecipients(array $data) : array
    {
        $storeId = $this->identityContainer->getStore()->getStoreId();
        $recipients = isset($data['recipient']) 
            ? $data['recipient'] 
            : $this->_helper->getConfigValue('trans_email/ident_support/email', $storeId);

        return is_array($recipients) ? $recipients : array_map('trim', explode(',', $recipients));            
    }
}