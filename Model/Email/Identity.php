<?php

namespace Shirtplatform\Core\Model\Email;

use Magento\Sales\Model\Order\Email\Container\Container;

class Identity extends Container
{
    /**
     * @inheritdoc
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getEmailCopyTo()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getCopyMethod()
    {
        return 'bcc';
    }

    /**
     * @inheritdoc
     */
    public function getGuestTemplateId()
    {
        return 'shirtplatform_default_email';
    }

    /**
     * @inheritdoc
     */
    public function getTemplateId()
    {
        return 'shirtplatform_default_email';
    }

    /**
     * @inheritdoc
     */
    public function getEmailIdentity()
    {
        return 'general';
    }
}