<?php

namespace Shirtplatform\Core\Model\ImageLinksFix;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NotFoundException;
use Magento\Store\Model\ScopeInterface;
use Shirtplatform\Core\Api\ImageLinksFixInterface;

/**
 * Class ImageLinksFixManagement
 * @package Shirtplatform\Core\Model\ImageLinksFix
 */
class ImageLinksFixManagement implements ImageLinksFixInterface {

    /** @var string */
    const USE_FLAT_CATALOG_PRODUCT_PATH = 'catalog/frontend/flat_catalog_product';

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $_directoryList;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $_ioFile;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    private $_mediaConfig;

    /** @var ResourceConnection */
    private $resourceConnection;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**     
     * ImageLinksFixManagement constructor.
     * 
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param \Magento\Catalog\Model\Product\Media\Config $mediaConfig
     * @param ResourceConnection $resourceConnection
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
                                \Magento\Framework\Filesystem\Io\File $ioFile,
                                \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
                                ResourceConnection $resourceConnection,
                                ScopeConfigInterface $scopeConfig
    ) {
        $this->_directoryList = $directoryList;
        $this->_ioFile = $ioFile;
        $this->_mediaConfig = $mediaConfig;
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param string $sku
     * @throws NotFoundException
     * @throws \Zend_Db_Statement_Exception
     */
    public function processFix($sku) {        
        $connection = $this->resourceConnection->getConnection();
        $configurableProductId = $connection->query('SELECT entity_id FROM catalog_product_entity WHERE sku = "' . $sku . '"')->fetchColumn();
        if ($configurableProductId == null) {
            throw new NotFoundException('Product with SKU "' . $sku . '" not found!');
        }

        $productIds = [];
        $productIds[] = $configurableProductId;
        foreach ($connection->query('SELECT product_id FROM catalog_product_super_link WHERE parent_id = ' . $configurableProductId)->fetchAll() as $link) {
            $productIds[] = $link['product_id'];
        }

        if (empty($productIds)) {
            return;
        }
        
        // $connection->query('DELETE FROM catalog_product_entity_varchar 
        //       WHERE attribute_id IN (SELECT attribute_id FROM eav_attribute WHERE attribute_code IN("thumbnail", "small_image", "image")) 
        //       AND entity_id IN(' . implode(',', $productIds) . ')');                      

        $select = $connection->select()
            ->from(['main_table' => $connection->getTableName('catalog_product_entity_media_gallery')])
            ->join(['etv_table' => $connection->getTableName('catalog_product_entity_media_gallery_value_to_entity')], 'main_table.value_id = etv_table.value_id', ['entity_id'])
            ->where('etv_table.entity_id IN (?)', $productIds);

            echo (string)$select . '<br><br>';

        //image entries of the main product and its child products
        $imageGallery = $connection->fetchAll($select);
                
        if (!empty($imageGallery)) {
            //image IDs of the the main product and its child product
            $allImageIds = array_column($imageGallery, 'value_id');

            //divide images per product
            $imagesPerProduct = [];
            foreach ($imageGallery as $image) {
                if (!isset($imagesPerProduct[$image['entity_id']])) {
                    $imagesPerProduct[$image['entity_id']] = [];
                }

                $imagesPerProduct[$image['entity_id']][] = $image;
            }
            
            //get platform images
            $select = $connection->select()
                ->from($connection->getTableName('shirtplatform_media_gallery_assigned_view'))
                ->where('image_id IN (?)', $allImageIds);
            $platformImages = $connection->fetchAll($select);                                    
        }                

        $attributeIds = $connection->query('SELECT attribute_id FROM eav_attribute WHERE attribute_code IN("thumbnail", "small_image", "image") AND backend_model IS NULL')->fetchAll();
        foreach ($productIds as $productId) {
            //determine the main image
            $mainImage = $this->_determineMainImage($productId, $imagesPerProduct, $platformImages);            

            foreach ($attributeIds as $attribute) {
                $varcharItems = $connection->query('SELECT value_id FROM catalog_product_entity_varchar WHERE store_id = 0 AND attribute_id = ' . $attribute['attribute_id'] . ' AND entity_id = ' . $productId)->fetchAll();
                
                if (empty($varcharItems) && !empty($mainImage)) {
                    $bind = [
                        'attribute_id' => $attribute['attribute_id'],
                        'store_id' => 0,
                        'entity_id' => $productId,
                        'value' => $mainImage['value']
                    ];
                    $connection->insert($connection->getTableName('catalog_product_entity_varchar'), $bind);                                        
                }
            }
        }        

        $connection->query('DELETE FROM shirtplatform_media_gallery_cache WHERE product_id IN(' . implode(',', $productIds) . ')');
        $connection->query('INSERT INTO shirtplatform_media_gallery_cache (image_id, product_id, assigned_view_id)
          SELECT 
          mgvte.value_id, 
          mgvte.entity_id, 
          shav.assigned_view_id 
          FROM catalog_product_entity_media_gallery_value_to_entity mgvte 
          LEFT JOIN shirtplatform_media_gallery_assigned_view shav ON shav.image_id = mgvte.value_id 
          WHERE mgvte.entity_id IS NOT NULL 
          AND shav.assigned_view_id IS NOT NULL 
          AND mgvte.entity_id IN(' . implode(',', $productIds) . ');
        ');

        //Reindex
        if ($this->scopeConfig->getValue(self::USE_FLAT_CATALOG_PRODUCT_PATH, ScopeInterface::SCOPE_STORE)) {
            $indexerFactory = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Indexer\Model\IndexerFactory');
            $indexerIds = [
                'catalog_product_flat',
                'catalogrule_rule'
            ];
            foreach ($indexerIds as $indexerId) {
                $indexer = $indexerFactory->create();
                $indexer->load($indexerId);
                $indexer->reindexAll();
            }
        }

        //delete all images that don't have connection to any product
        $oldMedia = $connection->query('SELECT * FROM catalog_product_entity_media_gallery WHERE value_id NOT IN (SELECT value_id FROM catalog_product_entity_media_gallery_value_to_entity)')->fetchAll();
        if (!empty($oldMedia)) {           
            $imagePath = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            
            foreach ($oldMedia as $entry) {
                $imageFile = $imagePath . '/' . $this->_mediaConfig->getMediaPath($entry['value']);
                if ($this->_ioFile->fileExists($imageFile) and $this->_ioFile->isWriteable($imageFile)) {
                    $this->_ioFile->rm($imageFile);
                }
            }           
        }
        $connection->query('DELETE FROM catalog_product_entity_media_gallery WHERE value_id NOT IN (SELECT value_id FROM catalog_product_entity_media_gallery_value_to_entity)');                
    }
    
    /**
     * Determine main image for product
     *
     * @access private
     * @param int $productId entity_id of the product
     * @param mixed $allImages all images per productId
     * @param mixed $platformImages all platform images for the main and child products
     * @return array
     */
    private function _determineMainImage($productId, $allImages, $platformImages)
    {
        //if the product doesn't have any image
        if (!isset($allImages[$productId])) {
            return [];
        }

        //if no platform images are present -> choose the first image
        if (empty($platformImages)) {
            return reset($allImages[$productId]);
        }

        //try to find HUMAN type image
        foreach ($allImages[$productId] as $image) {
            $imageId = $image['value_id'];
            
            foreach ($platformImages as $pImage) {
                if ($pImage['image_id'] == $imageId) {
                    if ($pImage['view_type'] == 'HUMAN') {
                        return $image;
                    }
                    break;
                }
            }
        }

        //if no HUMAN image exists, choose the first platform image
        foreach ($allImages[$productId] as $image) {
            foreach ($platformImages as $pImage) {
                if ($pImage['image_id'] == $image['value_id']) {
                    return $image;
                }
            }
        }
    }
}
