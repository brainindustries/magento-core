<?php

namespace Shirtplatform\Core\Model\Product\Attribute;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Exception\StateException;
use Shirtplatform\Core\Api\ProductAttributeOptionManagementInterface;
use Magento\Framework\Exception\InputException;

class OptionManagement implements ProductAttributeOptionManagementInterface
{
    /**
     * @var \Magento\Eav\Model\AttributeRepository
     */
    private $_attributeRepository;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $_eavConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resourceConnection;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    private $_resourceModel;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var SerializerInterface
     */
    private $_serializer;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;    

    /**
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $resourceModel
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SerializerInterface $serializer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        \Magento\Eav\Model\Config $eavConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $resourceModel,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        SerializerInterface $serializer,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_attributeRepository = $attributeRepository;
        $this->_eavConfig = $eavConfig;
        $this->_logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_resourceConnection = $resourceConnection;
        $this->_resourceModel = $resourceModel;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_serializer = $serializer;
        $this->_storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function fixColors($sku)
    {
        $colorAttr = $this->_eavConfig->getAttribute(ProductAttributeInterface::ENTITY_TYPE_CODE, 'color');
        if (!$colorAttr->getId()) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Color attribute doesn't exist"));
        }

        $searchCriteria = $this->_searchCriteriaBuilder->addFilter('sku', $sku . '%', 'like')->create();
        $productList = $this->_productRepository->getList($searchCriteria)->getItems();

        if (empty($productList)) {
            return true;
        }

        $entityIds = [];
        foreach ($productList as $product) {
            $entityIds[] = $product->getId();
        }

        $totalUpdatedItemsCount = 0;
        $connection = $this->_resourceConnection->getConnection();
        $orderItemTable = $connection->getTableName('sales_order_item');
        $assignedProductColors = $this->_getAssignedProductColors($colorAttr, $entityIds);
        $startTime = microtime(true);
        
        foreach ($assignedProductColors as $productId => $optionInfo) {
            $singleProductItemsCount = 0;

            //get simple product items
            $sql = 'SELECT item_id, order_id, parent_item_id, product_id, product_type, product_options, sku FROM ' . $orderItemTable .
                ' WHERE ' . $connection->quoteInto('product_id = ? ', $productId) . ' AND product_type = "simple" ' .
                ' AND ' . $connection->quoteInto('product_options LIKE ?', '%"' . $colorAttr->getId() . '":"%') .
                ' AND ' . $connection->quoteInto('product_options NOT LIKE ?', '%"' . $colorAttr->getId() . '":"' . $optionInfo['option_value'] . '"%');
            $wrongItems = $connection->fetchAll($sql);

            if (count($wrongItems) == 0) {
                continue;
            }

            $parentItemIds = $skuMapper = [];
            $totalUpdatedItemsCount += count($wrongItems);
            $singleProductItemsCount += count($wrongItems);

            foreach ($wrongItems as $_wrongItem) {
                $parentItemIds[] = $_wrongItem['parent_item_id'];
                $skuMapper[$_wrongItem['parent_item_id']] = $_wrongItem['sku'];
            }

            //get configurable items
            $sql = 'SELECT item_id, order_id, product_id, product_type, product_options FROM ' . $orderItemTable .
                ' WHERE ' . $connection->quoteInto('item_id IN (?)', $parentItemIds) .
                ' AND ' . $connection->quoteInto('product_options LIKE ?', '%"' . $colorAttr->getId() . '":"%') .
                ' AND ' . $connection->quoteInto('product_options NOT LIKE ?', '%"' . $colorAttr->getId() . '":"' . $optionInfo['option_value'] . '"%');
            $wrongParents = $connection->fetchAll($sql);
            $totalUpdatedItemsCount += count($wrongParents);
            $singleProductItemsCount += count($wrongParents);

            //process simple items
            foreach ($wrongItems as $_wrongItem) {
                $oldProductOptions = $this->_serializer->unserialize($_wrongItem['product_options'], true);
                $oldProductOptions['info_buyRequest']['super_attribute'][$colorAttr->getId()] = $optionInfo['option_value'];
                $newProductOptions = $this->_serializer->serialize($oldProductOptions);

                $sql = 'UPDATE ' . $orderItemTable . ' SET ' . $connection->quoteInto('product_options = ? ', $newProductOptions) .
                    ' WHERE ' . $connection->quoteInto('item_id = ?', $_wrongItem['item_id']);
                $connection->query($sql);
            }

            //process configurable items
            foreach ($wrongParents as $_wrongParent) {
                $oldProductOptions = $this->_serializer->unserialize($_wrongParent['product_options'], true);
                $oldProductOptions['info_buyRequest']['super_attribute'][$colorAttr->getId()] = $optionInfo['option_value'];

                foreach ($oldProductOptions['attributes_info'] as &$attributesInfo) {
                    if ($attributesInfo['option_id'] == $colorAttr->getId()) {
                        $attributesInfo['option_value'] = $optionInfo['option_value'];
                    }
                }

                $oldProductOptions['simple_sku'] = $skuMapper[$_wrongParent['item_id']];
                $newProductOptions = $this->_serializer->serialize($oldProductOptions);
                
                $sql = 'UPDATE ' . $orderItemTable . ' SET ' . $connection->quoteInto('product_options = ? ', $newProductOptions) .
                    ' WHERE ' . $connection->quoteInto('item_id = ?', $_wrongParent['item_id']);
                $connection->query($sql);                
            }

            $msg = 'Product ID: ' . $productId . ', color: ' . $optionInfo['option_label'] . ', number of updated order items: ' . $singleProductItemsCount;
            $this->_logger->info($msg);
        }
        
        $endTime = microtime(true);        
        $msg = 'ProductColorFix API: Total number of updated order items for all products: ' . $totalUpdatedItemsCount;
        $this->_logger->info($msg);
        $msg = 'Script execution time ' . ($endTime - $startTime) . ' s';
        $this->_logger->info($msg);
        return true;
    }

    /**
     * Compose color mapper mapping value => label
     * 
     * @access private
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $colorAttr    
     * @return array
     */
    private function _getColorMapper($colorAttr)
    {
        $colorMapper = [];
        $allColorOptions = $colorAttr->getSource()->getAllOptions(false, true);

        foreach ($allColorOptions as $option) {
            $colorMapper[$option['value']] = $option['label'];
        }

        return $colorMapper;
    }

    /**
     * Get assigned product colors in the form of
     * entity_id => ['option_value' => int, 'option_label' => string]
     * 
     * @access private
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $colorAttr
     * @param int[] $entityIds product IDs     
     * @return array
     */
    private function _getAssignedProductColors($colorAttr, $entityIds)
    {
        $assignedProductColors = [];
        $colorMapper = $this->_getColorMapper($colorAttr);
        $connection = $this->_resourceConnection->getConnection();
        $optionTable = $connection->getTableName('catalog_product_entity_int');
        $sql = 'SELECT * FROM ' . $optionTable . ' WHERE '
            . $connection->quoteInto('attribute_id = ?', $colorAttr->getId()) . ' AND '
            . $connection->quoteInto('entity_id IN (?)', $entityIds);
        $options = $connection->fetchAll($sql);

        foreach ($options as $_option) {
            if (!isset($colorMapper[$_option['value']])) {
                continue;
            }

            $optionValue = $_option['value'];
            $optionLabel = $colorMapper[$optionValue];

            $assignedProductColors[$_option['entity_id']] = [
                'option_value' => $optionValue,
                'option_label' => $optionLabel
            ];
        }

        return $assignedProductColors;
    }

    /**
     * {@inheritdoc}
     */
    public function update($attributeCode, $optionId, $option)
    {
        if (empty($attributeCode)) {
            throw new InputException(__('Empty attribute code'));
        }

        $attribute = $this->_attributeRepository->get('catalog_product', $attributeCode);
        if (!$attribute->usesSource()) {
            throw new StateException(__('Attribute %1 doesn\'t work with options', $attributeCode));
        }

        $options = [];
        $storeViews = $this->_storeManager->getStores();

        $attribute->setStoreId(0); // admin store
        $options['value'][$optionId][0] = $attribute->getSource()->getOptionText($optionId);

        foreach ($storeViews as $storeView) {
            $storeViewId = $storeView->getId();
            $attribute->setStoreId($storeViewId);
            $storeLabel = $attribute->getSource()->getOptionText($optionId);
            if ($storeLabel !== $options['value'][$optionId][0]) {
                $options['value'][$optionId][$storeViewId] = $storeLabel;
            }
        }

//		file_put_contents('/var/www/magento2/file.log', print_r($options, true));
        $storeLabels = $option->getStoreLabels();
        if (count($storeLabels)) {
            foreach ($storeLabels as $storeLabel) {
                $storeId = $storeLabel['store_id'];
                $options['value'][$optionId][$storeId] = $storeLabel['label'];
            }
        }

        $attribute->setOption($options);
        try {
            $this->_resourceModel->save($attribute);
        } catch (\Exception $e) {
            throw new StateException(__('Cannot save attribute %1', $attributeCode));
        }

        return true;
    }
}
