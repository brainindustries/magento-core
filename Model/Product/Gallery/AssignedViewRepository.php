<?php

namespace Shirtplatform\Core\Model\Product\Gallery;

use Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView as ResourceAssignedView;
use Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterfaceFactory;
use Shirtplatform\Core\Api\AssignedViewRepositoryInterface;
use Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewSearchResultsInterfaceFactory;
use Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView\CollectionFactory as AssignedViewCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

class AssignedViewRepository implements AssignedViewRepositoryInterface {

    /**
     * @var AssignedViewInterfaceFactory
     */
    protected $_assignedViewFactory;

    /**
     * @var AssignedViewCollectionFactory
     */
    protected $_assignedViewCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $_collectionProcessor;

    /**
     * @var ResourceAssignedView
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var AssignedViewSearchResultsInterfaceFactory
     */
    protected $_searchResultsFactory;

    /**
     * 
     * @param AssignedViewInterfaceFactory $assignedViewFactory
     * @param AssignedViewCollectionFactory $assignedViewCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ResourceAssignedView $resource
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AssignedViewSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(AssignedViewInterfaceFactory $assignedViewFactory,
                                AssignedViewCollectionFactory $assignedViewCollectionFactory,
                                CollectionProcessorInterface $collectionProcessor,
                                ResourceAssignedView $resource,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                AssignedViewSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->_assignedViewFactory = $assignedViewFactory;
        $this->_assignedViewCollectionFactory = $assignedViewCollectionFactory;
        $this->_collectionProcessor = $collectionProcessor;
        $this->_resource = $resource;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView) {
        try {
            $this->_resource->save($assignedView);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                    'Could not save the assigned view: %1', $exception->getMessage()
            ));
        }
        return $assignedView;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($assignedViewId) {
        $assignedView = $this->_assignedViewFactory->create();
        $this->_resource->load($assignedView, $assignedViewId);
        if (!$assignedView->getId()) {
            throw new NoSuchEntityException(__('Assigned view with id "%1" does not exist.', $assignedViewId));
        }
        return $assignedView;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria) {
        /** @var Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView\Collection $collection */
        $collection = $this->_assignedViewCollectionFactory->create();
        $this->_collectionProcessor->process($criteria, $collection);

        /** @var Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewSearchResultsInterface $searchResults */
        $searchResults = $this->_searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface $assignedView) {
        try {
            $this->_resource->delete($assignedView);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                    'Could not delete the assigned view: %1', $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($assignedViewId) {
        return $this->delete($this->getById($assignedViewId));
    }

    /**
     * Retrieve assignedView.
     *
     * @access public
     * @param string $imageId
     * @return AssignedViewInterface[]
     */
    public function getByImageId($imageId) {
        $this->_searchCriteriaBuilder->addFilter('image_id', $imageId);
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        return $this->getList($searchCriteria)->getItems();
        /**
         * @var $assignedViewCollection \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
         */
//        $assignedViewCollection = $this->_assignedViewCollectionFactory->create();
//        return $assignedViewCollection->addFieldToFilter('image_id', $imageId)->getFirstItem();
    }

    /**
     * Get assigned views by image IDs
     * 
     * @access public
     * @param int[] $imageIds
     * @return AssignedViewInterface[]
     */
    public function getByGallery($imageIds) {
        $this->_searchCriteriaBuilder->addFilter('image_id', $imageIds, 'in');
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        return $this->getList($searchCriteria)->getItems();
    }

}
