<?php

namespace Shirtplatform\Core\Model\Product\Gallery;

class AssignedView extends \Magento\Framework\Model\AbstractModel implements \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'Shirtplatform_Core_assignedView';

    protected $_cacheTag = 'Shirtplatform_Core_assignedView';
    protected $_eventPrefix = 'Shirtplatform_Core_assignedView';

    protected function _construct() {        
        $this->_init(\Shirtplatform\Core\Model\ResourceModel\Product\Gallery\AssignedView::class);
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get id
     * @return int
     */
    public function getId() {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param int $id
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setId($id) {
        return $this->setData(self::ID, $id);
    }
    
    /**
     * Get id
     * @return int
     */
    public function getImageId() {
        return $this->getData(self::IMAGE_ID);
    }

    /**
     * Set id
     * @param int $imageId
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setImageId($imageId) {
        return $this->setData(self::IMAGE_ID, $imageId);
    }
    /**
     * Get id
     * @return int
     */
    public function getAssignedViewId() {
        return $this->getData(self::ASSIGNED_VIEW_ID);
    }

    /**
     * Set id
     * @param int $assignedViewId
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setAssignedViewId($assignedViewId) {
        return $this->setData(self::ASSIGNED_VIEW_ID, $assignedViewId);
    }

    /**
     * Get view type
     * @return string
     */
    public function getViewType()
    {
        return $this->getData(self::VIEW_TYPE);
    }

    /**
     * Set view type
     * @param string $viewType
     * @return \Shirtplatform\Core\Api\Data\Product\Gallery\AssignedViewInterface
     */
    public function setViewType($viewType)
    {
        return $this->setData(self::VIEW_TYPE, $viewType);
    }

}
