<?php

namespace Shirtplatform\Core\Model\Product;

use Magento\Store\Model\ScopeInterface;
use Shirtplatform\Core\Api\ProductAttributeManagementInterface;

class AttributeManagement implements ProductAttributeManagementInterface {

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $_configurableProduct;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    private $_indexerFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    private $_productResource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * Array mapping language code to storeId
     * 
     * @var aray
     */
    private $_storeLocaleMapper = [];

    /**
     * 
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct
     * @param \Magento\Indexer\Model\IndexerFactory $indexerFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct,
                                \Magento\Indexer\Model\IndexerFactory $indexerFactory,
                                \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
                                \Magento\Catalog\Model\ResourceModel\Product $productResource,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_configurableProduct = $configurableProduct;
        $this->_indexerFactory = $indexerFactory;
        $this->_productRepository = $productRepository;
        $this->_productResource = $productResource;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function updatePrice($sku,
//                                $price) {
//        $product = $this->_productRepository->get($sku);
//        $product->setPrice($price);
//        $childProducts = $this->_configurableProduct->getUsedProducts($product);
//
//        foreach ($childProducts as $child) {
//            $child->setPrice($price);
//            $this->_productResource->saveAttribute($child, 'price');
//        }
//    }

    /**
     * {@inheritdoc}
     */
    public function updatePrice($sku,
                                $price,
                                $prices = []) {
        $product = $this->_productRepository->get($sku);
        $childProducts = $this->_configurableProduct->getUsedProducts($product);

        foreach ($this->_storeManager->getStores() as $store) {
            $locale = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $localeParts = explode('_', $locale);
            $this->_storeLocaleMapper[$localeParts[1]] = $store->getId();
        }

        if (!empty($prices)) {
            foreach ($prices as $sku => $skuPrices) {
//                if ($product->getSku() == $sku) {                    
//                    foreach ($skuPrices as $lang => $storePrice) {
//                        if (isset($this->_storeLocaleMapper[$lang])) {
//                            $product->setStoreId($this->_storeLocaleMapper[$lang]);
//                            $product->setPrice($storePrice);
//                                                        
//                            $this->_productResource->saveAttribute($product, 'price');
//                        }
//                    }
//                }

                foreach ($childProducts as $child) {
                    if ($child->getSku() == $sku) {
                        foreach ($skuPrices as $lang => $storePrice) {
                            if (isset($this->_storeLocaleMapper[$lang])) {
                                $child->setStoreId($this->_storeLocaleMapper[$lang]);
                                $child->setPrice($storePrice);
                                $this->_productResource->saveAttribute($child, 'price');
                            }
                        }
                    }
                }
            }
        }
        else {
            foreach ($childProducts as $child) {
                $child->setPrice($price);
                $this->_productResource->saveAttribute($child, 'price');
            }
        }
        
        $indexer = $this->_indexerFactory->create();
        $indexer->load('catalog_product_price');
        $indexer->reindexAll();
    }

}
