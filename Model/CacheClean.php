<?php

namespace Shirtplatform\Core\Model;

class CacheClean implements \Shirtplatform\Core\Api\CacheCleanInterface
{    
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $_typeList;
    
    /**     
     *
     * @access public
     * @param \Magento\Framework\App\Cache\TypeListInterface $typeList
     * @return void
     */
    public function __construct(\Magento\Framework\App\Cache\TypeListInterface $typeList)
    {
        $this->_typeList = $typeList;
    }

    /**
     * @inheritdoc
     */
    public function fullPage()
    {
        $this->_typeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);        
    }
}