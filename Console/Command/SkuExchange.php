<?php

namespace Shirtplatform\Core\Console\Command;

use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use shirtplatform\entity\product\Product;
use shirtplatform\entity\collection\DesignedCollectionProduct;
use shirtplatform\filter\WsParameters;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SkuExchange extends Command {

    const INPUT_KEY_DRY_RUN = 'dry-run';
    const INPUT_KEY_REVERSE = 'reverse';

    /**
     * @var array
     * table name => sku column name
     */
    const TABLES = [
        'catalog_product_entity' => 'sku',
        'quote_item' => 'sku',
        'sales_creditmemo_item' => 'sku',
        'sales_invoice_item' => 'sku',
        'sales_order_item' => 'sku',
        'sales_shipment_item' => 'sku',
        'mst_rma_item' => 'product_sku',
    ];

    /**
     * @var AdapterInterface
     */
    private $_connection;

    /**
     * @var CoreHelper
     */
    private $_coreHelper;

    /**
     * @var int
     */
    private $_count = 0;

    /**
     * @var bool
     */
    private $_dryRun;

    /**
     * @var array
     */
    private $_notFoundSkus = [];

    /**
     * @var OutputInterface
     */
    private $_output;

    /**
     * @var ProductCollectionFactory
     */
    private $_productCollectionFactory;

    /**
     * @var bool
     */
    private $_reverse;

    /**
     * @var array
     */
    private $_skuMap = [];

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @param CoreHelper $coreHelper
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductResource $productResource
     * @param StoreManagerInterface $storeManager
     * @param string $name
     */
    public function __construct(
        CoreHelper $coreHelper,
        ProductCollectionFactory $productCollectionFactory,
        ProductResource $productResource,
        StoreManagerInterface $storeManager,
        $name = null
    ){
        $this->_coreHelper = $coreHelper;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_connection = $productResource->getConnection();
        $this->_storeManager = $storeManager;
        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure() {
        $this->setName('shirtplatform:sku:exchange');
        $this->setDescription('Exchange old product SKUs for new ones.');
        $this->setDefinition([
            new InputOption(self::INPUT_KEY_DRY_RUN, null, InputOption::VALUE_NONE, 'Dry run the script'),
            new InputOption(self::INPUT_KEY_REVERSE, null, InputOption::VALUE_NONE, 'Reverse script')
        ]);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->_output = $output;
            $this->_dryRun = $input->getOption(self::INPUT_KEY_DRY_RUN);
            $this->_reverse = $input->getOption(self::INPUT_KEY_REVERSE);

            $output->writeln('<info>SKU exchange script has started.</info>');

            $this->createSkuMap();
            if (!empty($this->_skuMap)){
                foreach (self::TABLES as $tableName => $skuColumn){
                    $this->updateTable($tableName, $skuColumn);
                }
            }

            $output->writeln('<info>SKU exchange script has finished.</info>');
            $output->writeln(sprintf('<info>%s</info> items %s changed.', $this->_count, $this->_dryRun ? 'will be' : 'have been'));

            if (count($this->_notFoundSkus)){
                $this->_coreHelper->logMessage('Shirtplatform stock ids of following products have not been found during SKU exchange script:');
                $this->_coreHelper->logMessage($this->_notFoundSkus);
                $output->writeln('Shirtplatform stock ids of <info>'. count($this->_notFoundSkus) .'</info> products have not been found. You can find their SKUs in shirtplatform_core log file.');
            }
            return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
        } catch (\Exception $ex) {
            $output->writeln('<error>' . $ex->getMessage() . '</error>');
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }
    }

    /**
     * @return void
     */
    private function createSkuMap(){
        $this->_output->writeln('Creating SKU map.');

        // get existing skus
        $existingSkus = $this->getExistingSkus();
        if (empty($existingSkus)){
            return;
        }
        $this->_output->writeln('Got all simple product SKUs');

        // get platform product ids
        $platformProductIds = $this->getPlatformProductIds($existingSkus);

        // shirtplatform auth
        $this->_coreHelper->shirtplatformAuth($this->_storeManager->getStore()->getId());

        // get base products of collection products
        $wsParamsIds = $platformProductIds['BP'];
        if (isset($platformProductIds['CP'])){
            $collectionProductsIdMap = $this->getCollectionProductsIdMap($platformProductIds['CP']);
            $wsParamsIds = array_unique(array_merge($wsParamsIds, array_values($collectionProductsIdMap)));
        }

        // get platform products
        $wsParams = new WsParameters();
        $wsParams->addExpressionContain('id', $wsParamsIds);
        $platformProducts = Product::findAll($wsParams);
        $this->_output->writeln('Got all platform products');

        // mapping
        foreach ($existingSkus as $sku){
            $skuParts = explode('-', $sku);
            $prefix = substr($skuParts[0], 0, 2);

            // get product id
            $productId = str_replace($prefix, '', $skuParts[0]);
            if ($prefix == 'CP'){
                if (!isset($collectionProductsIdMap[$productId])){
                    $this->_notFoundSkus[] = $sku;
                    continue;
                }
                $productId = $collectionProductsIdMap[$productId];
            }

            // product not found
            $found = false;
            if (!isset($platformProducts[$productId])){
                $this->_notFoundSkus[] = $sku;
                continue;
            }

            // check stock, get the new SKU and add it to map
            foreach ($platformProducts[$productId]->getSku() as $stock){
                if ($newSku = $this->checkStock($stock, $skuParts)){
                    if (!isset($this->_skuMap[$skuParts[0]])){
                        $this->_skuMap[$skuParts[0]] = [];
                    }    
                    $this->_skuMap[$skuParts[0]][$sku] = $newSku;
                    $found = true;
                    break;
                }
            }

            // product not found
            if (!$found){
                $this->_notFoundSkus[] = $sku;
            }
        }

        $this->_output->writeln('SKU map is complete.');
    }

    /**
     * @param string $tableName
     * @param string $skuColumn
     * @return void
     */
    private function updateTable($tableName, $skuColumn){
        $this->_output->writeln("Updating table <info>$tableName</info>:");
        foreach ($this->_skuMap as $_product => $_variants){
            try {
                if ($this->_dryRun){
                    $select = $this->_connection->select()
                        ->from($this->_connection->getTableName($tableName), ['count' => 'COUNT(*)'])
                        ->where("$skuColumn IN(?)", array_keys($_variants));

                    $this->_count += $this->_connection->fetchOne($select);
                } else {
                    $sql = "UPDATE `$tableName` SET `$skuColumn` = CASE `$skuColumn`";
                    foreach ($_variants as $_oldSku => $_newSku){
                        $sql .= " WHEN '$_oldSku' THEN '$_newSku'";
                    }
                    $sql .= " ELSE `$skuColumn` END WHERE `$skuColumn` IN('". implode("', '", array_keys($_variants)) ."')";

                    $result = $this->_connection->query($sql);
                    $this->_count += $result->rowCount();
                }

                $this->_output->writeln('Changed simple products SKUs for product: <info>'. $_product .'</info>.');
            } catch (\Exception $e){
                if ($e->getCode() == 1146){
                    $this->_output->writeln("<error>Table '$tableName' does not exist.</error>");
                    break;
                } else {
                    $this->_output->writeln('<error>'. $e->getMessage() .'</error>');
                    continue;
                }
            }
        }
    }

    /**
     * @param \shirtplatform\entity\product\ProductSku $stock
     * @param array $skuParts
     * @return string|bool
     */
    private function checkStock($stock, $skuParts){
        if (!$stock->stockId){
            return false;
        }

        $size = $stock->assignedSize->id;
        $color = $stock->assignedColor->id;

        if ($this->_reverse){
            if ($stock->stockId == $skuParts[1]){
                return implode('-', [$skuParts[0], $size, $color]);
            }
        } else {
            if ($size == $skuParts[1] && $color == $skuParts[2]){
                return $skuParts[0] .'-'. $stock->stockId;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function getExistingSkus(){
        $collection = $this->_productCollectionFactory->create()
            ->addFieldToFilter('type_id', 'simple')
            ->addFieldToFilter('sku', [
                ['like' => $this->_reverse ? 'BP%-%' : 'BP%-%-%'],
                ['like' => $this->_reverse ? 'CP%-%' : 'CP%-%-%']
            ]);

        if ($this->_reverse){
            $collection->addFieldToFilter('sku', ['nlike' => 'BP%-%-%']);
            $collection->addFieldToFilter('sku', ['nlike' => 'CP%-%-%']);
        }

        return array_column($collection->getData(), 'sku');
    }

    /**
     * @param array $skus
     * @return array
     */
    private function getPlatformProductIds($skus){
        $result = [];
        foreach ($skus as $_sku){
            $productSku = substr($_sku, 0, strpos($_sku, '-'));
            
            $prefix = substr($productSku, 0, 2);
            if (!isset($result[$prefix])){
                $result[$prefix] = [];
            }

            $productId = substr($productSku, 2);
            if (!in_array($productId, $result[$prefix])){
                $result[$prefix][] = $productId;
            }
        }
        return $result;
    }

    /**
     * @param array $ids
     * @return array
     */
    private function getCollectionProductsIdMap($ids){
        $result = [];

        $wsParams = new WsParameters();
        $wsParams->addExpressionContain('id', $ids);
        $collectionProducts = DesignedCollectionProduct::findAllByShop($wsParams);
        
        foreach ($collectionProducts as $id => $_collectionProduct){
            $result[$id] = $_collectionProduct->design->product->id;
        }

        return $result;
    }

}