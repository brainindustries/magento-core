<?php

namespace Shirtplatform\Core\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FillSalesGrids extends Command {

    const INPUT_GRID_TYPES = 'grid_types';
    const SQL_PAGE_SIZE = 1000;    
    
    /**     
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**     
     * @param \Magento\Framework\ObjectManagerInterface $objectManager     
     * @param string $name
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager,                                
                                $name = null) {
        parent::__construct($name);
        $this->_objectManager = $objectManager;        
    }

    /**
     * Get available grid types
     * 
     * @access private
     * @return array
     */
    private function _getAvailableGridTypes() {
        return ['order', 'invoice', 'creditmemo', 'shipment'];
    }

    /**
     * Insert rows to grid table
     * 
     * @access private
     * @param string $type (order, invoice, creditmemo, shipment)
     * @param OutputInterface $output
     */
    private function _insertRowsToGridTable($type, $output) {        
        switch ($type) {
            case 'order':                
                $gridModel = $this->_objectManager->get('\Magento\Sales\Model\ResourceModel\Order\Grid');
                $mainTable = $gridModel->getTable('sales_order');
                break;
            case 'invoice':                
                $gridModel = $this->_objectManager->get('\Magento\Sales\Model\ResourceModel\Order\Invoice\Grid');
                $mainTable = $gridModel->getTable('sales_invoice');
                break;
            case 'creditmemo':                
                $gridModel = $this->_objectManager->get('CreditmemoGridAggregator');
                $mainTable = $gridModel->getTable('sales_creditmemo');
                break;
            case 'shipment':                
                $gridModel = $this->_objectManager->get('ShipmentGridAggregator');
                $mainTable = $gridModel->getTable('sales_shipment');
                break;
        }
        
        $output->writeln('Start processing records from ' . $mainTable);        
        $connection = $gridModel->getConnection();
        $sql = 'SELECT COUNT(*) FROM ' . $mainTable;
        $rowCount = $connection->query($sql)->fetchColumn();
        $end = false;
        $offset = $processed = 0;
        
        while (!$end) {
            $sql = 'SELECT entity_id FROM ' . $mainTable . ' ORDER BY entity_id ASC LIMIT ' . self::SQL_PAGE_SIZE . ' OFFSET ' . $offset;            
            $results = $connection->query($sql)->fetchAll();
            
            foreach ($results as $row) {
                $gridModel->refresh($row['entity_id']);
                $processed++;
            }
            
            $offset += self::SQL_PAGE_SIZE;            
            if ($offset >= $rowCount) {
                $end = true;
            }
            
            $output->writeln('Records processed: ' . $processed);
        }       
        
        $output->writeln('Records from ' . $mainTable . ' processed');
    }

    /**
     * Configure command task
     * 
     * @access protected
     */
    protected function configure() {
        $this->setName('shirtplatform:sales_grids:fill');
        $this->setDescription('Refresh records in grid tables (order, invoice, creditmemo, shipment) and insert missing records from the main tables');
        $this->addArgument(
                self::INPUT_GRID_TYPES, InputArgument::IS_ARRAY, 'Space-separated list of grid types or omit to apply to all grid types.'
        );
        parent::configure();
    }

    /**
     * Execute command
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input,
                               OutputInterface $output) {
        $gridTypes = [];
        $availableGridTypes = $this->_getAvailableGridTypes();

        if ($input->getArgument(self::INPUT_GRID_TYPES)) {
            $gridTypes = $input->getArgument(self::INPUT_GRID_TYPES);
            $gridTypes = array_filter(array_map('trim', $gridTypes), 'strlen');

            foreach ($gridTypes as $key => $type) {
                if (!in_array($type, $availableGridTypes)) {
                    unset($gridTypes[$key]);
                }
            }
        }
        else {
            $gridTypes = $availableGridTypes;
        }

        foreach ($gridTypes as $type) {
            $this->_insertRowsToGridTable($type, $output);
        }
        
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

}
