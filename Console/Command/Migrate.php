<?php

namespace Shirtplatform\Core\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends Command {

    const INPUT_KEY_TEST_AFTER_MIGRATION = 'test-after-migration';

    /**
     * @var \Shirtplatform\Core\Model\Migration\Entity
     */
    private $_entity;

    public function __construct(\Shirtplatform\Core\Model\Migration\Entity $entity,
                                $name = null,
                                $description = null) {
        parent::__construct($name);
        $this->_entity = $entity;
        $this->setDescription($description);
    }

    /**
     * @inheritdoc
     */
    protected function configure() {
        $options = [
            new InputOption(
                    self::INPUT_KEY_TEST_AFTER_MIGRATION, null, InputOption::VALUE_NONE, 'Compares source and destination data after migration if they are equal'
            )
        ];
        $this->setDefinition($options);
        parent::configure();
    }

    /**
     * Execute command
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input,
                               OutputInterface $output) {
        try {
            $testData = $input->getOption(self::INPUT_KEY_TEST_AFTER_MIGRATION);            
            $this->_entity->migrate($output);
            
            if ($testData) {
                $this->_entity->testMigratedData($output);
            }
            
            $output->writeln('<info>Migration script has finished</info>');
            return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
        } catch (\Exception $ex) {
            $output->writeln('<error>' . $ex->getMessage() . '</error>');
            // we must have an exit code higher than zero to indicate something was wrong
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }
    }

}
